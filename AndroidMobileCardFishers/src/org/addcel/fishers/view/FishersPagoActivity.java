package org.addcel.fishers.view;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.addcel.fishers.to.CuentaTO;
import org.addcel.fishers.to.PagoResponseTO;
import org.addcel.fishers.ws.GetTokenWSClient;
import org.addcel.fishers.ws.PagoAmexWSClient;
import org.addcel.fishers.ws.PagoVisaWSClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.Validador;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.GetCardTypesWSClient;
import com.ironbit.mc.web.webservices.events.OnCardTypesResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseReceivedListener;
import com.ironbit.mc.widget.SpinnerValues;
import com.ironbit.mc.widget.SpinnerValuesListener;

public class FishersPagoActivity extends MenuActivity {
	
	TextView montoView;
	Bitmap img;
	ImageView marcaImageView;
	LongProcess tarjetasProcess, purchaseProcess;
	GetCardTypesWSClient tarjetasClient;
	GetTokenWSClient tokenClient;
	Spinner tarjetaSpinner, mesSpinner, anioSpinner;
	SpinnerValues tarjetaValues;
	private EditText nombreText, emailText, tarjetaText, cvvText, direccionText, cpText;
	LinearLayout amexLayout;
	
	CuentaTO cuenta;
	JSONObject json;
	
	protected final FormValidator validator = new FormValidator(this, true);

	private static final String TAG = "FishersPagoActivity";
	
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.FISHERS_PAGO;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_fishers_pago);
		
		tarjetasProcess = new LongProcess(FishersPagoActivity.this, true, 30, true, "get_cards", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != tarjetasClient) {
					tarjetasClient.cancel();
					tarjetasClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				tarjetasClient = new GetCardTypesWSClient(FishersPagoActivity.this);
				tarjetasClient.execute(new OnCardTypesResponseReceivedListener() {
					
					public void onCardTypesResponseReceived(
							ArrayList<BasicNameValuePair> cardTypes) {
						// TODO Auto-generated method stub
						tarjetaValues = new SpinnerValues(FishersPagoActivity.this, tarjetaSpinner, cardTypes);
						tarjetaValues.setSpinListener(new SpinnerValuesListener() {
							
							public void ItemSelected(String texto, String value) {
								// TODO Auto-generated method stub
								if ("3".equals(value)) {
									amexLayout.setVisibility(View.VISIBLE);
								} else {
									amexLayout.setVisibility(View.GONE);
								}
							}
						});
						
						tarjetasProcess.processFinished();
					}
				});
			}
		});
		
		tarjetasProcess.start();
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		initData();
		
		marcaImageView = (ImageView) findViewById(R.id.img_marca);
		montoView = (TextView) findViewById(R.id.text_monto);
		tarjetaSpinner = (Spinner) findViewById(R.id.spinner_tarjeta);
		mesSpinner = (Spinner) findViewById(R.id.cmbFechaVencimientoMes);
		anioSpinner = (Spinner) findViewById(R.id.cmbFechaVencimientoAnio);
		
		amexLayout = (LinearLayout) findViewById(R.id.layout_amex);
		
		
		loadData();
	}
	
	protected EditText getNombreText() {
		if (null == nombreText)
			nombreText = (EditText) findViewById(R.id.edit_nombre);
		
		return nombreText;
	}
	
	protected EditText getEmailText() {
		if (null == emailText)
			emailText = (EditText) findViewById(R.id.edit_email);
		
		return emailText;
	}
	
	protected EditText getTarjetaText() {
		if (null == tarjetaText)
			tarjetaText = (EditText) findViewById(R.id.edit_tarjeta);
		
		return tarjetaText;
	}
	
	protected EditText getCvvText() {
		if (null == cvvText)
			cvvText = (EditText) findViewById(R.id.edit_cvv);
		
		return cvvText;
	}
	
	protected EditText getDireccionText() {
		if (null == direccionText)
			direccionText = (EditText) findViewById(R.id.edit_direccion);
		
		return direccionText;
	}
	
	protected EditText getCpText() {
		if (null == cpText)
			cpText = (EditText) findViewById(R.id.edit_cp);
		
		return cpText;
	}
	
	@Override
	public void configValidations() {
		// TODO Auto-generated method stub
		validator.addItem(getNombreText(), "Nombre")
		.isRequired(true)
		.maxLength(50)
		.minLength(4);
		
		validator.addItem(getEmailText(), "Email")
		.isRequired(true)
		.isEmail(true)
		.maxLength(100)
		.minLength(16);
		
		validator.addItem(getTarjetaText(), "Tarjeta", new Validable() {
			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				if (tarjetaValues.getValorSeleccionado().equals("3")) {
					if (!Validador.esTarjetaDeCreditoAmericanExpress(getTarjetaText().getText().toString().trim())) {
						throw new ErrorSys("Tarjeta AMEX no v�lida");
					}
				} else if (tarjetaValues.getValorSeleccionado().equals("2")) {
					if (!Validador.esTarjetaDeCreditoMasterCard(getTarjetaText().getText().toString().trim())) {
						throw new ErrorSys("Tarjeta Mastercard no v�lida");
					}
					
				} else if (tarjetaValues.getValorSeleccionado().equals("1")) {
					if (!Validador.esTarjetaDeCreditoVisa(getTarjetaText().getText().toString().trim())) {
						throw new ErrorSys("Tarjeta Visa no v�lida");
					}
				}
			}
		}).canBeEmpty(false)
			.isRequired(true)
			.isNumeric(true);
		
		validator.addItem(getCvvText(), "Cvv2")
		.isRequired(true)
		.isNumeric(true)
		.maxLength(4)
		.minLength(3);
		
		validator.addItem(getDireccionText(), "Direcci�n", new Validable() {
			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				
				if (tarjetaValues.getValorSeleccionado().equals("3")) {
					
					String direccion = getDireccionText().getText().toString().trim();
					
					if (direccion.isEmpty())
						throw new ErrorSys("El campo Direcci�n no puede estar vac�o.");
				}
			}
		}).canBeEmpty(true);
		
		validator.addItem(getCpText(), "C�digo Postal", new Validable() {
			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				if (tarjetaValues.getValorSeleccionado().equals("3")) {
					
					String cp = getCpText().getText().toString().trim();
					
					if (cp.isEmpty())
						throw new ErrorSys("El campo C�digo Postal no puede estar vac�o.");
				}
			}
		}).canBeEmpty(true);
	}
	
	private void initData() {
		img = getIntent().getParcelableExtra("img");
		cuenta = getIntent().getParcelableExtra("cuentaTO");
		
		try {
			json = new JSONObject(cuenta.getJson());
			json.put("total_pago", cuenta.getTotalPago());
			json.put("propina", cuenta.getPropina());
		} catch (JSONException e) {
			Log.e(TAG, "", e);
		}
	}
	
	private void loadData() {
		marcaImageView.setImageBitmap(img);
		montoView.setText(Text.formatCurrency(cuenta.getTotalPago(), true));
		
		String[] aniosVencimiento = new String[13];
		try {
			Fecha fechaNow = new Fecha();
			int anioActual = fechaNow.getAnio();

			for (int a = anioActual, i = 0; a <= anioActual + 12; a++, i++) {
				aniosVencimiento[i] = a + "";
			}

			// metemos array al Spinner
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, aniosVencimiento);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			anioSpinner.setAdapter(adapter);
		} catch (Exception e) {
			Sys.log(e);
		}
	}
	
	private void run() {
		
		if (null == purchaseProcess) {
			
			purchaseProcess = new LongProcess(FishersPagoActivity.this, true, 90, true, "fishers_process", new LongProcessListener() {
				
				public void stopProcess() {
					// TODO Auto-generated method stub
					if (null != tokenClient) {
						tokenClient.cancel();
						tokenClient = null;
					}
				}
				
				public void doProcess() {
					// TODO Auto-generated method stub
					tokenClient = new GetTokenWSClient(FishersPagoActivity.this);
					tokenClient.executeWrapper(new OnResponseJSONReceivedListener() {
						
						public void onResponseStringReceived(String jsonresponse) {
							// TODO Auto-generated method stub
							purchaseProcess.processFinished();
						}
						
						public void onResponseJSONReceived(JSONObject jsonResponse) {
							// TODO Auto-generated method stub
							String token = jsonResponse.optString("token", "");
							
							if (!token.isEmpty()) {
								
								String cvv = getCvvText().getText().toString().trim();
								String email = getEmailText().getText().toString().trim();
								String nombre = getNombreText().getText().toString().trim();
								String tarjeta = getTarjetaText().getText().toString().trim();
								
								int mes = Fecha.mesStringToInt(mesSpinner.getSelectedItem().toString());
								int anio = Integer.parseInt(anioSpinner.getSelectedItem().toString());
								
								if (tarjetaValues.getValorSeleccionado().equals("3")) {
									
									String direccion = getDireccionText().getText().toString().trim();
									String cp = getCpText().getText().toString().trim();
									
									PagoAmexWSClient client = new PagoAmexWSClient(FishersPagoActivity.this);
									
									client.setCvv(cvv)
											.setEmail(email)
											.setNombre(nombre)
											.setTarjeta(tarjeta)
											.setToken(token)
											.setVigenciaMes(mes)
											.setVigenciaAnio(anio)
											.setDireccion(direccion)
											.setCP(cp)
											.setJSON(json);
									
									client.executePurchase(new OnResponseJSONReceivedListener() {
										
										public void onResponseStringReceived(String jsonresponse) {
											// TODO Auto-generated method stub
											purchaseProcess.processFinished();
											
										}
										
										public void onResponseJSONReceived(JSONObject jsonResponse) {
											// TODO Auto-generated method stub
											if (null == jsonResponse) {
												purchaseProcess.processFinished();
												
											} else {
												
												int idError = jsonResponse.optInt("idError", -1);
												Log.d(TAG, "Id Error: " + idError);
												
												PagoResponseTO response = new PagoResponseTO();
												
												Intent intent = new Intent(FishersPagoActivity.this, FishersConfirmacionActivity.class);
												
												
												/*
												 * {
												 * 	"transaccion":"13165",
												 * 	"autorizacion":"6515",
												 * 	"idError":0,
												 * 	"cuentaTO":"123123",
												 * 	"caja":"caja 1",
												 * 	"tarjeta":"XXXX XXXX XXXX8029",
												 * 	"mensajeError":"El pago fue exitoso.",
												 * 	"referencia":"149991",
												 * 	"monto":"1060"
												 * }
												*/
												
												switch (idError) {
												case 0:
													response.setIdError(idError)
															.setAutorizacion(jsonResponse.optString("autorizacion"))
															.setMensajeError(jsonResponse.optString("mensajeError"))
															.setReferencia(jsonResponse.optString("referencia"))
															.setTransaccion(jsonResponse.optString("transaccion"))
															.setTarjeta(jsonResponse.optString("tarjeta"))
															.setCaja(jsonResponse.optString("caja"))
															.setCuenta(jsonResponse.optString("cuentaTO"))
															.setMonto(jsonResponse.optString("monto"));
													
													intent.putExtra("pago", response);
													intent.putExtra("img", img);
													
													finish();
													break;

												default:
													response.setIdError(idError)
															.setMensajeError(jsonResponse.optString("mensajeError", "Error de conexi�n int�ntelo de nuevo m�s tarde."));
													
													intent.putExtra("pago", response);
													intent.putExtra("img", img);
													break;
												}
											
												startActivity(intent);
												
												purchaseProcess.processFinished();
											}
										}
										
										public void onResponseJSONAReceived(JSONArray jsonResponse) {
											// TODO Auto-generated method stub
											purchaseProcess.processFinished();
											
										}
									});
									
								} else {
											
									
									PagoVisaWSClient client = new PagoVisaWSClient(FishersPagoActivity.this);
									
									client.setLogged(false)
											.setCvv(cvv)
											.setEmail(email)
											.setNombre(nombre)
											.setTarjeta(tarjeta)
											.setToken(token)
											.setVigenciaMes(mes)
											.setVigenciaAnio(anio)
											.setJSON(json);
									
									client.executePurchase(new OnResponseJSONReceivedListener() {
										
										public void onResponseStringReceived(String jsonresponse) {
											// TODO Auto-generated method stub
											purchaseProcess.processFinished();
											
										}
										
										public void onResponseJSONReceived(JSONObject jsonResponse) {
											// TODO Auto-generated method stub
											if (null == jsonResponse) {
												purchaseProcess.processFinished();
												
											} else {
												Log.d(TAG, jsonResponse.toString());
												
//												{"idError":6,"mensajeError":"Ocurrio un error durante el pago."}
												
												int idError = jsonResponse.optInt("idError", -1);
												Log.d(TAG, "Id Error: " + idError);
												
												PagoResponseTO response = new PagoResponseTO();
												
												Intent intent = new Intent(FishersPagoActivity.this, FishersConfirmacionActivity.class);
												
												switch (idError) {
												case 0:
													response.setIdError(idError)
															.setAutorizacion(jsonResponse.optString("autorizacion"))
															.setMensajeError(jsonResponse.optString("mensajeError"))
															.setReferencia(jsonResponse.optString("referencia"))
															.setTransaccion(jsonResponse.optString("transaccion"))
															.setTarjeta(jsonResponse.optString("tarjeta"))
															.setCaja(jsonResponse.optString("caja"))
															.setCuenta(jsonResponse.optString("cuentaTO"))
															.setMonto(jsonResponse.optString("monto"));
													
													intent.putExtra("pago", response);
													intent.putExtra("img", img);
													
													finish();
													break;

												default:
													response.setIdError(idError)
															.setMensajeError(jsonResponse.optString("mensajeError", "Error de conexi�n int�ntelo de nuevo m�s tarde."));
													
													intent.putExtra("pago", response);
													intent.putExtra("img", img);
													break;
												}

												startActivity(intent);
												
												purchaseProcess.processFinished();
											}
										}
										
										public void onResponseJSONAReceived(JSONArray jsonResponse) {
											// TODO Auto-generated method stub
											purchaseProcess.processFinished();
											
										}
									});
								}
								
								
							} else {
								purchaseProcess.processFinished();
							}
						}
						
						public void onResponseJSONAReceived(JSONArray jsonResponse) {
							// TODO Auto-generated method stub
							purchaseProcess.processFinished();
						}
					});
				}
			});
		}
		
		purchaseProcess.start();
	}
	
	public void executeAction(View v) {
		
		if (null != v) {
			
			int id = v.getId();
			
			switch (id) {
			case R.id.button_realizar:
				if (validator.validate())
					run();
				break;

			default:
				onBackPressed();
				break;
			}
		}
	}

}
