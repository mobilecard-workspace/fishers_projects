package org.addcel.fishers.view;

import java.util.List;

import org.addcel.fishers.adapter.MarcaAdapter;
import org.addcel.fishers.listener.OnMarcasResponseListener;
import org.addcel.fishers.to.MarcaTO;
import org.addcel.fishers.ws.GetMarcasWSClient;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;

public class FishersMenuActivity extends MenuActivity {
	
	GridView gridMarcas;
	List<MarcaTO> marcasList;
	LongProcess marcaProcess;
	GetMarcasWSClient marcasClient;
	
	private static final String TAG = "FishersMenuActivity";

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.FISHERS_MENU;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_fishers_menu);
		
		marcaProcess = new LongProcess(FishersMenuActivity.this, true, 30, true, "fishers_marcas", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != marcasClient) {
					marcasClient.cancel();
					marcasClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				marcasClient = new GetMarcasWSClient(FishersMenuActivity.this);
				
				marcasClient.execute(new OnMarcasResponseListener() {
					
					public void onMarcasResponseReceived(List<MarcaTO> marcas) {
						// TODO Auto-generated method stub
						marcasList = marcas;
						gridMarcas.setAdapter(new MarcaAdapter(FishersMenuActivity.this, marcasList));
						gridMarcas.setOnItemClickListener(new OnItemClickListener() {
							
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								// TODO Auto-generated method stub
								MarcaTO marca = (MarcaTO) ((MarcaAdapter) arg0.getAdapter()).getItem(arg2);
								
								
								Intent intent = new Intent(FishersMenuActivity.this, FishersConsultaActivity.class);
								intent.putExtra("marca", marca);
								
								ImageView img = (ImageView) arg1;
								Bitmap bmp = ((BitmapDrawable) img.getDrawable()).getBitmap();
								
								intent.putExtra("img", bmp);
								
								startActivity(intent);
							}
						});
						marcaProcess.processFinished();
					}
				});
			}
		});
		
		marcaProcess.start();
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		gridMarcas = (GridView) findViewById(R.id.grid_data);
	}
	
	public void executeAction(View v) {
		
		if (null != v) {
			
			onBackPressed();
		}
	}

}
