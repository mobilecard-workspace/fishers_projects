package org.addcel.fishers.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.fishers.listener.OnCuentaResponseListener;
import org.addcel.fishers.listener.OnSucursalesResponseListener;
import org.addcel.fishers.to.CuentaTO;
import org.addcel.fishers.to.MarcaTO;
import org.addcel.fishers.to.SucursalTO;
import org.addcel.fishers.ws.ConsultaCuentaWSClient;
import org.addcel.fishers.ws.GetCajasWSClient;
import org.addcel.fishers.ws.GetSucursalesWSClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.system.errores.ErrorSys;

public class FishersConsultaActivity extends MenuActivity {
	
	MarcaTO marca;
	Bitmap img;
	ImageView marcaImageView;
	Spinner sucursalSpinner, cajaSpinner;
	EditText cuentaEdit;
	LongProcess sucursalProcess, cajaProcess, consultaProcess;
	GetSucursalesWSClient sucursalClient;
	GetCajasWSClient cajasClient;
	ConsultaCuentaWSClient consultaClient;
	ArrayList<BasicNameValuePair> sucursalesList, cajasList;
	
	final FormValidator validator = new FormValidator(this, true);
	
	private static final String TAG = "FishersConsultaActivity";

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.FISHERS_CONSULTA;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_fishers_consulta);
		
		sucursalProcess = new LongProcess(FishersConsultaActivity.this, true, 30, true, "fishers_sucursales", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != sucursalClient) {
					sucursalClient.cancel();
					sucursalClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				sucursalClient = new GetSucursalesWSClient(FishersConsultaActivity.this);
				sucursalClient.setIdMarca(marca.getIdMarca());
				sucursalClient.execute(new OnSucursalesResponseListener() {
					
					public void onSucursalesResponseReceived(List<SucursalTO> sucursales) {
						// TODO Auto-generated method stub
						sucursalSpinner.setAdapter(SucursalTO.BuildSucursalAdapter(FishersConsultaActivity.this, sucursales));
						sucursalProcess.processFinished();
					}

					public void onSucursalErrorReceived(int idError, String mensajeError) {
						// TODO Auto-generated method stub
						ErrorSys.showMsj(FishersConsultaActivity.this, mensajeError);
						finish();
						sucursalProcess.processFinished();
					}
				});
			}
		});
		
		sucursalProcess.start();
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		marcaImageView = (ImageView) findViewById(R.id.img_marca);
		marca = getIntent().getParcelableExtra("marca");
		img = getIntent().getParcelableExtra("img");
		
		marcaImageView.setImageBitmap(img);
		
		sucursalSpinner = (Spinner) findViewById(R.id.spinner_sucursal);
		cajaSpinner = (Spinner) findViewById(R.id.spinner_caja);
		
	}
	
	@Override
	public void configValidations() {
		// TODO Auto-generated method stub
		validator.addItem(getCuentaEdit(), "N�mero de cuentaTO")
					.canBeEmpty(false)
					.isRequired(true)
					.maxLength(20)
					.minLength(3);
		
	}
	
	protected EditText getCuentaEdit() {
		if (null == cuentaEdit)
			cuentaEdit = (EditText) findViewById(R.id.edit_cuenta);
		
		return cuentaEdit;
	}
	
	private void setConsultaProcess() {
		
		if (consultaProcess == null) {
			
			consultaProcess = new LongProcess(FishersConsultaActivity.this, true, 90, true, "fishers_consulta", new LongProcessListener() {
				
				public void stopProcess() {
					// TODO Auto-generated method stub
					if (consultaClient != null) {
						consultaClient.cancel();
						consultaClient = null;
					}
				}
				
				public void doProcess() {
					// TODO Auto-generated method stub
					consultaClient = new ConsultaCuentaWSClient(FishersConsultaActivity.this);
					
					SucursalTO sucursal = (SucursalTO) sucursalSpinner.getSelectedItem();
					
					consultaClient.setIdSucursal(sucursal.getIdSucursal());
					consultaClient.setIdCaja(sucursal.getIdCaja());
					consultaClient.setNumeroCuenta(getCuentaEdit().getText().toString().trim());
					
					consultaClient.execute(new OnCuentaResponseListener() {
						
						public void onCuentaResponseReceived(CuentaTO cuenta) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(FishersConsultaActivity.this, FishersDetalleActivity.class);
							intent.putExtra("cuentaTO", cuenta);
							intent.putExtra("img", img);
							
							startActivity(intent);
							
							consultaProcess.processFinished();
						}
					});
				}
			});
			
		}
		
		consultaProcess.start();
	}
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			
			switch (id) {
			case R.id.button_enviar:
				if (validator.validate())
					setConsultaProcess();
				break;

			default:
				onBackPressed();
				break;
			}
		}
	}

}
