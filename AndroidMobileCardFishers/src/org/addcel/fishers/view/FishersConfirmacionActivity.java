package org.addcel.fishers.view;

import org.addcel.fishers.to.PagoResponseTO;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.system.Text;

public class FishersConfirmacionActivity extends MenuActivity {
	
	PagoResponseTO pago;
	
	Bitmap img;
	ImageView marcaImageView;
	TextView tituloView, tarjetaView, autorizacionView, cajaView, cuentaView, montoView, mensajeView;
	LinearLayout tarjetaLayout, autorizacionLayout, cajaLayout, cuentaLayout, montoLayout;

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.FISHERS_CONFIRMACION;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_fishers_confirmacion);
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		initData();
		
		marcaImageView = (ImageView) findViewById(R.id.img_marca);
		tituloView = (TextView) findViewById(R.id.text_titulo);
		tarjetaView = (TextView) findViewById(R.id.text_tarjeta);
		autorizacionView = (TextView) findViewById(R.id.text_autorizacion);
		cajaView = (TextView) findViewById(R.id.text_caja);
		cuentaView = (TextView) findViewById(R.id.text_cuenta);
		montoView = (TextView) findViewById(R.id.text_monto);
		mensajeView = (TextView) findViewById(R.id.text_mensaje);
		
		tarjetaLayout = (LinearLayout) findViewById(R.id.layout_tarjeta);
		autorizacionLayout = (LinearLayout) findViewById(R.id.layout_autorizacion);
		cajaLayout = (LinearLayout) findViewById(R.id.layout_caja);
		cuentaLayout = (LinearLayout) findViewById(R.id.layout_cuenta);
		montoLayout = (LinearLayout) findViewById(R.id.layout_monto);
		
		loadData();
		
	}
	
	private void initData() {
		pago = getIntent().getParcelableExtra("pago");
		img = getIntent().getParcelableExtra("img");
	}
	
	private void loadData() {
		
		marcaImageView.setImageBitmap(img);
		
		int idError = pago.getIdError();
		String titulo = "";
		
		switch (idError) {
		case 0:
			titulo = pago.getMensajeError();
			
			tituloView.setText(titulo);
			
			tarjetaLayout.setVisibility(View.VISIBLE);
			tarjetaView.setText(pago.getTarjeta()); // Mau debe agregar Tarjeta enmascarada.
			
			if (null != pago.getAutorizacion() && !pago.getAutorizacion().isEmpty()) {

				autorizacionLayout.setVisibility(View.VISIBLE);
				autorizacionView.setText(pago.getAutorizacion());
			}
				
			
			cajaLayout.setVisibility(View.VISIBLE);
			cajaView.setText(pago.getCaja());
			
			cuentaLayout.setVisibility(View.VISIBLE);
			cuentaView.setText(pago.getCuenta());
			
			montoLayout.setVisibility(View.VISIBLE);
			
			try {
				double montoDouble = Double.valueOf(pago.getMonto());
				montoView.setText(Text.formatCurrency(montoDouble, true));
				
			} catch (NumberFormatException e) {

				montoView.setText(pago.getMonto());
			}
			
			mensajeView.setVisibility(View.GONE);
			
			
			break;

		default:
			titulo = "Pago Declinado";
			String mensaje = pago.getMensajeError();
			
			tituloView.setText(titulo);
			mensajeView.setText(mensaje);
			break;
		}
	}
	
	public void executeAction(View v) {
		
		if (null != v) {
			int id = v.getId();
			
			switch (id) {
			case R.id.button_back:
				onBackPressed();
				break;

			default:
				break;
			}
		}
	}
	
	

}
