package org.addcel.fishers.view;

import org.addcel.fishers.listener.OnCuentaResponseListener;
import org.addcel.fishers.to.ConsumoTO;
import org.addcel.fishers.to.CuentaTO;
import org.addcel.fishers.ws.ConsultaCuentaWSClient;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.contador.Contador;
import com.ironbit.mc.system.contador.OnContadorTerminadoListener;
import com.ironbit.mc.usuario.Usuario;

public class FishersDetalleActivity extends MenuActivity {
	
	
	CuentaTO cuentaTO;
	Bitmap img;
	
	TextView subtotalView, descuentoView, porcentajeView, totalView, propinaView, cargoView;
	LinearLayout comidaLayout, bebidaLayout, otroLayout, cargoLayout;
	EditText porcentajeText, cantidadText, divideText;
	double propina, totalPago;
	final FormValidator validator = new FormValidator(this, true);
	
	LongProcess updateProcess;
	ConsultaCuentaWSClient updateClient;
	
	boolean isCantidad;
	
	Contador updateCounter;
	
	private static final String TAG = "FishersDetalleActivity";

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.FISHERS_DETALLE;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_fishers_detalle);
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		initData();
		
		totalPago = cuentaTO.getTotal();
		
		comidaLayout = (LinearLayout) findViewById(R.id.layout_comida);
		bebidaLayout = (LinearLayout) findViewById(R.id.layout_bebida);
		otroLayout = (LinearLayout) findViewById(R.id.layout_otros);
		cargoLayout = (LinearLayout) findViewById(R.id.layout_cargo);
		
		
		porcentajeText = (EditText) findViewById(R.id.edit_pct);
		
		subtotalView = (TextView) findViewById(R.id.text_subtotal);
		descuentoView = (TextView) findViewById(R.id.text_descuento);
		porcentajeView = (TextView) findViewById(R.id.text_porcentaje);
		totalView = (TextView) findViewById(R.id.text_total);
		cargoView = (TextView) findViewById(R.id.text_cargo);
		
		propinaView = (TextView) findViewById(R.id.text_propina);
		
		loadData();
		
		
	}
	
	@Override
	public void configValidations() {
		// TODO Auto-generated method stub
	}
	
	protected EditText getCantidadText() {
		if (null == cantidadText) {
			cantidadText = (EditText) findViewById(R.id.edit_cantidad);
			cantidadText.setText(String.valueOf(0));
		}
		
		return cantidadText;
	}
	
	protected EditText getDivideText() {
		if (null == divideText) {
			divideText = (EditText) findViewById(R.id.edit_divide);
			divideText.setText(String.valueOf(1));
		}	
		
		return divideText;
	}
	
	private void initData() {
		cuentaTO = getIntent().getParcelableExtra("cuentaTO");
		img = getIntent().getParcelableExtra("img");
	}
	
	private void loadData() {
		subtotalView.setText(Text.formatCurrency(cuentaTO.getSubtotal(), true));
		descuentoView.setText(Text.formatCurrency(cuentaTO.getDescuento(), true));
		porcentajeView.setText(Text.formatCurrency(cuentaTO.getDescuento(), true));
		totalView.setText(Text.formatCurrency(cuentaTO.getTotal(), true));
		
		for (ConsumoTO comida : cuentaTO.getComida()) {
			comidaLayout.addView(buildConsumoLayout(comida));
		}
		
		for (ConsumoTO bebida: cuentaTO.getBebida()) {
			bebidaLayout.addView(buildConsumoLayout(bebida));
		}
		
		for (ConsumoTO otro: cuentaTO.getOtro()) {
			otroLayout.addView(buildConsumoLayout(otro));
		}
		
		porcentajeText.addTextChangedListener(new TextWatcher() {
			
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}
			
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			public void afterTextChanged(Editable s) {
				if (s.length() > 0) {
					if (Integer.valueOf(s.toString())  <= 100) {
						
						double pct = Double.valueOf(s.toString()) / 100;
						propina = cuentaTO.getTotal() * pct;
						
						totalPago = totalPago + propina;
						
//						cuentaTO.setTotalPago(cuentaTO.getTotal() + propina);
						propinaView.setText(Text.formatCurrency(propina, true));
						cargoView.setText(Text.formatCurrency(totalPago, true));
						
					} 
				} else  {
					double pct = 0.0;
					propina = cuentaTO.getTotal() * pct;
					
					totalPago = totalPago + propina;
					
//					cuentaTO.setTotalPago(cuentaTO.getTotal() + propina);
					propinaView.setText(Text.formatCurrency(propina, true));
					cargoView.setText(Text.formatCurrency(totalPago, true));
				}
			}
		});
		
		getDivideText().addTextChangedListener(new TextWatcher() {
			
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				try {
					
					if (s.length() > 0) {
						totalPago = (totalPago + propina) / Integer.valueOf(s.toString());
						cargoView.setText(Text.formatCurrency(totalPago, true));
					} else {
						
						totalPago = totalPago + propina;
						cargoView.setText(Text.formatCurrency(totalPago, true));
					}
				} catch (NumberFormatException e) {
					
				}
			}
		});	
		
		updateCounter = new Contador(new Handler(), new OnContadorTerminadoListener() {
			
			public void onContadorTerminado() {
				// TODO Auto-generated method stub
				Log.d(TAG, "Termin� Contador");
				buildUpdateProcess(cuentaTO);
			}
		}, 600, "update_counter");
		
	}
	
	private void buildUpdateProcess(final CuentaTO paramCuenta) {
		
		updateProcess = new LongProcess(FishersDetalleActivity.this, true, 60, true, "fishers_update", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != updateClient) {
					updateClient.cancel();
					updateClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				updateClient = new ConsultaCuentaWSClient(FishersDetalleActivity.this);
				
				updateClient.setIdCaja(paramCuenta.getIdCaja());
				updateClient.setIdSucursal(paramCuenta.getIdSucursal());
				updateClient.setNumeroCuenta(paramCuenta.getNumeroCuenta());
				
				updateClient.execute(new OnCuentaResponseListener() {
					
					public void onCuentaResponseReceived(CuentaTO cuenta) {
						// TODO Auto-generated method stub
						cuentaTO = cuenta;
						totalPago = cuentaTO.getTotal();
						
						comidaLayout.removeAllViews();
						bebidaLayout.removeAllViews();
						otroLayout.removeAllViews();
						
						subtotalView.setText(Text.formatCurrency(cuentaTO.getSubtotal(), true));
						descuentoView.setText(Text.formatCurrency(cuentaTO.getDescuento(), true));
						porcentajeView.setText(Text.formatCurrency(cuentaTO.getDescuento(), true));
						totalView.setText(Text.formatCurrency(cuentaTO.getTotal(), true));
						
						for (ConsumoTO comida : cuentaTO.getComida()) {
							comidaLayout.addView(buildConsumoLayout(comida));
						}
						
						for (ConsumoTO bebida: cuentaTO.getBebida()) {
							bebidaLayout.addView(buildConsumoLayout(bebida));
						}
						
						for (ConsumoTO otro: cuentaTO.getOtro()) {
							otroLayout.addView(buildConsumoLayout(otro));
						}
						
						updateProcess.processFinished();
					}
				});
			}
		});
		
		updateProcess.start();
	}
	
	
	/**
	 * 
	 * @param consumo Contiene la informaci�n a mostrar en el layout.
	 * @return LinearLayout que agrupa TextViews informativos
	 */
	private LinearLayout buildConsumoLayout(ConsumoTO consumo) {
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		LinearLayout layout = new LinearLayout(FishersDetalleActivity.this);
		layout.setLayoutParams(params);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		layout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
		layout.addView(buildConsumoTextView("" + consumo.getCantidad(), false), 0);
		layout.addView(buildConsumoTextView(consumo.getDescripcionProducto(), false), 1);
		layout.addView(buildConsumoTextView(Text.formatCurrency(consumo.getImporte(), true), true), 2);
		
		
		return layout;
	}
	
	/**
	 * 
	 * @param text String a pintar en el Textview
	 * @param isImporte Valida si lo que se va a pintar es un atributo importe, si es verdadero se modifica la gravedad del TextView
	 * @return TextView con formato
	 */
	private TextView buildConsumoTextView(String text, boolean isImporte) {
		
		LayoutParams params = null;
		
		if (isImporte)
			params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		else
			params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		TextView texto = new TextView(FishersDetalleActivity.this);
		texto.setLayoutParams(params);
		
		if (isImporte)
			texto.setGravity(Gravity.RIGHT);
		
		texto.setTextAppearance(FishersDetalleActivity.this, android.R.attr.textAppearanceSmall);
		texto.setTextColor(getResources().getColor(R.color.abs__bright_foreground_holo_light));
		texto.setTypeface(null, Typeface.BOLD);
		texto.setPadding(0, 0, 8, 0);
		texto.setText(text);
		
		return texto;
	}
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			
			switch (id) {
			case R.id.button_enviar:
				
				if (validator.validate()) {
					
					Intent intent = null;
					cuentaTO.setPropina(propina);
					cuentaTO.setTotalPago(totalPago);
					
					if (Usuario.getIdUser(FishersDetalleActivity.this).isEmpty())
						intent = new Intent(FishersDetalleActivity.this, FishersPagoActivity.class);
					else
						intent = new Intent(FishersDetalleActivity.this, FishersPagoMobilecardActivity.class);
					
					intent.putExtra("cuentaTO", cuentaTO);
					intent.putExtra("img", img);
					updateCounter.cancelar();
					startActivity(intent);
				}
				
				break;
				
			case R.id.button_actualizar:
				buildUpdateProcess(cuentaTO);
				break;

			default:
				onBackPressed();
				break;
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		updateCounter.cancelar();
	}

}
