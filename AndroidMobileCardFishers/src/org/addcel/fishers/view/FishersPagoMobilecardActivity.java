package org.addcel.fishers.view;

import java.math.BigDecimal;

import org.addcel.fishers.to.CuentaTO;
import org.addcel.fishers.to.PagoResponseTO;
import org.addcel.fishers.ws.GetTokenWSClient;
import org.addcel.fishers.ws.PagoVisaWSClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class FishersPagoMobilecardActivity extends MenuActivity {
	
	TextView montoView;
	Bitmap img;
	ImageView marcaImageView;
	LongProcess purchaseProcess;
	GetTokenWSClient tokenClient;
	private EditText passwordText, cvvText;
	
	CuentaTO cuenta;
	JSONObject json;
	
	protected final FormValidator validator = new FormValidator(this, true);
	
	private static final String TAG = "FishersPagoMobilecardActivity";

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.FISHERS_PAGO_MOBILECARD;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_fishers_pago_mc);
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		initData();
		
		marcaImageView = (ImageView) findViewById(R.id.img_marca);
		montoView = (TextView) findViewById(R.id.text_monto);
		
		loadData();
	}
	
	protected EditText getPasswordText() {
		if (null == passwordText)
			passwordText = (EditText) findViewById(R.id.edit_pass);
		
		return passwordText;
	}
	
	protected EditText getCvvText() {
		if (null == cvvText)
			cvvText = (EditText) findViewById(R.id.edit_cvv);
		
		return cvvText;
	}
	
	@Override
	public void configValidations() {
		// TODO Auto-generated method stub
		validator.addItem(getCvvText(), "C�digo Verificador")
		.isRequired(true)
		.maxLength(4)
		.minLength(3);
		
		validator.addItem(getPasswordText(), "Contrase�a Mobilecard")
		.isRequired(true)
		.maxLength(12)
		.minLength(8);
	}
	
	private void initData() {
		img = getIntent().getParcelableExtra("img");
		cuenta = getIntent().getParcelableExtra("cuentaTO");
		
		try {
			json = new JSONObject(cuenta.getJson());
			json.put("total_pago", cuenta.getTotalPago());
			json.put("propina", cuenta.getPropina());
		} catch (JSONException e) {
			
		}
	}
	
	private void loadData() {
		marcaImageView.setImageBitmap(img);
		montoView.setText(Text.formatCurrency(cuenta.getTotalPago(), true));
		
	}
	
	private void run() {
		if (null == purchaseProcess) {
			
			purchaseProcess = new LongProcess(FishersPagoMobilecardActivity.this, true, 90, true, "fishers_pago_mc", new LongProcessListener() {
				
				public void stopProcess() {
					// TODO Auto-generated method stub
					if (null != tokenClient) {
						tokenClient.cancel();
						tokenClient = null;
					}
				}
				
				public void doProcess() {
					// TODO Auto-generated method stub
					tokenClient = new GetTokenWSClient(FishersPagoMobilecardActivity.this);
					tokenClient.executeWrapper(new OnResponseJSONReceivedListener() {
						
						public void onResponseStringReceived(String jsonresponse) {
							// TODO Auto-generated method stub
							purchaseProcess.processFinished();
						}
						
						public void onResponseJSONReceived(JSONObject jsonResponse) {
							// TODO Auto-generated method stub
							String token = jsonResponse.optString("token", "");
							
							if (!token.isEmpty()) {
								
								String cvv = getCvvText().getText().toString().trim();
								String pass = getPasswordText().getText().toString().trim();
								
								PagoVisaWSClient client = new PagoVisaWSClient(FishersPagoMobilecardActivity.this);
								
								client.setLogged(true).
										setCvv(cvv).
										setPassword(pass).
										setToken(token).
										setJSON(json);
								
								client.executePurchase(new OnResponseJSONReceivedListener() {
									
									public void onResponseStringReceived(String jsonresponse) {
										// TODO Auto-generated method stub
										purchaseProcess.processFinished();
										
									}
									
									public void onResponseJSONReceived(JSONObject jsonResponse) {
										// TODO Auto-generated method stub
										if (null == jsonResponse) {
											purchaseProcess.processFinished();
											
										} else {
											Log.d(TAG, jsonResponse.toString());
											
											int idError = jsonResponse.optInt("idError", -1);
											Log.d(TAG, "Id Error: " + idError);
											
											PagoResponseTO response = new PagoResponseTO();
											
											Intent intent = new Intent(FishersPagoMobilecardActivity.this, FishersConfirmacionActivity.class);
											
											switch (idError) {
											case 0:
												response.setIdError(idError)
												.setAutorizacion(jsonResponse.optString("autorizacion"))
												.setMensajeError(jsonResponse.optString("mensajeError"))
												.setReferencia(jsonResponse.optString("referencia"))
												.setTransaccion(jsonResponse.optString("transaccion"))
												.setTarjeta(jsonResponse.optString("tarjeta"))
												.setCaja(jsonResponse.optString("caja"))
												.setCuenta(jsonResponse.optString("cuentaTO"))
												.setMonto(jsonResponse.optString("monto"));
										
												intent.putExtra("pago", response);
												intent.putExtra("img", img);
												
												finish();
												break;

											default:
												response.setIdError(idError)
												.setMensajeError(jsonResponse.optString("mensajeError", "Error de conexi�n int�ntelo de nuevo m�s tarde."));
										
												intent.putExtra("pago", response);
												intent.putExtra("img", img);
												break;
											}

											startActivity(intent);
											purchaseProcess.processFinished();
										}
									}
									
									public void onResponseJSONAReceived(JSONArray jsonResponse) {
										// TODO Auto-generated method stub
										purchaseProcess.processFinished();
										
									}
								});
								
							} else {
								
								
							}
						}
						
						public void onResponseJSONAReceived(JSONArray jsonResponse) {
							// TODO Auto-generated method stub
							purchaseProcess.processFinished();
						}
					});
				}
			});
			

		}
		
		purchaseProcess.start();
	}
	
	public void executeAction(View v) {
		
		if (null != v) {
			
			int id = v.getId();
			
			switch (id) {
			case R.id.button_realizar:
				if (validator.validate())
					run();
				break;

			default:
				onBackPressed();
				break;
			}
		}
	}

}
