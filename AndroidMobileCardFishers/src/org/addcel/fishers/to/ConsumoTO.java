package org.addcel.fishers.to;

import android.os.Parcel;
import android.os.Parcelable;

public class ConsumoTO implements Parcelable {
	
	private String idPago;
	private int cantidad;
	private long idProducto;
	private String descripcionProducto;
	private String precioUnitario;
	private double importe;
	
	public ConsumoTO() {}
	
	public ConsumoTO(Parcel in) {
		readFromParcel(in);
	}

	public String getIdPago() {
		return idPago;
	}

	public void setIdPago(String idPago) {
		this.idPago = idPago;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public String getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Consumo: " + cantidad + " " + descripcionProducto + " " + importe;
	}

	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(cantidad);
		dest.writeString(descripcionProducto);
		dest.writeString(idPago);
		dest.writeLong(idProducto);
		dest.writeDouble(importe);
		dest.writeString(precioUnitario);
	}
	
	public void readFromParcel(Parcel in) {
		this.cantidad = in.readInt();
		this.descripcionProducto = in.readString();
		this.idPago = in.readString();
		this.idProducto = in.readLong();
		this.importe = in.readDouble();
		this.precioUnitario = in.readString();
	}
	
	public static final Parcelable.Creator<ConsumoTO> CREATOR = new Creator<ConsumoTO>() {
	
		public ConsumoTO createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new ConsumoTO(source);
		}
		
		public ConsumoTO[] newArray(int size) {
			// TODO Auto-generated method stub
			return new ConsumoTO[size];
		}
	
	};

}
