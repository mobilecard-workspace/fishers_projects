package org.addcel.fishers.to;

import java.util.List;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

public class SucursalTO implements Parcelable {

	private int idSucursal;
	private int idMarca;
	private int idCaja;
	private String descripcion;
	
	public SucursalTO() {}
	
	public SucursalTO(Parcel in) {
		readFromParcel(in);
	}
	
	public int getIdSucursal() {
		return idSucursal;
	}
	
	public SucursalTO setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
		
		return this;
	}

	public int getIdMarca() {
		return idMarca;
	}
	
	public SucursalTO setIdMarca(int idMarca) {
		this.idMarca = idMarca;
		
		return this;
	}

	public int getIdCaja() {
		return idCaja;
	}
	
	public SucursalTO setIdCaja(int idCaja) {
		this.idCaja = idCaja;
		
		return this;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public SucursalTO setDescripcion(String descripcion) {
		if (!TextUtils.isEmpty(descripcion)) {
			this.descripcion = descripcion;
		}
		
		return this;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return descripcion;
	}


	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(idSucursal);
		dest.writeInt(idMarca);
		dest.writeInt(idCaja);
		dest.writeString(descripcion);
	}
	
	private void readFromParcel(Parcel in) {
		idSucursal = in.readInt();
		idMarca = in.readInt();
		idSucursal = in.readInt();
		descripcion = in.readString();
	}
	
	public static final Parcelable.Creator<SucursalTO> CREATOR = new Creator<SucursalTO>() {
		
		public SucursalTO[] newArray(int size) {
			// TODO Auto-generated method stub
			return new SucursalTO[size];
		}
		
		public SucursalTO createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new SucursalTO(source);
		}
	};
	
	public static SpinnerAdapter BuildSucursalAdapter(Context _con, List<SucursalTO> sucursales) {
		ArrayAdapter<SucursalTO> adapter = new ArrayAdapter<SucursalTO>(_con, android.R.layout.simple_spinner_item, sucursales);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		return adapter;
	}

}
