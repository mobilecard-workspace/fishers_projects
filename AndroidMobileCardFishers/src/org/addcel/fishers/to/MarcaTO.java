package org.addcel.fishers.to;

import android.os.Parcel;
import android.os.Parcelable;

public class MarcaTO implements Parcelable {

	private int idMarca;
	private String descripcion;
	private String imgUrl;
	

	public MarcaTO() {
	}
	
	public MarcaTO(Parcel in) {
		readFromParcel(in);
	}

	public int getIdMarca() {
		return idMarca;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub
		arg0.writeInt(getIdMarca());
		arg0.writeString(getDescripcion());
		arg0.writeString(getImgUrl());
	}
	
	private void readFromParcel(Parcel in) {
		setIdMarca(in.readInt());
		setDescripcion(in.readString());
		setImgUrl(in.readString());
	}
	
	public static final Parcelable.Creator<MarcaTO> CREATOR = new Creator<MarcaTO>() {
		
		public MarcaTO createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new MarcaTO(source);
		}
		
		public MarcaTO[] newArray(int size) {
			// TODO Auto-generated method stub
			return new MarcaTO[size];
		}
	};

}
