package org.addcel.fishers.to;

import android.os.Parcel;
import android.os.Parcelable;

public class PagoResponseTO implements Parcelable {
	
	private int idError;
	private String mensajeError;
	private String transaccion;
	private String autorizacion;
	private String referencia;
	private String tarjeta;
	private String caja;
	private String cuenta;
	private String monto;
	
/*
 * {
 * 	"transaccion":"13165",
 * 	"autorizacion":"6515",
 * 	"idError":0,
 * 	"cuentaTO":"123123",
 * 	"caja":"caja 1",
 * 	"tarjeta":"XXXX XXXX XXXX8029",
 * 	"mensajeError":"El pago fue exitoso.",
 * 	"referencia":"149991",
 * 	"monto":"1060"
 * }
*/
	
	public PagoResponseTO() {}
	
	public PagoResponseTO(Parcel in) {
		readFromParcel(in);
	}
	
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public int getIdError() {
		return idError;
	}
	
	public String getMensajeError() {
		return mensajeError;
	}

	public String getTransaccion() {
		return transaccion;
	}

	public String getAutorizacion() {
		return autorizacion;
	}

	public String getReferencia() {
		return referencia;
	}
	
	public String getTarjeta() {
		return tarjeta;
	}

	public String getCaja() {
		return caja;
	}

	public String getCuenta() {
		return cuenta;
	}

	public String getMonto() {
		return monto;
	}

	public PagoResponseTO setIdError(int idError) {
		this.idError = idError;
		
		return this;
	}
	
	public PagoResponseTO setMensajeError(String mensajeError) {
		if (!mensajeError.isEmpty()) {
			this.mensajeError = mensajeError;
		}
		
		return this;
	}
	
	public PagoResponseTO setTransaccion(String transaccion) {
		if (!transaccion.isEmpty()) {
			this.transaccion = transaccion;
		}
		
		return this;
	}
	
	public PagoResponseTO setAutorizacion(String autorizacion) {
		if (!autorizacion.isEmpty()) {
			this.autorizacion = autorizacion;
		}
		
		return this;
	}
	
	public PagoResponseTO setReferencia(String referencia) {
		if (!referencia.isEmpty()) {
			this.referencia = referencia;
		}
		
		return this;
	}
	
	public PagoResponseTO setTarjeta(String tarjeta) {
		if (!tarjeta.isEmpty()) {
			this.tarjeta = tarjeta;
		}
		
		return this;
	}
	
	public PagoResponseTO setCaja(String caja) {
		if (!caja.isEmpty()) {
			this.caja = caja;
		}
		
		return this;
	}
	
	public PagoResponseTO setCuenta(String cuenta) {
		if (!cuenta.isEmpty()) {
			this.cuenta = cuenta;
		}
		
		return this;
	}
	
	public PagoResponseTO setMonto(String monto) {
		if (!monto.isEmpty()) {
			this.monto = monto;
		}
		
		return this;
	}

	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(idError);
		dest.writeString(mensajeError);
		dest.writeString(transaccion);
		dest.writeString(autorizacion);
		dest.writeString(referencia);
		dest.writeString(tarjeta);
		dest.writeString(caja);
		dest.writeString(cuenta);
		dest.writeString(monto);
	}
	
	public void readFromParcel(Parcel in) {
		idError = in.readInt();
		mensajeError = in.readString();
		transaccion = in.readString();
		autorizacion = in.readString();
		referencia = in.readString();
		tarjeta = in.readString();
		caja = in.readString();
		cuenta = in.readString();
		monto = in.readString();
	}
	
	public static final Parcelable.Creator<PagoResponseTO> CREATOR = new Creator<PagoResponseTO>() {
		
		public PagoResponseTO[] newArray(int size) {
			// TODO Auto-generated method stub
			return new PagoResponseTO[size];
		}
		
		public PagoResponseTO createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new PagoResponseTO(source);
		}
	};

}
