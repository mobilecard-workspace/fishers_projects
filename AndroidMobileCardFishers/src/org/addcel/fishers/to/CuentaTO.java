package org.addcel.fishers.to;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class CuentaTO implements Parcelable {
	
	private String idPago;
	private String idBitacora;
	private String idUsuario;
	private String fecha;
	private int status;
	private String email;
	private int idSucursal;
	private int idCaja;
	private String numeroCuenta;
	private double subtotal;
	private double descuento;
	private double iva;
	private double total;
	private double propina;
	private double totalPago;
	private String json;
	
	/*    
	"id_pago": null,
    "id_bitacora": null,
    "id_usuario": null,
    "fecha": null,
    "status": 0,
    "email": null,
    "id_sucursal": 3,
    "id_caja": 1,
    "numero_cuenta": "41438",
    "subtotal": 1000,
    "descuento": 100,
    "iva": 160,
    "total": 1060,
    "propina": null,
    "total_pago": null,
    */
	
	private List<ConsumoTO> comida;
	private List<ConsumoTO> bebida;
	private List<ConsumoTO> otro;
	
	public CuentaTO() {
		comida = new ArrayList<ConsumoTO>();
		bebida = new ArrayList<ConsumoTO>();
		otro = new ArrayList<ConsumoTO>();
	}
	
	public CuentaTO(Parcel in) {
		
		this();
		readFromParcel(in);
	}
	

	public String getIdPago() {
		return idPago;
	}

	public void setIdPago(String idPago) {
		this.idPago = idPago;
	}

	public String getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}

	public int getIdCaja() {
		return idCaja;
	}

	public void setIdCaja(int idCaja) {
		this.idCaja = idCaja;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
	public double getPropina() {
		return propina;
	}
	
	public void setPropina(double propina) {
		this.propina = propina;
	}

	public double getTotalPago() {
		return totalPago;
	}

	public void setTotalPago(double totalPago) {
		this.totalPago = totalPago;
	}

	public List<ConsumoTO> getComida() {
		return comida;
	}

	public void setComida(List<ConsumoTO> comida) {
		this.comida = comida;
	}

	public List<ConsumoTO> getBebida() {
		return bebida;
	}

	public void setBebida(List<ConsumoTO> bebida) {
		this.bebida = bebida;
	}

	public List<ConsumoTO> getOtro() {
		return otro;
	}

	public void setOtro(List<ConsumoTO> otro) {
		this.otro = otro;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Num. cuentaTO: " + numeroCuenta + ", Total: " + total;
	}

	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeTypedList(getBebida());
		dest.writeTypedList(getComida());
		dest.writeTypedList(getOtro());
		dest.writeDouble(getDescuento());
		dest.writeString(getEmail());
		dest.writeString(getFecha());
		dest.writeString(getIdBitacora());
		dest.writeInt(getIdCaja());
		dest.writeString(getIdPago());
		dest.writeInt(getIdSucursal());
		dest.writeString(getIdUsuario());
		dest.writeDouble(getIva());
		dest.writeString(getNumeroCuenta());
		dest.writeInt(getStatus());
		dest.writeDouble(getSubtotal());
		dest.writeDouble(getTotal());
		dest.writeDouble(getPropina());
		dest.writeDouble(getTotalPago());
		dest.writeString(getJson());
	}
	
	public void readFromParcel(Parcel in) {
		
		in.readTypedList(getBebida(), ConsumoTO.CREATOR);
		in.readTypedList(getComida(), ConsumoTO.CREATOR);
		in.readTypedList(getOtro(), ConsumoTO.CREATOR);
		
		this.descuento = in.readDouble();
		this.email = in.readString();
		this.fecha = in.readString();
		this.idBitacora = in.readString();
		this.idCaja = in.readInt();
		this.idPago = in.readString();
		this.idSucursal = in.readInt();
		this.idUsuario = in.readString();
		this.iva = in.readDouble();
		this.numeroCuenta = in.readString();
		this.status = in.readInt();
		this.subtotal = in.readDouble();
		this.total = in.readDouble();
		this.propina = in.readDouble();
		this.totalPago = in.readDouble();
		this.json = in.readString();
	}
	
	public static final Parcelable.Creator<CuentaTO> CREATOR = new Creator<CuentaTO>() {
	
		public CuentaTO createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new CuentaTO(source);
		}
		
		public CuentaTO[] newArray(int size) {
			// TODO Auto-generated method stub
			return new CuentaTO[size];
		}
	};
	
	

}
