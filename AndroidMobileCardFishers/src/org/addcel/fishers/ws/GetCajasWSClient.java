package org.addcel.fishers.ws;

import java.util.ArrayList;

import org.addcel.fishers.constant.Url;
import org.addcel.fishers.listener.OnCajasResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.WebServiceClient;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseReceivedListener;

public class GetCajasWSClient extends WebServiceClient {
	
	private String idSucursal;

	public GetCajasWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return Url.FISHERS_CAJAS;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.FISHERS_WS_CAJAS;
	}
	
	public GetCajasWSClient setIdSucursal(String idSucursal) {
		if (idSucursal != null && !idSucursal.isEmpty()) {
			this.idSucursal = idSucursal;
		}
		
		return this;
	}
	
	private String buildRequest() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("id_sucursal", idSucursal);
		} catch (JSONException e) {
			return "";
		}
		
		return Crypto.aesEncrypt(Sys.getKey(), json.toString());
	}
	
	public WebServiceClient execute(final OnCajasResponseListener listener) {
		addPostParameter("json", buildRequest());
		
		return executeBadDesign(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				listener.onCajasErrorReceived(-1, "Error de conexi�n. Intente de nuevo m�s tarde.");
			}
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				
				if (jsonResponse.has("idError")) {
					int idError = jsonResponse.optInt("idError");
					String mensajeError = jsonResponse.optString("mensajeError", "Error de conexi�n. Intente de nuevo m�s tarde.");

					
					listener.onCajasErrorReceived(idError, mensajeError);
				} else {

					ArrayList<BasicNameValuePair> cajas = new ArrayList<BasicNameValuePair>();
					
					JSONArray arrCajas = jsonResponse.optJSONArray("cajas");
					
					for (int i = 0; i < arrCajas.length(); i++) {
						JSONObject caja = arrCajas.optJSONObject(i);
						
						cajas.add(new BasicNameValuePair(caja.optString("descripcion"), caja.optString("id_caja")));
					}
					
					listener.onCajasResponseReceived(cajas);
				}
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				listener.onCajasErrorReceived(-1, "Error de conexi�n. Intente de nuevo m�s tarde.");
				
			}
			
			
		});
	}

}
