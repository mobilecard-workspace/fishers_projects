package org.addcel.fishers.ws;

import org.addcel.fishers.constant.Url;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.web.webservices.WebServiceClient;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetTokenWSClient extends WebServiceClient {

	public GetTokenWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return Url.FISHERS_TOKEN;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.FISHERS_WS_TOKEN;
	}
	
	public WebServiceClient executeWrapper(final OnResponseJSONReceivedListener listener) {
		
		return executePost(new OnResponseJSONReceivedListener() {
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (null != listener) {
					listener.onResponseJSONReceived(jsonResponse);
				}
			}
			
			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}

}
