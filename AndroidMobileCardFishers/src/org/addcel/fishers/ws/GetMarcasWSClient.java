package org.addcel.fishers.ws;

import java.util.ArrayList;
import java.util.List;

import org.addcel.fishers.constant.Url;
import org.addcel.fishers.listener.OnMarcasResponseListener;
import org.addcel.fishers.to.MarcaTO;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.web.webservices.WebServiceClient;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetMarcasWSClient extends WebServiceClient {

	public GetMarcasWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return Url.FISHERS_MARCAS;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.FISHERS_WS_MARCAS;
	}
	
	public WebServiceClient execute(final OnMarcasResponseListener listener) {
		
		return executePost(new OnResponseJSONReceivedListener() {
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (null != listener) {
					JSONArray array = jsonResponse.optJSONArray("Marcas");
					
					List<MarcaTO> marcas = new ArrayList<MarcaTO>();
					
					for (int i = 0; i < array.length(); i++) {
						JSONObject json = array.optJSONObject(i);
						
						MarcaTO marca = new MarcaTO();
						marca.setIdMarca(json.optInt("id_marca"));
						marca.setDescripcion(json.optString("descripcion"));
						marca.setImgUrl(json.optString("img_url"));
						
						marcas.add(marca);
						
					}
					
					listener.onMarcasResponseReceived(marcas);
				}
			}
			
			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}

}
