package org.addcel.fishers.ws;

import java.util.ArrayList;
import java.util.List;

import org.addcel.fishers.constant.Url;
import org.addcel.fishers.listener.OnSucursalesResponseListener;
import org.addcel.fishers.to.SucursalTO;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.WebServiceClient;
import com.ironbit.mc.web.webservices.events.OnResponseJSONAReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetSucursalesWSClient extends WebServiceClient {
	
	private int idMarca;

	public GetSucursalesWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetSucursalesWSClient setIdMarca(int idMarca) {
		this.idMarca = idMarca;
		
		return this;
	}
	
	private String buildRequest() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("id_marca", idMarca);
		} catch (JSONException e) {
			return "";
		}
		
		return Crypto.aesEncrypt(Sys.getKey(), json.toString());
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return Url.FISHERS_SUCURSALES;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.FISHERS_WS_SUCURSAL;
	}
	
	public WebServiceClient execute(final OnSucursalesResponseListener listener) {
		// TODO Auto-generated method stub
		
		addPostParameter("json", buildRequest());
		
		return executePost(new OnResponseJSONReceivedListener() {
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (null != listener) {
					
					if (jsonResponse.has("idError")) {
						listener.onSucursalErrorReceived(
								jsonResponse.optInt("idError", -1), 
								jsonResponse.optString("mensajeError", "Error de conexi�n. Intenta de nuevo m�s tarde."));
					} else {
					
						JSONArray sucursalesArr = jsonResponse.optJSONArray("sucursales");
						
						List<SucursalTO> sucursales = new ArrayList<SucursalTO>();
						
						for (int i = 0; i < sucursalesArr.length(); i++) {
							
							JSONObject json = sucursalesArr.optJSONObject(i);
							SucursalTO sucursal = new SucursalTO()
									.setIdSucursal(json.optInt("id_sucursal"))
									.setIdMarca(json.optInt("id_marca"))
									.setIdCaja(json.optInt("id_caja"))
									.setDescripcion(json.optString("descripcion"));
							
							sucursales.add(sucursal);
						}
					
						listener.onSucursalesResponseReceived(sucursales);
					}
				}
			}
			
			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	

}
