package org.addcel.fishers.ws;

import java.util.ArrayList;
import java.util.List;

import org.addcel.fishers.constant.Url;
import org.addcel.fishers.listener.OnCuentaResponseListener;
import org.addcel.fishers.to.ConsumoTO;
import org.addcel.fishers.to.CuentaTO;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.WebServiceClient;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class ConsultaCuentaWSClient extends WebServiceClient {
	
	private int idSucursal;
	private int idCaja;
	private String numeroCuenta;
	
	private static final String TAG = "ConsultaCuentaWSClient";

	public ConsultaCuentaWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return Url.FISHERS_CUENTA;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.FISHERS_WS_CAJAS;
	}
	
	public ConsultaCuentaWSClient setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
		
		return this;
	}
	
	public ConsultaCuentaWSClient setIdCaja(int idCaja) {
		this.idCaja = idCaja;	
		
		return this;
	}
	
	public ConsultaCuentaWSClient setNumeroCuenta(String numeroCuenta) {
		if (!TextUtils.isEmpty(numeroCuenta)) {
			this.numeroCuenta = numeroCuenta;
		}
		
		return this;
	}
	
	private String buildRequest() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("id_sucursal", idSucursal);
			json.put("id_caja", idCaja);
			json.put("numero_cuenta", numeroCuenta);
			
			Log.d(TAG, "Request: " + json.toString());
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		return Crypto.aesEncrypt(Sys.getKey(), json.toString());
	}
	
	public WebServiceClient execute(final OnCuentaResponseListener listener) {
		addPostParameter("json", buildRequest());
		
		return execute(new OnResponseJSONReceivedListener() {
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				CuentaTO cuenta = new CuentaTO();
				
				List<ConsumoTO> comida = new ArrayList<ConsumoTO>();
				List<ConsumoTO> bebida = new ArrayList<ConsumoTO>();
				List<ConsumoTO> otro = new ArrayList<ConsumoTO>();
				
				cuenta.setJson(jsonResponse.toString());
				
				cuenta.setDescuento(jsonResponse.optDouble("descuento"));
				cuenta.setEmail(jsonResponse.optString("email"));
				cuenta.setFecha(jsonResponse.optString("fecha"));
				cuenta.setIdBitacora(jsonResponse.optString("id_bitacora"));
				cuenta.setIdCaja(jsonResponse.optInt("id_caja"));
				cuenta.setIdPago(jsonResponse.optString("id_pago"));
				cuenta.setIdSucursal(jsonResponse.optInt("id_sucursal"));
				cuenta.setIdUsuario(jsonResponse.optString("id_usuario"));
				cuenta.setIva(jsonResponse.optDouble("iva"));
				cuenta.setNumeroCuenta(jsonResponse.optString("numero_cuenta"));
				cuenta.setStatus(jsonResponse.optInt("status"));
				cuenta.setSubtotal(jsonResponse.optDouble("subtotal"));
				cuenta.setTotal(jsonResponse.optDouble("total"));
				cuenta.setTotalPago(jsonResponse.optDouble("total_pago"));
				
				JSONArray arrComida = jsonResponse.optJSONArray("comida");
				JSONArray arrBebida = jsonResponse.optJSONArray("bebida");
				JSONArray arrOtros = jsonResponse.optJSONArray("otros");
				
				for (int i = 0; i < arrComida.length(); i++) {
					ConsumoTO consumo = new ConsumoTO();
					consumo.setCantidad(arrComida.optJSONObject(i).optInt("cantidad"));
					consumo.setDescripcionProducto(arrComida.optJSONObject(i).optString("descripcion_producto"));
					consumo.setIdPago(arrComida.optJSONObject(i).optString("id_pago"));
					consumo.setIdProducto(arrComida.optJSONObject(i).optLong("id_producto"));
					consumo.setImporte(arrComida.optJSONObject(i).optDouble("importe"));
					consumo.setPrecioUnitario(arrComida.optJSONObject(i).optString("precio_unitario"));
					comida.add(consumo);
				}
				
				for (int i = 0; i < arrBebida.length(); i++) {
					ConsumoTO consumo = new ConsumoTO();
					consumo.setCantidad(arrBebida.optJSONObject(i).optInt("cantidad"));
					consumo.setDescripcionProducto(arrBebida.optJSONObject(i).optString("descripcion_producto"));
					consumo.setIdPago(arrBebida.optJSONObject(i).optString("id_pago"));
					consumo.setIdProducto(arrBebida.optJSONObject(i).optLong("id_producto"));
					consumo.setImporte(arrBebida.optJSONObject(i).optDouble("importe"));
					consumo.setPrecioUnitario(arrBebida.optJSONObject(i).optString("precio_unitario"));
					bebida.add(consumo);
				}
				
				for (int i = 0; i < arrOtros.length(); i++) {
					ConsumoTO consumo = new ConsumoTO();
					consumo.setCantidad(arrOtros.optJSONObject(i).optInt("cantidad"));
					consumo.setDescripcionProducto(arrOtros.optJSONObject(i).optString("descripcion_producto"));
					consumo.setIdPago(arrOtros.optJSONObject(i).optString("id_pago"));
					consumo.setIdProducto(arrOtros.optJSONObject(i).optLong("id_producto"));
					consumo.setImporte(arrOtros.optJSONObject(i).optDouble("importe"));
					consumo.setPrecioUnitario(arrOtros.optJSONObject(i).optString("precio_unitario"));
					otro.add(consumo);
				}
				
				Log.d(TAG, "Comida: " + comida.toString());
				cuenta.setComida(comida);
				

				Log.d(TAG, "Bebida: " + bebida.toString());
				cuenta.setBebida(bebida);
				

				Log.d(TAG, "Otro: " + otro.toString());
				cuenta.setOtro(otro);
				
				listener.onCuentaResponseReceived(cuenta);
				
				
				
			}
			
			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	

}
