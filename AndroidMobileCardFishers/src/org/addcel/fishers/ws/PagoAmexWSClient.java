package org.addcel.fishers.ws;

import org.addcel.fishers.constant.Url;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.crypto.AddcelCrypto;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.WebServiceClient;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseReceivedListener;

public class PagoAmexWSClient extends WebServiceClient {

	private String tarjeta, nombre, email, cvv, direccion, cp, token;
	private int vigenciaMes, vigenciaAnio;
	private JSONObject json;
	
	private static final String TAG = "PagoAmexWSClient";

	public PagoAmexWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return Url.FISHERS_AMEX;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.FISHERS_WS_AMEX;
	}
	
	public PagoAmexWSClient setTarjeta(String tarjeta) {
		if (!tarjeta.isEmpty())
			this.tarjeta = tarjeta;
		
		return this;
	}
	
	public PagoAmexWSClient setNombre(String nombre) {
		if (!nombre.isEmpty())
			this.nombre = nombre;
		
		return this;
	}
	
	public PagoAmexWSClient setEmail(String email) {
		if (!email.isEmpty())
			this.email = email;
		
		return this;
	}
	
	public PagoAmexWSClient setCvv(String cvv) {
		if (!cvv.isEmpty())
			this.cvv = cvv;
		
		return this;
	}
	
	public PagoAmexWSClient setVigenciaMes(int vigenciaMes) {
		if (0 < vigenciaMes)
			this.vigenciaMes = vigenciaMes;
		
		return this;
	}
	
	public PagoAmexWSClient setVigenciaAnio(int vigenciaAnio) {
		if (0 < vigenciaAnio)
			this.vigenciaAnio = vigenciaAnio;
		
		return this;
	}
	
	public PagoAmexWSClient setToken(String token) {
		if (!token.isEmpty())
			this.token = token;
		
		return this;
	}
	
	public PagoAmexWSClient setDireccion(String direccion) {
		if (!direccion.isEmpty())
			this.direccion = direccion;
		
		return this;
	}
	
	public PagoAmexWSClient setCP(String cp) {
		if (!cp.isEmpty())
			this.cp = cp;
		
		return this;
	}
	
	public PagoAmexWSClient setJSON(JSONObject json) {
		if (null != json)
			this.json = json;
		
		return this;
	}
	
	private String buildRequest() {
		
		try {
			json.put("nombre_completo", nombre);
			json.put("cvv2", cvv);
			json.put("email", email);
			json.put("tarjeta", tarjeta);
			json.put("token", token);
			json.put("vigencia", getFechaVencimientoFormateada());
			json.put("direccion", direccion);
			json.put("cp", cp);
			
			json.put("wkey", Sys.getIMEI(ctx));
			json.put("software", Sys.getSWVersion());
			json.put("tipo", Sys.getTipo());
			json.put("modelo", Sys.getModel());
			json.put("imei", Sys.getIMEI(ctx));
		} catch (JSONException e) {
			return "";
		}
	
	return AddcelCrypto.encryptSensitive(AddcelCrypto.getKey(), json.toString());

	}
	
	public WebServiceClient executePurchase(final OnResponseJSONReceivedListener listener) {
		
		addPostParameter("json", buildRequest());
		
		return execute2(new OnResponseReceivedListener() {
			
			public void onResponseReceived(String response) {
				// TODO Auto-generated method stub
				if (null != response && !response.isEmpty()) {
					response = AddcelCrypto.decryptSensitive(response);
					
					try {
						JSONObject json = new JSONObject(response);
						listener.onResponseJSONReceived(json);
					} catch (JSONException e) {
						Log.e(TAG, "", e);
						listener.onResponseJSONReceived(null);
					}
					
				} else {
					listener.onResponseJSONReceived(null);
				}
			}
		});
	}
	
	protected String getFechaVencimientoFormateada(){
		String f = "";
		
		try{
			Fecha fecha = new Fecha();
			fecha.setMes(vigenciaMes);
			fecha.setAnio(vigenciaAnio);
			f = fecha.toFechaFormateada(FormatoFecha.mesNumero + "/" + FormatoFecha.anioCorto);
		}catch(ErrorSys e){
			Sys.log(e);
		}
		
		return f;
	}

}
