package org.addcel.fishers.adapter;

import java.util.List;

import org.addcel.fishers.to.MarcaTO;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ironbit.mc.R;
import com.ironbit.mc.system.cache.image.ImageCacheManager;
import com.ironbit.mc.system.cache.image.OnImagenObtenidaListener;
import com.ironbit.mc.widget.FixedRadioImageView;

public class MarcaAdapter extends BaseAdapter {

	Context context;
	List<MarcaTO> data;
	LayoutInflater inflater;
	Bitmap images[];
	
	public MarcaAdapter(Context _context, List<MarcaTO> _data) {
		context = _context;
		data = _data;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		images = new Bitmap[data.size()];
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		View view = inflater.inflate(R.layout.item_fishers_marca, parent, false);
		
		MarcaTO marca = data.get(position);
		
		if (null != marca) {
			final ImageView marcaImg = (ImageView) view.findViewById(R.id.img_marca);
			
			if (null == images[position]) {
				
				final int auxIndex = position;
				
				ImageCacheManager.getImagen(context, new OnImagenObtenidaListener() {
					
					public void onImagenObtenida(Bitmap imagen) {
						// TODO Auto-generated method stub
						marcaImg.setImageBitmap(imagen);
						images[auxIndex] = imagen;
					}
				}, marca.getImgUrl());
			} else {
				marcaImg.setImageBitmap(images[position]);
			}
		}
		
		return view;
	}

}
