package org.addcel.fishers.listener;

import org.addcel.fishers.to.CuentaTO;

public interface OnCuentaResponseListener {
	public void onCuentaResponseReceived(CuentaTO cuenta);
}
