package org.addcel.fishers.listener;

import java.util.List;

import org.addcel.fishers.to.SucursalTO;

public interface OnSucursalesResponseListener {
	public void onSucursalesResponseReceived(List<SucursalTO> sucursales);
	public void onSucursalErrorReceived(int idError, String mensajeError);
}
