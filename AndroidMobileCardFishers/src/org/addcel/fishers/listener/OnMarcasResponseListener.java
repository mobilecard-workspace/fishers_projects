package org.addcel.fishers.listener;

import java.util.List;

import org.addcel.fishers.to.MarcaTO;

public interface OnMarcasResponseListener {
	
	public void onMarcasResponseReceived(List<MarcaTO> marcas);

}
