package org.addcel.fishers.listener;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;

public interface OnCajasResponseListener {
	public void onCajasResponseReceived(ArrayList<BasicNameValuePair> cajas);
	public void onCajasErrorReceived(int idError, String mensajeError);
}
