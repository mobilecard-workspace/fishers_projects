package org.addcel.fishers.constant;

public class Url {

	//URLs FISHERS
	private static final String DEV = "http://50.57.192.214:8080/FishersServicios/";
	private static final String PROD = "http://www.mobilecard.mx:8080/FishersServicios/";
	private static final String CONTEXT = DEV;
	
	public static final String FISHERS_MARCAS = CONTEXT + "getMarcas";
	public static final String FISHERS_SUCURSALES = CONTEXT + "getSucursales";
	public static final String FISHERS_CAJAS = CONTEXT + "getCajas";
	public static final String FISHERS_CUENTA = CONTEXT + "consultaCuenta";
	public static final String FISHERS_VISA = CONTEXT + "pago-visa";
	public static final String FISHERS_AMEX = CONTEXT + "pago-amex";
	public static final String FISHERS_TOKEN = CONTEXT + "getToken";
	public static final String FISHERS_BUSQUEDA = CONTEXT + "busquedaPagos";
	public static final String FISHERS_REENVIO = CONTEXT + "reenvioRec";
}
