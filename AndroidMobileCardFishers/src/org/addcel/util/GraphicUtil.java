package org.addcel.util;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;

public class GraphicUtil {

	/**
	 * Valida el build del tel�fono para seleccionar que m�todo setBackgroundUtilizar
	 * @param _l Layout al cual se le modificar&aacute el background
	 * @param _d Drawable que contiene el gr&aacute;fico a utilizar
	 */
	public static void setBackgroundDrawableWithBuildValidation(View _v, Drawable _d) {
		if (Build.VERSION.SDK_INT >= 16)

		    _v.setBackground(_d);
		else
		    _v.setBackgroundDrawable(_d);
	}
}
