package com.ironbit.mc.web.webservices;

import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseReceivedListener;

public class SetSmsWSClient extends WebServiceClient {
	protected String numero = "";

	public SetSmsWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_SET_SMS;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_SET_SMS;
	}
	
	public SetSmsWSClient setNumero(String numero) {
		if (numero != null){
			this.numero = numero;
		}
		
		return this;
	}
	
	public WebServiceClient execute(final OnGeneralWSResponseListener listener) {
				
		addPostParameter("json", Crypto.aesEncrypt(Sys.getKey(), numero));
		//System.out.println("json = "+Crypto.aesEncrypt(Sys.getKey(), numero));
		return execute(new OnResponseReceivedListener(){
			public void onResponseReceived(String jsonResponse) {
				if (listener != null){	
					try{				
						JSONObject json = new JSONObject(jsonResponse);
						
						int res = json.getInt("resultado");
						
						if(res == 1){
							listener.onGeneralWSResponseListener(json.getString("mensaje"));
						}else{
							listener.onGeneralWSErrorListener(json.getString("mensaje"));
						}
					}catch(Exception e){
						Sys.log(e);
						
						listener.onGeneralWSErrorListener(e.getMessage());
					}
				}
			}
		});
	}
}
