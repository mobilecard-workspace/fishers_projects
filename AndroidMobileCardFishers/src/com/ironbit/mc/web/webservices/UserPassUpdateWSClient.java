package com.ironbit.mc.web.webservices;


import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mc.web.webservices.events.OnUserPassUpdatedResponseReceivedListener;

public class UserPassUpdateWSClient extends WebServiceClient{
	protected String password = "";
	protected String newPassword = "";
	
	
	public UserPassUpdateWSClient(Activity ctx){
		super(ctx);
	}
	
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_USER_UPDATE_PASS;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_USER_UPDATE_PASS;
	}
	

	/**
	 * @param newPassword
	 * Password nuevo (sin encriptar)
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	
	/**
	 * @param password
	 * Password antiguo (sin encriptar)
	 */
	public void setActualPassword(String password) {
		this.password = password;
	}


	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", Usuario.getLogin(ctx));
			//jsonObj.put("password", Crypto.sha1(password));
			jsonObj.put("password", password);
			//jsonObj.put("newPassword", Crypto.sha1(newPassword));
			jsonObj.put("newPassword", newPassword);
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnUserPassUpdatedResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(password), postData);
		String newEnc = Text.mergeStr(jEnc, password);
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(password));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){		
					try{
						listener.onUserPassUpdatedResponseReceived(
							jsonResponse.getString("resultado"), 
							jsonResponse.getString("mensaje")
						);
					}catch(Exception e){
						Sys.log(e);
						listener.onUserPassUpdatedResponseReceived("", "");
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}