package com.ironbit.mc.web.webservices;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.data.DataOperation;
import com.ironbit.mc.web.webservices.events.OnOperationsResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetPurchasesWSClient extends WebServiceClient{
	protected Periodo periodo = Periodo.hoy;

	public enum Periodo{
		hoy(1),
		semana(2),
		mes(3),
		mesAnterior(4);
		
		private int id = 0;
		
		
		Periodo(int id){
			this.id = id;
		}
		
		
		public int getId(){
			return id;
		}
	}
	
	
	public GetPurchasesWSClient(Activity ctx){
		super(ctx);
	}
	
	
	public void setPeriodo(Periodo periodo) {
		if (periodo != null){
			this.periodo = periodo;
		}
	}

	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_PURCHASES_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_PURCHASES;
	}
	
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", Usuario.getLogin(ctx));
			jsonObj.put("password", Usuario.getPass(ctx));
			jsonObj.put("periodo", periodo.getId());
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnOperationsResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(Usuario.getPass(ctx)), postData);
		String newEnc = Text.mergeStr(jEnc, Usuario.getPass(ctx));
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(Usuario.getPass(ctx)));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
					if (listener != null){
						ArrayList<DataOperation> operations = new ArrayList<DataOperation>();
						
						try{
							JSONArray jsonOperations = jsonResponse.getJSONArray("compras");
							JSONObject jsonOperation = null;
							
							for (int a = 0; a < jsonOperations.length(); a++){
								jsonOperation = jsonOperations.getJSONObject(a);
								
								operations.add(new DataOperation(
									jsonOperation.getString("fecha"), 
									jsonOperation.getString("concepto"),
									jsonOperation.getString("cargo"),
									jsonOperation.getString("no_autorizacion"),
									Integer.parseInt(jsonOperation.getString("codigo_error")),
									Integer.parseInt(jsonOperation.getString("status")),
									Integer.parseInt(jsonOperation.getString("proveedor")),
									Integer.parseInt(jsonOperation.getString("producto"))
								));
							}
						}catch(Exception e){
							Sys.log(e);
						}
						
						listener.onOperationsResponseReceived(operations);
					}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}