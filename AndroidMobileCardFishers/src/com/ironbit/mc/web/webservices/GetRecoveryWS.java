package com.ironbit.mc.web.webservices;


import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseReceivedListener;

public class GetRecoveryWS extends WebServiceClient{
	protected String user = "";
	
	
	public GetRecoveryWS(Activity ctx){
		super(ctx);
	}
	
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_RECOVERY_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_RECOVERY;
	}
	

	public GetRecoveryWS setUser(String user) {
		if (user != null){
			this.user = user;
		}
		
		return this;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("cadena", user);
			
		}catch(Exception e){
			Sys.log(e);
		}
		/*"{\"login\":\""+MainClass.userBean.getLogin()
		+"\",\"password\":"+"\""+Utils.sha1(basicPassword.getText().trim())+
		"\",\"cvv2\":\""+basicCvv2.getText().trim()+
		"\",\"vigencia\":\""+stringMonth+vigency+
		"\",\"producto\":\""+this.generalBean.getClave()+
		"\",\"tarjeta\":\""+basicClave.getText().trim()+
		"\",\"pin\":\""+basicPin.getText().trim()+"\"}";	*/	
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnGeneralWSResponseListener listener) {
		
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		
		addPostParameter("json", Crypto.aesEncrypt(Sys.getKey(), postData));
		
		return execute(new OnResponseReceivedListener(){
			public void onResponseReceived(String jsonResponse) {
				if (listener != null){	
					try{						
						listener.onGeneralWSResponseListener(jsonResponse);
					}catch(Exception e){
						Sys.log(e);
						
						listener.onGeneralWSErrorListener(jsonResponse);
					}
				}
			}
		});
	}
}