package com.ironbit.mc.web.webservices;

import java.net.URLDecoder;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnIdInterjetResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetTransactionIdInterjetWSClient extends WebServiceClient {
	
	protected String user;
	protected String pnr;
	protected String monto;

	public GetTransactionIdInterjetWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetTransactionIdInterjetWSClient setUser(String user){
		if(user != null){
			this.user = user;
		}
		
		return this;
	}
	
	public GetTransactionIdInterjetWSClient setPnr(String pnr){
		if(pnr != null){
			this.pnr = pnr;
		}
		
		return this;
	}
	
	public GetTransactionIdInterjetWSClient setMonto(String monto){
		if(monto != null){
			this.monto = monto;
		}
		
		return this;
	}
	
	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_INTERJET_GET_ID;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_TRANSACCION_INTERJET;
	}
	
	public WebServiceClient execute(final OnIdInterjetResponseListener listener){
		
		addGetParameter("user", this.user);
		addGetParameter("pnr", this.pnr);
		addGetParameter("monto", URLDecoder.decode(this.monto));
		
		return executeClean(new OnResponseJSONReceivedListener() {
			public void onResponseJSONReceived(JSONObject jsonResponse) {

				if(listener != null){					
					String id = jsonResponse.optString("id");
					listener.onIdInterjetResponseReceived(id);
				}			
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}

}
