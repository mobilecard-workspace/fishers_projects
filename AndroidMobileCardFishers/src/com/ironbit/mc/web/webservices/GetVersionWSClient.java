package com.ironbit.mc.web.webservices;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetVersionWSClient extends WebServiceClient {

	public GetVersionWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public WebServiceClient execute(final OnResponseJSONReceivedListener listener) {
		
		addGetParameter("idApp", "2");
		addGetParameter("idPlataforma", "4");
		
		
		return executeClean(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (null != listener) {
					listener.onResponseJSONReceived(jsonResponse);
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return "http://www.mobilecard.mx:8080/Vitamedica/getVersion";
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return  Hilo.DESCARGA_WS_VERSION;
	}

}
