package com.ironbit.mc.web.webservices;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.web.webservices.events.OnMontoInterjetResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetMontoInterjetWSClient extends WebServiceClient {
	
	protected String referencia;
	
	public GetMontoInterjetWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetMontoInterjetWSClient setReferencia(String referencia){
		if(referencia != null){
			this.referencia = referencia;
		}
		
		return this;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_INTERJET_GET_MONTO;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_DATOS_INTERJET;
	}
	
	public WebServiceClient execute(final OnMontoInterjetResponseListener listener){
		
		addGetParameter("pnr", this.referencia);
		
		return executeClean(new OnResponseJSONReceivedListener() {
			public void onResponseJSONReceived(JSONObject jsonResponse) {

				if(listener != null){
					
					String status = jsonResponse.optString("status", "");
					String monto = jsonResponse.optString("monto", "");
					String pnr = jsonResponse.optString("pnr", "");
					
					listener.onMontoInterjetResponseReceived(status, monto, pnr);
				}			
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {};
		});
	}
	
	

}
