package com.ironbit.mc.web.webservices;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class TenenciaWSClient extends WebServiceClient {
	protected String placa;
	public TenenciaWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public TenenciaWSClient setPlaca(String placa) {
		if (placa != null){
			this.placa = placa;
		}
		
		return this;
	}
	
	
	public String getPlaca(){
		return this.placa;
	}
	
	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_LINEA_EDOMEX;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_EDOMEX_LINEA_GET;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("placa", placa);
		}catch(Exception e){
			Sys.log(e);
		}
		return jsonObj;
	}
	
	public WebServiceClient execute(final OnGeneralWSResponseListener listener){
		String postData = getJsonParam().toString();
		
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(Sys.getKey()), postData);
		//String newEnc = Text.mergeStr(jEnc, password);
		addPostParameter("json", jEnc);
		//Sys.setPTelefono(Text.parsePass(password));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){	
					try{
						String folio, nombre;
						try{
							folio = jsonResponse.getString("folio");
							nombre = jsonResponse.getString("folio");
						}catch(Exception ee){
							Sys.log(ee);
							folio = "";
						}
						
						listener.onGeneralWSResponseListener(folio);
					}catch(Exception e){
						Sys.log(e);
						
						String message = "hubo problemas al tratar de hacer la compra. Intenta m�s tarde";
						if (jsonResponse != null){
							String msj = jsonResponse.optString("mensaje");
							if (msj != null && msj.trim().length() > 0){
								message = msj;
							}
						}
						listener.onGeneralWSErrorListener(message);
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}

}
