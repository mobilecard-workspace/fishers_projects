package com.ironbit.mc.web.webservices;


import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.web.webservices.events.OnCardTypesResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetCardTypesWSClient extends WebServiceClient{
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_CARD_TYPE_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_CARD_TYPES;
	}
	
	
	public GetCardTypesWSClient(Activity ctx){
		super(ctx);
	}
	
	
	public WebServiceClient execute(final OnCardTypesResponseReceivedListener listener) {
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
					if (listener != null){
						ArrayList<BasicNameValuePair> cardTypes = new ArrayList<BasicNameValuePair>();
						
						try{
							JSONArray jsonTarjetas = jsonResponse.getJSONArray("tarjetas");
							JSONObject jsonTarjeta = null;
							
							for (int a = 0; a < jsonTarjetas.length(); a++){
								jsonTarjeta = jsonTarjetas.getJSONObject(a);
								
								cardTypes.add(new BasicNameValuePair(
									jsonTarjeta.getString("descripcion"), 
									jsonTarjeta.getString("clave")
								));
							}
						}catch(Exception e){
							Sys.log(e);
						}
						
						listener.onCardTypesResponseReceived(cardTypes);
					}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}