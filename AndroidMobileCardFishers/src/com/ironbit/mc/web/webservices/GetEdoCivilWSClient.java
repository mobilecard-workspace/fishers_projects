package com.ironbit.mc.web.webservices;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.web.webservices.events.OnBanksResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetEdoCivilWSClient extends WebServiceClient {

	public GetEdoCivilWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_EDOCIVIL_GET;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_EDO_CIVIL_GET;
	}

	public WebServiceClient execute(final OnBanksResponseReceivedListener listener) {
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
					if (listener != null){
						ArrayList<BasicNameValuePair> estados = new ArrayList<BasicNameValuePair>();
						
						try{
							JSONArray jsonEsts = jsonResponse.getJSONArray("estados");
							JSONObject jsonBank = null;
							
							for (int a = 0; a < jsonEsts.length(); a++){
								jsonBank = jsonEsts.getJSONObject(a);
								
								estados.add(new BasicNameValuePair(
									jsonBank.getString("descripcion"), 
									jsonBank.getString("clave")
								));
							}
						}catch(Exception e){
							Sys.log(e);
						}
						
						listener.onBanksResponseReceived(estados);
					}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
