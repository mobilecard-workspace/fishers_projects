package com.ironbit.mc.web.webservices;

import java.io.ObjectOutputStream.PutField;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class InvitaWSClient extends WebServiceClient{
	
	private String login;
	private String nombre;
	private String telefono;

	public InvitaWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public InvitaWSClient setLogin(String login) {
		if (null != login) {
			this.login = login;
		}
		
		return this;
		
	}
	
	public InvitaWSClient setNombre(String nombre) {
		if (null != nombre) {
			this.nombre = nombre;
		}
		
		return this;
	}
	
	public InvitaWSClient setTelefono(String telefono) {
		if (null != telefono) {
			this.telefono = telefono;
		}
		
		return this;
	}
	
	public String buildRequest() {
		JSONObject request = new JSONObject();
		
		try {
			request.put("nombreAmigo", login);
			request.put("nombreInvitado", nombre);
			request.put("telefonoInvitado", telefono);
		} catch (JSONException e) {
			request = null;
			return "";
		}
		
		Log.d("InvitaWSClient", "Request: " + request.toString());
		
		return Crypto.aesEncrypt(Sys.getKey(), request.toString());
	}
	
	public WebServiceClient executeWS(final OnResponseJSONReceivedListener listener) {
		
		addPostParameter("json", buildRequest());
		Sys.setPTelefono("NULO");
		
		return execute(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (null != listener) {
					listener.onResponseJSONReceived(jsonResponse);
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_INVITA;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_INVITA;
	}

}
