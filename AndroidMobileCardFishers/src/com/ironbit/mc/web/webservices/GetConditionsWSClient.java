package com.ironbit.mc.web.webservices;


import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.web.webservices.data.DataCondition;
import com.ironbit.mc.web.webservices.events.OnConditionsResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetConditionsWSClient extends WebServiceClient{
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_CONDITIONS_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_CONDITIONS;
	}
	
	
	public GetConditionsWSClient(Activity ctx){
		super(ctx);
	}
	
	
	public WebServiceClient execute(final OnConditionsResponseReceivedListener listener) {
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){		
					try{
						listener.onConditionsResponseReceived(new DataCondition(
							jsonResponse.getString("clave"), 
							jsonResponse.getString("Descripcion")
						));
					}catch(Exception e){
						Sys.log(e);
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}