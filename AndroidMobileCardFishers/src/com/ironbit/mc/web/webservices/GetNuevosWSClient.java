package com.ironbit.mc.web.webservices;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.data.NuevoTO;
import com.ironbit.mc.web.webservices.events.OnNuevoResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONAReceivedListener;

public class GetNuevosWSClient extends WebServiceClient {
	
	private int idAplicacion;
	private int modulo;
	private int plataforma;
	
	private static final String TAG = "GetNuevosWSClient";

	public GetNuevosWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
		this.idAplicacion = 1;
		this.modulo = 1;
		this.plataforma = 1;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_NUEVO;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_NUEVO;
	}
	
	private String buildRequest() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("idAplicacion", idAplicacion);
			json.put("modulo", modulo);
			json.put("plataforma", plataforma);
		} catch (JSONException e) {
			return "";
		}
		
		String request = Crypto.aesEncrypt(Sys.getKey(), json.toString());
		
		Log.i(TAG, "Request Nuevo: " + request);
		
		return request;
		
		
	}
	
	public WebServiceClient execute(final OnNuevoResponseReceivedListener listener) {
		
		addPostParameter("json", buildRequest());
		Log.d(TAG, "Request nuevo: " + getWebServiceUrl() + "?json=" + buildRequest() );
		
		return execute(new OnResponseJSONAReceivedListener() {
			
			public void onResponseJSONReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				if (listener != null) {
					List<NuevoTO> nuevos = new ArrayList<NuevoTO>();
					
					JSONObject json = null;
					
					try {
						for (int i = 0; i < jsonResponse.length(); i++) {
							json = jsonResponse.getJSONObject(i);
							
							nuevos.add(new NuevoTO(json.getString("urlImagen"), json.getString("urlTienda")));
						}
					} catch (JSONException e) {
						Log.e(TAG, "", e);
					}
					
					listener.onNuevosResponseReceived(nuevos);
				}
			}
		});
		
	}
	
	

}
