package com.ironbit.mc.web.webservices;


import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mc.web.webservices.events.OnUserUpdatedResponseReceivedListener;

public class UserUpdateWSClient extends WebServiceClient{
	protected Fecha fechaNacimiento = null;
	protected String celular = "";
	protected String nombre = "";
	protected String apellidos = "";
	protected String tarjeta = "";
	protected String mail = "";
	
	protected String materno = "";
	protected String sexo = "";
	protected String tel_casa = "";
	protected String tel_oficina = "";
	
	protected String ciudad = "";
	protected String calle = "";
	protected int numExt = 0;
	protected String numInt = "";
	protected String colonia = "";
	protected int cp = 0;
	
	protected String dom_amex = "";
	protected String direccion = "";
	protected int fechaVencimientoMes = 0;
	protected int fechaVencimientoAnio = 0;
	protected int banco = 0;
	protected int tipoTarjeta = 0;
	protected int proveedor = 0;
	protected int status = 1;
	protected int estado = 0;
	
	public UserUpdateWSClient(Activity ctx){
		super(ctx);
	}

	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_USER_UPDATE;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_USER_UPDATE;
	}
	
	
	public UserUpdateWSClient setFechaNacimiento(Fecha fechaNacimiento) {
		if (fechaNacimiento != null){
			this.fechaNacimiento = fechaNacimiento;
		}else{
			try{
				this.fechaNacimiento = new Fecha();
			}catch (ErrorSys e) {
				Sys.log(e);
			}
		}
		
		return this;
	}


	public UserUpdateWSClient setCelular(String celular) {
		if (celular != null){
			this.celular = celular;
		}
		
		return this;
	}


	public UserUpdateWSClient setNombre(String nombre) {
		if (nombre != null){
			this.nombre = nombre;
		}
		
		return this;
	}
	
	
	public UserUpdateWSClient setApellidos(String apellidos) {
		if (apellidos != null){
			this.apellidos = apellidos;
		}
		
		return this;
	}

	public UserUpdateWSClient setMaterno(String materno) {
		if (materno != null){
			this.materno = materno;
		}
		
		return this;
	}

	public UserUpdateWSClient setSexo(String sexo) {
		if (sexo != null){
			this.sexo = sexo;
		}
		
		return this;
	}
	
	public UserUpdateWSClient setTelCasa(String tel_casa) {
		if (tel_casa != null){
			this.tel_casa = tel_casa;
		}
		
		return this;
	}
	
	public UserUpdateWSClient setTelOficina(String tel_oficina) {
		if (tel_oficina != null){
			this.tel_oficina = tel_oficina;
		}
		
		return this;
	}

	public UserUpdateWSClient setCiudad(String ciudad) {
		if (ciudad != null){
			this.ciudad = ciudad;
		}
		
		return this;
	}
	
	public UserUpdateWSClient setCalle(String calle) {
		if (calle != null){
			this.calle = calle;
		}
		
		return this;
	}
	
	public UserUpdateWSClient setNumExt(int numExt) {
		
		this.numExt = numExt;
		
		return this;
	}
	
	public UserUpdateWSClient setNumInt(String numInt) {
		if (numInt != null){
			this.numInt = numInt;
		}
		
		return this;
	}
	
	public UserUpdateWSClient setColonia(String colonia) {
		if (colonia != null){
			this.colonia = colonia;
		}
		
		return this;
	}
	
	public UserUpdateWSClient setCP(int cp) {
		
		this.cp = cp;
		
		return this;
	}
	
	public UserUpdateWSClient setDomAmex(String dom_amex) {
		if (dom_amex != null){
			this.dom_amex = dom_amex;
		}
		
		return this;
	}
	public UserUpdateWSClient setDireccion(String direccion) {
		if (direccion != null){
			this.direccion = direccion;
		}
		
		return this;
	}

	public UserUpdateWSClient setEMail(String mail) {
		if (mail != null){
			this.mail = mail;
		}
		
		return this;
	}
	
	public UserUpdateWSClient setTarjeta(String tarjeta) {
		if (tarjeta != null){
			this.tarjeta = tarjeta;
		}
		
		return this;
	}


	public UserUpdateWSClient setFechaVencimientoMes(int fechaVencimientoMes) {
		this.fechaVencimientoMes = fechaVencimientoMes;
		return this;
	}

	
	public UserUpdateWSClient setFechaVencimientoAnio(int fechaVencimientoAnio) {
		this.fechaVencimientoAnio = fechaVencimientoAnio;
		return this;
	}
	
	
	protected String getFechaVencimientoFormateada(){
		String f = "";
		
		try{
			Fecha fecha = new Fecha();
			fecha.setMes(fechaVencimientoMes);
			fecha.setAnio(fechaVencimientoAnio);
			f = fecha.toFechaFormateada(FormatoFecha.mesNumero + "/" + FormatoFecha.anioCorto);
		}catch(ErrorSys e){
			Sys.log(e);
		}
		
		return f;
	}

	
	public UserUpdateWSClient setBanco(int banco) {
		this.banco = banco;
		return this;
	}


	public UserUpdateWSClient setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
		return this;
	}


	public UserUpdateWSClient setProveedor(int proveedor) {
		this.proveedor = proveedor;
		return this;
	}
	
	public UserUpdateWSClient setEstado(int estado) {
		this.estado = estado;
		return this;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", Usuario.getLogin(ctx));
			//jsonObj.put("password", Crypto.sha1(Usuario.getPass(ctx)));
			jsonObj.put("password", Usuario.getPass(ctx));
			jsonObj.put("nacimiento", fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_YYYYdashMMdashDD));
			jsonObj.put("telefono", celular);
			jsonObj.put("registro", new Fecha().toFechaFormateada(FormatoFecha.fechaEsp_YYYYdashMMdashDD));
			jsonObj.put("nombre", nombre);
			jsonObj.put("apellido", apellidos);
			jsonObj.put("materno", materno);
			jsonObj.put("sexo", sexo);
			jsonObj.put("tel_casa", tel_casa);
			jsonObj.put("tel_oficina", tel_oficina);
			jsonObj.put("ciudad", ciudad);
			jsonObj.put("calle", calle);
			jsonObj.put("num_ext", numExt);
			jsonObj.put("num_interior", numInt);
			jsonObj.put("colonia", colonia);
			jsonObj.put("cp", cp);
			jsonObj.put("dom_amex", dom_amex);
			jsonObj.put("id_estado", estado);
			jsonObj.put("direccion", direccion);
			jsonObj.put("mail", mail);
			jsonObj.put("tarjeta", tarjeta);
			jsonObj.put("vigencia", getFechaVencimientoFormateada());
			jsonObj.put("banco", banco);
			jsonObj.put("tipotarjeta", tipoTarjeta);
			jsonObj.put("proveedor", proveedor);
			jsonObj.put("status", status);
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnUserUpdatedResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		//addPostParameter("json", Crypto.aesEncrypt(Sys.getKey(), postData));
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(Usuario.getPass(ctx)), postData);
		String newEnc = Text.mergeStr(jEnc, Usuario.getPass(ctx));
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(Usuario.getPass(ctx)));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){		
					try{
						listener.onUserUpdatedResponseReceived(
							jsonResponse.getString("resultado"), 
							jsonResponse.getString("mensaje")
						);
					}catch(Exception e){
						Sys.log(e);
						listener.onUserUpdatedResponseReceived("", "");
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}