package com.ironbit.mc.web.webservices;

import java.net.URLDecoder;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.web.webservices.events.OnIdInterjetResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseReceivedListener;

public class GetTransactionEdomexWSClient extends WebServiceClient {

	private String user;
	private String monto;
	private String linea;
	private String placa;
	
	public GetTransactionEdomexWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetTransactionEdomexWSClient setUser(String user) {
		if(user != null){
			this.user = user;
		}
		
		return this;
	}
	
	
	public GetTransactionEdomexWSClient setLinea(String linea) {
		if(linea != null){
			this.linea = linea;
		}
		
		return this;
	}
	
	public GetTransactionEdomexWSClient setPlaca(String placa) {
		if(placa != null){
			this.placa = placa;
		}
		
		return this;
	}
	
	public GetTransactionEdomexWSClient setMonto(String monto){
		if(monto != null){
			this.monto = monto;
		}
		
		return this;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_EDOMEX_SET_ID;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_PAGO_EDOMEX;
	}
	
	public WebServiceClient execute(final OnIdInterjetResponseListener listener) {
		addGetParameter("user", this.user);
		addGetParameter("ref", this.linea + "~" + this.placa );
		addGetParameter("monto", URLDecoder.decode(this.monto));
		addGetParameter("mod", "edomex");
		
		System.out.println(this.getParameters);
		
		return execute2(new OnResponseReceivedListener() {
			
			public void onResponseReceived(String response) {
				// TODO Auto-generated method stub
				listener.onIdInterjetResponseReceived(response);
			}
		});
	}

}
