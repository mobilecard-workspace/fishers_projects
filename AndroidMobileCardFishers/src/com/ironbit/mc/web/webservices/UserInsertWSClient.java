package com.ironbit.mc.web.webservices;


import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mc.web.webservices.events.OnUserRegisteredResponseReceivedListener;

public class UserInsertWSClient extends WebServiceClient{
	protected String login = "";
	protected String password = "";
	protected Fecha fechaNacimiento = null;
	protected String celular = "";
	protected String nombre = "";
	protected String apellido = "";
	protected String materno = "";
	protected String sexo = "";
	protected String tel_casa = "";
	protected String tel_oficina = "";
	protected String direccion = "";
	
	protected String ciudad = "";
	protected String calle = "";
	protected int numExt = 0;
	protected String numInt = "";
	protected String colonia = "";
	protected int cp = 0;
	
	protected String dom_amex = "";
	
	protected String tarjeta = "";
	protected String mail;
	protected int fechaVencimientoMes = 0;
	protected int fechaVencimientoAnio = 0;
	protected int banco = 0;
	protected int tipoTarjeta = 0;
	protected int proveedor = 0;
	protected int estado = 0;
	protected int status = 1;
	private int idtiporecargatag;
    private String etiqueta;
    private String numeroTag;
    public int getIdtiporecargatag() {
		return idtiporecargatag;
	}


	public void setIdtiporecargatag(int idtiporecargatag) {
		this.idtiporecargatag = idtiporecargatag;
	}


	public String getEtiqueta() {
		return etiqueta;
	}


	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}


	public String getNumeroTag() {
		return numeroTag;
	}


	public void setNumeroTag(String numeroTag) {
		this.numeroTag = numeroTag;
	}


	public int getDv() {
		return dv;
	}


	public void setDv(int dv) {
		this.dv = dv;
	}


	private int dv;

	
	
	public UserInsertWSClient(Activity ctx){
		super(ctx);
	}
	
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_USER_INSERT;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_USER_INSERT;
	}
	
	
	public UserInsertWSClient setLogin(String login){
		if (login != null){
			this.login = login;
		}
		
		return this;
	}
	
	
	/**
	 * @param password Contraseña (Sin encriptar)
	 * @return UserRegisterWSClient
	 */
	public UserInsertWSClient setPassword(String password){
		if (password != null){
			this.password = password;
		}
		
		return this;
	}
	
	public UserInsertWSClient setEmail(String mail){
		if (mail != null){
			this.mail = mail;
		}
		
		return this;
	}
	
	public UserInsertWSClient setFechaNacimiento(Fecha fechaNacimiento) {
		if (fechaNacimiento != null){
			this.fechaNacimiento = fechaNacimiento;
		}else{
			try{
				this.fechaNacimiento = new Fecha();
			}catch (ErrorSys e) {
				Sys.log(e);
			}
		}
		
		return this;
	}


	public UserInsertWSClient setCelular(String celular) {
		if (celular != null){
			this.celular = celular;
		}
		
		return this;
	}

	
	public UserInsertWSClient setApellido(String apellido) {
		if (apellido != null){
			this.apellido = apellido;
		}
		
		return this;
	}
	
	
	public UserInsertWSClient setNombre(String nombre) {
		if (nombre != null){
			this.nombre = nombre;
		}
		
		return this;
	}

	public UserInsertWSClient setMaterno(String materno) {
		if (materno != null){
			this.materno = materno;
		}
		
		return this;
	}

	public UserInsertWSClient setSexo(String sexo) {
		if (sexo != null){
			this.sexo = sexo;
		}
		
		return this;
	}
	
	public UserInsertWSClient setTelCasa(String tel_casa) {
		if (tel_casa != null){
			this.tel_casa = tel_casa;
		}
		
		return this;
	}
	
	public UserInsertWSClient setTelOficina(String tel_oficina) {
		if (tel_oficina != null){
			this.tel_oficina = tel_oficina;
		}
		
		return this;
	}

	public UserInsertWSClient setCiudad(String ciudad) {
		if (ciudad != null){
			this.ciudad = ciudad;
		}
		
		return this;
	}
	
	public UserInsertWSClient setCalle(String calle) {
		if (calle != null){
			this.calle = calle;
		}
		
		return this;
	}
	
	public UserInsertWSClient setNumExt(int numExt) {
		
		this.numExt = numExt;
		
		return this;
	}
	
	public UserInsertWSClient setNumInt(String numInt) {
		if (numInt != null){
			this.numInt = numInt;
		}
		
		return this;
	}
	
	public UserInsertWSClient setColonia(String colonia) {
		if (colonia != null){
			this.colonia = colonia;
		}
		
		return this;
	}
	
	public UserInsertWSClient setCP(int cp) {
		
		this.cp = cp;
		
		return this;
	}
	
	public UserInsertWSClient setDomAmex(String dom_amex) {
		if (dom_amex != null){
			this.dom_amex = dom_amex;
		}
		
		return this;
	}
	
	public UserInsertWSClient setDireccion(String direccion) {
		if (direccion != null){
			this.direccion = direccion;
		}
		
		return this;
	}


	public UserInsertWSClient setTarjeta(String tarjeta) {
		if (tarjeta != null){
			this.tarjeta = tarjeta;
		}
		
		return this;
	}


	public UserInsertWSClient setFechaVencimientoMes(int fechaVencimientoMes) {
		this.fechaVencimientoMes = fechaVencimientoMes;
		return this;
	}

	
	public UserInsertWSClient setFechaVencimientoAnio(int fechaVencimientoAnio) {
		this.fechaVencimientoAnio = fechaVencimientoAnio;
		return this;
	}
	
	
	protected String getFechaVencimientoFormateada(){
		String f = "";
		
		try{
			Fecha fecha = new Fecha();
			fecha.setMes(fechaVencimientoMes);
			fecha.setAnio(fechaVencimientoAnio);
			f = fecha.toFechaFormateada(FormatoFecha.mesNumero + "/" + FormatoFecha.anioCorto);
		}catch(ErrorSys e){
			Sys.log(e);
		}
		
		return f;
	}

	
	public UserInsertWSClient setBanco(int banco) {
		this.banco = banco;
		return this;
	}


	public UserInsertWSClient setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
		return this;
	}

	public UserInsertWSClient setProveedor(int proveedor) {
		this.proveedor = proveedor;
		return this;
	}
	
	public UserInsertWSClient setEstado(int estado) {
		this.estado = estado;
		return this;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", login);
			//jsonObj.put("password", Crypto.sha1(password));
			//jsonObj.put("password", Crypto.aesEncrypt(Sys.getKey(), password));
			//jsonObj.put("password", password);
			jsonObj.put("nacimiento", fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_YYYYdashMMdashDD));
			jsonObj.put("telefono", celular);
			jsonObj.put("registro", new Fecha().toFechaFormateada(FormatoFecha.fechaEsp_YYYYdashMMdashDD));
			jsonObj.put("nombre", nombre);
			jsonObj.put("apellido", apellido);
			jsonObj.put("materno", materno);
			jsonObj.put("sexo", sexo);
			jsonObj.put("tel_casa", tel_casa);
			jsonObj.put("tel_oficina", tel_oficina);
			jsonObj.put("ciudad", ciudad);
			jsonObj.put("calle", calle);
			jsonObj.put("num_ext", numExt);
			jsonObj.put("num_interior", numInt);
			jsonObj.put("colonia", colonia);
			jsonObj.put("cp", cp);
			jsonObj.put("dom_amex", dom_amex);
			jsonObj.put("id_estado", estado);
			
			jsonObj.put("direccion", direccion);
			jsonObj.put("mail", mail);
			jsonObj.put("tarjeta", tarjeta);
			jsonObj.put("vigencia", getFechaVencimientoFormateada());
			jsonObj.put("banco", banco);
			jsonObj.put("tipotarjeta", tipoTarjeta);
			jsonObj.put("proveedor", proveedor);
			jsonObj.put("status", status);
			jsonObj.put("imei", Sys.getIMEI(ctx));
			
			jsonObj.put("idtiporecargatag", idtiporecargatag);
			jsonObj.put("etiqueta", etiqueta);
			jsonObj.put("numero", numeroTag);
			jsonObj.put("dv", dv);
			
			jsonObj.put("tipo", Sys.getTipo());
			jsonObj.put("software", Sys.getSWVersion());
			jsonObj.put("modelo", Sys.getModel());
			jsonObj.put("key", Sys.getIMEI(ctx));
			
		}catch(Exception e){
			Sys.log(e);
			e.printStackTrace();
		}
		
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnUserRegisteredResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(password), postData);
		System.out.println("json=" + postData);
		String newEnc = Text.mergeStr(jEnc, password);
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(password));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){		
					try{
						listener.onUserRegisteredResponseReceived(
							jsonResponse.getString("resultado"), 
							jsonResponse.getString("mensaje")
						);
					}catch(Exception e){
						Sys.log(e);
						listener.onUserRegisteredResponseReceived("-1", e.getMessage());
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}