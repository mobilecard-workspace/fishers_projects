package com.ironbit.mc.web.webservices;


import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.web.webservices.events.OnBanksResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetBanksWSClient extends WebServiceClient{
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_BANKS_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_BANKS;
	}
	
	
	public GetBanksWSClient(Activity ctx){
		super(ctx);
	}
	
	public WebServiceClient execute(final OnBanksResponseReceivedListener listener) {
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
					if (listener != null){
						ArrayList<BasicNameValuePair> banks = new ArrayList<BasicNameValuePair>();
						
						try{
							JSONArray jsonBanks = jsonResponse.getJSONArray("bancos");
							JSONObject jsonBank = null;
							
							for (int a = 0; a < jsonBanks.length(); a++){
								jsonBank = jsonBanks.getJSONObject(a);
								
								banks.add(new BasicNameValuePair(
									jsonBank.getString("descripcion"), 
									jsonBank.getString("clave")
								));
							}
						}catch(Exception e){
							Sys.log(e);
						}
						
						listener.onBanksResponseReceived(banks);
					}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}