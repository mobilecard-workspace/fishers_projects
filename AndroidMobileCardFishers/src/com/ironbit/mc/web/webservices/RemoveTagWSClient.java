package com.ironbit.mc.web.webservices;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class RemoveTagWSClient extends WebServiceClient {
	private long usuario;
    private int tipotag;
    private String etiqueta;
    private String numero;
    private int dv;
    
	public RemoveTagWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_REMOVE_TAG;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_REMOVE_TAG;
	}

	public RemoveTagWSClient setUsuario(long usuario){
		if (usuario != 0){
			this.usuario = usuario;
		}
		
		return this;
	}
	
	public RemoveTagWSClient setTipotag(int tipotag){
		if (tipotag != 0){
			this.tipotag = tipotag;
		}
		
		return this;
	}
	
	public RemoveTagWSClient setEtiqueta(String etiqueta){
		if (etiqueta != null){
			this.etiqueta = etiqueta;
		}
		
		return this;
	}
	
	public RemoveTagWSClient setNumero(String numero){
		if (numero != null){
			this.numero = numero;
		}
		
		return this;
	}
	
	public RemoveTagWSClient setDV(int dv){
		if (dv != 0){
			this.dv = dv;
		}
		
		return this;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("usuario", Long.toString(usuario));
			jsonObj.put("tipotag", Integer.toString(tipotag));
			jsonObj.put("etiqueta", etiqueta);
			jsonObj.put("numero", numero);
			jsonObj.put("dv", Integer.toString(dv));
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	public WebServiceClient execute(final OnGeneralWSResponseListener listener) {
		String postData = getJsonParam().toString();
		
		addPostParameter("json", Crypto.aesEncrypt(Sys.getKey(), postData));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){		
					try{
						int res = jsonResponse.getInt("resultado");
						if(res == 0){
							listener.onGeneralWSResponseListener(jsonResponse.getString("mensaje"));
						}else{
							listener.onGeneralWSErrorListener(jsonResponse.getString("mensaje"));
						}
						
					}catch(Exception e){
						Sys.log(e);
						
						listener.onGeneralWSErrorListener("Vuelva a intentarlo m�s tarde.");
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
