package com.ironbit.mc.web.webservices.data;

public class DataItinerario {
	private String oficina;
	private String fechaSalida;
	private String horaSalida;
	private String errorNumero;
	private String errorCadena;
	
	public DataItinerario(String oficina, String fechaSalida,
			String horaSalida, String errorNumero, String errorCadena) {
		this.oficina = oficina;
		this.fechaSalida = fechaSalida;
		this.horaSalida = horaSalida;
		this.errorNumero = errorNumero;
		this.errorCadena = errorCadena;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public String getFechaSalida() {
		return fechaSalida;
	}

	public void setFechaSalida(String fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public String getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(String horaSalida) {
		this.horaSalida = horaSalida;
	}

	public String getErrorNumero() {
		return errorNumero;
	}

	public void setErrorNumero(String errorNumero) {
		this.errorNumero = errorNumero;
	}

	public String getErrorCadena() {
		return errorCadena;
	}

	public void setErrorCadena(String errorCadena) {
		this.errorCadena = errorCadena;
	}

	@Override
	public String toString() {
		return "Terminal: " + oficina + ", Fecha de salida: "
				+ fechaSalida + ", Hora: " + horaSalida + "\n";
	}
	
	
	
	
}
