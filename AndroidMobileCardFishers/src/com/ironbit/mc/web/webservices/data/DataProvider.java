package com.ironbit.mc.web.webservices.data;

public class DataProvider {
	protected String description = "";
	protected int clave = 0;
	protected String claveWS = "";
	protected String path = "";
	protected int compatible = 1;
	protected int tipoTarjeta = 0;
	
	public int getTipoTarjeta() {
		return tipoTarjeta;
	}


	public void setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}


	public String getPath(){
		return path;
	}
	
	
	public void setPath(String path){
		this.path = path;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public int getClave() {
		return clave;
	}
	
	
	public void setClave(int clave) {
		this.clave = clave;
	}
	
	
	public String getClaveWS() {
		return claveWS;
	}
	
	
	public void setClaveWS(String claveWS) {
		this.claveWS = claveWS;
	}
	
	
	public DataProvider(int clave, String description, String claveWS, String path, int comp, int tipo){
		setClave(clave);
		setDescription(description);
		setClaveWS(claveWS);
		setPath(path);
		setCompatible(comp);
		setTipoTarjeta(tipo);
	}


	public int getCompatible() {
		return compatible;
	}


	public void setCompatible(int compatible) {
		this.compatible = compatible;
	}
}