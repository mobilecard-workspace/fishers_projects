package com.ironbit.mc.web.webservices.data;

public class NuevoTO {
	private int idAplicacion;
	private int modulo;
	private int plataforma;
	private String urlImagen;
	private String urlTienda;
	
	public NuevoTO(){};
	
	public NuevoTO(String _urlImagen, String _urlTienda) {
		urlImagen = _urlImagen;
		urlTienda = _urlTienda;
	}

	public int getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public int getModulo() {
		return modulo;
	}

	public void setModulo(int modulo) {
		this.modulo = modulo;
	}

	public int getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(int plataforma) {
		this.plataforma = plataforma;
	}

	public String getUrlImagen() {
		return urlImagen;
	}

	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}

	public String getUrlTienda() {
		return urlTienda;
	}

	public void setUrlTienda(String urlTienda) {
		this.urlTienda = urlTienda;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return urlTienda;
	}
	
	
}
