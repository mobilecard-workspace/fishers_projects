package com.ironbit.mc.web.webservices.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

public class DataOcupacion {

	private ArrayList<String> asientos;
	private String codigoError;
	private String descripcionError;
	
	public ArrayList<String> getAsientos() {
		return asientos;
	}
	
	public void setAsientos(JSONArray asientos) throws JSONException {
		this.asientos = new ArrayList<String>();
		
		for (int i = 0; i < asientos.length(); i++) {
			this.asientos.add(asientos.getString(i));
		}
	}
	
	public String getCodigoError() {
		return codigoError;
	}
	
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	
	public String getDescripcionError() {
		return descripcionError;
	}
	
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}
	
	@Override
	public String toString() {
		return "DataOcupacion [asientos=" + asientos + ", codigoError="
				+ codigoError + ", descripcionError=" + descripcionError + "]";
	}
	
	
	
	
}
