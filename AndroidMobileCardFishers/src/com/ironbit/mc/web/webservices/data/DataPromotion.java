package com.ironbit.mc.web.webservices.data;

public class DataPromotion {
	protected String path = "";
	protected String clave = "";
	protected String description = "";
	
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getPath() {
		return path;
	}
	
	
	public void setPath(String path) {
		this.path = path;
	}
	
	
	public String getClave() {
		return clave;
	}
	
	
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	
	public DataPromotion(String clave, String descripcion, String path){
		setPath(path);
		setDescription(descripcion);
		setClave(clave);
	}
}