package com.ironbit.mc.web.webservices.data;

import android.os.Parcel;
import android.os.Parcelable;

public class DataOperation implements Parcelable {
	
	protected String fecha;
	protected String concepto;
	protected String monto;
	protected String numAutorizacion;
	protected int codeError = 0;
	protected int status = 0;
	protected int proveedor = 0;
	protected int producto = 0;
	
	public DataOperation(Parcel in) {
		readFromParcel(in);
	}
	
	
	public DataOperation(String fecha, String concepto, String monto, String numAutorizacion, 
			int codeError, int status, int proveedor, int producto){
		setFecha(fecha);
		setConcepto(concepto);
		setMonto(monto);
		setNumAutorizacion(numAutorizacion);
		setCodeError(codeError);
		setStatus(status);
		setProveedor(proveedor);
		setProducto(producto);
	}
	
	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getConcepto() {
		return concepto;
	}


	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}


	public String getMonto() {
		return monto;
	}


	public void setMonto(String monto) {
		this.monto = monto;
	}


	public String getNumAutorizacion() {
		return numAutorizacion;
	}


	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}


	public int getCodeError() {
		return codeError;
	}


	public void setCodeError(int codeError) {
		this.codeError = codeError;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public int getProveedor() {
		return proveedor;
	}


	public void setProveedor(int proveedor) {
		this.proveedor = proveedor;
	}


	public int getProducto() {
		return producto;
	}


	public void setProducto(int producto) {
		this.producto = producto;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getConcepto() + " " + getFecha();
	}


	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(fecha);
		dest.writeString(concepto);
		dest.writeString(monto);
		dest.writeString(numAutorizacion);
		dest.writeInt(codeError);
		dest.writeInt(status);
		dest.writeInt(proveedor);
		dest.writeInt(producto);
	}
	
	public void readFromParcel(Parcel in) {
		fecha = in.readString();
		concepto = in.readString();
		monto = in.readString();
		numAutorizacion = in.readString();
		codeError = in.readInt();
		status = in.readInt();
		proveedor = in.readInt();
		producto = in.readInt();
	}
	
	public static final Parcelable.Creator<DataOperation> CREATOR = new Parcelable.Creator<DataOperation>() {
	
		public DataOperation createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new DataOperation(source);
		}
		
		public DataOperation[] newArray(int size) {
			// TODO Auto-generated method stub
			return new DataOperation[size];
		}
	};
}