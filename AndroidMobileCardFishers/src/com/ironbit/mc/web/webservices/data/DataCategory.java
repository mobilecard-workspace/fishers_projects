package com.ironbit.mc.web.webservices.data;

public class DataCategory {
	protected String path = "";
	protected String clave = "";
	protected String description = "";
	
	
	public String getPath() {
		return path;
	}
	
	
	public void setPath(String path) {
		this.path = path;
	}
	
	
	public String getClave() {
		return clave;
	}
	
	
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public DataCategory(String clave, String description, String path){
		setPath(path);
		setClave(clave);
		setDescription(description);
	}
}