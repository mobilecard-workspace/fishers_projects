package com.ironbit.mc.web.webservices.data;

import org.json.JSONArray;

public class DataDiagrama {
	private JSONArray diagramaAutobus;
	private String numFilas;
	private int numError;
	private String descError;
	private int numAsientos;	

	public JSONArray getDiagramaAutobus() {
		return diagramaAutobus;
	}

	public void setDiagramaAutobus(JSONArray diagramaAutobus) {
		this.diagramaAutobus = diagramaAutobus;
	}

	public String getNumFilas() {
		return numFilas;
	}

	public void setNumFilas(String numFilas) {
		this.numFilas = numFilas;
	}

	public int getNumError() {
		return numError;
	}

	public void setNumError(int numError) {
		this.numError = numError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	public int getNumAsientos() {
		return numAsientos;
	}

	public void setNumAsientos(int numAsientos) {
		this.numAsientos = numAsientos;
	}
	
	public int getNumFilasInt(){
		return Integer.valueOf(getNumFilas());
	}
	
	public int getNumColumnas(){
		return (getNumAsientos() + (getNumFilasInt() - 1))/ (getNumFilasInt() - 1);
	}

	@Override
	public String toString() {
		return "DataDiagrama [diagramaAutobus=" + diagramaAutobus
				+ ", numFilas=" + numFilas + ", numError=" + numError
				+ ", descError=" + descError + ", numAsientos=" + numAsientos
				+ "]";
	}
	
	
}
