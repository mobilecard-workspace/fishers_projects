package com.ironbit.mc.web.webservices.data;

public class DataCondition {
	protected String description = "";
	protected String clave = "";
	
	
	public String getDescription() {
		return description;
	}
	
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}


	public DataCondition(String clave, String description){
		setClave(clave);
		setDescription(description);
	}
}