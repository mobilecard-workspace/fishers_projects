package com.ironbit.mc.web.webservices.data;

public class DataCorrida {


	protected double tarifa;
	protected int dispEstudiante;
	protected int dispInsen;
	protected int dispMaestro;
	protected int dispGeneral;
	protected double tarifaAdulto;
	protected double tarifaEstudiante;
	protected double tarifaMaestro;
	protected double tarifaNino;
	protected double tarifaInsen;
	protected int cupoAutobus;
	protected int errorNumero;
	protected String errorCadena;
	protected int dispNinos;
	protected int dispPromo;
	protected double tarifaPromo;
	protected int dispPaisano;
	protected double tarifaProgPaisano;
	protected double porcentajeTramoFrontera;
	protected String fechaDesfaza;
	protected String cveDestino;
	protected String moneda;
	protected String tipoLectura;
	protected String corrida;
	protected String oficinaFrontera;
	protected String secuencia;
	protected String anden;
	protected String tipoItinerario;
	protected String tipoCorrida;
	protected String servicio;
	protected String cveCtl;
	protected String ubicacion;
	protected String empresaCorrida;
	protected String sala;
	protected String destino;
	protected String fechaLlegada;
	protected String fechaInicial;
	protected String horaCorrida;
	

	public DataCorrida(double tarifa, int dispEstudiante, int dispInsen,
			int dispMaestro, int dispGeneral, double tarifaAdulto,
			double tarifaEstudiante, double tarifaMaestro, double tarifaNino,
			double tarifaInsen, int cupoAutobus, int errorNumero, 
			String errorCadena, int dispNinos, int dispPromo,
			double tarifaPromo, int dispPaisano, double tarifaProgPaisano,
			double porcentajeTramoFrontera, String fechaDesfaza,
			String cveDestino, String moneda, String tipoLectura,
			String corrida, String oficinaFrontera, String secuencia,
			String anden, String tipoItinerario, String tipoCorrida,
			String servicio, String cveCtl, String ubicacion,
			String empresaCorrida, String sala, String destino,
			String fechaLlegada, String fechaInicial, String horaCorrida) {
		this.tarifa = tarifa;
		this.dispEstudiante = dispEstudiante;
		this.dispInsen = dispInsen;
		this.dispMaestro = dispMaestro;
		this.dispGeneral = dispGeneral;
		this.tarifaAdulto = tarifaAdulto;
		this.tarifaEstudiante = tarifaEstudiante;
		this.tarifaMaestro = tarifaMaestro;
		this.tarifaNino = tarifaNino;
		this.tarifaInsen = tarifaInsen;
		this.cupoAutobus = cupoAutobus;
		this.errorNumero = errorNumero;
		this.errorCadena = errorCadena;
		this.dispNinos = dispNinos;
		this.dispPromo = dispPromo;
		this.tarifaPromo = tarifaPromo;
		this.dispPaisano = dispPaisano;
		this.tarifaProgPaisano = tarifaProgPaisano;
		this.porcentajeTramoFrontera = porcentajeTramoFrontera;
		this.fechaDesfaza = fechaDesfaza;
		this.cveDestino = cveDestino;
		this.moneda = moneda;
		this.tipoLectura = tipoLectura;
		this.corrida = corrida;
		this.oficinaFrontera = oficinaFrontera;
		this.secuencia = secuencia;
		this.anden = anden;
		this.tipoItinerario = tipoItinerario;
		this.tipoCorrida = tipoCorrida;
		this.servicio = servicio;
		this.cveCtl = cveCtl;
		this.ubicacion = ubicacion;
		this.empresaCorrida = empresaCorrida;
		this.sala = sala;
		this.destino = destino;
		this.fechaLlegada = fechaLlegada;
		this.fechaInicial = fechaInicial;
		this.horaCorrida = horaCorrida;
		
	}
	
	public double getTarifa() {
		return tarifa;
	}
	public void setTarifa(double tarifa) {
		this.tarifa = tarifa;
	}
	public int getDispEstudiante() {
		return dispEstudiante;
	}
	public void setDispEstudiante(int dispEstudiante) {
		this.dispEstudiante = dispEstudiante;
	}
	public int getDispInsen() {
		return dispInsen;
	}
	public void setDispInsen(int dispInsen) {
		this.dispInsen = dispInsen;
	}
	public int getDispMaestro() {
		return dispMaestro;
	}
	public void setDispMaestro(int dispMaestro) {
		this.dispMaestro = dispMaestro;
	}
	public int getDispGeneral() {
		return dispGeneral;
	}
	public void setDispGeneral(int dispGeneral) {
		this.dispGeneral = dispGeneral;
	}
	public double getTarifaAdulto() {
		return tarifaAdulto;
	}
	public void setTarifaAdulto(double tarifaAdulto) {
		this.tarifaAdulto = tarifaAdulto;
	}
	public double getTarifaEstudiante() {
		return tarifaEstudiante;
	}
	public void setTarifaEstudiante(double tarifaEstudiante) {
		this.tarifaEstudiante = tarifaEstudiante;
	}
	public double getTarifaMaestro() {
		return tarifaMaestro;
	}
	public void setTarifaMaestro(double tarifaMaestro) {
		this.tarifaMaestro = tarifaMaestro;
	}
	public double getTarifaNino() {
		return tarifaNino;
	}

	public void setTarifaNino(double tarifaNino) {
		this.tarifaNino = tarifaNino;
	}

	public double getTarifaInsen() {
		return tarifaInsen;
	}

	public void setTarifaInsen(double tarifaInsen) {
		this.tarifaInsen = tarifaInsen;
	}

	public int getCupoAutobus() {
		return cupoAutobus;
	}
	public void setCupoAutobus(int cupoAutobus) {
		this.cupoAutobus = cupoAutobus;
	}
	public int getErrorNumero() {
		return errorNumero;
	}
	public void setErrorNumero(int errorNumero) {
		this.errorNumero = errorNumero;
	}
	public String getErrorCadena() {
		return errorCadena;
	}
	public void setErrorCadena(String errorCadena) {
		this.errorCadena = errorCadena;
	}
	public int getDispNinos() {
		return dispNinos;
	}
	public void setDispNinos(int dispNinos) {
		this.dispNinos = dispNinos;
	}
	public int getDispPromo() {
		return dispPromo;
	}
	public void setDispPromo(int dispPromo) {
		this.dispPromo = dispPromo;
	}
	public double getTarifaPromo() {
		return tarifaPromo;
	}
	public void setTarifaPromo(double tarifaPromo) {
		this.tarifaPromo = tarifaPromo;
	}
	public int getDispPaisano() {
		return dispPaisano;
	}
	public void setDispPaisano(int dispPaisano) {
		this.dispPaisano = dispPaisano;
	}
	public double getTarifaProgPaisano() {
		return tarifaProgPaisano;
	}
	public void setTarifaProgPaisano(double tarifaProgPaisano) {
		this.tarifaProgPaisano = tarifaProgPaisano;
	}
	public double getPorcentajeTramoFrontera() {
		return porcentajeTramoFrontera;
	}
	public void setPorcentajeTramoFrontera(double porcentajeTramoFrontera) {
		this.porcentajeTramoFrontera = porcentajeTramoFrontera;
	}

	public String getFechaDesfaza() {
		return fechaDesfaza;
	}

	public void setFechaDesfaza(String fechaDesfaza) {
		this.fechaDesfaza = fechaDesfaza;
	}

	public String getCveDestino() {
		return cveDestino;
	}

	public void setCveDestino(String cveDestino) {
		this.cveDestino = cveDestino;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getTipoLectura() {
		return tipoLectura;
	}

	public void setTipoLectura(String tipoLectura) {
		this.tipoLectura = tipoLectura;
	}

	public String getCorrida() {
		return corrida;
	}

	public void setCorrida(String corrida) {
		this.corrida = corrida;
	}

	public String getOficinaFrontera() {
		return oficinaFrontera;
	}

	public void setOficinaFrontera(String oficinaFrontera) {
		this.oficinaFrontera = oficinaFrontera;
	}

	public String getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}

	public String getAnden() {
		return anden;
	}

	public void setAnden(String anden) {
		this.anden = anden;
	}

	public String getTipoItinerario() {
		return tipoItinerario;
	}

	public void setTipoItinerario(String tipoItinerario) {
		this.tipoItinerario = tipoItinerario;
	}

	public String getTipoCorrida() {
		return tipoCorrida;
	}

	public void setTipoCorrida(String tipoCorrida) {
		this.tipoCorrida = tipoCorrida;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getCveCtl() {
		return cveCtl;
	}

	public void setCveCtl(String cveCtl) {
		this.cveCtl = cveCtl;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getEmpresaCorrida() {
		return empresaCorrida;
	}

	public void setEmpresaCorrida(String empresaCorrida) {
		this.empresaCorrida = empresaCorrida;
	}

	public String getSala() {
		return sala;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getFechaLlegada() {
		return fechaLlegada;
	}

	public void setFechaLlegada(String fechaLlegada) {
		this.fechaLlegada = fechaLlegada;
	}

	public String getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	
	public String getHoraCorrida() {
		return horaCorrida;
	}
	
	public void setHoraCorrida(String horaCorrida) {
		this.horaCorrida = horaCorrida;
	}

	@Override
	public String toString() {
		return "DataCorrida [tarifa=" + tarifa + ", dispEstudiante="
				+ dispEstudiante + ", dispInsen=" + dispInsen
				+ ", dispMaestro=" + dispMaestro + ", dispGeneral="
				+ dispGeneral + ", tarifaAdulto=" + tarifaAdulto
				+ ", tarifaEstudiante=" + tarifaEstudiante + ", tarifaMaestro="
				+ tarifaMaestro + ", tarifaNino=" + tarifaNino
				+ ", tarifaInsen=" + tarifaInsen + ", cupoAutobus="
				+ cupoAutobus + ", errorNumero=" + errorNumero
				+ ", errorCadena=" + errorCadena + ", dispNinos=" + dispNinos
				+ ", dispPromo=" + dispPromo + ", tarifaPromo=" + tarifaPromo
				+ ", dispPaisano=" + dispPaisano + ", tarifaProgPaisano="
				+ tarifaProgPaisano + ", porcentajeTramoFrontera="
				+ porcentajeTramoFrontera + ", fechaDesfaza=" + fechaDesfaza
				+ ", cveDestino=" + cveDestino + ", moneda=" + moneda
				+ ", tipoLectura=" + tipoLectura + ", corrida=" + corrida
				+ ", oficinaFrontera=" + oficinaFrontera + ", secuencia="
				+ secuencia + ", anden=" + anden + ", tipoItinerario="
				+ tipoItinerario + ", tipoCorrida=" + tipoCorrida
				+ ", servicio=" + servicio + ", cveCtl=" + cveCtl
				+ ", ubicacion=" + ubicacion + ", empresaCorrida="
				+ empresaCorrida + ", sala=" + sala + ", destino=" + destino
				+ ", fechaLlegada=" + fechaLlegada + ", fechaInicial="
				+ fechaInicial + ", horaCorrida=" + horaCorrida + "]";
	}

	
	
}
