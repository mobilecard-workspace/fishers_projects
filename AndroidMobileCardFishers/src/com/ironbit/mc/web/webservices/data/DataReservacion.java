package com.ironbit.mc.web.webservices.data;

public class DataReservacion {
	private String idReservacion;
	private double importeTotal;
	private int numError;
	private String descError;

	public String getIdReservacion() {
		return idReservacion;
	}

	public void setIdReservacion(String idReservacion) {
		this.idReservacion = idReservacion;
	}

	public double getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(double importeTotal) {
		this.importeTotal = importeTotal;
	}

	public int getNumError() {
		return numError;
	}

	public void setNumError(int numError) {
		this.numError = numError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	@Override
	public String toString() {
		return "DataReservacion [idReservacion=" + idReservacion
				+ ", importeTotal=" + importeTotal + ", numError=" + numError
				+ ", descError=" + descError + "]";
	}
	
	
	
	
	
}
