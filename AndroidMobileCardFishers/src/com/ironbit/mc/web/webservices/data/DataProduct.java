package com.ironbit.mc.web.webservices.data;

public class DataProduct {
	protected String description = "";
	protected String claveWS = "";
	protected String nombre = "";
	protected String clave = "";

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getClaveWS() {
		return claveWS;
	}

	public void setClaveWS(String claveWS) {
		this.claveWS = claveWS;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public DataProduct(String description, String claveWS, String nombre, String clave) {
		setDescription(description);
		setClaveWS(claveWS);
		setNombre(nombre);
		setClave(clave);
	}
}