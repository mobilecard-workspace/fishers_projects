package com.ironbit.mc.web.webservices.data;

public class DataTag {
	private long usuario;
    private int tipotag;
    private String etiqueta;
    private String numero;
    private int dv;
    
	public DataTag(long usuario, int tipotag, String etiqueta, String numero, int dv) {
		super();
		this.usuario = usuario;
		this.tipotag = tipotag;
		this.etiqueta = etiqueta;
		this.numero = numero;
		this.dv = dv;
	}

	public long getUsuario() {
		return usuario;
	}

	public void setUsuario(long usuario) {
		this.usuario = usuario;
	}

	public int getTipotag() {
		return tipotag;
	}

	public void setTipotag(int tipotag) {
		this.tipotag = tipotag;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public int getDv() {
		return dv;
	}

	public void setDv(int dv) {
		this.dv = dv;
	}

	public String toString(){
		return etiqueta;
	}
}
