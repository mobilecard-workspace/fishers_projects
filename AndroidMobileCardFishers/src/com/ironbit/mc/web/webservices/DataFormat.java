package com.ironbit.mc.web.webservices;

/**
 * @author angel
 * @description Formatos utilizados para el intercambio de informaci�n entre el WebService y el Cliente
 */
public enum DataFormat{
	json,
	xml
}