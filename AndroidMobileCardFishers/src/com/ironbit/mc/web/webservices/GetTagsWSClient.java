package com.ironbit.mc.web.webservices;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.data.DataTag;
import com.ironbit.mc.web.webservices.events.OnGetTagResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetTagsWSClient extends WebServiceClient {
	protected String idUser = null;
	protected String idTipo = null;

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GET_TAG;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_GET_TAG;
	}
	
	public String getIdUser() {
		return idUser;
	}

	public GetTagsWSClient setIdUser(String idUser) {
		if(idUser != null){
			this.idUser = idUser;
		}
		
		return this;
	}

	public String getIdTipo() {
		return idTipo;
	}

	public GetTagsWSClient setIdTipo(String idTipo) {
		if(idTipo != null){
			this.idTipo = idTipo;
		}
		
		return this;
	}

	public GetTagsWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("cadena", idUser+"|"+idTipo);
			
		}catch(Exception e){
			Sys.log(e);
		}

		return jsonObj;
	}
	
	public WebServiceClient execute(final OnGetTagResponseListener listener){
		String postData = getJsonParam().toString();
		Log.d("GeTagsWSClient", postData);
		addPostParameter("json", Crypto.aesEncrypt(Sys.getKey(), postData));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){
					ArrayList<DataTag> tags = new ArrayList<DataTag>();
					try{
						JSONArray array = jsonResponse.getJSONArray("usuarioTag");
						/*
	{"usuarioTag":[{"usuario":1323106298622,
					"tipotag":1,
					"etiqueta":"CHARLIE5",
					"numero":"12345678901234",
					"dv":3
				   }]
	}
						 */
						for(int i=0; i<array.length(); i++){
							JSONObject obj = array.getJSONObject(i);
							long usuario = obj.getLong("usuario");
							int tipotag = obj.getInt("tipotag");
							String etiqueta = obj.getString("etiqueta");
							String numero = obj.getString("numero");
							int dv = obj.getInt("dv");
						
							DataTag tag = new DataTag(usuario, tipotag, etiqueta, numero, dv);
							tags.add(tag);
						}
						
						listener.onGetTagResponseListener(tags);
						
					}catch(Exception e){
						Sys.log(e);
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
