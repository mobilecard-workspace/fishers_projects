package com.ironbit.mc.web.webservices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.events.OnPurchaseInsertedResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class VitamedicaCompraWSClient extends WebServiceClient {
	protected String password;
	protected String producto;
	protected String id_tag;
    protected String amount;
    protected String imei;
    protected String cvv2;
    protected String vigencia;
    protected String cx;
    protected String cy;
    protected int fechaVencimientoMes = 0;
	protected int fechaVencimientoAnio = 0;
	protected int tipoTarjeta = 0;
	protected String json;
	
	public VitamedicaCompraWSClient(Activity ctx) {
		super(ctx);
		cx = "0.0";
		cy = "0.0";
	}
	
	public VitamedicaCompraWSClient setCvv2(String cvv2) {
		if (cvv2 != null){
			this.cvv2 = cvv2;
		}
		
		return this;
	}
	
	
	public VitamedicaCompraWSClient setPassword(String password) {
		if (password != null){
			this.password = password;
		}
		
		return this;
	}
	
	public VitamedicaCompraWSClient setJson(String json) {
		if (json != null){
			this.json = json;
		}
		
		return this;
	}
	
	public VitamedicaCompraWSClient setAmount(String amount) {
		if (amount != null){
			this.amount = amount;
		}
		
		return this;
	}
	
	public VitamedicaCompraWSClient setFechaVencimientoMes(int fechaVencimientoMes) {
		this.fechaVencimientoMes = fechaVencimientoMes;
		return this;
	}

	
	public VitamedicaCompraWSClient setFechaVencimientoAnio(int fechaVencimientoAnio) {
		this.fechaVencimientoAnio = fechaVencimientoAnio;
		return this;
	}
	public VitamedicaCompraWSClient setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
		return this;
	}
	
	protected String getFechaVencimientoFormateada(){
		String f = "";
		
		try{
			Fecha fecha = new Fecha();
			fecha.setMes(fechaVencimientoMes);
			fecha.setAnio(fechaVencimientoAnio);
			f = fecha.toFechaFormateada(FormatoFecha.mesNumero + FormatoFecha.anioCorto);
		}catch(ErrorSys e){
			Sys.log(e);
		}
		
		return f;
	}


	/**
	 * Clave WS del producto a comprar
	 * @param producto
	 */
	public VitamedicaCompraWSClient setProducto(String producto) {
		if (producto != null){
			this.producto = producto;
		}
		
		return this;
	}
	
	public VitamedicaCompraWSClient setX(double X) {
		if (X != 0.0){
			this.cx = Double.toString(X);
		}
		
		return this;
	}
	
	public VitamedicaCompraWSClient setY(double Y) {
		if (Y != 0.0){
			this.cy = Double.toString(Y);
		}
		
		return this;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = null;
		try {
			jsonObj = new JSONObject(json);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			jsonObj =  new JSONObject();
		}
		try{
			jsonObj.put("login", Usuario.getLogin(ctx));
			jsonObj.put("password", password);
			jsonObj.put("cvv2", cvv2);
			jsonObj.put("amount", amount);
			jsonObj.put("producto", producto);
			jsonObj.put("imei", Sys.getIMEI(ctx));
			jsonObj.put("cx", cx);
			jsonObj.put("cy", cy);
			jsonObj.put("tipotarjeta", tipoTarjeta);
			jsonObj.put("tipo", Sys.getTipo());
			jsonObj.put("software", Sys.getSWVersion());
			jsonObj.put("modelo", Sys.getModel());
			jsonObj.put("key", Sys.getIMEI(ctx));
		}catch(Exception e){
			Sys.log(e);
			e.printStackTrace();
		}
		return jsonObj;
	}
	
	public WebServiceClient execute(final OnPurchaseInsertedResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		System.out.println("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(password), postData);
		String newEnc = Text.mergeStr(jEnc, password);
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(password));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){	
					try{
						String folio;
						try{
							folio = jsonResponse.getString("folio");
						}catch(Exception ee){
							Sys.log(ee);
							folio = "";
						}
						
						listener.onPurchaseInsertedResponseReceived(
							folio, 
							jsonResponse.getString("resultado"), 
							jsonResponse.getString("mensaje")
						);
					}catch(Exception e){
						Sys.log(e);
						
						String message = "hubo problemas al tratar de hacer la compra. Intenta m�s tarde";
						if (jsonResponse != null){
							String msj = jsonResponse.optString("mensaje");
							if (msj != null && msj.trim().length() > 0){
								message = msj;
							}
						}
						listener.onPurchaseInsertedResponseReceived("", "", message);
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_PURCHASE_VITA;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_PURCHASE_VITA;
	}

}
