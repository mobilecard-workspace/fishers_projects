package com.ironbit.mc.web.webservices;

import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseReceivedListener;

public class GetComisionWS extends WebServiceClient {
	protected String idProveedor;
	public GetComisionWS(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_COMOSION;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_COMISION_GET;
	}

	public GetComisionWS setProveedor(String idProveedor) {
		if (idProveedor != null){
			this.idProveedor = idProveedor;
		}
		
		return this;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("String", idProveedor);
			
		}catch(Exception e){
			Sys.log(e);
		}
		/*"{\"login\":\""+MainClass.userBean.getLogin()
		+"\",\"password\":"+"\""+Utils.sha1(basicPassword.getText().trim())+
		"\",\"cvv2\":\""+basicCvv2.getText().trim()+
		"\",\"vigencia\":\""+stringMonth+vigency+
		"\",\"producto\":\""+this.generalBean.getClave()+
		"\",\"tarjeta\":\""+basicClave.getText().trim()+
		"\",\"pin\":\""+basicPin.getText().trim()+"\"}";	*/	
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnGeneralWSResponseListener listener) {
		/*String postData = getJsonParam().toString();
		Sys.log("json=" + postData);*/
		
		addPostParameter("json", Crypto.aesEncrypt(Sys.getKey(), idProveedor));
		
		//System.out.println("json="+ Crypto.aesEncrypt(Sys.getKey(), idProveedor));
		return execute(new OnResponseReceivedListener(){
			public void onResponseReceived(String jsonResponse) {
				if (listener != null){	
					try{						
						listener.onGeneralWSResponseListener(jsonResponse);
					}catch(Exception e){
						Sys.log(e);
						
						listener.onGeneralWSErrorListener(jsonResponse);
					}
				}
			}
		});
	}
}
