package com.ironbit.mc.web.webservices;


import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mc.conexion.ConexionHttp;
import com.ironbit.mc.conexion.http.HttpConexion.Charset;
import com.ironbit.mc.conexion.http.OnRespuestaHttpRecibidaListener;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.util.Url;
import com.ironbit.mc.web.webservices.events.OnResponseJSONAReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseReceivedListener;

public abstract class WebServiceClient {
	protected DataFormat formatResponse = DataFormat.json;
	protected final Hashtable<String, String> postParameters = new Hashtable<String, String>();
	protected final Hashtable<String, String> getParameters = new Hashtable<String, String>();
	protected Activity ctx = null;
	
	
	public WebServiceClient(Activity ctx){
		this.ctx = ctx;
	}
	
	protected abstract String getWebServiceUrl();
	
	
	protected abstract ConexionHttp.Hilo getHiloId();
	
	
	public WebServiceClient addPostParameters(Hashtable<String, String> postParameters){
		this.postParameters.putAll(postParameters);
		return this;
	}
	
	
	public WebServiceClient addPostParameter(String name, JSONObject jsonValue){
		return addPostParameter(name, jsonValue.toString());
	}
	
	
	public WebServiceClient addPostParameter(String name, String value){
		postParameters.put(name, value);
		return this;
	}
	
	
	public WebServiceClient addGetParameters(Hashtable<String, String> getParameters){
		this.getParameters.putAll(getParameters);
		return this;
	}
	
	
	public WebServiceClient addGetParameter(String name, JSONObject jsonValue){
		return addGetParameter(name, jsonValue.toString());
	}
	
	
	public WebServiceClient addGetParameter(String name, String value){
		getParameters.put(name, value);
		return this;
	}
	
	
	private String getWebServiceUrlWithGetParameters(){
		String url = null;
		
		try{
			url = Url.setUrl(getWebServiceUrl()).agregarParametros(getParameters).toString();
		}catch(Exception e){
			url = getWebServiceUrl();
			Sys.log(e);
		}
		
		return url;
	}
	
	public boolean isPostRequest(){
		return (postParameters.size() > 0);
	}
	
	
	public WebServiceClient cancel(){
		ConexionHttp.cancelarConexionAsync(getHiloId());
		return this;
	}
	
	
	protected WebServiceClient execute2(final OnResponseReceivedListener listener){
		try{
			OnRespuestaHttpRecibidaListener onRespuestaHttpRecibidaListener = new OnRespuestaHttpRecibidaListener(){
				public void onRespuestaHttpRecibida(int idRespuesta, String respuesta) {
					if (listener != null){
						/**
						 * @NOTE: por default todas los HttpResponces se están desencriptando 
						 */
						Log.i("WebServiceClient", "Response execute2(): " + respuesta);
						Sys.setPTelefono("NULO");
						listener.onResponseReceived(respuesta);
					}
				}
			};
			
			if (isPostRequest()){
				ConexionHttp.getInstancia().postAsync(
					onRespuestaHttpRecibidaListener, 
					getHiloId(), 
					getWebServiceUrlWithGetParameters(), 
					postParameters,
					Charset.ISO_8859_1,
					Charset.ISO_8859_1
				);
			}else{
				ConexionHttp.getInstancia().getAsync(
					onRespuestaHttpRecibidaListener, 
					getHiloId(), 
					getWebServiceUrlWithGetParameters(),
					Charset.ISO_8859_1,
					Charset.ISO_8859_1
				);
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		return this;
	
	}
	
	protected WebServiceClient executePost(final OnResponseReceivedListener listener) {
		
		try{
			OnRespuestaHttpRecibidaListener onRespuestaHttpRecibidaListener = new OnRespuestaHttpRecibidaListener(){
				public void onRespuestaHttpRecibida(int idRespuesta, String respuesta) {
					if (listener != null){
						/**
						 * @NOTE: por default todas los HttpResponces se están desencriptando 
						 */
						String code = Sys.getTelefono();
						if(code.equals("NULO")){
							code = Sys.getKey();
						}
						Log.i("WebServiceClient", "Response sin cifrar executePost(): "+respuesta);
						//aqui lo descriptas
						respuesta = Crypto.aesDecrypt(code, respuesta);
						Log.i("WebServiceClient", "Response executePost(): " + respuesta);
						Sys.setPTelefono("NULO");
						listener.onResponseReceived(respuesta);
					}
				}
			};
			
			ConexionHttp.getInstancia().postAsync(
				onRespuestaHttpRecibidaListener, 
				getHiloId(), 
				getWebServiceUrlWithGetParameters(), 
				postParameters,
				Charset.ISO_8859_1,
				Charset.ISO_8859_1
			);
		}catch(Exception e){
			Sys.log(e);
		}
		
		return this;
	}
	
	protected WebServiceClient execute(final OnResponseReceivedListener listener){
		try{
			OnRespuestaHttpRecibidaListener onRespuestaHttpRecibidaListener = new OnRespuestaHttpRecibidaListener(){
				public void onRespuestaHttpRecibida(int idRespuesta, String respuesta) {
					if (listener != null){
						/**
						 * @NOTE: por default todas los HttpResponces se están desencriptando 
						 */
					
						String code = Sys.getTelefono();
						if(code.equals("NULO")){
							code = Sys.getKey();
						}
						Log.i("WebServiceClient", "Response sin cifrar execute(): "+respuesta);
						//aqui lo descriptas
						respuesta = Crypto.aesDecrypt(code, respuesta);
						Log.i("WebServiceClient", "Response execute(): " + respuesta);
						Sys.setPTelefono("NULO");
						listener.onResponseReceived(respuesta);
					}
				}
			};
			
			if (isPostRequest()){
				ConexionHttp.getInstancia().postAsync(
					onRespuestaHttpRecibidaListener, 
					getHiloId(), 
					getWebServiceUrlWithGetParameters(), 
					postParameters,
					Charset.ISO_8859_1,
					Charset.ISO_8859_1
				);
			}else{
				ConexionHttp.getInstancia().getAsync(
					onRespuestaHttpRecibidaListener, 
					getHiloId(), 
					getWebServiceUrlWithGetParameters(),
					Charset.ISO_8859_1,
					Charset.ISO_8859_1
				);
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		return this;
	}
	
	
	protected WebServiceClient execute(final OnResponseJSONReceivedListener listener){
		return execute(new OnResponseReceivedListener(){
			public void onResponseReceived(String response) {
				JSONObject jsonObj = null;
				
				try{
					jsonObj = new JSONObject(response);
				}catch(Exception e){
					jsonObj = new JSONObject();
					Sys.log(e);
				}
				
				if (listener != null){
					listener.onResponseJSONReceived(jsonObj);
				}
			}
		});
	}
	
	protected WebServiceClient executeBadDesign(final OnResponseJSONReceivedListener listener) {
		return execute(new OnResponseReceivedListener() {
			
			public void onResponseReceived(String response) {
				// TODO Auto-generated method stub
				
				if (listener != null) {
					
					try {
						JSONObject json = new JSONObject(response);
						listener.onResponseJSONReceived(json);
					} catch (JSONException e) {
						
						try {
						
							JSONArray json = new JSONArray(response);
							listener.onResponseJSONAReceived(json);
						} catch (JSONException e1) {
							listener.onResponseStringReceived(response);
						} catch (Exception e2) {
							Log.e("WebServiceClient", "ExecuteBadDesign: " + response);
							listener.onResponseStringReceived(response);
						}
					} catch (Exception e3) {
						listener.onResponseStringReceived(response);
					}
				}
			}
		});
	}
	
	protected WebServiceClient executeClean(final OnResponseJSONReceivedListener listener){
		return execute2(new OnResponseReceivedListener(){
			public void onResponseReceived(String response) {
				JSONObject jsonObj = null;
				
				try{
					jsonObj = new JSONObject(response);
					System.out.println(":::RESPUESTA JSON:::" + jsonObj.toString(1));
				}catch(Exception e){
					jsonObj = new JSONObject();
					Sys.log(e);
				}
				
				if (listener != null){
					listener.onResponseJSONReceived(jsonObj);
				}
			}
		});
	}
	
	protected WebServiceClient execute(final OnResponseJSONAReceivedListener listener) {
		return execute(new OnResponseReceivedListener() {
			
			public void onResponseReceived(String response) {
				// TODO Auto-generated method stub
				JSONArray jsonArray = null;
				
				try{
					jsonArray = new JSONArray(response);
				}catch(Exception e){
					jsonArray = new JSONArray();
					Sys.log(e);
				}
				
				if(listener != null){
					listener.onResponseJSONReceived(jsonArray);
				}
			}
		});
	}
	
	protected WebServiceClient executeCleanArray(final OnResponseJSONAReceivedListener listener){
		return execute2(new OnResponseReceivedListener() {
			
			public void onResponseReceived(String response) {
				// TODO Auto-generated method stub
				JSONArray jsonArray = null;
				
				try{
					jsonArray = new JSONArray(response);
				}catch(Exception e){
					jsonArray = new JSONArray();
					Sys.log(e);
				}
				
				if(listener != null){
					listener.onResponseJSONReceived(jsonArray);
				}
			}
		});
	}
	
	protected WebServiceClient executePost(final OnResponseJSONReceivedListener listener) {
		return executePost(new OnResponseReceivedListener(){
			public void onResponseReceived(String response) {
				JSONObject jsonObj = null;
				
				try{
					jsonObj = new JSONObject(response);
				}catch(Exception e){
					jsonObj = new JSONObject();
					Sys.log(e);
				}
				
				if (listener != null){
					listener.onResponseJSONReceived(jsonObj);
				}
			}
		});
	}
	
	protected WebServiceClient executePostArr(final OnResponseJSONAReceivedListener listener) {
		return executePost(new OnResponseReceivedListener() {
			
			public void onResponseReceived(String response) {
				// TODO Auto-generated method stub
				JSONArray jsonArray = null;
				
				try{
					jsonArray = new JSONArray(response);
				}catch(Exception e){
					jsonArray = new JSONArray();
					Sys.log(e);
				}
				
				if(listener != null){
					listener.onResponseJSONReceived(jsonArray);
				}
			}
		});
	}
}