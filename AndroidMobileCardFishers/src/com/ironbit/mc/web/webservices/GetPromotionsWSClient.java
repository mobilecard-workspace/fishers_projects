package com.ironbit.mc.web.webservices;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.web.webservices.data.DataPromotion;
import com.ironbit.mc.web.webservices.events.OnPromotionsResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetPromotionsWSClient extends WebServiceClient{
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_PROMOTIONS_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_PROMOTIONS;
	}
	
	
	public GetPromotionsWSClient(Activity ctx){
		super(ctx);
	}
	
	
	public WebServiceClient execute(final OnPromotionsResponseReceivedListener listener) {
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse){
					if (listener != null){
						ArrayList<DataPromotion> promotions = new ArrayList<DataPromotion>();
						
						try{
							JSONArray jsonPromotions = jsonResponse.getJSONArray("promociones");
							JSONObject jsonPromotion = null;
							
							for (int a = 0; a < jsonPromotions.length(); a++){
								jsonPromotion = jsonPromotions.getJSONObject(a);
								
								promotions.add(new DataPromotion(
									jsonPromotion.getString("clave"), 
									jsonPromotion.getString("descripcion"), 
									jsonPromotion.getString("path")
								));
							}
						}catch(Exception e){
							Sys.log(e);
						}
						
						listener.onPromotionsResponseReceived(promotions);
					}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}