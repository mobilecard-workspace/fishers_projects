package com.ironbit.mc.web.webservices;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.data.DataProvider;
import com.ironbit.mc.web.webservices.events.OnProvidersResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetProvidersWSClient extends WebServiceClient{
	protected String claveCategoria = null;
	
	
	public void setClaveCategoria(String claveCategoria) {
		this.claveCategoria = claveCategoria;
	}


	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_PROVIDERS_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_PROVIDERS;
	}
	
	
	public GetProvidersWSClient(Activity ctx){
		super(ctx);
	}
	
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("clave", claveCategoria);
			jsonObj.put("idusuario", Usuario.getIdUser(ctx));
			//jsonObj.put("plataforma", "2");
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnProvidersResponseReceivedListener listener) {
		if (claveCategoria != null){
			String postData = getJsonParam().toString();
			Sys.log("json=" + postData);
			System.out.println(postData);
			addPostParameter("json", Crypto.aesEncrypt(Sys.getKey(), postData));
		}
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
					if (listener != null){
						ArrayList<DataProvider> providers = new ArrayList<DataProvider>();
						
						try{
							JSONArray jsonProviders = jsonResponse.getJSONArray("proveedores");
							JSONObject jsonProvider = null;
							
							for (int a = 0; a < jsonProviders.length(); a++){
								jsonProvider = jsonProviders.getJSONObject(a);
								
								int clave = 0;
								try{
									clave = Integer.parseInt(jsonProvider.getString("clave"));
								}catch(Exception e){
									Sys.log(e);
								}
								
								providers.add(new DataProvider(
									clave, 
									jsonProvider.getString("descripcion"),
									jsonProvider.getString("claveWS"),
									jsonProvider.getString("path"),
									jsonProvider.getInt("compatible"),
									jsonProvider.getInt("tipotarjeta")
								));
							}
						}catch(Exception e){
							Sys.log(e);
						}
						
						listener.onProvidersResponseReceived(providers);
					}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}

			
		});
	}
}