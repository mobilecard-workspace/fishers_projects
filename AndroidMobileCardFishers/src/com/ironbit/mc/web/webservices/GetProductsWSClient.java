package com.ironbit.mc.web.webservices;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.data.DataProduct;
import com.ironbit.mc.web.webservices.events.OnProductsResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetProductsWSClient extends WebServiceClient{
	protected String claveWSProvider = "";
	

	public void setClaveWSProvider(String claveWSProvider) {
		this.claveWSProvider = claveWSProvider;
	}

	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_PRODUCTOS_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_PRODUCTS;
	}
	
	
	public GetProductsWSClient(Activity ctx){
		super(ctx);
	}
	
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("proveedor", claveWSProvider);
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnProductsResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		addPostParameter("json", Crypto.aesEncrypt(Sys.getKey(), postData));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
					if (listener != null){
						
						Log.d("GetProductsWSClient", "PRODUCTOS :" + jsonResponse.toString());
						
						ArrayList<DataProduct> products = new ArrayList<DataProduct>();
						
						try{
							JSONArray jsonProviders = jsonResponse.getJSONArray("productos");
							JSONObject jsonProvider = null;
							
							for (int a = 0; a < jsonProviders.length(); a++){
								jsonProvider = jsonProviders.getJSONObject(a);
								String nom = "";
								if(jsonProvider.has("nombre")){
									nom = jsonProvider.getString("nombre");
								}
								products.add(new DataProduct(
									jsonProvider.getString("monto"),
									jsonProvider.getString("claveWS"),
									nom,
									jsonProvider.getString("clave")
								));
							}
						}catch(Exception e){
							Sys.log(e);
						}
						
						listener.onProductsResponseReceived(products);
					}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}