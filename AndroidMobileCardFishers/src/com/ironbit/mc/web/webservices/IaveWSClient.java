package com.ironbit.mc.web.webservices;


import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.events.OnPurchaseInsertedResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class IaveWSClient extends WebServiceClient{
	protected String cvv2 = "";
	protected String producto = "";
	protected String verificador = "";
	protected String tag = "";
	protected String password = "";
	protected int fechaVencimientoMes = 0;
	protected int fechaVencimientoAnio = 0;
	protected double X = 0.0;
	protected double Y = 0.0;
	
	public IaveWSClient(Activity ctx){
		super(ctx);
	}
	
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_IAVE_INSERT;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_IAVE;
	}
	

	public IaveWSClient setCvv2(String cvv2) {
		if (cvv2 != null){
			this.cvv2 = cvv2;
		}
		
		return this;
	}
	
	
	public IaveWSClient setPassword(String password) {
		if (password != null){
			this.password = password;
		}
		
		return this;
	}
	
	
	public IaveWSClient setFechaVencimientoMes(int fechaVencimientoMes) {
		this.fechaVencimientoMes = fechaVencimientoMes;
		return this;
	}

	
	public IaveWSClient setFechaVencimientoAnio(int fechaVencimientoAnio) {
		this.fechaVencimientoAnio = fechaVencimientoAnio;
		return this;
	}
	
	
	protected String getFechaVencimientoFormateada(){
		String f = "";
		
		try{
			Fecha fecha = new Fecha();
			fecha.setMes(fechaVencimientoMes);
			fecha.setAnio(fechaVencimientoAnio);
			f = fecha.toFechaFormateada(FormatoFecha.mesNumero + FormatoFecha.anioCorto);
		}catch(ErrorSys e){
			Sys.log(e);
		}
		
		return f;
	}


	/**
	 * Clave WS del producto a comprar
	 * @param producto
	 */
	public IaveWSClient setProducto(String producto) {
		if (producto != null){
			this.producto = producto;
		}
		
		return this;
	}
	
	
	public IaveWSClient setTag(String tag){
		if (tag != null){
			this.tag = tag;
		}
		
		return this;
	}
	
	public IaveWSClient setVerificador(String verificador){
		if (verificador != null){
			this.verificador = verificador;
		}
		
		return this;
	}
	
	public IaveWSClient setX(double X) {
		if (X != 0.0){
			this.X = X;
		}
		
		return this;
	}
	
	public IaveWSClient setY(double Y) {
		if (Y != 0.0){
			this.Y = Y;
		}
		
		return this;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", Usuario.getLogin(ctx));
			//jsonObj.put("password", Crypto.sha1(password));
			jsonObj.put("password", password);
			jsonObj.put("cvv2", cvv2);
			jsonObj.put("pin", verificador);
			jsonObj.put("tarjeta", tag);
			jsonObj.put("producto", producto);
			jsonObj.put("imei", Sys.getIMEI(ctx));
			jsonObj.put("cx", Double.toString(X));
			jsonObj.put("cy", Double.toString(Y));
			jsonObj.put("tipo", Sys.getTipo());
			jsonObj.put("software", Sys.getSWVersion());
			jsonObj.put("modelo", Sys.getModel());
			jsonObj.put("key", Sys.getIMEI(ctx));
		}catch(Exception e){
			Sys.log(e);
		}
		/*"{\"login\":\""+MainClass.userBean.getLogin()
		+"\",\"password\":"+"\""+Utils.sha1(basicPassword.getText().trim())+
		"\",\"cvv2\":\""+basicCvv2.getText().trim()+
		"\",\"vigencia\":\""+stringMonth+vigency+
		"\",\"producto\":\""+this.generalBean.getClave()+
		"\",\"tarjeta\":\""+basicClave.getText().trim()+
		"\",\"pin\":\""+basicPin.getText().trim()+"\"}";	*/	
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnPurchaseInsertedResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(password), postData);
		String newEnc = Text.mergeStr(jEnc, password);
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(password));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){	
					try{
						String folio;
						try{
							folio = jsonResponse.getString("folio");
						}catch(Exception ee){
							Sys.log(ee);
							folio = "";
						}
						
						listener.onPurchaseInsertedResponseReceived(
							folio, 
							jsonResponse.getString("resultado"), 
							jsonResponse.getString("mensaje")
						);
					}catch(Exception e){
						Sys.log(e);
						
						String message = "hubo problemas al tratar de hacer la compra. Intenta m�s tarde";
						if (jsonResponse != null){
							String msj = jsonResponse.optString("mensaje");
							if (msj != null && msj.trim().length() > 0){
								message = msj;
							}
						}
						listener.onPurchaseInsertedResponseReceived("", "", message);
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}