package com.ironbit.mc.web.webservices;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.web.webservices.events.OnBitacoraInsertResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

/**
 * <p>Servicio que inserta en base de datos la transacci�n y regresa un id en caso de que el proceso sea exitoso</p>
 * @author ADDCEL14
 * @version 2.2
 */

public class SetPagoEdomexWSClient extends WebServiceClient {
	
	private long usuario;
	private double cargo;
	private String proveedor;
	private String producto;
	private int noAutorizacion;
	private String ticket;
	private int codigoError;
	private String concepto;
	private int status;
	private String destino;
	
	public SetPagoEdomexWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public SetPagoEdomexWSClient setUsuario(long usuario) {
		this.usuario = usuario;
		
		return this;
	}
	
	public SetPagoEdomexWSClient setCargo(double cargo) {
		this.cargo = cargo;
		
		return this;
	}
	
	public SetPagoEdomexWSClient setProveedor(String proveedor) {
		if (proveedor != null) {
			this.proveedor = proveedor;
		}
		
		return this;
	}
	
	public SetPagoEdomexWSClient setProducto(String producto) {
		if (producto != null) {
			this.producto = producto;
		}
		
		return this;
	}
	
	public SetPagoEdomexWSClient setNoAutorizacion(int noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
		
		return this;
	}
	
	public SetPagoEdomexWSClient setTicket(String ticket) {
		if (ticket != null) {
			this.ticket = ticket;
		}
		
		return this;
	}
	
	public SetPagoEdomexWSClient setCodigoError(int codigoError) {
		this.codigoError = codigoError;
		
		return this;
	}
	
	public SetPagoEdomexWSClient setConcepto(String concepto) {
		if (concepto != null) {
			this.concepto = concepto;
		}
		
		return this;
	}
	
	public SetPagoEdomexWSClient setStatus(int status) {
		this.status = status;
		
		return this;
	}
	
	public SetPagoEdomexWSClient setDestino(String destino) {
		if (destino != null) {
			this.destino = destino;
		}
		
		return this;
	}
	
	public WebServiceClient execute(final OnBitacoraInsertResponseListener listener) {
		
		String idUsuario = Long.toString(usuario);
		String monto = Double.toString(cargo);
		String autorizacion = Integer.toString(noAutorizacion);
		String codError = Integer.toString(codigoError);
		String estatus = Integer.toString(status);
		
		addPostParameter("idUsuario", idUsuario);
		addPostParameter("monto", monto);
		addPostParameter("proveedor", proveedor);
		addPostParameter("producto", producto);
		addPostParameter("autorizacion", autorizacion);
		addPostParameter("folioETN", ticket);
		addPostParameter("codError",codError);
		addPostParameter("detError", concepto);
		addPostParameter("estatus", estatus);
		addPostParameter("detalle", destino);
		
		
//		Par�metros de �quipo
		
		addPostParameter("imei", Sys.getIMEI(ctx));
		addPostParameter("modelo", Sys.getModel());
		addPostParameter("software", Sys.getSWVersion());
		addPostParameter("tipoProducto", Sys.getTipo());
		addPostParameter("wkey", Sys.getIMEI(ctx));
		
		System.out.println("idUsuario: " + idUsuario);
		System.out.println("monto: " + monto);
		System.out.println("proveedor: " + proveedor);
		System.out.println("producto: " + producto);
		System.out.println("autorizacion: " + autorizacion);
		System.out.println("folioETN: " + ticket);
		System.out.println("codError: " + codError);
		System.out.println("detError: " + concepto);
		System.out.println("estatus: " + estatus);
		System.out.println("detalle: " + destino);
		
		return executeClean(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (listener != null) {
					long idBitacora = jsonResponse.optLong("registrado", 0);
					listener.onBitacoraInsertResponseReceived(idBitacora);
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_BITACORA_AGREGA;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_PAGO_EDOMEX;
	}

}
