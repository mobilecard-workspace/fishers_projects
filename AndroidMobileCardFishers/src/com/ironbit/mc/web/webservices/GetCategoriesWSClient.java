package com.ironbit.mc.web.webservices;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.web.webservices.data.DataCategory;
import com.ironbit.mc.web.webservices.events.OnCategoriesResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class GetCategoriesWSClient extends WebServiceClient{
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_CATEGORIAS_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_CATEGORIES;
	}
	
	
	public GetCategoriesWSClient(Activity ctx){
		super(ctx);
	}
	
	
	public WebServiceClient execute(final OnCategoriesResponseReceivedListener listener) {
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
					if (listener != null){
						ArrayList<DataCategory> categories = new ArrayList<DataCategory>();
						
						try{
							JSONArray jsonProviders = jsonResponse.getJSONArray("categorias");
							JSONObject jsonProvider = null;
							
							for (int a = 0; a < jsonProviders.length(); a++){
								jsonProvider = jsonProviders.getJSONObject(a);
								categories.add(new DataCategory(
									jsonProvider.getString("id"), 
									
									jsonProvider.getString("descripcion"), 
									jsonProvider.getString("path")
								));
							}
						}catch(Exception e){
							Sys.log(e);
						}
						
						listener.onCategoriesResponseReceived(categories);
					}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}

		
		});
	}
}