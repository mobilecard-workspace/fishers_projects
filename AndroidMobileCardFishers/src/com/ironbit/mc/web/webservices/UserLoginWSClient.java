package com.ironbit.mc.web.webservices;


import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.events.OnLoginResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class UserLoginWSClient extends WebServiceClient{
	protected String login = "";
	protected String password = "";
	
	
	public UserLoginWSClient(Activity ctx){
		super(ctx);
	}
	
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_USER_LOGIN;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_USER_LOGIN;
	}
	
	
	public UserLoginWSClient setLogin(String login){
		if (login != null){
			this.login = login;
		}
		
		return this;
	}
	
	
	/**
	 * @param password Contraseña (Sin encriptar)
	 * @return
	 */
	public UserLoginWSClient setPassword(String password){
		if (password != null){
			this.password = password;
		}
		
		return this;
	}
	
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", login);
			jsonObj.put("passwordS", Crypto.sha1(password));
			jsonObj.put("password", password);
			
			jsonObj.put("imei", Sys.getIMEI(ctx));
			jsonObj.put("tipo", Sys.getTipo());
			jsonObj.put("software", Sys.getSWVersion());
			jsonObj.put("modelo", Sys.getModel());
			jsonObj.put("key", Sys.getIMEI(ctx));
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnLoginResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		System.out.println("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(password), postData);
		String newEnc = Text.mergeStr(jEnc, password);
		addPostParameter("json", newEnc);
		System.out.println("cadena: "+newEnc);
		Sys.setPTelefono(Text.parsePass(password));
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){		
					try{
						listener.onLoginResponseReceived(
							jsonResponse.getString("resultado"), 
							jsonResponse.getString("mensaje")
						);
					}catch(Exception e){
						Sys.log(e);
						
						listener.onLoginResponseReceived("Error", e.getMessage());
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}

			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}