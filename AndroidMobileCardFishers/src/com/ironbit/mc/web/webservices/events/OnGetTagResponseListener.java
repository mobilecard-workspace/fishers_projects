package com.ironbit.mc.web.webservices.events;

import java.util.ArrayList;

import com.ironbit.mc.web.webservices.data.DataTag;

public interface OnGetTagResponseListener {
	public void onGetTagResponseListener(ArrayList<DataTag> tags);
}
