package com.ironbit.mc.web.webservices.events;

import org.json.JSONArray;
import org.json.JSONObject;

public interface OnResponseJSONReceivedListener {

	public void onResponseJSONReceived(JSONObject jsonResponse);

	public void onResponseJSONAReceived(JSONArray jsonResponse);
	
	public void onResponseStringReceived(String jsonresponse);

}