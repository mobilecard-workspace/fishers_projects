package com.ironbit.mc.web.webservices.events;

public interface OnMontoInterjetResponseListener {
	public void onMontoInterjetResponseReceived(String Status, String Monto, String Pnr);
}
