package com.ironbit.mc.web.webservices.events;

public interface OnUserPassUpdatedResponseReceivedListener {
	public void onUserPassUpdatedResponseReceived(String resultado, String mensaje);
}