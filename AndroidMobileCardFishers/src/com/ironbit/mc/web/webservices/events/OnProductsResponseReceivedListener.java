package com.ironbit.mc.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mc.web.webservices.data.DataProduct;

public interface OnProductsResponseReceivedListener {
	public void onProductsResponseReceived(ArrayList<DataProduct> products);
}