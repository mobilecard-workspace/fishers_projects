package com.ironbit.mc.web.webservices.events;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;

public interface OnOrigenResponseReceivedListener {
	public void onBanksResponseReceived(ArrayList<BasicNameValuePair> banks);
}