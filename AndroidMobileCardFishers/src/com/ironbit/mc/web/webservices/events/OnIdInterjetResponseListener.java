package com.ironbit.mc.web.webservices.events;

public interface OnIdInterjetResponseListener {
	public void onIdInterjetResponseReceived(String id);
}
