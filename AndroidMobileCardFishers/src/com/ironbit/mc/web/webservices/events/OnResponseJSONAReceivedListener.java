package com.ironbit.mc.web.webservices.events;


import org.json.JSONArray;


public interface OnResponseJSONAReceivedListener {

	public void onResponseJSONReceived(JSONArray jsonResponse);

	
}
