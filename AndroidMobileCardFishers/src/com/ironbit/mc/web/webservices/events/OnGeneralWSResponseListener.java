package com.ironbit.mc.web.webservices.events;

public interface OnGeneralWSResponseListener {
	public void onGeneralWSResponseListener(String response);
	public void onGeneralWSErrorListener(String error);
	
}
