package com.ironbit.mc.web.webservices.events;

import com.ironbit.mc.web.webservices.data.DataReservacion;

public interface OnReservacionResponseListener {

	public void onReservacionResponseReceived(DataReservacion reservacion);
}
