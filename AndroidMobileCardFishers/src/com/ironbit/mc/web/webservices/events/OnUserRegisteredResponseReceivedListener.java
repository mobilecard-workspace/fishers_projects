package com.ironbit.mc.web.webservices.events;

public interface OnUserRegisteredResponseReceivedListener {
	public void onUserRegisteredResponseReceived(String resultado, String mensaje);
}