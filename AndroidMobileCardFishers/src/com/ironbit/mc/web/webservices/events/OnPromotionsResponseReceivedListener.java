package com.ironbit.mc.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mc.web.webservices.data.DataPromotion;

public interface OnPromotionsResponseReceivedListener {
	public void onPromotionsResponseReceived(ArrayList<DataPromotion> promotions);
}