package com.ironbit.mc.web.webservices.events;

public interface OnReferenceValidatedResponseReceivedListener {
	public void onReferenceValidatedResponseReceived(String mensaje);
}
