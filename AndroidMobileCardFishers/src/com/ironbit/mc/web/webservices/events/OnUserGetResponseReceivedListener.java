package com.ironbit.mc.web.webservices.events;

import com.ironbit.mc.system.Fecha;

public interface OnUserGetResponseReceivedListener {
	public void onUserGetResponseReceived(long id, Fecha fechaNacimiento, 
			String telefono, Fecha registro, String nombre, String apellidos,
			String direccion, String tarjeta, Fecha vigencia, int banco, int tipoTarjeta, 
			int proveedor, int status, String mail, String amaterno, String sexo, String tel_casa,
			String tel_oficina, int id_estado, String ciudad, String calle, int num_ext, String num_int,
			String colonia, int cp, String dom_amex);
	
	/*
	 * {"usuario":1340742802597,"login":"benito","password":"bqBtuc/YeMZ2i1YP3tYI7g\u003d\u003d",
	 * "nacimiento":"1986-06-26","telefono":"5519491618","registro":"2012-06-26","nombre":"benito",
	 * "apellido":"Bodoque","direccion":"fffuuuuuuuu","tarjeta":"42546363463432424325","vigencia":"01/16",
	 * "banco":1,"tipotarjeta":1,"proveedor":1,"status":1,"mail":"benito@mail.com","idtiporecargatag":0,"dv":0,
	 * "materno":"Miau","sexo":"Seleccionar...","tel_casa":"5656565656","tel_oficina":"5454545454","id_estado":1,
	 * "ciudad":"DF","calle":"Callejon","num_ext":1,"num_interior":"0","colonia":"colonia","cp":342342,"dom_amex":""}
	 */
}