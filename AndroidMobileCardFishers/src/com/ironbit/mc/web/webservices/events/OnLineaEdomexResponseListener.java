package com.ironbit.mc.web.webservices.events;

public interface OnLineaEdomexResponseListener {
	public void onLineaEdomexResponseReceived(String monto, String linea);
	public void onLineaEdomexErrorReceived(String mensaje);
}
