package com.ironbit.mc.web.webservices.events;

import java.util.ArrayList;

import com.ironbit.mc.web.webservices.data.DataTipoRecargaTag;

public interface OnTipRecargaTagReceivedListener {
	public void onTipoRecargaTagReceivedListener(ArrayList<DataTipoRecargaTag> listaTipos);
}
