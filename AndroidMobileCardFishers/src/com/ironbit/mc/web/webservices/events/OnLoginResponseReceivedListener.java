package com.ironbit.mc.web.webservices.events;

public interface OnLoginResponseReceivedListener {
	public void onLoginResponseReceived(String resultado, String mensaje);
}