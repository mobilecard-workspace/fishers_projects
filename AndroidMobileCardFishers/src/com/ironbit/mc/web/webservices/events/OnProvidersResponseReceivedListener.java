package com.ironbit.mc.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mc.web.webservices.data.DataProvider;

public interface OnProvidersResponseReceivedListener {
	public void onProvidersResponseReceived(ArrayList<DataProvider> providers);
}