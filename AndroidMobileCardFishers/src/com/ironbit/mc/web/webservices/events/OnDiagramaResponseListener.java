package com.ironbit.mc.web.webservices.events;

import com.ironbit.mc.web.webservices.data.DataDiagrama;

public interface OnDiagramaResponseListener {
	public void onDiagramaResponseReceived(DataDiagrama diagrama);
}
