package com.ironbit.mc.web.webservices.events;

import java.util.ArrayList;

import com.ironbit.mc.web.webservices.data.DataItinerario;

public interface OnItinerariosResponseReceivedListener {
	public void onItinerariosResponseReceived(ArrayList<DataItinerario> itinerarios);
}
