package com.ironbit.mc.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mc.web.webservices.data.DataOperation;

public interface OnOperationsResponseReceivedListener {
	public void onOperationsResponseReceived(ArrayList<DataOperation> operations);
}