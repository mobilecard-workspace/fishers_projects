package com.ironbit.mc.web.webservices.events;

public interface OnResponseReceivedListener {
	public void onResponseReceived(String response);
}