package com.ironbit.mc.web.webservices.events;

public interface OnBitacoraInsertResponseListener {
	public void onBitacoraInsertResponseReceived(long idBitacora);
}
