package com.ironbit.mc.web.webservices.events;

import com.ironbit.mc.web.webservices.data.DataOcupacion;

public interface OnOcupacionResponseReceivedListener {
	public void onOcupacionResponseReceived(DataOcupacion ocupacion);
}
