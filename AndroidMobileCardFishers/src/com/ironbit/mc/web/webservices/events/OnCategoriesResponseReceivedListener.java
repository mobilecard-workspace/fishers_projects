package com.ironbit.mc.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mc.web.webservices.data.DataCategory;

public interface OnCategoriesResponseReceivedListener {
	public void onCategoriesResponseReceived(ArrayList<DataCategory> categories);
}