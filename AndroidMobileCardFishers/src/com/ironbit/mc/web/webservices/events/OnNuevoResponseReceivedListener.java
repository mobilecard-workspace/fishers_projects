package com.ironbit.mc.web.webservices.events;

import java.util.List;

import com.ironbit.mc.web.webservices.data.NuevoTO;

public interface OnNuevoResponseReceivedListener {
	
	public void onNuevosResponseReceived(List<NuevoTO> nuevos);

}
