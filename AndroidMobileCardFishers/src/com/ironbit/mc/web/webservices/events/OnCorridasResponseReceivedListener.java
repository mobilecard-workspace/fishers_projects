package com.ironbit.mc.web.webservices.events;

import java.util.ArrayList;

import com.ironbit.mc.web.webservices.data.DataCorrida;

public interface OnCorridasResponseReceivedListener {
	public void onCorridasResponseReceived(ArrayList<DataCorrida> corridas);
}
