package com.ironbit.mc.web.webservices.events;

public interface OnUserUpdatedResponseReceivedListener {
	public void onUserUpdatedResponseReceived(String resultado, String mensaje);
}