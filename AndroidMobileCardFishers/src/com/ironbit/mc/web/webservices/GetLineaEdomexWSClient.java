package com.ironbit.mc.web.webservices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.web.webservices.events.OnLineaEdomexResponseListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

/**
 * <p>Servicio que env�a login, placa y tel�fono del usuario. Y recibe la l�nea de captura y el monto a pagar.</p>
 * @author ADDCEL14
 * @version 2.2
 */

public class GetLineaEdomexWSClient extends WebServiceClient {
	
	private String login;
	private String placa;
	private String telefono;

	public GetLineaEdomexWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetLineaEdomexWSClient setLogin(String login) {
		if (login != null) {
			this.login = login;
		}
		
		return this;
	}
	
	public GetLineaEdomexWSClient setPlaca(String placa) {
		if (placa != null) {
			this.placa = placa;
		}
		
		return this;
	}
	
	public GetLineaEdomexWSClient setTelefono(String telefono) {
		if (telefono != null) {
			this.telefono = telefono;
		}
		
		return this;
	}
	
	public JSONObject getJsonParams() {
		JSONObject json = new JSONObject();
		try {
			json.put("login", login);
			json.put("placa", placa);
			json.put("telefono", telefono);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return json;
		}
		return json;
	}
	
	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return  UrlWebServices.URL_WS_EDOMEX_LINEA_GET;
//				UrlWebServices.URL_WS_EDOMEX_BRIDGE;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_LINEA_EDOMEX;
	}
	
	
	public WebServiceClient execute(final OnLineaEdomexResponseListener listener) {
		String jsonString = getJsonParams().toString();
		Sys.log("json=" + jsonString);
		System.out.println(jsonString);
		
//		String AESJson = Crypto.aesEncrypt(Sys.getKey(), jsonString);
		System.out.println(jsonString);
		
		addGetParameter("json", jsonString);
		
		return executeClean(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (listener != null) {
					int resultado = jsonResponse.optInt("resultado", -1);
					String mensaje = jsonResponse.optString("mensaje", "");
					
					if (resultado == 9) {
						try {
							JSONObject mensajeJSON = new JSONObject(mensaje);
							String monto = mensajeJSON.optString("monto", "");
							String linea = mensajeJSON.optString("linea", "");
							

							listener.onLineaEdomexResponseReceived(monto, linea);
						} catch (JSONException e) {

							listener.onLineaEdomexErrorReceived(e.getMessage());
						}
						
					} else {
						
						listener.onLineaEdomexErrorReceived(mensaje);
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	

}
