package com.ironbit.mc.web.webservices;


import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.conexion.ConexionHttp.Hilo;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.Fecha.TipoFecha;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mc.web.webservices.events.OnUserGetResponseReceivedListener;

public class GetUserWSClient extends WebServiceClient{
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_USER_GET;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_USER_GET;
	}
	
	
	public GetUserWSClient(Activity ctx){
		super(ctx);
	}
	
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", Usuario.getLogin(ctx));
			//jsonObj.put("password", Crypto.sha1(Usuario.getPass(ctx)));
			jsonObj.put("password", Usuario.getPass(ctx));
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnUserGetResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		System.out.println("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(Usuario.getPass(ctx)), postData);
		System.out.println(jEnc);
		String newEnc = Text.mergeStr(jEnc, Usuario.getPass(ctx));
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(Usuario.getPass(ctx)));
				
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){
					try{
						Fecha fechaNacimiento = new Fecha(jsonResponse.getString("nacimiento"), TipoFecha.date);
						Fecha fechaRegistro = new Fecha(jsonResponse.getString("registro"), TipoFecha.date);
						Fecha fechaVigencia = new Fecha();
						
						try{
							String[] vigencia = jsonResponse.getString("vigencia").split("/");
							fechaVigencia.setMes(Integer.parseInt(vigencia[0]));
							fechaVigencia.setAnio(Integer.parseInt("20" + vigencia[1]));
						}catch(Exception e){
							Sys.log(e);
						}
						
						String mat=jsonResponse.has("materno")?jsonResponse.getString("materno"):"";
						String sex=jsonResponse.has("sexo")?jsonResponse.getString("sexo"):"M";
						String telcasa=jsonResponse.has("tel_casa")?jsonResponse.getString("tel_casa"):"";
						String telof=jsonResponse.has("tel_oficina")?jsonResponse.getString("tel_oficina"):"";
						String ciudad=jsonResponse.has("ciudad")?jsonResponse.getString("ciudad"):"";
						String calle=jsonResponse.has("calle")?jsonResponse.getString("calle"):"";
						String numext=jsonResponse.has("num_ext")?jsonResponse.getString("num_ext"):"0";
						String num_interior=jsonResponse.has("num_interior")?jsonResponse.getString("num_interior"):"";
						String colonia=jsonResponse.has("colonia")?jsonResponse.getString("colonia"):"";
						String cp=jsonResponse.has("cp")?jsonResponse.getString("cp"):"";
						String amex=jsonResponse.has("dom_amex")?jsonResponse.getString("dom_amex"):"";
						listener.onUserGetResponseReceived(
							jsonResponse.getLong("usuario"),
							fechaNacimiento, 
							jsonResponse.getString("telefono"), 
							fechaRegistro, 
							jsonResponse.getString("nombre"),
							jsonResponse.getString("apellido"),
							"",
							jsonResponse.getString("tarjeta"), 
							fechaVigencia, 
							Integer.parseInt(jsonResponse.getString("banco")),
							Integer.parseInt(jsonResponse.getString("tipotarjeta")),
							Integer.parseInt(jsonResponse.getString("proveedor")),
							Integer.parseInt(jsonResponse.getString("status")),
							jsonResponse.getString("mail"),
							mat,
							sex,
							telcasa,
							telof,
							Integer.parseInt(jsonResponse.getString("id_estado")),
							ciudad,
							calle,
							Integer.parseInt(numext),
							num_interior,
							colonia,
							Integer.parseInt(cp),
							amex
						);
						/*
						 * {"usuario":1340742802597,"login":"benito","password":"bqBtuc/YeMZ2i1YP3tYI7g\u003d\u003d",
						 * "nacimiento":"1986-06-26","telefono":"5519491618","registro":"2012-06-26","nombre":"benito",
						 * "apellido":"Bodoque","direccion":"fffuuuuuuuu","tarjeta":"42546363463432424325",
						 * "vigencia":"01/16","banco":1,"tipotarjeta":1,"proveedor":1,"status":1,
						 * "mail":"benito@mail.com","idtiporecargatag":0,"dv":0,"materno":"Miau","sexo":"Seleccionar...",
						 * "tel_casa":"5656565656","tel_oficina":"5454545454","id_estado":1,"ciudad":"DF",
						 * "calle":"Callejon","num_ext":1,"num_interior":"0","colonia":"colonia","cp":342342,
						 * "dom_amex":""}
						 */
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
			
			public void onResponseStringReceived(String jsonresponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}