package com.ironbit.mc.web.util;

import com.ironbit.mc.R;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.errores.ErrorSys;

import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * @author Angel López
 * @description
 * Clase que sirve para manipular una url
 * @example
 * 	Hashtable<String, String> vars = new Hashtable<String, String>();<br/>
 * 	vars.put("nombre", "jose");<br/>
 * 	vars.put("edad", "17");	<br/><br/>
 * 
 *  if (Url.setUrl("http://www.page.com/file.php").agregarParametros(vars).tieneQuery()){<br/>
 *  	System.out.print("La Url ahora si tiene variables");<br/>
 *  }<br/>
 *  
 *  =================================
 *  
 *  System.out.print("Url: " + Url.setUrl("http://www.web.mx?nombre=juan").agregarParametros("edad=12").getUrl());
 */
public class Url {
	private static Url instancia = null;
	private static URL thisUrl = null;
	
	
	/**
	 * Constructor privado. Esta clase no puede ser instanciada fuera de aquí.
	 */
	private Url(){}
	
	
	/**
	 * Metodos
	 */
	
	protected static Url getInstancia(){
		if (instancia == null){
			instancia  = new Url();
		}
		
		return instancia;
	}
	
	
	public static Url setUrl(String url){
		Url objUrl = getInstancia();
		
		try{
			thisUrl = new URL(url);
		}catch(Exception e){
			Sys.log(e);
		}
		
		return objUrl;
	}
	
	
	public URL getURLClass(){
		return thisUrl;
	}
	
	
	public String getUrl(){
		return thisUrl.toString();
	}
	
	
	public Url agregarParametros(Hashtable<String, String> parametros) throws ErrorSys{
		//creamos query a agregar
		String 	strParametros = "",
				llave,
				separador = "";
		Enumeration<String> e = parametros.keys();
		
		while (e.hasMoreElements()){
			llave = e.nextElement();
			strParametros += separador + llave + "=" + URLEncoder.encode(parametros.get(llave));
			separador = "&";
		}
		
		return agregarParametros(strParametros);
	}
	
	
	public Url agregarParametros(String parametros) throws ErrorSys{
		Url urlObj = getInstancia();
		
		if (thisUrl == null){
			throw new ErrorSys(R.string.err_urlAunNoAgregada);
		}else{
			String newUrl = getUrl();
			
			try{
				if (tieneQuery()){
					newUrl += "&" + parametros;
				}else{
					if (newUrl.substring(newUrl.length()-1) == "?"){
						newUrl += parametros;
					}else{
						newUrl += "?" + parametros;
					}
				}
				
				thisUrl = new URL(newUrl);
			}catch(Exception ex){
				throw new ErrorSys(R.string.err_urlAgregandoParametros);
			}
		}
		
		return urlObj;
	}
	
	
	public boolean tieneQuery(){
		boolean tiene = true;
		
		if (thisUrl == null){
			tiene = false;
		}else{
			if (thisUrl.getQuery() == null){
				tiene = false;
			}
		}
			
		return tiene;
	}
	
	
	@Override
	public String toString() {
		return getUrl().toString();
	}
}
