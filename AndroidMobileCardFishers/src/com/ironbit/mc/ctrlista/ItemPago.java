package com.ironbit.mc.ctrlista;

public class ItemPago {
	protected long id;	  
	  protected String comercio;
	  protected String fechahora;
	         
	  public ItemPago() {
	    this.comercio = "";
	    this.fechahora = "";
	    
	  }
	     
	  public ItemPago(long id, String nombre, String tipo) {
	    this.id = id;
	    this.comercio = nombre;
	    this.fechahora = tipo;	    
	  }
	     	  
	     
	  public long getId() {
	    return id;
	  }
	     
	  public void setId(long id) {
	    this.id = id;
	  }
	     	 	     	  
	  public String getNombre() {
	    return comercio;
	  }
	     
	  public void setNombre(String nombre) {
	    this.comercio = nombre;
	  }
	     
	  public String getFecha() {
	    return fechahora;
	  }
	     
	  public void setFecha(String tipo) {
	    this.fechahora = tipo;
	  }
}
