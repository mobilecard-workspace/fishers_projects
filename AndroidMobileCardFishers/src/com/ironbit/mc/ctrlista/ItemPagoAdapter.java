package com.ironbit.mc.ctrlista;

import java.util.ArrayList;

import com.ironbit.mc.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ItemPagoAdapter extends BaseAdapter {
	protected Activity activity;
	  protected ArrayList<ItemPago> items;
	         
	  public ItemPagoAdapter(Activity activity, ArrayList<ItemPago> items) {
	    this.activity = activity;
	    this.items = items;
	  }
	 
	  public int getCount() {
	    return items.size();
	  }
	 
	  public Object getItem(int position) {
	    return items.get(position);
	  }
	 
	  public long getItemId(int position) {
	    return items.get(position).getId();
	  }
	 
	  public View getView(int position, View convertView, ViewGroup parent) {
	    View vi=convertView;
	         
	    if(convertView == null) {
	      LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	      vi = inflater.inflate(R.layout.item_pago_estilo, null);
	    }
	             
	    ItemPago item = items.get(position);
	         
	    //ImageView image = (ImageView) vi.findViewById(R.id.comercio);
	    //int imageResource = activity.getResources().getIdentifier(item.getRutaImagen(), null, activity.getPackageName());
	    //image.setImageDrawable(activity.getResources().getDrawable(imageResource));
	         
	    TextView nombre = (TextView) vi.findViewById(R.id.comercio);
	    nombre.setText(item.getNombre());
	         
	    TextView tipo = (TextView) vi.findViewById(R.id.fechahora);
	    tipo.setText(item.getFecha());
	 
	    return vi;
	  }
}
