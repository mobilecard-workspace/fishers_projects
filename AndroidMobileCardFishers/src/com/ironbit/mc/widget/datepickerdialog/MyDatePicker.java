package com.ironbit.mc.widget.datepickerdialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;

public class MyDatePicker {
	protected DatePicker datePicker = null;
	protected Calendar minDate = null;
	protected Calendar maxDate = null;
	protected OnDateChangedListener onDateChangedListener = null;
	
	
	public void setOnDateChangedListener(OnDateChangedListener onDateChangedListener) {
		this.onDateChangedListener = onDateChangedListener;
	}


	public Calendar getMinDate() {
		return minDate;
	}


	public void setMinDate(Calendar minDate) {
		this.minDate = minDate;
		this.minDate.set(Calendar.HOUR_OF_DAY, 0);
		this.minDate.set(Calendar.MINUTE, 0);
		this.minDate.set(Calendar.SECOND, 0);
	}


	public Calendar getMaxDate() {
		return maxDate;
	}


	public void setMaxDate(Calendar maxDate) {
		this.maxDate = maxDate;
		this.maxDate.set(Calendar.HOUR_OF_DAY, 0);
		this.maxDate.set(Calendar.MINUTE, 0);
		this.maxDate.set(Calendar.SECOND, 0);
	}


	public DatePicker getDatePicker() {
		return datePicker;
	}

	
	public void setDatePicker(DatePicker datePicker) {
		this.datePicker = datePicker;
	}


	public MyDatePicker(Activity context, int resIdDatePicker, Calendar date) {
		this(context, (DatePicker)context.findViewById(resIdDatePicker), date);
	}
	
	
	public MyDatePicker(Activity context, DatePicker datePicker, Calendar date) {
		this.datePicker = datePicker;	
		datePicker.init(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), new OnDateChangedListener() {
			
			public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				try{
					DateFormat df = new SimpleDateFormat ("yy-MM-dd");
				    Date thisDate = df.parse(
			    		Text.dosDigitos(year) + "-" + 
						Text.dosDigitos(monthOfYear+1) + "-" + 
						Text.dosDigitos(dayOfMonth)
				    );
				    
					if (minDate != null && thisDate.before(minDate.getTime())){
						MyDatePicker.this.datePicker.updateDate(minDate.get(Calendar.YEAR), minDate.get(Calendar.MONTH), minDate.get(Calendar.DAY_OF_MONTH));
					}
					
					if (maxDate != null && thisDate.after(maxDate.getTime())){
						MyDatePicker.this.datePicker.updateDate(maxDate.get(Calendar.YEAR), maxDate.get(Calendar.MONTH), maxDate.get(Calendar.DAY_OF_MONTH));
					}
					
					if (onDateChangedListener != null){
						onDateChangedListener.onDateChanged(
							view, 
							MyDatePicker.this.datePicker.getYear(),
							MyDatePicker.this.datePicker.getMonth(),
							MyDatePicker.this.datePicker.getDayOfMonth()
						);
					}
				}catch(Exception e){
					Sys.log(e);
				}
			}
		});
	}
}