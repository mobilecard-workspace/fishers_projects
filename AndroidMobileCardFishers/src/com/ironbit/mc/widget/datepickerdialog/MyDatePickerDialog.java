package com.ironbit.mc.widget.datepickerdialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

import com.ironbit.mc.R;
import com.ironbit.mc.system.Fecha;

public class MyDatePickerDialog implements OnDateChangedListener{
	protected MyDatePicker myDatePicker = null;
	protected DatePicker datePicker = null;
	protected Activity ctx = null;
	protected AlertDialog alertDialog = null;
	protected Calendar thisFecha = Calendar.getInstance();
	protected DateFormat df = new SimpleDateFormat ("EEE, dd MMMM, yyyy");
	protected OnDateSetListener onDateSetListener = null;
	protected Calendar fechaRespaldoInicial = null;
	protected boolean userFijofecha = false;
	
	
	protected Calendar getThisFecha(){
		if (thisFecha == null){
			thisFecha = Calendar.getInstance();
		}
		
		return thisFecha;
	}
	

	public void setOnDateSetListener(OnDateSetListener onDateSetListener) {
		this.onDateSetListener = onDateSetListener;
	}


	public MyDatePickerDialog(Activity ctx, Calendar fecha){
		this.ctx = ctx;
		setFecha(fecha);
	}
	
	
	public MyDatePickerDialog(Activity ctx){
		this.ctx = ctx;
	}
	
	
	public MyDatePickerDialog(Activity ctx, OnDateSetListener listener){
		this.ctx = ctx;
		setOnDateSetListener(listener);
	}
	
	
	public MyDatePickerDialog(Activity ctx, OnDateSetListener listener, int anio, int mes, int dia){
		this(ctx, listener);
		setFecha(anio, mes, dia);
	}
	
	
	protected void setFecha(int anio, int mes, int dia){
		getThisFecha().set(Calendar.YEAR, anio);
		getThisFecha().set(Calendar.MONTH, mes);
		getThisFecha().set(Calendar.DAY_OF_MONTH, dia);
	}
	
	
	protected void setFecha(Calendar fecha){
		thisFecha = fecha;
	}
	
	
	public void updateFecha(Fecha newFecha){
		setFecha(newFecha.getAnio(), newFecha.getMesBase0(), newFecha.getDia());
		
		getDatePicker().updateDate(
			newFecha.getAnio(), 
			newFecha.getMesBase0(), 
			newFecha.getDia()
		);
	}
	
	
	public MyDatePickerDialog setMinDate(int dia, int mes, int anio){
		Calendar cMinDate = Calendar.getInstance();
		cMinDate.set(anio, mes, dia);
		return setMinDate(cMinDate);
	}
	
	
	public MyDatePickerDialog setMinDate(Calendar minDate){
		getMyDatePicker().setMinDate(minDate);
		return this;
	}
	
	
	public MyDatePickerDialog setMaxDate(int dia, int mes, int anio){
		Calendar cMaxDate = Calendar.getInstance();
		cMaxDate.set(anio, mes, dia);
		return setMaxDate(cMaxDate);
	}
	
	
	public MyDatePickerDialog setMaxDate(Calendar maxDate){
		getMyDatePicker().setMaxDate(maxDate);
		return this;
	}
	
	
	public DatePicker getDatePicker() {
		if (datePicker == null){
			datePicker = (DatePicker)ctx.getLayoutInflater().inflate(R.layout.datepicker, null);
		}
		
		return datePicker;
	}
	
	
	public MyDatePicker getMyDatePicker(){
		if (myDatePicker == null){
			myDatePicker = new MyDatePicker(ctx, getDatePicker(), getThisFecha());
			myDatePicker.setOnDateChangedListener(this);
		}
		
		return myDatePicker;
	}
	
	
	
	public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		getThisFecha();
		thisFecha.set(Calendar.YEAR, year);
		thisFecha.set(Calendar.MONTH, monthOfYear);
		thisFecha.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		
		getAlertDialog().setTitle(getFechaActualAsString());
	}
	
	
	public String getFechaActualAsString(){
		return df.format(getThisFecha().getTime());
	}
	
	
	protected AlertDialog getAlertDialog() {
		if (alertDialog == null){
			Builder builder = new AlertDialog.Builder(ctx);
			builder.setView(getDatePicker());
			alertDialog = builder.create();
			alertDialog.setTitle(getFechaActualAsString());
			alertDialog.setButton(ctx.getString(R.string.btn_dialog_set), new OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					userFijofecha = true;
					
					if (onDateSetListener != null){
						onDateSetListener.onDateSet(
							getDatePicker(), 
							getDatePicker().getYear(), 
							getDatePicker().getMonth(),
							getDatePicker().getDayOfMonth()
						);
					}
				}
			});
			alertDialog.setButton2(ctx.getString(R.string.btn_dialog_cancel), new OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					getAlertDialog().dismiss();
				}
			});
			alertDialog.setOnDismissListener(new OnDismissListener() {
				
				public void onDismiss(DialogInterface dialog) {
					if (!userFijofecha){
						//regresamos el datePicker a la fecha inicial (ya que el usuario o: presionó Cancel ó presionó back)
						if (fechaRespaldoInicial != null){
							getDatePicker().updateDate(
								fechaRespaldoInicial.get(Calendar.YEAR), 
								fechaRespaldoInicial.get(Calendar.MONTH), 
								fechaRespaldoInicial.get(Calendar.DAY_OF_MONTH
							));
						}
					}
					
					userFijofecha = false;
				}
			});
		}
		
		return alertDialog;
	}
	
	
	public void show(){
		fechaRespaldoInicial = (Calendar)getThisFecha().clone();
		getAlertDialog().show();
	}
}