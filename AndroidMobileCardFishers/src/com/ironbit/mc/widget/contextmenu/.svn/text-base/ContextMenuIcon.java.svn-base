package com.ironbit.mc.widget.contextmenu;

import com.ironbit.mc.R;

import java.util.Hashtable;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ContextMenuIcon {
	/**
	 * Propiedades
	 */
	protected Activity ctx = null;
	private Hashtable<Integer, ContextMenuItem> items = new Hashtable<Integer, ContextMenuItem>();
	private String titulo = "";
	private OnContextMenuIconItemSelectedListener listener = null;
	private LinearLayout linLayContextMenu = null;
	private AlertDialog alertDialog;
	private AlertDialog.Builder builder;
	
	
	public LinearLayout getLinLayContextMenu(){
		if (linLayContextMenu == null){
			linLayContextMenu = (LinearLayout)ctx.getLayoutInflater().inflate(R.layout.context_menu, null);
		}
		
		return linLayContextMenu;
	}
	
	
	public Hashtable<Integer, ContextMenuItem> getItems() {
		return items;
	}
	
	
	public void setItems(Hashtable<Integer, ContextMenuItem> items) {
		this.items = items;
	}
	
	
	public String getTitulo() {
		return titulo;
	}
	
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	
	public void setOnContextMenuIconItemSelectedListener(OnContextMenuIconItemSelectedListener listener) {
		this.listener = listener;
	}
	
	
	/**
	 * Constructores
	 */
	
	public ContextMenuIcon(Activity ctx, String titulo, OnContextMenuIconItemSelectedListener listener){
		this.ctx = ctx;
		setTitulo(titulo);
		setOnContextMenuIconItemSelectedListener(listener);
		
		//creamos dialog, pero a�n no lo mostramos
		builder = new AlertDialog.Builder(ctx);
		builder.setView(getLinLayContextMenu());
		builder.setTitle(getTitulo());
		
		alertDialog = builder.create();
	}
	
	
	public ContextMenuIcon(Activity ctx, String titulo){
		this(ctx, titulo, null);
	}


	/**
	 * M�todos
	 */
	
	public ContextMenuIcon agregarItem(int id, String titulo, int resIdImg){
		ContextMenuItem item = new ContextMenuItem(id, titulo, resIdImg);
		agregarItemToLayout(item);
		items.put(id, item);
		
		return this;
	}
	
	
	public ContextMenuIcon quitarItem(int id){
		quitarItemFromLayout(items.get(id));
		items.remove(id);
		
		return this;
	}
	
	
	protected void agregarItemToLayout(final ContextMenuItem item){
		//personalizamos item View
		LinearLayout linLayItem = (LinearLayout)ctx.getLayoutInflater().inflate(R.layout.context_menu_item, null);
		RelativeLayout linLayItemSinDiv = (RelativeLayout)linLayItem.findViewById(R.id.linLay_context_menu_item);
		
		linLayItemSinDiv.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				ocultar();
				
				if (listener != null){
					listener.onContextMenuIconItemSelected(item.getId());
				}
			}
		});
		
		//agregamos funcionalidad de cambiar color onFocus
		linLayItemSinDiv.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus){
					v.setBackgroundResource(R.drawable.context_menu_hover); 
				}else{
					v.setBackgroundDrawable(null);
				}
			}
		});
		
		//imagen
		ImageView imgView = (ImageView)linLayItem.findViewById(R.id.context_menu_imagen);
		imgView.setImageResource(item.getResIdImg());
		
		//texto
		TextView txtView = (TextView)linLayItem.findViewById(R.id.context_menu_texto);
		txtView.setText(item.getTexto());
		
		//agregamos referencia de su view al item
		item.setView(linLayItem);
		
		//agregamos nuevo item
		getLinLayContextMenu().addView(linLayItem);
	}
	
	
	protected void quitarItemFromLayout(final ContextMenuItem item){
		//quitamos el view del item
		getLinLayContextMenu().removeView(item.getView());
	}
	
	
	public void mostrar(){
		if (alertDialog!= null){
			if (!alertDialog.isShowing()){
				alertDialog.show();
			}
		}
	}
	
	
	public void ocultar(){
		if (alertDialog != null){
			alertDialog.dismiss();
		}
	}
}