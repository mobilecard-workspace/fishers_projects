package com.ironbit.mc.widget.contextmenu;

public interface OnContextMenuIconItemSelectedListener {
	public void onContextMenuIconItemSelected(int itemId);
}