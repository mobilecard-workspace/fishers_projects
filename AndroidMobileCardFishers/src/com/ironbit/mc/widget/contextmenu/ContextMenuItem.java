package com.ironbit.mc.widget.contextmenu;

import android.view.View;

public class ContextMenuItem {
	private String texto = "";
	private int resIdImg = 0;
	private int id = 0;
	private View view = null;
	
	
	public View getView() {
		return view;
	}


	public void setView(View view) {
		this.view = view;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTexto() {
		return texto;
	}
	
	
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	
	public int getResIdImg() {
		return resIdImg;
	}
	
	
	public void setResIdImg(int resIdImg) {
		this.resIdImg = resIdImg;
	}
	
	
	public ContextMenuItem(){}
	
	
	public ContextMenuItem(int id, String texto, int resIdImg){
		setId(id);
		setTexto(texto);
		setResIdImg(resIdImg);
	}
}