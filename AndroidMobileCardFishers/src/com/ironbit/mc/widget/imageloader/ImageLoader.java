package com.ironbit.mc.widget.imageloader;

import com.ironbit.mc.R;
import com.ironbit.mc.system.repeater.Repeater;
import com.ironbit.mc.system.repeater.Repetible;

import android.app.Activity;
import android.os.Handler;
import android.widget.ImageView;

public class ImageLoader {
	protected Activity ctx = null;
	protected ImageView imagen = null;
	protected String nombreImagenBase = "";
	protected int numImagenes = 0;
	private Repeater repeater = null;
	protected int imgActual = 1;
	
	
	public ImageLoader(Activity ctx, ImageView imagen, String nombreImagenBase, int numImagenes, boolean iniciar){
		this.ctx = ctx;
		this.imagen = imagen;
		this.nombreImagenBase = nombreImagenBase;
		this.numImagenes = numImagenes;
		
		if (iniciar){
			iniciar();
		}
	}
	
	
	public void detener(){
		if (repeater != null){
			repeater.detener();
		}
	}
	
	
	public void iniciar(){
		detener();
		
		repeater = new Repeater(new Handler(), 0.1f, "imgLoader", true, new Repetible() {
			public void repetir() {
				int resIdImg = 0;
			
				switch (imgActual) {
					case 1: resIdImg = R.drawable.loader1; break;
					case 2: resIdImg = R.drawable.loader2; break;
					case 3: resIdImg = R.drawable.loader3; break;
					case 4: resIdImg = R.drawable.loader4; break;
					case 5: resIdImg = R.drawable.loader5; break;
					case 6: resIdImg = R.drawable.loader6; break;
					case 7: resIdImg = R.drawable.loader7; break;
					case 8: resIdImg = R.drawable.loader8; break;
				}
				
				if (imagen != null){
					imagen.setImageResource(resIdImg);
				}
				
				++imgActual;
				if (imgActual > numImagenes){
					imgActual = 1;
				}
			}
		});
	}
}