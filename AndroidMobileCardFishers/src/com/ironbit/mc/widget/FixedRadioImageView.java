package com.ironbit.mc.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class FixedRadioImageView extends ImageView {

	public FixedRadioImageView(Context context) {
		super(context);
	}

	public FixedRadioImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FixedRadioImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
	}
	

}
