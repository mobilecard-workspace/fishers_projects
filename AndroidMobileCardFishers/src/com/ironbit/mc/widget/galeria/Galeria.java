package com.ironbit.mc.widget.galeria;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.CategoriesActivity;
import com.ironbit.mc.activities.MenuAppActivity;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Sys.Dispositivo;
import com.ironbit.mc.system.cache.image.ImageCacheManager;
import com.ironbit.mc.system.image.ImageUtil;

public class Galeria {
	protected Activity ctx = null;
	public static int resIdGaleriaLayout = 0;
	public static int resIdGaleriaItemLayout = 0;
	protected ViewFlipper viewFlipper = null;
	protected OnImagenSeleccionadaListener listener = null;
	protected OnImagenObservadaListener onImagenObservadaListener = null;
	protected LinearLayout linLayGaleria = null;
	protected ArrayList<ImagenItem> imagenes = new ArrayList<ImagenItem>();
	protected int posActual = 0;
	protected static int resIdBitmapDefault = R.drawable.dummy_loader;
	protected static Bitmap defaultImg = null;
	
	
	public static int getImgWidth(Activity ctx){
		return Dispositivo.getWidthPortrait(ctx);
	}
	
	
	public void setOnImagenObservadaListener(OnImagenObservadaListener listener){
		onImagenObservadaListener = listener;
	}
	
	
	public static Bitmap getDefaultImg(Context ctx){
		if (defaultImg == null){
			defaultImg = BitmapFactory.decodeResource(ctx.getResources(), resIdBitmapDefault);
		}
		
		return defaultImg;
	}
	
	
	public int getPosicionActual(){
		return posActual;
	}
	
	
	public static int getResIdGaleriaItemLayout() {
		return resIdGaleriaItemLayout;
	}


	public static void setResIdGaleriaItemLayout(int resIdGaleriaItemLayout) {
		Galeria.resIdGaleriaItemLayout = resIdGaleriaItemLayout;
	}


	public ArrayList<ImagenItem> getImagenes() {
		return imagenes;
	}


	public void setImagenes(ArrayList<ImagenItem> imagenes) {
		this.imagenes = imagenes;
	}


	public ViewFlipper getViewFlipper(){
		if (viewFlipper == null){
			viewFlipper = (ViewFlipper)ctx.getLayoutInflater().inflate(getResIdGaleriaLayout(), null);
		}
		
		return viewFlipper;
	}
	
	
	public static int getResIdGaleriaLayout() {
		return resIdGaleriaLayout;
	}


	public static void setResIdGaleriaLayout(int resIdGaleriaLayout) {
		Galeria.resIdGaleriaLayout = resIdGaleriaLayout;
	}


	public Galeria(final Activity ctx, LinearLayout linLayGaleria, int resIdGaleriaLayout, int resIdGaleriaItemLayout, OnImagenSeleccionadaListener listener){
		this.linLayGaleria = linLayGaleria;
		this.ctx = ctx;
		this.listener = listener;
		setResIdGaleriaLayout(resIdGaleriaLayout);
		setResIdGaleriaItemLayout(resIdGaleriaItemLayout);
		System.out.println("Galeria init");
		//mostramos galeria
		this.linLayGaleria.addView(getViewFlipper());
		
		//inicializamos el viewFlipper con un view chidren
		RelativeLayout vista = (RelativeLayout)ctx.getLayoutInflater().inflate(getResIdGaleriaItemLayout(), null);
		//ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
		
		
		viewFlipper.addView(vista);
	}
	
	
	protected void actualizarVista(View view, ImagenItem item){
		System.out.println("Actualiza Imagen");
		ImageView imagen =(ImageView)view.findViewById(R.id.galeria_imagen);
			
		//asignamos imagen
		imagen.setImageBitmap(item.getImagen());
		
		//asignamos titulo
		((TextView)view.findViewById(R.id.galeria_titulo)).setText(item.getTitulo());
	}
	
	
	public boolean hayImagenSiguiente(){
		return (posActual+1 < getImagenes().size());
	}
	
	
	public void verImagenSiguiente(){
		if (hayImagenSiguiente()){
			//llenamos con los nuevos datos, la vista de enmedio
			actualizarVista(viewFlipper.getChildAt(2), getImagenes().get(posActual+1));
			
			Sys.log("posActual: "+(posActual+1));
			
			//mostramos esta imagen
			viewFlipper.setInAnimation(AnimationUtils.loadAnimation(ctx, R.anim.push_left_in));
			viewFlipper.showNext();
	        
	        //pasamos la primer vista a la ultima
			View view = viewFlipper.getChildAt(0);
			viewFlipper.removeView(view);
			viewFlipper.addView(view);
	        
			//avisamos que se esta mostrando otra imagen
	        onImagenObservada(++posActual);
		}
	}
	
	
	public boolean hayImagenAnterior(){
		return (posActual > 0);
	}
	
	
	public void verImagenAnterior(){
		if (hayImagenAnterior()){
			//llenamos con los nuevos datos, la vista de enmedio
			actualizarVista(viewFlipper.getChildAt(0), getImagenes().get(posActual-1));
			
			Sys.log("posActual: "+(posActual-1));
			
			viewFlipper.setInAnimation(AnimationUtils.loadAnimation(ctx, R.anim.push_right_in));
			viewFlipper.showPrevious();
	        
	        //pasamos la ultima vista a la primera
			View view = viewFlipper.getChildAt(2);
			viewFlipper.removeView(view);
			viewFlipper.addView(view, 0);
			
			viewFlipper.setDisplayedChild(1);
	        
	        //avisamos que se esta mostrando otra imagen
	        onImagenObservada(--posActual);
		}
	}
	
	
	/**
	 * listener que es llamado cuando una imagen es colocada como imagen principal de la galería
	 */
	protected void onImagenObservada(int posicion){
		ArrayList<ImagenItem> items = getImagenes();
		
		for (int a = 0; a < items.size(); a++){
			if (a > posicion-3 && a < posicion+3){
				items.get(a).preparar(ctx);
			}else{
				items.get(a).liberar();
			}
		}
		
		if (onImagenObservadaListener != null){
			onImagenObservadaListener.onImagenObservada(posicion);
		}
	}
	
	
	public ImagenItem getItemActual(){
		return getImagenes().get(posActual);
	}
	
	
	public int getTotalImagenes(){
		return getImagenes().size();
	}
	
	
	public Galeria agregarImagen(String urlImg, String tinyUrl, String titulo){
		ImagenItem itemImg = new ImagenItem(ctx, urlImg);
		itemImg.setTitulo(titulo);
		itemImg.setTinyUrl(tinyUrl);
		
		agregarChildrenToViewFlipper(itemImg);
		getImagenes().add(itemImg);
		
		//preparamos las primeras 3 imagenes de la galería
		if (getImagenes().size() < 4){
			onImagenObservada(posActual);
		}
		
		return this;
	}
	
	
	
	protected void agregarChildrenToViewFlipper(ImagenItem item){
		//el viewFlipper solo tendrá 3 hijos maximo
		System.out.println("Agrega Child");
		if (viewFlipper.getChildCount() < 3){
			RelativeLayout relLay = (RelativeLayout)ctx.getLayoutInflater().inflate(getResIdGaleriaItemLayout(), null);
			viewFlipper.addView(relLay);
			
			//si agrego la primer imagen, la mostramos
			if (viewFlipper.getChildCount() == 2){
				//obtenemos imagen (la primer imagen la descargamos sincronamente)
				Bitmap img = ImageCacheManager.getSyncImagen(ctx, item.getUrl());
				Sys.log("img " + item.getUrl());
				Sys.log("imgOriginal: w=" + img.getWidth() + ", h=" + img.getHeight());
				Sys.log("holderW=" + getImgWidth(ctx));
				
				if (getImgWidth(ctx) < img.getWidth()){
					Sys.log("redimensionamos");
					img = ImageUtil.resizeImageToWidth(img, getImgWidth(ctx));
				}
				
				item.setImagen(img);
				item.setIsPreparado(true);
				
				//asignamos imagen
				ImageView imagen = (ImageView)relLay.findViewById(R.id.galeria_imagen);
				imagen.setImageBitmap(item.getImagen());
				
				imagen.setOnClickListener(new OnClickListener(){

					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						System.out.println("Imagen Click");
						ctx.startActivity(new Intent(ctx, CategoriesActivity.class));
					}
					
				});
				//asignamos titulo
				((TextView)relLay.findViewById(R.id.galeria_titulo)).setText(item.getTitulo());
				
				//centramos la imagen en el widget
				viewFlipper.setDisplayedChild(1);
			}
		}
	}
}