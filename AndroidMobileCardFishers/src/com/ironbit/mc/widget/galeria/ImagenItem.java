package com.ironbit.mc.widget.galeria;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.cache.image.ImageCacheManager;
import com.ironbit.mc.system.cache.image.OnImagenObtenidaListener;
import com.ironbit.mc.system.contador.Contador;
import com.ironbit.mc.system.contador.OnContadorTerminadoListener;
import com.ironbit.mc.system.image.ImageUtil;

public class ImagenItem {
	private String titulo = "";
	private String tinyUrl = "";
	private String url = "";
	private boolean descargada = false;
	private Bitmap imagen = null;
	private boolean isPreparado = false;
	private boolean isPreparando = false;
	private Contador contador = null;
	private Context ctx = null;
	
	
	public String getTinyUrl() {
		return tinyUrl;
	}


	public void setTinyUrl(String tinyUrl) {
		this.tinyUrl = tinyUrl;
	}


	public void setIsPreparado(boolean isPreparado){
		this.isPreparado = isPreparado;
	}
	
	
	public boolean isPreparado(){
		return isPreparado;
	}
	
	
	public boolean isPreparando(){
		return isPreparando;
	}
	
	
	public String getTitulo() {
		return titulo;
	}
	
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	
	public String getUrl() {
		return url;
	}
	
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public boolean isDescargada() {
		return descargada;
	}
	
	
	public void setDescargada(boolean descargada) {
		this.descargada = descargada;
	}
	
	
	public Bitmap getImagen() {
		if (imagen == null){
			return Galeria.getDefaultImg(ctx);
		}
		
		return imagen;
	}


	public void setImagen(Bitmap imagen) {
		this.imagen = imagen;
	}

	
	/**
	 * Contructor
	 */

	public ImagenItem(Context ctx, String url){
		this.ctx = ctx;
		setUrl(url);
	}
	
	
	/**
	 * Metodos
	 */
	
	public void preparar(final Context ctx){
		if (!isPreparando && !isPreparado){
			Sys.log("preparando "+getTitulo());
			isPreparando = true;
			
			contador = new Contador(new Handler(), new OnContadorTerminadoListener() {
				public void onContadorTerminado() {
					isPreparando = false;
					ImageCacheManager.cancelarObtencionImagen(getUrl());
				}
			}, 20, getTitulo());
			
			//obtenemos imagen
			ImageCacheManager.getImagen(ctx, new OnImagenObtenidaListener() {
				public void onImagenObtenida(Bitmap imagen) {
					int widthHolder = Galeria.getImgWidth((Activity)ctx);
					
					Sys.log("img " + getUrl());
					Sys.log("imgOriginal: w=" + imagen.getWidth() + ", h=" + imagen.getHeight());
					Sys.log("holderW=" + widthHolder);
					
					if (widthHolder < imagen.getWidth()){
						Sys.log("redimensionando");
						imagen = ImageUtil.resizeImageToWidth(imagen, widthHolder);
					}
					
					setImagen(imagen);
					
					if (contador != null){
						contador.cancelar();
					}
					
					isPreparado = true;
					isPreparando = false;
				}
			}, getUrl());
		}
	}
	
	
	/**
	 * Libera la memoria ocupada por la imagen de este item
	 */
	public void liberar(){
		Sys.log("liberando "+getTitulo());
		
		//cancelamos la descarga de la imagen (si esque había)
		ImageCacheManager.cancelarObtencionImagen(getUrl());
		
		//cancelamos el contador si esque había
		if (contador != null){
			contador.cancelar();
		}
		
		imagen = null;
		isPreparado = false;
		isPreparando = false;
	}
}