package com.ironbit.mc.widget.galeria;

public interface OnImagenSeleccionadaListener {
	public void onImagenSeleccionada(int posicion);
}