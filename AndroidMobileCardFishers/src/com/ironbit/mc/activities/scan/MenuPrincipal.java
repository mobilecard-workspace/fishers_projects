package com.ironbit.mc.activities.scan;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.activities.MenuAppActivity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;

public class MenuPrincipal extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_menu_principal);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {						
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_principal, menu);		
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.item1:
	        	finish();
	            Intent Escanear = new Intent (this, QR_reader_actividad.class);
	            Escanear.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            startActivity(Escanear);
	            return true;
	        case R.id.item2:	 
	        	finish();
	        	Intent Consultar = new Intent (this, Consulta_Pagos.class);
	        	Consultar.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        	startActivity(Consultar);
	            return true;
	        case R.id.item3:	        	
	        	Intent intent = new Intent(getApplicationContext(), MenuAppActivity.class);
	        	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        	intent.putExtra("EXIT","si");
	        	startActivity(intent);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	@Override
		public void onAttachedToWindow() {
		    openOptionsMenu(); 
		};
	
	

}
