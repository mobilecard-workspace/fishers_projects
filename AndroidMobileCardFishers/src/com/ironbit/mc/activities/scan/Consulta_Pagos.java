package com.ironbit.mc.activities.scan;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ironbit.mc.R;
import com.ironbit.mc.ctrlista.ItemPago;
import com.ironbit.mc.ctrlista.ItemPagoAdapter;
import com.ironbit.mc.mbd.PagosDataSource;
import com.ironbit.mc.mbd.pagos;

public class Consulta_Pagos extends Activity {
	
	//private pagos qpayments = new pagos();
	private PagosDataSource DSpagos; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
					
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_consulta__pagos);
		/*
		Bundle res = getIntent().getExtras();
		String otra = res.getString("mensaje");
	    if(otra.equals("restart")){
	    	finish();
	    	startActivity(getIntent());
	    }
	    */
		
		DSpagos = new PagosDataSource(Consulta_Pagos.this);
		DSpagos.Open();
		
		List<pagos> values = DSpagos.PaymentsList();
		ListView lista=(ListView) findViewById(R.id.listapagos);
		
		System.out.println("Numero de pagos: "+values.size());
		
		ArrayList<ItemPago> itemsPay = obtenerItems(values);
		
		ItemPagoAdapter adapter = new ItemPagoAdapter(this, itemsPay);
        
	    lista.setAdapter(adapter);
	   	
	    //ImageView lupe = (ImageView) findViewById(R.id.imageLupe);
	    
	    lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id){
	        	System.out.println(position);
	        	tostada(id);
	        }
	    });
	    
	    DSpagos.Close();	    
	}
	/*
	 * Obtenecion de elementos para el adaptador hecho a medida
	 */
	private ArrayList<ItemPago> obtenerItems(List<pagos> a) {
		
	    ArrayList<ItemPago> items = new ArrayList<ItemPago>();
	    Iterator<pagos> iter = a.iterator();	    
	         while(iter.hasNext()){	     	        	
	        	 pagos p = (pagos) iter.next();
	        	 items.add(new ItemPago(p.getId(),p.getEstablecimiento() , p.getFechahora()));	        	
	         }	         	   	         
	    return items;
	  }
	/*
	 * Despliegue de dialog box para mostrar el detalle de la compra
	 */
	public void tostada(long id){
		 pagos detail = new pagos();
		 detail = DSpagos.DetallePago(id);
		 
		 AlertDialog.Builder Detalles = new AlertDialog.Builder(Consulta_Pagos.this);
		 Detalles.setTitle("Pagaste en: "+detail.getEstablecimiento());
		 		
		 String [] dp = { detail.getFechahora(),
				 		 "Monto: $ "+String.valueOf(detail.getMonto()),
				 		 "Propina: $ "+String.valueOf(detail.getPropina()),
				 		 "No. autorización: "+String.valueOf(detail.getEstatus())};
		
		Detalles.setItems(dp, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            // The 'which' argument contains the index position
            // of the selected item
        }
 });
		 Detalles.setPositiveButton(R.string.ok_tag, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	               
	           }
	       });
		 Detalles.create();
		 Detalles.show();		 
	 }
	@Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	finish();
            startActivity(new  Intent(this, MenuPrincipal.class));
            finish();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

	/*
	 * cerrando las conexiones a la base de datos
	 */
	@Override
	public void onResume(){
		DSpagos.Open();
		super.onResume();
	}
	@Override 
	public void onPause(){
		DSpagos.Close();
		super.onPause();
	}

}
