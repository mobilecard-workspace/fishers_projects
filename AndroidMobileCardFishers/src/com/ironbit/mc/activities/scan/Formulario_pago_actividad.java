package com.ironbit.mc.activities.scan;

import com.ironbit.mc.R;
import com.ironbit.mc.httpclient.PayForm_process;
import com.ironbit.mc.httpclient.PayForm_process.OnExecuteAsyncPostListener;
import com.ironbit.mc.mbd.UsuariosDataSource;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.crypto.Crypto;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class Formulario_pago_actividad extends Activity {
	private PayForm_process paying = new PayForm_process ();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
	
		new UsuariosDataSource (this);
		
		paying.setOnExecuteAsyncPostListener(new OnExecuteAsyncPostListener() {
			   public void onFinishAsyncPost(String message) {				   
			      Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();  
			   }
			   
			   public void onErrorAsyncPost(String message) {
			    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();  
			   }
			  });
		
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_formulario_pago_actividad);
		
		
		
		Bundle recibe = getIntent().getExtras();
		String cadenaQR = recibe.getString("mensaje");
		
		//Decodificacion y separacion de los datos
		String[] QRident = cadenaQR.split("\\?"); 
		if (QRident.length>1){
		String decContent = Crypto.aesDecrypt(Text.parsePass("xCL8m39sJ1"), QRident[1]);
		//System.out.println("Esta es la cadena: "+decContent);
		//el string que hace el split de lo que decodificaremos
		String [] QRdecode = decContent.split("\\|");
		
		if (QRdecode.length == 3){
			System.out.println("Mi array es: "+QRdecode[2]);
		
			paying.setAfiliacion(Long.parseLong(QRdecode[0]));
			paying.setEstablecimiento(QRdecode[1].toString());
			paying.setoAfiliacion(Long.parseLong(QRdecode[2]));
			
			
			TextView leyend = (TextView) findViewById(R.id.textView1);
			leyend.setText(QRdecode[1]);
			
			
			Button buttonPay = (Button) findViewById(R.id.button_pay);
			buttonPay.setOnClickListener(new OnClickListener (){
			public void onClick(View v) {
					 
					 final EditText monto = (EditText) findViewById(R.id.editText2);
					 //System.out.println("VALOR DEL CAMPO DE NUMEROS: "+monto.getText().toString());
					 if (monto.getText().toString().equals("")){
						 paying.setMonto((double) 0.00);
						 //System.out.println("VALOR DEL CAMPO DE NUMEROS: "+paying.getMonto());
					 }else{
						 paying.setMonto(Double.valueOf(monto.getText().toString()));
					 }				 				 
					 final EditText propina = (EditText) findViewById(R.id.editText3);
					 if (propina.getText().toString().equals("")){
							paying.setPropina((double) 0.00);
					 }else{
							paying.setPropina(Double.valueOf(propina.getText().toString()));
					 }
					 final EditText referencia = (EditText) findViewById(R.id.editText4);
					 if (referencia.getText().toString().equals("")){
							paying.setReferencia("N/A");
					}else{
							paying.setReferencia(referencia.getText().toString());
					}					 
					 final EditText pass = (EditText) findViewById(R.id.editText1);
					if(pass.getText().toString().equals("")){
						paying.setPass("a");
					}else{
						paying.setPass(pass.getText().toString());
					}
					final EditText cvv = (EditText) findViewById(R.id.editText10);
					if(cvv.getText().toString().equals("")){
						paying.setCvv("");
					}else{
						paying.setCvv(cvv.getText().toString());
					}
					
					
					
					/*
					 * validacion de existencia de password correcto
					 */
					/*Dsusern.Open();
					String passBD = Dsusern.obtenUsr().getPass();
					String passFld = Crypto.aesEncrypt(Text.parsePass("xCL8m39sJ1"), paying.getPassword().toString());
					Dsusern.Close();*/
					//System.out.println(paying.getMonto());
					
					if(paying.getMonto().equals(0.0) || paying.getPassword().toString().equals("")){
												
						Toast notificacion4 = Toast.makeText(getApplicationContext(), R.string.mensaje_log,Toast.LENGTH_LONG);
						 notificacion4.show();
						 
					}else{
							/*if (!passFld.equals(passBD)){
								Toast notificacionx = Toast.makeText(getApplicationContext(), R.string.erro_pass,Toast.LENGTH_LONG);
								 notificacionx.show();
								 pass.setText("");
							}else{*/
								//paying.makePayment(Formulario_pago_actividad.this);
								String[] dconf = {"Monto: $"+paying.getMonto().toString(),
												  "Propina: $"+paying.getPropina().toString()};
								
								AlertDialog.Builder Confirma = new AlertDialog.Builder(Formulario_pago_actividad.this);
								Confirma.setTitle(R.string.pago_detail);
								Confirma.setItems(dconf, new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog, int which) {						            
						            }
								});
								/*
								 * Bot�n confirmar 
								 */
								Confirma.setPositiveButton(R.string.cont_tag, new DialogInterface.OnClickListener() {
							           public void onClick(DialogInterface dialog, int id) {
							        	   
							        	   paying.makePayment(Formulario_pago_actividad.this);
																		        								               							               
											monto.setText("");
											propina.setText("");
											referencia.setText("");
											pass.setText("");
											
											
											/*
											Intent consulta = new Intent (Formulario_pago_actividad.this,Consulta_Pagos.class);											
											consulta.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);								
											startActivity(consulta);
											 */
							           }
							       });
								/*
								 * Bot�n Cancelar
								 */
								Confirma.setNegativeButton(R.string.cancel_tag, new DialogInterface.OnClickListener() {
							           public void onClick(DialogInterface dialog, int id) {
							        	   monto.setText("");
							           }
							       });
								
								Confirma.create();
								Confirma.show();
							}
							
					}
					
					
				 }
			 //}
		);
		}else{
			
			Intent regreso = new Intent(Formulario_pago_actividad.this,MenuPrincipal.class);
			regreso.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(regreso);
			
			Toast notificacion5 = Toast.makeText(getApplicationContext(), R.string.qr_fail,Toast.LENGTH_LONG);
			 notificacion5.show();	
		}
		
		}else{
			Intent regreso = new Intent(Formulario_pago_actividad.this,MenuPrincipal.class);
			regreso.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(regreso);
			
			Toast notificacion5 = Toast.makeText(getApplicationContext(), R.string.qr_fail,Toast.LENGTH_LONG);
			 notificacion5.show();
		}										
	/*
	 * Definicion de toast de ayuda para los campos del formulario
	 * 	
	 */
		(findViewById(R.id.textView2)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), getString(R.string.help_pay1), Toast.LENGTH_SHORT).show();
				
			}
		});
		
		(findViewById(R.id.textView7)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), getString(R.string.help_pay2), Toast.LENGTH_SHORT).show();
				
			}
		});
		(findViewById(R.id.textView8)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), getString(R.string.help_pay3), Toast.LENGTH_SHORT).show();
				
			}
		});
		
		(findViewById(R.id.textView9)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), getString(R.string.help_pay4), Toast.LENGTH_SHORT).show();
				
			}
		});
		
		(findViewById(R.id.textView11)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es Cvv2?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				View vista = getLayoutInflater().inflate(R.layout.galeria_imagen, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tarjetacvv);
				alert.setView(vista);
				AlertDialog alertDialog = alert.create();
				alertDialog.show();
			}
		});
	/*
	 * Fin de definicion de actividades de la interrogacion
	 */
	}	
	
	@Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	finish();
            startActivity(new  Intent(this, MenuPrincipal.class));
            finish();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
	
}
