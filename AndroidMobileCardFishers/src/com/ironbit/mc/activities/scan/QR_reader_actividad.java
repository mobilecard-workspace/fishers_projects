package com.ironbit.mc.activities.scan;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MainActivity;
import com.ironbit.mc.activities.ProductsActivity;
import com.ironbit.mc.activities.ViapassActivity;
import com.ironbit.mc.qrreader.CameraPreview;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.crypto.Crypto;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;


import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Button;

import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;

import android.widget.TextView;

/* Import ZBar Class files */
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import net.sourceforge.zbar.Config;

public class QR_reader_actividad extends Activity
{
    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;      

    TextView scanText;
    Button scanButton;

    ImageScanner scanner;

    @SuppressWarnings("unused")
	private boolean barcodeScanned = false;
    private boolean previewing = true;

    static {
        System.loadLibrary("iconv");
    } 

    @Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        QR_reader_actividad.this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.activity_qr_reader_actividad);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        autoFocusHandler = new Handler();
        
        mCamera = getCameraInstance();

        /* Instance barcode scanner */
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
        FrameLayout preview = (FrameLayout)findViewById(R.id.cameraPreview);
        preview.addView(mPreview);

        scanText = (TextView)findViewById(R.id.scanText);

        scanButton = (Button)findViewById(R.id.ScanButton);

        scanButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                	finish();
                	Intent menu = new Intent(QR_reader_actividad.this,MenuPrincipal.class);
                	menu.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(menu);
                    /*if (barcodeScanned) {
                        barcodeScanned = false;
                        scanText.setText("Scanning...");
                        mCamera.setPreviewCallback(previewCb);
                        mCamera.startPreview();
                        previewing = true;
                        mCamera.autoFocus(autoFocusCB);
                    }*/
                }
            });
    }
    /*
    @Override
    public void onAttachedToWindow() {
        // TODO Auto-generated method stub
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
        super.onAttachedToWindow();
    }*/
    @Override
    public void onPause() {
    	releaseCamera();
        super.onPause();        
    }
    @Override
    public void onStop() {
    	releaseCamera();
        super.onPause();        
    }
                
    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e){
        }
        return c;
    }

    private void releaseCamera() {
    	
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);      
            //mPreview.getHolder().removeCallback(mPreview);
            mCamera.release();
            mCamera = null;
        }
    }

    private Runnable doAutoFocus = new Runnable() {
            public void run() {
                if (previewing)
                    mCamera.autoFocus(autoFocusCB);
            }
        };

    PreviewCallback previewCb = new PreviewCallback() {
            public void onPreviewFrame(byte[] data, Camera camera) {
                Camera.Parameters parameters = camera.getParameters();
                Size size = parameters.getPreviewSize();

                Image barcode = new Image(size.width, size.height, "Y800");
                barcode.setData(data);

                int result = scanner.scanImage(barcode);
                
                if (result != 0) {
                    previewing = false;
                    mCamera.setPreviewCallback(null);
                    mCamera.stopPreview();
                    
                    SymbolSet syms = scanner.getResults();
                    for (Symbol sym : syms) {
                        //scanText.setText("barcode result " + sym.getData());
                        if (sym.getData().toString() != null){
                        	
                        	///CAMBIO PARA PROCESAR QR'S CON VIAPASS, BORRAR SI NO FUNCIONA
                        	
                        	String[] QRident = sym.getData().toString().split("\\?");
                        	if(QRident.length > 1){
                        		String decContent = Crypto.aesDecrypt(Text.parsePass("xCL8m39sJ1"),QRident[1]);
                        		System.out.println(":::QR DECODIFICADO:::" + decContent);
                				if(decContent.contains("viapass")){
                					String[] contArr = decContent.split("\\|"); 
                					MainActivity.setAppConf(QR_reader_actividad.this, ViapassActivity.CONF_KEY_CATEGORIA, "TELEPEAJE"); 
                					MainActivity.setAppConf(QR_reader_actividad.this, ViapassActivity.CONF_KEY_PROVEEDOR, contArr[1].toUpperCase());
                					MainActivity.setAppConf(QR_reader_actividad.this, ViapassActivity.CONF_KEY_PRODUCTO, contArr[4]);
                					MainActivity.setAppConf(QR_reader_actividad.this, ViapassActivity.CONF_KEY_PRODUCTO_CLAVE, contArr[3]);
                					MainActivity.setAppConf(QR_reader_actividad.this, ProductsActivity.CONF_KEY_ID_PROVEEDOR, "7");
                					startActivity(new Intent(QR_reader_actividad.this, ViapassActivity.class));
                				}
                				else{
                					Intent formPay = new Intent (QR_reader_actividad.this,Formulario_pago_actividad.class);
                                	Bundle bound = new Bundle();
                                	String message = sym.getData().toString();
                                	bound.putString("mensaje", message);
                                    formPay.putExtras(bound);
                                	startActivity(formPay);
                				}
                        	}
                        	
                        	/*****************************/
                        	
                        	/*
                        	 * QUITAR COMMENTS SI NO FUNCIONA
                        	Intent formPay = new Intent (QR_reader_actividad.this,Formulario_pago_actividad.class);
                        	Bundle bound = new Bundle();
                        	String message = sym.getData().toString();
                        	bound.putString("mensaje", message);
                            formPay.putExtras(bound);
                        	startActivity(formPay); 
                        	*/                       	
                        }//if   
                        barcodeScanned = true;
                    }                    
                }
            }
        };

    // Mimic continuous auto-focusing
    AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
            public void onAutoFocus(boolean success, Camera camera) {
                autoFocusHandler.postDelayed(doAutoFocus, 1000);
            }
        };
}
