package com.ironbit.mc.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.GetTipoRecargaTagWS;
import com.ironbit.mc.web.webservices.SetTagWSClient;
import com.ironbit.mc.web.webservices.data.DataTipoRecargaTag;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnTipRecargaTagReceivedListener;

public class InsertTagActivity extends MenuActivity {
	
	protected LongProcess loadDataProcess = null;
	protected LongProcess setTagProcess = null;
	protected GetTipoRecargaTagWS ws= null;
	protected SetTagWSClient wsSet= null;
	protected DataTipoRecargaTag[] lista = null;
	protected Spinner spinTipos = null;
	protected EditText txtNick = null, txtNum = null, txtVerif=null ;
	protected TextView lblVerif = null;
	protected FormValidator formValidator = null;
	protected long idUser = 0;
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.tag);
		
		spinTipos = (Spinner) findViewById(R.id.tagTipo);
		txtNick = (EditText) findViewById(R.id.tagNick);
		txtNum = (EditText) findViewById(R.id.tagNum);
		txtVerif = (EditText) findViewById(R.id.tagVerif);
		lblVerif = (TextView) findViewById(R.id.tagLblVerif);
		
		idUser = getIntent().getLongExtra("idUser", 0);
		View btn = findViewById(R.id.btnTagCancelar);
		findViewById(R.id.btnTagGuardar).setBackgroundDrawable(getResources().getDrawable(R.drawable.selector_guardar));
		
		((ViewManager)btn.getParent()).removeView(btn);
		
		
		findViewById(R.id.btnTagGuardar).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (formValidator.validate()){
					setTagProcess.start();
				}
			}
			
		});
		
		spinTipos.setOnItemSelectedListener(new OnItemSelectedListener(){

			
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				DataTipoRecargaTag item = (DataTipoRecargaTag)parent.getSelectedItem();
				System.out.println(item.getNombre() + " - " + item.getClave());
				if(item.getClave().trim().equals("2")){
					txtVerif.setText("");
					lblVerif.setText(getString(R.string.confTag));
					
					addOhlListener();
					
				}else if(item.getClave().trim().equals("3")){
					txtVerif.setText("");
					lblVerif.setText(getString(R.string.num_cliente));
					addViapassListener();
				}else{
					txtVerif.setText("");
					//txtVerif.setEnabled(true);
					lblVerif.setText(getString(R.string.nverif));
					addIaveListener();
				}
				
				configValidations();
			}

			
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		
		(findViewById(R.id.tagLblEti)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(arg0.getContext(), getString(R.string.eti_help), Toast.LENGTH_LONG).show();
			}
			
		});
		
		//addIaveListener();
		
		loadDataProcess = new LongProcess(this, true, 60, true, "LoadTipoRecargaTag", new LongProcessListener(){
			public void doProcess() {
				//inicializamos proveedores
				ws = new GetTipoRecargaTagWS(InsertTagActivity.this);
				ws.execute(new OnTipRecargaTagReceivedListener(){
					
					
					
					public void onTipoRecargaTagReceivedListener(
							ArrayList<DataTipoRecargaTag> listaTipos) {
						// TODO Auto-generated method stub
						
						lista = new DataTipoRecargaTag[listaTipos.size()];
						
						for (int i = 0; i < listaTipos.size(); i++) {
							lista[i] = listaTipos.get(i);
						}
						
						ArrayAdapter<DataTipoRecargaTag> adapter = new ArrayAdapter<DataTipoRecargaTag>(getApplicationContext(), 
								android.R.layout.simple_spinner_dropdown_item, 
								lista);
						
						spinTipos.setAdapter(adapter);
						loadDataProcess.processFinished();
						configValidations();
					}
				});
			}
			
			public void stopProcess() {
				if (ws != null){
					ws.cancel();
					ws = null;
				}
				
				if (ws != null){
					ws.cancel();
					ws = null;
				}
				
				if (ws != null){
					ws.cancel();
					ws = null;
				}
			}
		});
		
		setTagProcess = new LongProcess(this, true, 60, true, "SetTipoRecargaTag", new LongProcessListener(){
			public void doProcess() {
				//inicializamos proveedores
				wsSet = new SetTagWSClient(InsertTagActivity.this);
				
				DataTipoRecargaTag item = (DataTipoRecargaTag) spinTipos.getSelectedItem();
				wsSet.setUsuario(idUser);
				int idTag = Integer.parseInt(item.getClave());
				wsSet.setTipotag(idTag);
				wsSet.setEtiqueta(txtNick.getText().toString());
				wsSet.setNumero(txtNum.getText().toString());
				
				if(idTag == 2){
					wsSet.setDV(1);
				}else{
					wsSet.setDV(Integer.parseInt(txtVerif.getText().toString()));
				}
				wsSet.execute(new OnGeneralWSResponseListener(){

					
					public void onGeneralWSErrorListener(String error) {
						// TODO Auto-generated method stub
						
						setTagProcess.processFinished();
						Toast.makeText(InsertTagActivity.this, error, Toast.LENGTH_SHORT).show();
					}

					
					public void onGeneralWSResponseListener(String response) {
						// TODO Auto-generated method stub
						
						setTagProcess.processFinished();
						Toast.makeText(InsertTagActivity.this, response, Toast.LENGTH_LONG).show();
						finish();
					}
					
				});
			}
			
			public void stopProcess() {
				if (wsSet != null){
					wsSet.cancel();
					wsSet = null;
				}
				
				if (wsSet != null){
					wsSet.cancel();
					wsSet = null;
				}
				
				if (wsSet != null){
					wsSet.cancel();
					wsSet = null;
				}
			}
		});
		
		loadDataProcess.start();
		
		
	}

	private void addIaveListener() {
		
		lblVerif.setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 AlertDialog alertDialog;
				 
					Builder alert = new AlertDialog.Builder(v.getContext());
					alert.setTitle("�Qu� es N�mero Verificador?");
					alert.setIcon(R.drawable.dlg_alert_icon);
					
					View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
					ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
					imagen.setImageResource(R.drawable.dviave);
					
					ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
					imagen2.setImageResource(R.drawable.iave);
					
					alert.setView(vista);
					alertDialog = alert.create();
					alert.show();
			}
			
		});
		InputFilter[] filter = {new InputFilter.LengthFilter(1)};
		txtVerif.setFilters(filter);
		
		InputFilter[] filter2 = {new InputFilter.LengthFilter(14)};
		txtNum.setFilters(filter2);
		//txtVerif.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		(findViewById(R.id.tagLblNum)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tagiave);
				
				ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
				imagen2.setImageResource(R.drawable.iave);
				
				alert.setView(vista);
				
				AlertDialog alertDialog = alert.create();
				alert.show();
			}
			
		});
		
	}
	
	private void addOhlListener() {
		lblVerif.setOnClickListener(null);
		
		InputFilter[] filter = {new InputFilter.LengthFilter(14)};
		txtVerif.setFilters(filter);
		
		InputFilter[] filter2 = {new InputFilter.LengthFilter(14)};
		txtNum.setFilters(filter2);
		
		//txtVerif.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		(findViewById(R.id.tagLblNum)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_1, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tag_ohl);
								
				alert.setView(vista);
				
				AlertDialog alertDialog = alert.create();
				alert.show();
			}
			
		});
		
	}
	
	private void addViapassListener() {
		lblVerif.setOnClickListener(null);
		
		InputFilter[] filter = {new InputFilter.LengthFilter(8)};
		txtVerif.setFilters(filter);
		
		InputFilter[] filter2 = {new InputFilter.LengthFilter(12)};
		txtNum.setFilters(filter2);
		
		//txtVerif.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		(findViewById(R.id.tagLblNum)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 AlertDialog alertDialog;
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tagviapass1);
			
				ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
				imagen2.setImageResource(R.drawable.tagviapass2);
				
				alert.setView(vista);
				
				alertDialog = alert.create();
				alert.show();
			}
			
		});
		
	}

	@Override
	public void configValidations() {
		formValidator = new FormValidator(this, true);
		int clave = 1;
		if(spinTipos != null){
			DataTipoRecargaTag item = (DataTipoRecargaTag) spinTipos.getSelectedItem();
			clave = Integer.parseInt(item.getClave());
			
		
			if(clave == 1){
				formValidator.addItem(txtNick, "Etiqueta")
					.canBeEmpty(false)
					.maxLength(12);
				
				formValidator.addItem(txtNum, "Numero de Tag")
					.canBeEmpty(false)
					.maxLength(14)
					.isNumeric(true)
					.minLength(4)
					.isRequired(true);
				
				formValidator.addItem(txtVerif, "Numero Verificador")
				.canBeEmpty(false)
				.maxLength(1)
				.isNumeric(true)
				.isRequired(true);
			}
			
			if(clave == 2){
				formValidator.addItem(txtNick, "Etiqueta")
				.canBeEmpty(false)
				.maxLength(12);
			
				formValidator.addItem(txtNum, "Numero de Tag")
					.canBeEmpty(false)
					.isRequired(true)
					.maxLength(14)
					.isNumeric(true)
					.minLength(4);
				
				formValidator.addItem(txtVerif, "Confirmar Tag")
				.canBeEmpty(false)
				.isRequired(true)
				.maxLength(14)
				.isNumeric(true);
				
				formValidator.addItem(txtVerif, "Confirmar Tag", new Validable(){
					public void validate() throws ErrorSys {
						
						if (!txtVerif.getText().toString().equals(txtNum.getText().toString())){
							txtVerif.requestFocus();
							throw new ErrorSys("Los n�meros de Tag no coinciden");
						}else{
							txtVerif.setText("0");
						}
					}
				});
			}
			
			if(clave == 3){
				formValidator.addItem(txtNick, "Etiqueta")
				.canBeEmpty(false)
				.maxLength(12);
			
			formValidator.addItem(txtNum, "Numero de Tag")
				.canBeEmpty(true)
				.maxLength(12)
				.isNumeric(true)
				.minLength(4);
			
			formValidator.addItem(txtVerif, "Numero de Cliente")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(8)
			.isNumeric(true)
			.minLength(4);
			
			}
		}
	}
	
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.INSERT_TAG;
	}

}
