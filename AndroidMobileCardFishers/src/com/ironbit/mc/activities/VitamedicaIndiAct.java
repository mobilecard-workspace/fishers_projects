package com.ironbit.mc.activities;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.GetEdoCivilWSClient;
import com.ironbit.mc.web.webservices.GetUserWSClient;
import com.ironbit.mc.web.webservices.events.OnBanksResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnUserGetResponseReceivedListener;
import com.ironbit.mc.widget.SpinnerValues;
import com.ironbit.mc.widget.datepickerdialog.MyDatePickerDialog;

public class VitamedicaIndiAct extends MenuActivity {
	public static final String CONF_KEY_CATEGORIA = "adc_resumen_categoria";
	public static final String CONF_KEY_PROVEEDOR = "adc_resumen_proveedor";
	public static final String CONF_KEY_PRODUCTO = "adc_resumen_producto";
	public static final String CONF_KEY_PRODUCTO_CLAVE = "adc_resumen_producto_clave";
	public static final int CONF_DIALOG = 1;
	long idUser =0;
	
	protected final FormValidator formValidator = new FormValidator(this, true);
	
	protected EditText txtNombre = null;
	protected EditText txtAPat = null;
	protected EditText txtAMat= null;
	protected Button btnFechaNacimiento = null;
	protected Button backButton;
	protected EditText txtDireccion = null;
	protected EditText txtCelular = null;
	protected EditText txtMail = null;
	protected EditText txtTelCasa = null;
	//protected EditText txtTelOficina = null;
	protected EditText txtCalle= null;
	protected EditText txtNumExt = null;
	protected EditText txtNumFam = null;
	protected EditText txtNumInt= null;
	protected EditText txtColonia= null;
	protected EditText txtCP= null;
	
	protected Spinner cmbSexo= null;
	protected Spinner cmbEdoCivil= null;
	protected SpinnerValues cmbSVEstadoC= null;
	
	protected MyDatePickerDialog fechaNacimientoDialog = null;
	protected Fecha fechaNacimiento = null;
	
	protected LongProcess loadDataProcess = null;
	protected GetUserWSClient userGet = null;
	protected GetEdoCivilWSClient edoCivilGet = null;
	
	private String clave;
	private String claveWS;
	private String descripcion;
	private String categoria;
	private String claveProveedor;
	private String proveedor;
	
	private static final String TAG = "VitamedicaIndiAct";
	
	protected MyDatePickerDialog getFechaNacimientoDialog(){
		if (fechaNacimientoDialog == null){
			fechaNacimientoDialog = new MyDatePickerDialog(this, new DatePickerDialog.OnDateSetListener(){
				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
					try{
						fechaNacimiento.setAnio(year);
						fechaNacimiento.setMesBase0(monthOfYear);
						fechaNacimiento.setDia(dayOfMonth);
						fechaNacimiento.actualizar();
						
						updateFechaNacimiento();
					}catch (ErrorSys e) {
						e.showMsj(VitamedicaIndiAct.this);
					}
				}
			},fechaNacimiento.getAnio() ,fechaNacimiento.getMesBase0(), fechaNacimiento.getDia());
		}
		return fechaNacimientoDialog;
	}
	
	protected EditText getTxtMail(){
		if (txtMail == null){
			txtMail= (EditText)findViewById(R.id.txtEmail);
		}
		
		return txtMail;
	}
	
	protected EditText getTxtTelCasa(){
		if (txtTelCasa == null){
			txtTelCasa= (EditText)findViewById(R.id.txtTelCasa);
		}
		
		return txtTelCasa;
	}
	
	/*protected EditText getTxtTelOficina(){
		if (txtTelOficina == null){
			txtTelOficina= (EditText)findViewById(R.id.txtTelOficina);
		}
		
		return txtTelOficina;
	}*/
	

	
	protected EditText getTxtCalle(){
		if (txtCalle == null){
			txtCalle = (EditText)findViewById(R.id.txtCalle);
		}
		
		return txtCalle;
	}
	
	protected EditText getTxtColonia(){
		if (txtColonia == null){
			txtColonia = (EditText)findViewById(R.id.txtColonia);
		}
		
		return txtColonia;
	}
	
	protected EditText getTxtCP(){
		if (txtCP == null){
			txtCP = (EditText)findViewById(R.id.txtCP);
		}
		
		return txtCP;
	}
	
	protected EditText getTxtNumExt(){
		if (txtNumExt== null){
			txtNumExt= (EditText)findViewById(R.id.txtNumExt);
		}
		
		return txtNumExt;
	}
	
	protected EditText getTxtNumFam(){
		if (txtNumFam== null){
			txtNumFam= (EditText)findViewById(R.id.txtNumFam);
		}
		
		return txtNumFam;
	}
	
	protected EditText getTxtNumInt(){
		if (txtNumInt== null){
			txtNumInt= (EditText)findViewById(R.id.txtNumInt);
		}
		
		return txtNumInt;
	}
	
	protected EditText getTxtNombre(){
		if (txtNombre == null){
			txtNombre = (EditText)findViewById(R.id.txtNombre);
		}
		
		return txtNombre;
	}
	
	
	protected EditText getTxtApellidoPaterno(){
		if (txtAPat == null){
			txtAPat = (EditText)findViewById(R.id.txtApellidoP);
		}
		
		return txtAPat;
	}
	
	protected EditText getTxtApellidoMaterno(){
		if (txtAMat == null){
			txtAMat = (EditText)findViewById(R.id.txtApellidoM);
		}
		
		return txtAMat;
	}
	
	protected EditText getTxtCelular(){
		if (txtCelular == null){
			txtCelular = (EditText)findViewById(R.id.txtNoCelular);
		}
		
		return txtCelular;
	}
	
	protected Spinner getCmbSexo(){
		if (cmbSexo == null){
			cmbSexo = (Spinner)findViewById(R.id.spin_sexo);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
		            this, R.array.sexo, android.R.layout.simple_spinner_item);
		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    cmbSexo.setAdapter(adapter);
		}
		
		return cmbSexo;
	}
	
	protected Spinner getCmbEdoCivil(){
		if (cmbEdoCivil== null){
			cmbEdoCivil = (Spinner)findViewById(R.id.spin_estado_civil);
			/*ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
		            this, R.array.edo_civil, android.R.layout.simple_spinner_item);
		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    cmbEdoCivil.setAdapter(adapter);*/
		}
		
		return cmbEdoCivil;
	}
	
	protected SpinnerValues getCmbSVEstadoC(){
		if (cmbSVEstadoC== null){
			cmbSVEstadoC =  new SpinnerValues(this, getCmbEdoCivil());
			
		}
		
		return cmbSVEstadoC;
	}
	
	protected Button getBtnFechaNacimiento(){
		if (btnFechaNacimiento == null){
			btnFechaNacimiento = (Button)findViewById(R.id.btnFechaNacimiento);
		}
		
		return btnFechaNacimiento;
	}
	
	protected Button getBackButton() {
		if (null == backButton)
			backButton = (Button) findViewById(R.id.button_back);
		
		return backButton;
	}
		
	protected void updateFechaNacimiento(){
		if (fechaNacimiento != null){
			getBtnFechaNacimiento().setText("Seleccionar Fecha [" + fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYY) + "]");
		}
	}
	@Override
	public void configValidations() {
		
		formValidator.addItem(getTxtNombre(), "Nombre")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(50)
		.minLength(3);
	
	
	
	formValidator.addItem(getTxtApellidoPaterno(), "Apellido Paterno")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(50);
	formValidator.addItem(getTxtApellidoPaterno(), "Apellido Materno")
		.canBeEmpty(true)
		.isRequired(false)
		.maxLength(50);
		
		formValidator.addItem(getCmbSexo(), "Sexo", new Validable(){

			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				//System.out.println("Sex item pos: "+getCmbSexo().getSelectedItemPosition());
				
				if(getCmbSexo().getSelectedItemPosition()==0){
					throw new ErrorSys("Debe seleccionar su sexo");
				}
			}
			
		});
		
		formValidator.addItem(getCmbEdoCivil(), "Estado Civil", new Validable(){

			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				/*if(getCmbEdoCivil().getSelectedItemPosition()==0){
					throw new ErrorSys("Debe seleccionar su estado civil");
				}*/
			}
			
		});
		
		formValidator.addItem(getTxtTelCasa(), "Tel�fono Casa")
		.canBeEmpty(true)
		.isRequired(false)
		.isPhoneNumber(true)
		.maxLength(10);
		
		/*formValidator.addItem(getTxtTelOficina(), "Tel�fono Oficina")
		.canBeEmpty(true)
		.isRequired(false)
		.isPhoneNumber(true)
		.maxLength(10);*/
		
		formValidator.addItem(getTxtNumFam(), "N�mero de Familiares")
		.canBeEmpty(false)
		.isRequired(true)
		.isNumeric(true)
		.maxLength(2);
		
		
		formValidator.addItem(getTxtCalle(), "Calle")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(50);
		
		formValidator.addItem(getTxtNumExt(), "N�mero exterior")
		.canBeEmpty(false)
		.isRequired(true)
		.isNumeric(true)
		.maxLength(5);
		
		formValidator.addItem(getTxtNumInt(), "N�mero interior")
		.canBeEmpty(true)
		.isRequired(false)
		.maxLength(20);
		
		formValidator.addItem(getTxtColonia(), "Colonia")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(50);
		
		formValidator.addItem(getTxtCP(), "C�digo Postal")
		.canBeEmpty(true)
		.isRequired(false)
		.isNumeric(true)
		.maxLength(6);
		
	}
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.vitamedica_individual);
		
	}
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		clave = getIntent().getStringExtra("clave");
		Log.d(TAG, "CLAVE: " + clave);
		
		claveWS = getIntent().getStringExtra("claveWS");
		Log.d(TAG, "CLAVEWS: " + claveWS);
		
		descripcion = getIntent().getStringExtra("descripcion");
		Log.d(TAG, "DESCRIPCION: " + descripcion);
		
		categoria = getIntent().getStringExtra("categoria");
		Log.d(TAG, "CATEGORIA: " + categoria);
		
		proveedor = getIntent().getStringExtra("proveedor");
		Log.d(TAG, "PROVEEDOR: " + proveedor);
		
		claveProveedor = getIntent().getStringExtra("claveProveedor");
		Log.d(TAG, "CLAVE_PROVEEDOR: " + claveProveedor);
		
		try{
			fechaNacimiento = new Fecha();
			fechaNacimiento.restarAnios(18);
		}catch (ErrorSys e) {
			e.showMsj(this);
		}
		
		getBackButton().setOnClickListener( new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		getBtnFechaNacimiento().setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				//showDialog(FECHA_NACIMIENTO_DIALOG_ID);
				getFechaNacimientoDialog().show();
			}
		});
		getFechaNacimientoDialog().setMinDate(1, 0, 1911);
		getFechaNacimientoDialog().setMaxDate(fechaNacimiento.getDia(), fechaNacimiento.getMesBase0(), fechaNacimiento.getAnio());
		
		(findViewById(R.id.button_menuprincipal)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(VitamedicaIndiAct.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
		
		(findViewById(R.id.btnOk)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(formValidator.validate())
					showDialog(CONF_DIALOG);
			}
			
		});
		
		loadDataProcess = new LongProcess(this, true, 60, true, "LoadUserData", new LongProcessListener(){
			public void doProcess() {
				userGet = new GetUserWSClient(VitamedicaIndiAct.this);
				userGet.execute(new OnUserGetResponseReceivedListener(){
					public void onUserGetResponseReceived(long id, Fecha fechaNacimiento,
							String telefono, Fecha registro, String nombre,
							String apellidos, String direccion, String tarjeta, 
							Fecha vigencia, int banco, int tipoTarjeta, int proveedor, int status, String mail,
							String amaterno, String sexo, String tel_casa,
							String tel_oficina, int id_estado, String ciudad, String calle, int num_ext, String num_int,
							String colonia, int cp, String dom_amex) {
						//llenamos el formulario con los datos
						idUser = id;
						getTxtNombre().setText(nombre);
						getTxtApellidoPaterno().setText(apellidos);
						getTxtCelular().setText(telefono);
						getTxtMail().setText(mail);
		
						if(amaterno != null && !amaterno.equals("")){
							getTxtApellidoMaterno().setText(amaterno);
						}
					
						if(sexo!=null && !sexo.equals("")){
							String txtSexo = sexo.toUpperCase();
							
							for(int i=0; i< getCmbSexo().getCount(); i++){
								String item =  ""+getCmbSexo().getItemAtPosition(i).toString().toUpperCase().charAt(0);
								
								if(item.equals(txtSexo)){
									 getCmbSexo().setSelection(i);
									break;
								}
							}
						}
						
						
						if(tel_casa != null && !tel_casa.equals("")){
							getTxtTelCasa().setText(tel_casa);
						}
						
						if(calle != null && !calle.equals("")){
							getTxtCalle().setText(calle);
						}
						
						if(num_int != null && !num_int.equals("")){
							getTxtNumInt().setText(num_int);
						}
						if(num_ext != 0){
							getTxtNumExt().setText(""+num_ext);
						}
						
						if(colonia != null && !colonia.equals("")){
							getTxtColonia().setText(colonia);
						}
						
						if(cp!=0){
							getTxtCP().setText(""+cp);
						}
					
						//getTxtTelOficina().setText(tel_oficina);
						
						
						
					
					
					
						
						VitamedicaIndiAct.this.fechaNacimiento = fechaNacimiento;
						updateFechaNacimiento(true);
						edoCivilGet = new GetEdoCivilWSClient(VitamedicaIndiAct.this);
						edoCivilGet.execute(new OnBanksResponseReceivedListener(){

							
							public void onBanksResponseReceived(ArrayList<BasicNameValuePair> ests) {
								// TODO Auto-generated method stub
								getCmbSVEstadoC().setDatos(ests);
								loadDataProcess.processFinished();
							}
							
						});
						//terminamos proceso
						
					}
				});
			}

			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (userGet != null){
					userGet.cancel();
					userGet = null;
				}
			}
		});
		
		loadDataProcess.start();
	}
	
	protected void updateFechaNacimiento(boolean updateDatePicker){
		if (fechaNacimiento != null){
			getBtnFechaNacimiento().setText("Seleccionar Fecha [" + fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYY) + "]");
			
			if (updateDatePicker){
				getFechaNacimientoDialog().updateFecha(fechaNacimiento);
			}
		}
	}
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.VITAMEDICA_INDIVIDUAL;
	}
	
	protected Dialog onCreateDialog(int id) {
	    Dialog dialog=null;
	    switch(id) {
	    case CONF_DIALOG:
	    	dialog = new Dialog(VitamedicaIndiAct.this);

	    	dialog.setContentView(R.layout.vitamedica_conf_dialog);
	    	dialog.setTitle("Confirme sus datos...");
	    	
	    	TextView text = (TextView) dialog.findViewById(R.id.txtNombre);
	    	text.setText(getTxtNombre().getText().toString().trim()+" "+
	    			getTxtApellidoPaterno().getText().toString().trim()+" "+
	    			getTxtApellidoMaterno().getText().toString().trim());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtTipoVita);
	    	text.setText("Membres�a Individual");
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtFechaNacimiento);
	    	text.setText(fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYY));
	    	
	    	text = (TextView) dialog.findViewById(R.id.txt_sexo);
	    	text.setText(getCmbSexo().getSelectedItem().toString());
	    	
	    	
	    	text = (TextView) dialog.findViewById(R.id.txt_estado_civil);
	    	try {
	    		text.setText(getCmbEdoCivil().getSelectedItem().toString());
	    	} catch (Exception e) {
	    		text.setText("-");
	    	}
	    	text = (TextView) dialog.findViewById(R.id.txtTelCasa);
	    	text.setText(getTxtTelCasa().getText().toString());
	    	
	    	//text = (TextView) dialog.findViewById(R.id.txtTelOficina);
	    	//text.setText(getTxtTelOficina().getText().toString());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtNoCelular);
	    	text.setText(getTxtCelular().getText().toString());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtEmail);
	    	text.setText(getTxtMail().getText().toString());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtNumFam);
	    	text.setText(getTxtNumFam().getText().toString());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtNumFam);
	    	text.setText(getTxtNumFam().getText().toString());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtCalle);
	    	text.setText(getTxtCalle().getText().toString());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtNumExt);
	    	text.setText(getTxtNumExt().getText().toString());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtNumInt);
	    	text.setText(getTxtNumInt().getText().toString());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtColonia);
	    	text.setText(getTxtColonia().getText().toString());
	    	
	    	text = (TextView) dialog.findViewById(R.id.txtCP);
	    	text.setText(getTxtCP().getText().toString());
	    	
	    	(dialog.findViewById(R.id.btnCancelar)).setOnClickListener(new OnClickListener(){

				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					removeDialog(CONF_DIALOG);
				}
	    		
	    	});
	    	
	    	(dialog.findViewById(R.id.btnOk)).setOnClickListener(new OnClickListener(){

				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					removeDialog(CONF_DIALOG);
					Intent intent = new Intent(VitamedicaIndiAct.this, VitamedicaCompraAct.class);
					JSONObject json = new JSONObject();
					try {
						/*
					{"FecNac":"20\/05\/1970","apMat":"PEREZ","apPat":"LOPEZ",
					"beneficiario":[{"FecNac":"","apMat":"","apPat":"","id_edoc":"","id_gen":"","id_par":"","nombre":""}],
					calle":"CALLE ","col":"CAMPESTRE","cp":"04400","cvv2":"076","cx":"0","cy":"0","id_edoc":"1","id_gen":"M",
					"imei":"1111111","key":"","login":"usuarioooo","mail":"MLEYVAR@HOTMAIL.COM","modelo":"modelo","nombre":"JUAN",
					"numExt":"4","numFam":"0","numInt":"INT B","password":"passsss","pin":"1","producto":"8","software":"soft",
					"tarjeta":"4555102052859685","telCasa":"5585859685","telCel":"5525963232","telOfna":"","tipo":"tipo",
					"vigencia":"01\/2015"}
						 */
						json.put("nombre", getTxtNombre().getText().toString().toUpperCase());
					
						json.put("apPat", getTxtApellidoPaterno().getText().toString().toUpperCase());
						json.put("apMat", getTxtApellidoMaterno().getText().toString().toUpperCase());
						json.put("tipo", "Android");
						json.put("FecNac", fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYY));
						json.put("id_gen", ""+getCmbSexo().getSelectedItem().toString().charAt(0));
						
						
						json.put("numFam","0");
						
						try {
							json.put("id_edoc", getCmbSVEstadoC().getValorSeleccionado() );
						} catch (Exception e) {
							Log.e(TAG, "", e);
							json.put("id_edoc","1");
						}
						
						json.put("telCasa", getTxtTelCasa().getText().toString());
				    	json.put("telOfna","");
				    	json.put("telCel", getTxtCelular().getText().toString());
				    	json.put("mail", getTxtMail().getText().toString());
				    	json.put("calle", getTxtCalle().getText().toString().toUpperCase());
				    	json.put("numExt", getTxtNumExt().getText().toString());
				    	json.put("numInt", getTxtNumInt().getText().toString().toUpperCase());
				    	json.put("col", getTxtColonia().getText().toString().toUpperCase());
				    	json.put("cp", getTxtCP().getText().toString());
				    	
				    	Log.d("VitamedicaIndividual", json.toString());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					intent.putExtra("json", json.toString());
					intent.putExtra("tipo", "INDIVIDUAL");
					intent.putExtra("clave", clave);
					intent.putExtra("claveWS", claveWS);
					intent.putExtra("descripcion", descripcion);
					intent.putExtra("categoria", categoria);
					intent.putExtra("proveedor", proveedor);
					intent.putExtra("claveProveedor", "9");
					startActivity(intent);
				}
	    		
	    	});
	    	
	        break;
	   
	    default:
	        dialog = null;
	    }
	    return dialog;
	}
}
