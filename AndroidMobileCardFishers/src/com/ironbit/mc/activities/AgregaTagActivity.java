package com.ironbit.mc.activities;

import java.util.ArrayList;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.GetTipoRecargaTagWS;
import com.ironbit.mc.web.webservices.data.DataTipoRecargaTag;
import com.ironbit.mc.web.webservices.events.OnTipRecargaTagReceivedListener;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class AgregaTagActivity extends MainActivity {
	protected Intent intent = null;
	protected Spinner spinTipos = null;
	protected EditText txtNick = null, txtNum = null, txtVerif=null ;
	protected TextView lblVerif = null, lblNum= null;
	protected LongProcess loadDataProcess = null;
	protected GetTipoRecargaTagWS ws= null;
	protected DataTipoRecargaTag[] lista = null;
	protected FormValidator formValidator;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.tag);
		
		intent = getIntent();
		
		spinTipos = (Spinner) findViewById(R.id.tagTipo);
		txtNick = (EditText) findViewById(R.id.tagNick);
		txtNum = (EditText) findViewById(R.id.tagNum);
		txtVerif = (EditText) findViewById(R.id.tagVerif);
		lblVerif = (TextView) findViewById(R.id.tagLblVerif);
		lblNum= (TextView) findViewById(R.id.tagLblNum);
		
		findViewById(R.id.btnTagCancelar).setOnClickListener(new OnClickListener(){

		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(2);
				finish();
			}
			
		});
		
		findViewById(R.id.btnTagGuardar).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
					
					
				if (formValidator.validate()){
					String tipo = spinTipos.getSelectedItem().toString();
					String nick = txtNick.getText().toString();
					String num = txtNum.getText().toString();
					String verif = txtVerif.getText().toString();
					String tipoId = ((DataTipoRecargaTag)spinTipos.getSelectedItem()).getClave();
					intent.putExtra("tipo", tipo);
					intent.putExtra("nick", nick);
					intent.putExtra("num", num);
					if(tipoId.equals("2")){
						intent.putExtra("verif",  1);
					}else{
						intent.putExtra("verif",  Integer.parseInt(verif));
					}
					
					intent.putExtra("tipoId", Integer.parseInt(tipoId));
					
					setResult(RESULT_OK, intent);
					finish();
				}
			}
			
		});
		
		spinTipos.setOnItemSelectedListener(new OnItemSelectedListener(){

		
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				DataTipoRecargaTag item = (DataTipoRecargaTag)parent.getSelectedItem();
				if(item.getClave().trim().equals("2")){
					txtVerif.setText("");
					//lblVerif.setText(getString(R.string.confTag));
					lblVerif.setText(R.string.confTag);
					addOhlListener();
					
				}else if(item.getClave().trim().equals("3")){
					txtVerif.setText("");
					//lblVerif.setText(getString(R.string.num_cliente));
					lblVerif.setText(R.string.num_cliente);
					addViapassListener();
				}else{
					txtVerif.setText("");
					//lblVerif.setText(getString(R.string.nverif));
					lblVerif.setText(R.string.nverif);
					addIaveListener();
				}
				
				configValidations();
			}

		
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//addIaveListener();
		
		(findViewById(R.id.tagLblEti)).setOnClickListener(new OnClickListener(){

		
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(arg0.getContext(), getString(R.string.eti_help), Toast.LENGTH_LONG).show();
			}
			
		});
		loadDataProcess = new LongProcess(this, true, 60, true, "LoadTipoRecargaTag", new LongProcessListener(){
			public void doProcess() {
				//inicializamos proveedores
				ws = new GetTipoRecargaTagWS(AgregaTagActivity.this);
				ws.execute(new OnTipRecargaTagReceivedListener(){
					
					
					
					public void onTipoRecargaTagReceivedListener(
							ArrayList<DataTipoRecargaTag> listaTipos) {
						// TODO Auto-generated method stub
						
						lista = new DataTipoRecargaTag[listaTipos.size()];
						
						for (int i = 0; i < listaTipos.size(); i++) {
							lista[i] = listaTipos.get(i);
						}
						
						ArrayAdapter<DataTipoRecargaTag> adapter = new ArrayAdapter<DataTipoRecargaTag>(getApplicationContext(), 
								android.R.layout.simple_spinner_dropdown_item, 
								lista);
						
						spinTipos.setAdapter(adapter);
						loadDataProcess.processFinished();
						
						String tagTipo = intent.getStringExtra("tipo");
						txtNick.setText(intent.getStringExtra("nick"));
						txtNum.setText(intent.getStringExtra("num"));
						int dv = intent.getIntExtra("verif", 0);
						
						if(dv != 0){
							txtVerif.setText(""+dv);
						}
						
						
						int count = spinTipos.getCount();
						
						for (int i = 0; i < count; i++) {
							if(tagTipo.equals(spinTipos.getItemAtPosition(i).toString())){
								spinTipos.setSelection(i);
							}
							
						}
						configValidations();
					}
				});
			}
			
			public void stopProcess() {
				if (ws != null){
					ws.cancel();
					ws = null;
				}
				
				if (ws != null){
					ws.cancel();
					ws = null;
				}
				
				if (ws != null){
					ws.cancel();
					ws = null;
				}
			}
		});
		loadDataProcess.start();
		
		
	}
	
	private void addIaveListener() {
		System.out.println("IAVE Listener");
		lblVerif.setOnClickListener(new OnClickListener(){

		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 AlertDialog alertDialog;
				 
					Builder alert = new AlertDialog.Builder(v.getContext());
					alert.setTitle("�Qu� es N�mero Verificador?");
					alert.setIcon(R.drawable.dlg_alert_icon);
					
					View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
					ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
					imagen.setImageResource(R.drawable.dviave);
					
					ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
					imagen2.setImageResource(R.drawable.iave);
					
					alert.setView(vista);
					alertDialog = alert.create();
					alert.show();
			}
			
		});
		InputFilter[] filter = {new InputFilter.LengthFilter(1)};
		txtVerif.setFilters(filter);
		//txtVerif.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		lblNum.setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tagiave);
				
				ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
				imagen2.setImageResource(R.drawable.iave);
				
				alert.setView(vista);
				
				AlertDialog alertDialog = alert.create();
				alert.show();
			}
			
		});
		
	}
	
	private void addOhlListener() {
		System.out.println("OHL Listener");
		lblVerif.setOnClickListener(null);
		
		InputFilter[] filter = {new InputFilter.LengthFilter(14)};
		txtVerif.setFilters(filter);
		
		//txtVerif.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		lblNum.setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_1, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tag_ohl);
								
				alert.setView(vista);
				
				AlertDialog alertDialog = alert.create();
				alert.show();
			}
			
		});
		
	}
	
	private void addViapassListener() {
		System.out.println("ViaPass Listener");
		lblVerif.setOnClickListener(null);
		
		InputFilter[] filter = {new InputFilter.LengthFilter(8)};
		txtVerif.setFilters(filter);
		
		InputFilter[] filter2 = {new InputFilter.LengthFilter(12)};
		txtNum.setFilters(filter2);
		
		//txtVerif.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		(findViewById(R.id.tagLblNum)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 AlertDialog alertDialog;
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tagviapass1);
			
				ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
				imagen2.setImageResource(R.drawable.tagviapass2);
				
				alert.setView(vista);
				
				alertDialog = alert.create();
				alert.show();
			}
			
		});
		
	}
	
	@Override
	public void configValidations() {
		formValidator = new FormValidator(this, true);
		int clave = 1;
		if(spinTipos != null){
			DataTipoRecargaTag item = (DataTipoRecargaTag) spinTipos.getSelectedItem();
			clave = Integer.parseInt(item.getClave());
		
			if(clave == 1){
				formValidator.addItem(txtNick, "Etiqueta")
					.canBeEmpty(false)
					.isRequired(true)
					.maxLength(12);
				
				formValidator.addItem(txtNum, "Numero de Tag")
					.canBeEmpty(false)
					.isRequired(true)
					.maxLength(14)
					.isNumeric(true);
				
				formValidator.addItem(txtVerif, "Numero Verificador")
				.canBeEmpty(false)
				.isRequired(true)
				.maxLength(1)
				.isNumeric(true);
			}
			
			if(clave == 2){
				formValidator.addItem(txtNick, "Etiqueta")
				.canBeEmpty(false)
				.maxLength(12);
			
				formValidator.addItem(txtNum, "Numero de Tag")
					.canBeEmpty(false)
					.isRequired(true)
					.maxLength(14)
					.isNumeric(true);
				
				formValidator.addItem(txtVerif, "Confirmar Tag")
				.canBeEmpty(false)
				.isRequired(true)
				.maxLength(14)
				.isNumeric(true);
							
				formValidator.addItem(txtVerif, "Confirmar Tag", new Validable(){
					public void validate() throws ErrorSys {
						
						if (!txtVerif.getText().toString().equals(txtNum.getText().toString())){
							txtVerif.requestFocus();
							throw new ErrorSys("Los n�meros de Tag no coinciden");
						}else{
							txtVerif.setText("0");
						}
					}
				});
			}
			
			if(clave == 3){
				formValidator.addItem(txtNick, "Etiqueta")
				.canBeEmpty(false)
				.maxLength(12);
			
			formValidator.addItem(txtNum, "Numero de Tag")
				.canBeEmpty(true)
				.maxLength(12)
				.minLength(4);
			
			formValidator.addItem(txtVerif, "Numero de Cliente")
			.canBeEmpty(false)
			.maxLength(8)
			.isNumeric(true)
			.isRequired(true)
			.minLength(4);
			
			}
		}
	}
		
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.AGREGA_TAG;
	}
}
