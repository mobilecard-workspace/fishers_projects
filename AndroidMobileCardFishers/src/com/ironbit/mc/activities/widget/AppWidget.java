package com.ironbit.mc.activities.widget;

import android.app.Activity;

public abstract class AppWidget {
	protected Activity activity = null;
	
	
	public AppWidget(Activity activity){
		this(activity, true);
	}
	
	
	public AppWidget(Activity activity, boolean inicializarGUI){
		this.activity = activity;
		
		if (inicializarGUI){
			inicializarGUI();
		}
	}
	
	
	protected void inicializarGUI(){}
}