package com.ironbit.mc.activities.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MainActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.web.webservices.GetConditionsWSClient;
import com.ironbit.mc.web.webservices.data.DataCondition;
import com.ironbit.mc.web.webservices.events.OnConditionsResponseReceivedListener;

public class TerminosWidget extends AppWidget{
	protected LongProcess process = null;
	protected GetConditionsWSClient getConditionsWS = null;
	protected AlertDialog alertDialog = null;
	protected boolean terminosObtenidos = false;
	
	
	public TerminosWidget(Activity activity){
		super(activity, true);
		
		process = new LongProcess((MainActivity)activity, true, 30, true, "loadCondiciones", new LongProcessListener(){
			public void doProcess() {
				getConditionsWS = new GetConditionsWSClient(TerminosWidget.this.activity);
				getConditionsWS.execute(new OnConditionsResponseReceivedListener(){
					public void onConditionsResponseReceived(DataCondition condition) {
						getAlertDialog().setMessage( remplazacadena( condition.getDescription() ) );
						terminosObtenidos = true;
						
						getAlertDialog().show();
						process.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (getConditionsWS != null){
					getConditionsWS.cancel();
				}
			}
		});
	}
	
	
	protected AlertDialog getAlertDialog(){
		if (alertDialog == null){
			Builder alert = new AlertDialog.Builder(activity);
			alert.setTitle("T�RMINOS Y CONDICIONES");
			alert.setIcon(R.drawable.dlg_alert_icon);
			alertDialog = alert.create();
		}
		
		return alertDialog;
	}
	
	public String reemplaza(String src, String orig, String nuevo) {
        if (src == null) {
            return null;
        }
        int tmp = 0;
        String srcnew = "";
        while (tmp >= 0) {
            tmp = src.indexOf(orig);
            if (tmp >= 0) {
                if (tmp > 0) {
                     srcnew += src.substring(0, tmp);
                }
                srcnew += nuevo;
                src = src.substring(tmp + orig.length());
            }
        }
        srcnew += src;
        return srcnew;
    }
	
	public String remplazacadena(String conAcentos) {
        String caracteres[] = {"�?", "á", "É", "é", "�?", "í", "Ó", "ó", "Ú", "ú", "Ñ", "ñ"};
        String letras[] = {"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�"};
        for (int counter = 0; counter < caracteres.length; counter++) {
            conAcentos = reemplaza(conAcentos, caracteres[counter], letras[counter]);
        }
        return conAcentos;
	}
	
	public void show(){
		if (terminosObtenidos){
			getAlertDialog().show();
		}else{
			process.start();
		}
	}
	
	
	public void onStop(){
		if (process != null){
			process.stop();
		}
		
		if (alertDialog != null && alertDialog.isShowing()){
			alertDialog.dismiss();
		}
	}
}