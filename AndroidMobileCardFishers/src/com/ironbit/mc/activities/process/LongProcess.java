package com.ironbit.mc.activities.process;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MainActivity;
import com.ironbit.mc.activities.events.OnPressBackListener;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.contador.Contador;
import com.ironbit.mc.system.contador.OnContadorTerminadoListener;
import com.ironbit.mc.system.errores.AlertaSys;

import android.os.Handler;

public class LongProcess {
	protected boolean showLoader = false;
	protected LongProcessListener listener = null;
	protected Contador contador = null;
	protected String processName = "";
	protected MainActivity activity = null;
	/**
	 * si isAsyncProcess = true, es necesario mandar llamara LongProcess.processFinished() una vez que halla concluido
	 * el proceso asyncrono.
	 */
	protected boolean isAsyncProcess = true;
	
	
	public LongProcess(MainActivity activity, boolean isSyncProcess, int segsTimeOut, boolean showLoader){
		this(activity, isSyncProcess, segsTimeOut, showLoader, "");
	}
	
	
	public LongProcess(MainActivity activity, boolean isSyncProcess, int segsTimeOut, boolean showLoader, String processName){
		this(activity, isSyncProcess, segsTimeOut, showLoader, processName, null);
	}
	
	
	public LongProcess(MainActivity activity, boolean isSyncProcess, int segsTimeOut, boolean showLoader, LongProcessListener listener){
		this(activity, isSyncProcess, segsTimeOut, showLoader, "", listener);
	}
	
	
	public LongProcess(MainActivity activity, boolean isAsyncProcess, int segsTimeOut, boolean showLoader, String processName, LongProcessListener listener){
		this.isAsyncProcess = isAsyncProcess;
		this.activity = activity;
		this.showLoader = showLoader;
		this.listener = listener;
		this.processName = processName;
		contador = new Contador(new Handler(), new OnContadorTerminadoListener() {
			public void onContadorTerminado() {
				if (LongProcess.this.listener != null){
					LongProcess.this.listener.stopProcess();
					
					new AlertaSys(R.string.alert_conexionLenta).showMsj(LongProcess.this.activity);
					ocultarProgressBar();
				}
			}
		}, segsTimeOut, this.processName, false);
	}
	
	
	protected void ocultarProgressBar(){
		if (showLoader){
			activity.ocultarProgressDialog();
		}
	}
	
	
	public void start(){
		start(listener);
	}
	
	
	public void start(LongProcessListener listener){
		if (listener != null){
			Sys.log("processStart: " + processName);
			
			this.listener = listener;
			contador.iniciar();
			
			if (showLoader){
				activity.mostrarProgressDialog(new OnPressBackListener(){
					public void onPressBack() {
						new AlertaSys(R.string.alert_procesoCancelado).showMsj(activity);
						contador.cancelar();
						LongProcess.this.listener.stopProcess();
						ocultarProgressBar();
					}
				});
			}
			
			//ejecutamos el proceso
			this.listener.doProcess();
			
			if (!isAsyncProcess){
				//finalizamos el proceso nosotros
				processFinished();
			}
		}
	}
	
	
	public void stop(){
		if (listener != null){
			Sys.log("processStop: " + processName);
			
			contador.cancelar();
			listener.stopProcess();
			ocultarProgressBar();
		}
	}
	
	
	/**
	 * Este m�todo solo necesita ser llamadado si isAsyncProcess = true.
	 * Este m�todo tiene que ser llamado cuando el LongProcessListener.doProcess termine de hacer su trabajo.
	 * Esto se debe a que quiz� su trabajo es asincrono.
	 */
	public void processFinished(){
		Sys.log("processFinished: " + processName);
		contador.cancelar();
		ocultarProgressBar();
	}
}