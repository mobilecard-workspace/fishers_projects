package com.ironbit.mc.activities;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.web.webservices.GetRecoveryWS;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RecUsuarioActivity extends MainActivity {
	protected Button btnOk = null;
	protected EditText txtEmail = null;
	protected final FormValidator formValidator = new FormValidator(this, true);
	protected LongProcess sendDataProcess = null;
	protected GetRecoveryWS recoveryWS = null;
	
	protected EditText getTxtEmail(){
		if (txtEmail == null){
			txtEmail = (EditText)findViewById(R.id.txtEmail);
		}
		
		return txtEmail;
	}
	
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inicializarGUI(R.layout.recuperar_usuario);
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		sendDataProcess = new LongProcess(this, true, 30, true, "processUpdatePass", new LongProcessListener(){

			
			public void doProcess() {
				// TODO Auto-generated method stub
				System.out.println(getTxtEmail().getText().toString());
				recoveryWS = new GetRecoveryWS(RecUsuarioActivity.this);
				String email = getTxtEmail().getText().toString();
				//email = email.replace(".", "|punto|");
				
				System.out.println(email);
				recoveryWS.setUser(email);
				
				recoveryWS.execute(new OnGeneralWSResponseListener(){

					
					public void onGeneralWSErrorListener(String error) {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(), "Error. Int�ntelo de nuevo. "+error, Toast.LENGTH_LONG).show();
						sendDataProcess.processFinished();
					}

					
					public void onGeneralWSResponseListener(String response) {
						// TODO Auto-generated method stub
						if(response.equals("0")){
							Toast.makeText(getApplicationContext(), "Su usuario y contrase�a han sido enviadas a su correo electr�nico.", Toast.LENGTH_LONG).show();
							getTxtEmail().setText("");
						}else if(response.equals("-2")){
							Toast.makeText(getApplicationContext(), "No se encontr� registrado el correo electr�nico.", Toast.LENGTH_LONG).show();
						}else if(response.equals("-1")){
							Toast.makeText(getApplicationContext(), "Error. Int�ntelo de nuevo", Toast.LENGTH_LONG).show();
						}else{
							Toast.makeText(getApplicationContext(), "Error. Int�ntelo de nuevo", Toast.LENGTH_LONG).show();
						}
						sendDataProcess.processFinished();
					}
					
				});
			}

			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (recoveryWS != null){
					recoveryWS.cancel();
					recoveryWS = null;
				}
			}
			
		});
		
		getBtnOk().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				if (formValidator.validate()){
					sendDataProcess.start();
				}
			}
		});
	}
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtEmail(), "Correo elctr�nico")
			.canBeEmpty(false)
			.isEmail(true);
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.REC_PASS;
	}
	
	
}
