package com.ironbit.mc.activities;

import org.addcel.util.GraphicUtil;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.scan.QR_reader_actividad;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.usuario.Usuario;

public class MenuAppActivity extends MenuActivity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (getIntent().getBooleanExtra("EXIT", false)) {
		    finish();
		}
		
		inicializarGUI(R.layout.menu);
	}
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.MENU;
	}
	
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		
		if (!Usuario.getIdUser(MenuAppActivity.this).isEmpty()) {
			View v = findViewById(R.id.button_sesion);
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_cerrarsesion));
		}
		
		findViewById(R.id.btnMenuMiCuenta).setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				if (isLoggedIn())
					startActivity(new Intent(MenuAppActivity.this, ModificarDatosActivity.class));
				else
					Toast.makeText(MenuAppActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
				
			}
		});
		
		
		findViewById(R.id.btnMenuComprar).setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
//				startActivity(new Intent(MenuAppActivity.this, CategoriesActivity.class));
				startActivity(new Intent(MenuAppActivity.this, CategoriasNewActivity.class));
			}
		});
		
		
		findViewById(R.id.btnMenuConsultar).setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				if (isLoggedIn())
					startActivity(new Intent(MenuAppActivity.this, ComprasActivity.class));
				else
					Toast.makeText(MenuAppActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
			
			}
		});
		
		
		findViewById(R.id.btnMenuNuevo).setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				startActivity(new Intent(MenuAppActivity.this, NuevoActivity.class));
			}
		});
		
		findViewById(R.id.btnMenuScanPay).setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (isLoggedIn()) {
					if (isCameraAvailable()) {
						Intent intento = new Intent (MenuAppActivity.this, QR_reader_actividad.class);
				  		intento.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						startActivity(intento);
					} else {
						ErrorSys.showMsj(MenuAppActivity.this, "El equipo no cuentaTO con c�mara.");
					}
					
				} else
					Toast.makeText(MenuAppActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
			
				
			}
		});
		
		findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		findViewById(R.id.btnInvita).setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MenuAppActivity.this, InvitarAmigoActivity.class));
			}
		});
		
		findViewById(R.id.btnMenuPromocion).setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isLoggedIn())
					startActivity(new Intent(MenuAppActivity.this, RecordatorioActivity.class));
				else
					Toast.makeText(MenuAppActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
			}
		});
		
		findViewById(R.id.button_sesion).setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isLoggedIn()) {
					
					Usuario.reset(MenuAppActivity.this);
					GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_iniciar_full));
					
				} else
					startActivity(new Intent(MenuAppActivity.this, IniciarSesionActivity.class));
			}
		});
		

	}
	
	private boolean isLoggedIn() {
		return !Usuario.getIdUser(MenuAppActivity.this).isEmpty();
	}
	
    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
    
    @Override
    protected void onRestart() {
    	// TODO Auto-generated method stub
    	super.onRestart();
    	
    	View v = findViewById(R.id.button_sesion);
    	
		if (isLoggedIn()) {
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_cerrarsesion));
		} else {
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_iniciar_full));
		}
    }
	
	@Override
	public void onBackPressed(){
		Usuario.reset(MenuAppActivity.this);
		finish();
	}
}