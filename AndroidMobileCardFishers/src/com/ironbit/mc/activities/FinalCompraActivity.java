package com.ironbit.mc.activities;

import com.ironbit.mc.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FinalCompraActivity extends MenuActivity{
	protected Button btnOk = null;
	protected TextView txtFolio = null;
	protected TextView txtNota = null;
	public static final String CONF_KEY_COMPRA_FOLIO = "adc_compra_folio";
	public static final String CONF_COMPRA_CONCEPTO = "adc_compra_concepto";
	protected TextView txtPago = null;
	
	protected TextView getTxtFolio(){
		if (txtFolio == null){
			txtFolio = (TextView)findViewById(R.id.txt_resumen);
		}
		
		return txtFolio;
	}
	
	protected TextView getTxtPago(){
		if (txtPago == null){
			txtPago = (TextView)findViewById(R.id.txt_monto_pago);
		}
		
		return txtPago;
	}
	
	protected TextView getTxtNota(){
		if (txtNota == null){
			txtNota = (TextView)findViewById(R.id.compra_notas);
		}
		
		return txtNota;
	}
	
	
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.final_compra);
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		String concepto = getAppConf(this, CONF_COMPRA_CONCEPTO, "");
		getTxtFolio().setText("Autorizaci�n bancaria: " + getAppConf(this, CONF_KEY_COMPRA_FOLIO, "0"));
		/*getTxtPago().setText("Tu compra "+ concepto +
				" ha sido realizada");*/
		getTxtPago().setText(getIntent().getStringExtra("mensaje"));
		
		if(concepto.contains("IAVE")){
			getTxtNota().setText(getTxtNota().getText().toString() + " Tu recarga ser� activada en 30 minutos para todas las casetas IAVE de la Rep�blica mexicana.");
		}
		getBtnOk().setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Intent intent = new Intent(FinalCompraActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
	}
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.FINAL_COMPRA;
	}
}