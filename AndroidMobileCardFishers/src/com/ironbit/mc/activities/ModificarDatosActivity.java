package com.ironbit.mc.activities;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.activities.widget.TerminosWidget;
import com.ironbit.mc.constant.Font;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.Fecha.TipoFecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.system.errores.InfoSys;
import com.ironbit.mc.web.webservices.GetBanksWSClient;
import com.ironbit.mc.web.webservices.GetCardTypesWSClient;
import com.ironbit.mc.web.webservices.GetEstadosWSClient;
import com.ironbit.mc.web.webservices.GetProvidersWSClient;
import com.ironbit.mc.web.webservices.GetUserWSClient;
import com.ironbit.mc.web.webservices.UserUpdateWSClient;
import com.ironbit.mc.web.webservices.data.DataProvider;
import com.ironbit.mc.web.webservices.events.OnBanksResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnCardTypesResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnProvidersResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnUserGetResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnUserUpdatedResponseReceivedListener;
import com.ironbit.mc.widget.SpinnerValues;
import com.ironbit.mc.widget.SpinnerValuesListener;
import com.ironbit.mc.widget.datepickerdialog.MyDatePickerDialog;

public class ModificarDatosActivity extends MenuActivity{
	
	
	protected EditText txtNombre = null;
	protected EditText txtAPat = null;
	protected EditText txtAMat= null;
	
	protected Button btnFechaNacimiento = null;
	protected Button backButton;
	//protected EditText txtDireccion = null;
	protected EditText txtCelular = null;
	protected Spinner cmbCtrlProveedor = null;
	protected SpinnerValues cmbProveedor = null;
	protected EditText txtTarjeta = null;
	protected Spinner cmbCtrlTipoTarjeta = null;
	protected SpinnerValues cmbTipoTarjeta = null;
	protected EditText txtMail = null;
	protected EditText txtMail2 = null;
	
	protected EditText txtTelCasa = null;
	protected EditText txtTelOficina = null;
	protected EditText txtCiudad = null;
	protected EditText txtCalle= null;
	protected EditText txtNumExt = null;
	protected EditText txtNumInt= null;
	protected EditText txtColonia= null;
	protected EditText txtCP= null;
	protected EditText txDomFact= null;
	protected EditText txtCPAmex = null;
	
	protected Spinner cmbCtrlBanco = null;
	protected SpinnerValues cmbBanco = null;
	protected Spinner cmbFechaVencimientoMes = null;
	protected Spinner cmbFechaVencimientoAnio = null;
	
	protected Spinner cmbSexo= null;
	protected Spinner cmbEstados= null;
	protected SpinnerValues cmbSVEstados= null;
	
	protected Button btnActualizar = null;
	protected Button btnUpdatePass = null;
	protected MyDatePickerDialog fechaNacimientoDialog = null;
	protected Fecha fechaNacimiento = null;
	protected Fecha fechaVencimiento = null;
	protected final FormValidator formValidator = new FormValidator(this, true);
	protected LongProcess loadDataProcess = null;
	protected LongProcess sendDataProcess = null;
	protected GetProvidersWSClient providersWS = null;
	protected GetBanksWSClient banksWS = null;
	protected GetCardTypesWSClient cardTypesWS = null;
	protected GetUserWSClient userGet = null;
	protected GetEstadosWSClient estadosWS = null;
	protected UserUpdateWSClient userRegisterWS = null;
	protected Button btnCondiciones = null;
	protected TerminosWidget terminosWidget = null;
	protected CheckBox checkCondiciones = null;
	public String txtDom;
	protected Button btnNewTag = null;
	protected Button btnModTag = null;
	
	public String CreditoGuardado = "";
	public String CreditoModificado = "";
	public String CreditoFinal = "";
	long idUser =0;
	
	protected MyDatePickerDialog getFechaNacimientoDialog(){
		if (fechaNacimientoDialog == null){
			fechaNacimientoDialog = new MyDatePickerDialog(this, new DatePickerDialog.OnDateSetListener(){
				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
					try{
						fechaNacimiento.setAnio(year);
						fechaNacimiento.setMesBase0(monthOfYear);
						fechaNacimiento.setDia(dayOfMonth);
						fechaNacimiento.actualizar();
						
						updateFechaNacimiento(false);
					}catch (ErrorSys e) {
						e.showMsj(ModificarDatosActivity.this);
					}
				}
			},fechaNacimiento.getAnio() ,fechaNacimiento.getMesBase0(), fechaNacimiento.getDia());
		}
		
		
		return fechaNacimientoDialog;
	}
	
	
	protected Button getBtnCondiciones(){
		if (btnCondiciones == null){
			btnCondiciones = (Button) findViewById(R.id.btnCondiciones);
		}
		
		return btnCondiciones;
	}
	
	
	protected EditText getTxtNombre(){
		if (txtNombre == null){
			txtNombre = (EditText)findViewById(R.id.txtNombre);
		}
		
		return txtNombre;
	}
	
	
	protected EditText getTxtApellidoPaterno(){
		if (txtAPat == null){
			txtAPat = (EditText)findViewById(R.id.txtApellidoP);
		}
		
		return txtAPat;
	}
	
	protected EditText getTxtApellidoMaterno(){
		if (txtAMat == null){
			txtAMat = (EditText)findViewById(R.id.txtApellidoM);
		}
		
		return txtAMat;
	}
	
	protected Button getBackButton() {
		if (backButton == null) {
			backButton = (Button) findViewById(R.id.button_back);
		}
		
		return backButton;
	}
	
	protected Button getBtnFechaNacimiento(){
		if (btnFechaNacimiento == null){
			btnFechaNacimiento = (Button)findViewById(R.id.btnFechaNacimiento);
		}
		
		return btnFechaNacimiento;
	}
	
	
	/*protected EditText getTxtDireccion(){
		if (txtDireccion == null){
			txtDireccion = (EditText)findViewById(R.id.txtDireccion);
		}
		
		return txtDireccion;
	}*/
	
	
	protected EditText getTxtCelular(){
		if (txtCelular == null){
			txtCelular = (EditText)findViewById(R.id.txtNoCelular);
		}
		
		return txtCelular;
	}
	
	
	protected Spinner getCmbCtrlProveedor(){
		if (cmbCtrlProveedor == null){
			cmbCtrlProveedor = (Spinner)findViewById(R.id.cmbProveedor);
		}
		
		return cmbCtrlProveedor;
	}
	
	protected EditText getTxtMail(){
		if (txtMail == null){
			txtMail= (EditText)findViewById(R.id.txtEmail);
		}
		
		return txtMail;
	}
	
	protected EditText getTxtMail2(){
		if (txtMail2 == null){
			txtMail2 = (EditText)findViewById(R.id.txtEmail2);
		}
		
		return txtMail2;
	}
	
	protected EditText getTxtTelCasa(){
		if (txtTelCasa == null){
			txtTelCasa= (EditText)findViewById(R.id.txtTelCasa);
		}
		
		return txtTelCasa;
	}
	
	protected EditText getTxtTelOficina(){
		if (txtTelOficina == null){
			txtTelOficina= (EditText)findViewById(R.id.txtTelOficina);
		}
		
		return txtTelOficina;
	}
	
	protected EditText getTxtCiudad(){
		if (txtCiudad == null){
			txtCiudad= (EditText)findViewById(R.id.txtCiudad);
		}
		
		return txtCiudad;
	}
	
	protected EditText getTxtCalle(){
		if (txtCalle == null){
			txtCalle = (EditText)findViewById(R.id.txtCalle);
		}
		
		return txtCalle;
	}
	
	protected EditText getTxtColonia(){
		if (txtColonia == null){
			txtColonia = (EditText)findViewById(R.id.txtColonia);
		}
		
		return txtColonia;
	}
	
	protected EditText getTxtCP(){
		if (txtCP == null){
			txtCP = (EditText)findViewById(R.id.txtCP);
		}
		
		return txtCP;
	}
	
	protected EditText getTxtNumExt(){
		if (txtNumExt== null){
			txtNumExt= (EditText)findViewById(R.id.txtNumExt);
		}
		
		return txtNumExt;
	}
	
	protected EditText getTxtNumInt(){
		if (txtNumInt== null){
			txtNumInt= (EditText)findViewById(R.id.txtNumInt);
		}
		
		return txtNumInt;
	}
	
	protected EditText getTxtCpAmex(){
		if(txtCPAmex == null){
			txtCPAmex = (EditText)findViewById(R.id.txtCPAmex);
		}
		
		return txtCPAmex;
	}
	
	protected EditText getTxtDomFactura(){
		if (txDomFact== null){
			txDomFact= (EditText)findViewById(R.id.txtDomicilio);
		}
		
		return txDomFact;
	}
	
	protected SpinnerValues getCmbProveedor(){
		if (cmbProveedor == null){
			cmbProveedor = new SpinnerValues(this, getCmbCtrlProveedor());
		}
		
		return cmbProveedor;
	}
	
	
	/*protected Spinner getCmbCtrlBanco(){
		if (cmbCtrlBanco == null){
			cmbCtrlBanco = (Spinner)findViewById(R.id.cmbBanco);
		}
		
		return cmbCtrlBanco;
	}*/
	
	
	/*protected SpinnerValues getCmbBanco(){
		if (cmbBanco == null){
			cmbBanco = new SpinnerValues(this, getCmbCtrlBanco());
		}
		
		return cmbBanco;
	}*/
	
	
	protected Spinner getCmbCtrlTipoTarjeta(){
		if (cmbCtrlTipoTarjeta == null){
			cmbCtrlTipoTarjeta = (Spinner)findViewById(R.id.cmbTipoTarjeta);
		}
		
		return cmbCtrlTipoTarjeta;
	}
	
	
	protected SpinnerValues getCmbTipoTarjeta(){
		if (cmbTipoTarjeta == null){
			cmbTipoTarjeta = new SpinnerValues(this, getCmbCtrlTipoTarjeta());
		}
		
		return cmbTipoTarjeta;
	}
	
	
	protected EditText getTxtTarjeta(){
		if (txtTarjeta == null){
			txtTarjeta = (EditText)findViewById(R.id.txtNoTarjetaCredito);
		}
		
		return txtTarjeta;
	}
	
	protected Spinner getCmbSexo(){
		if (cmbSexo == null){
			cmbSexo = (Spinner)findViewById(R.id.spin_sexo);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
		            this, R.array.sexo, android.R.layout.simple_spinner_item);
		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    cmbSexo.setAdapter(adapter);
		}
		
		return cmbSexo;
	}
	
	protected Spinner getCmbEstados(){
		if (cmbEstados== null){
			cmbEstados = (Spinner)findViewById(R.id.spin_estados);
		}
		
		return cmbEstados;
	}
	
	protected SpinnerValues getCmbSVEstados(){
		if (cmbSVEstados== null){
			cmbSVEstados =  new SpinnerValues(this, getCmbEstados());
			/*ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
		            this, R.array.estados, android.R.layout.simple_spinner_item);
		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    cmbEstados.setAdapter(adapter);*/
		}
		
		return cmbSVEstados;
	}
	
	protected Spinner getCmbFechaVencimientoAnio(){
		if (cmbFechaVencimientoAnio == null){
			cmbFechaVencimientoAnio = (Spinner)findViewById(R.id.cmbFechaVencimientoAnio);
		}
		
		return cmbFechaVencimientoAnio;
	}
	
	
	protected Spinner getCmbFechaVencimientoMes(){
		if (cmbFechaVencimientoMes == null){
			cmbFechaVencimientoMes = (Spinner)findViewById(R.id.cmbFechaVencimientoMes);
		}
		
		return cmbFechaVencimientoMes;
	}
	
	
	protected Button getBtnActualizar(){
		if (btnActualizar == null){
			btnActualizar = (Button)findViewById(R.id.btnModificarDatos);
		}
		
		return btnActualizar;
	}
	
	
	protected Button getBtnUpdatePass(){
		if (btnUpdatePass == null){
			btnUpdatePass = (Button)findViewById(R.id.btnUpdatePass);
		}
		
		return btnUpdatePass;
	}
	
	protected CheckBox getCheckCondiciones(){
		if(checkCondiciones == null){
			checkCondiciones = (CheckBox) findViewById(R.id.checkCondiciones);
		}
		return checkCondiciones;
	}
	
	protected Button getBtnNewTag(){
		if (btnNewTag == null){
			btnNewTag = (Button)findViewById(R.id.btnAgregaTag);
		}
		
		return btnNewTag;
	}
	
	protected Button getBtnModTag(){
		if (btnModTag == null){
			btnModTag = (Button)findViewById(R.id.btnConsTag);
		}
		
		return btnModTag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.modificar_datos);
		
		loadDataProcess = new LongProcess(this, true, 60, true, "LoadUserData", new LongProcessListener(){
			public void doProcess() {
				//inicializamos proveedores
				providersWS = new GetProvidersWSClient(ModificarDatosActivity.this);
				providersWS.execute(new OnProvidersResponseReceivedListener(){
					public void onProvidersResponseReceived(ArrayList<DataProvider> providers) {
						ArrayList<BasicNameValuePair> data = new ArrayList<BasicNameValuePair>();
						
						for (DataProvider dp : providers){
							data.add(new BasicNameValuePair(
								dp.getDescription(),
								dp.getClave()+""
							));
						}
						
						getCmbProveedor().setDatos(data);
						
						//inicializamos bancos
						banksWS = new GetBanksWSClient(ModificarDatosActivity.this);
						banksWS.execute(new OnBanksResponseReceivedListener(){
							public void onBanksResponseReceived(ArrayList<BasicNameValuePair> bancos) {
								//getCmbBanco().setDatos(bancos);
								
								//inicializamos tipos de tarjetas
								cardTypesWS = new GetCardTypesWSClient(ModificarDatosActivity.this);
								cardTypesWS.execute(new OnCardTypesResponseReceivedListener(){
									public void onCardTypesResponseReceived(ArrayList<BasicNameValuePair> cardTypes) {
										getCmbTipoTarjeta().setDatos(cardTypes);
										
										estadosWS = new GetEstadosWSClient(ModificarDatosActivity.this);
										estadosWS.execute(new OnBanksResponseReceivedListener(){

											
											public void onBanksResponseReceived(
													ArrayList<BasicNameValuePair> ests) {
												// TODO Auto-generated method stu
												getCmbSVEstados().setDatos(ests);
												
												
												userGet = new GetUserWSClient(ModificarDatosActivity.this);
												userGet.execute(new OnUserGetResponseReceivedListener(){
													public void onUserGetResponseReceived(long id, Fecha fechaNacimiento,
															String telefono, Fecha registro, String nombre,
															String apellidos, String direccion, String tarjeta, 
															Fecha vigencia, int banco, int tipoTarjeta, int proveedor, int status, String mail,
															String amaterno, String sexo, String tel_casa,
															String tel_oficina, int id_estado, String ciudad, String calle, int num_ext, String num_int,
															String colonia, int cp, String dom_amex) {
														//llenamos el formulario con los datos
														idUser = id;
														getTxtNombre().setText(nombre);
														getTxtApellidoPaterno().setText(apellidos);
														getTxtDomFactura().setText(dom_amex);
														getTxtCelular().setText(telefono);
														System.out.println(telefono);
														getTxtMail().setText(mail);
														getTxtMail2().setText(mail);
														CreditoGuardado = tarjeta;
														txtDom = dom_amex;
														getTxtApellidoMaterno().setText(amaterno);
														
														String txtSexo = ("" + sexo.charAt(0)).toUpperCase();
														for(int i=0; i< getCmbSexo().getCount(); i++){
															String item =  ""+getCmbSexo().getItemAtPosition(i).toString().toUpperCase().charAt(0);
															
															if(item.equals(txtSexo)){
																 getCmbSexo().setSelection(i);
																break;
															}
														}

														getTxtTelCasa().setText(tel_casa);
														getTxtTelOficina().setText(tel_oficina);
														
														getTxtCiudad().setText(ciudad);
														getTxtCalle().setText(calle);
														getTxtNumInt().setText(num_int);
														getTxtNumExt().setText(""+num_ext);
														getTxtColonia().setText(colonia);
														
														getTxtCP().setText(""+cp);
														
														if(tipoTarjeta == 3)
															getTxtCpAmex().setText(""+cp);
														
														String credit= tarjeta.substring(tarjeta.length()-4,tarjeta.length());
														String credit2="";
														
														for(int x =0; x<tarjeta.length()-4;x++){
														 credit2+="X";
														}
														getTxtTarjeta().setText(credit2+credit);
														CreditoModificado = credit2+credit;
														//System.out.println(tarjeta);
														//System.out.println(credit2+credit);
														//getCmbBanco().setValorSeleccionado(banco+"");
														getCmbTipoTarjeta().setValorSeleccionado(tipoTarjeta+"");
														getCmbProveedor().setValorSeleccionado(proveedor+"");
														ModificarDatosActivity.this.fechaNacimiento = fechaNacimiento;
														ModificarDatosActivity.this.fechaVencimiento = vigencia;
														updateFechaNacimiento(true);
														getCmbFechaVencimientoMes().setSelection(fechaVencimiento.getMesBase0());
														
														//actualizamos año de vencimiento
														String anioVencimiento = fechaVencimiento.getAnio()+"";
														SpinnerAdapter sp = getCmbFechaVencimientoAnio().getAdapter();
														for (int a = 0; a < sp.getCount(); a++){
															if (anioVencimiento.equals(sp.getItem(a).toString())){
																getCmbFechaVencimientoAnio().setSelection(a);
																break;
															}
														}
														
														//desbloqueamos boton de registro
														getBtnActualizar().setEnabled(true);
														
														loadDataProcess.processFinished();
														cmbSVEstados.setValorSeleccionado(""+id_estado);
														//terminamos proceso
														
													}
												});
											}
											
										});
										//obtenemos datos del usuario
										
									}
								});
							}
						});
					}
				});
			}
			
			public void stopProcess() {
				if (providersWS != null){
					providersWS.cancel();
					providersWS = null;
				}
				
				if (banksWS != null){
					banksWS.cancel();
					banksWS = null;
				}
				
				if (cardTypesWS != null){
					cardTypesWS.cancel();
					cardTypesWS = null;
				}
			}
		});
		
		
		sendDataProcess = new LongProcess(this, true, 30, true, "SendUserData", new LongProcessListener(){
			public void doProcess() {
				userRegisterWS = new UserUpdateWSClient(ModificarDatosActivity.this);
				userRegisterWS.setNombre(getTxtNombre().getText().toString());
				userRegisterWS.setApellidos(getTxtApellidoPaterno().getText().toString());
				
				userRegisterWS.setMaterno(getTxtApellidoMaterno().getText().toString());
				userRegisterWS.setSexo(""+getCmbSexo().getSelectedItem().toString().charAt(0));
				userRegisterWS.setTelCasa(getTxtTelCasa().getText().toString());
				userRegisterWS.setTelOficina(getTxtTelOficina().getText().toString());
				userRegisterWS.setCiudad(getTxtCiudad().getText().toString());
				userRegisterWS.setCalle(getTxtCalle().getText().toString());
				userRegisterWS.setNumExt(Integer.parseInt(getTxtNumExt().getText().toString()));
				userRegisterWS.setNumInt(getTxtNumInt().getText().toString());
				userRegisterWS.setColonia(getTxtColonia().getText().toString());
				
				if(getCmbTipoTarjeta().getValorSeleccionado().equals("3")){
					userRegisterWS.setCP(Integer.parseInt( getTxtCpAmex().getText().toString()) );
				}
				else{
					userRegisterWS.setCP(Integer.parseInt( getTxtCP().getText().toString()) );
				}
				userRegisterWS.setEstado(Integer.parseInt(getCmbSVEstados().getValorSeleccionado()));
				
				userRegisterWS.setFechaNacimiento(fechaNacimiento);
				userRegisterWS.setDireccion(getTxtDomFactura().getText().toString());
				userRegisterWS.setDomAmex(getTxtDomFactura().getText().toString());
				userRegisterWS.setCelular(getTxtCelular().getText().toString());
				userRegisterWS.setTarjeta(CreditoFinal);
				userRegisterWS.setTipoTarjeta(Integer.parseInt(getCmbTipoTarjeta().getValorSeleccionado()));
				userRegisterWS.setEMail(getTxtMail().getText().toString());
				//userRegisterWS.setBanco(Integer.parseInt(getCmbBanco().getValorSeleccionado()));
				userRegisterWS.setBanco(1);
				userRegisterWS.setProveedor(Integer.parseInt(getCmbProveedor().getValorSeleccionado()));
				userRegisterWS.setFechaVencimientoMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
				userRegisterWS.setFechaVencimientoAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
				
				userRegisterWS.execute(new OnUserUpdatedResponseReceivedListener(){
					public void onUserUpdatedResponseReceived(String resultado, String mensaje) {
						if (resultado.trim().equals("1")){
							InfoSys.showMsj(ModificarDatosActivity.this, mensaje);
							
							Intent intent = new Intent(ModificarDatosActivity.this, MenuAppActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							
							startActivity(intent);
						}else{
							ErrorSys.showMsj(ModificarDatosActivity.this, mensaje);
						}
						
						sendDataProcess.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (userRegisterWS != null){
					userRegisterWS.cancel();
				}
			}
		});
		
		 setCmbCardsListener();
		//iniciamos el cargado de info
		loadDataProcess.start();
	}
	
	
	@Override
	protected void onStop() {
		if (terminosWidget != null){
			terminosWidget.onStop();
		}
		
		loadDataProcess.stop();
		sendDataProcess.stop();
		super.onStop();
	}
	
	public void setCmbCardsListener(){
		getCmbTipoTarjeta().setSpinListener(new SpinnerValuesListener(){

			
			public void ItemSelected(String texto, String value) {
				// TODO Auto-generated method stub
				if(value.equals("3")){
					getTxtDomFactura().setEnabled(true);
					getTxtDomFactura().setText(txtDom);
				}else{
					getTxtDomFactura().setEnabled(false);
					getTxtDomFactura().setText("");
				}
			}
			
		});
	}
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		terminosWidget = new TerminosWidget(this);
		txtDom = "";
		
		setTypeface();
		
		//inicializamos fechas
		try{
			fechaNacimiento = new Fecha("1990-01-01", TipoFecha.date);
			fechaVencimiento = new Fecha("1990-01-01", TipoFecha.date);
		}catch (ErrorSys e) {
			e.showMsj(this);
		}
		
		getBackButton().setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		
		//Botón Fecha Nacimiento
		getBtnFechaNacimiento().setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				getFechaNacimientoDialog().show();
			}
		});
		
		
		//Boton Registrarse
		getBtnActualizar().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				String val = checkaTarjeta();
				if (val.equals("") && formValidator.validate()){
					sendDataProcess.start();
				}else{
					Toast.makeText(getApplicationContext(), val, Toast.LENGTH_SHORT).show();
				}
			}

			
		});
		
		
		//Boton Update Pass
		getBtnUpdatePass().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				Intent intent = new Intent(ModificarDatosActivity.this, ModificarPassActivity.class);
				intent.putExtra("source", 1);
				startActivity(intent);
			}
		});
		
		
		//Botón condiciones
		getBtnCondiciones().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				terminosWidget.show();
			}
		});
		
		
		//fecha de nacimiento
		try{
			Fecha fechaMax = new Fecha();
			fechaMax.restarAnios(18);
			
			getFechaNacimientoDialog().setMinDate(1, 0, 1911);
			getFechaNacimientoDialog().setMaxDate(fechaMax.getDia(), fechaMax.getMesBase0(), fechaMax.getAnio());
		}catch (ErrorSys e) {
			e.showMsj(this);
		}
		
		
		//Año vencimiento
		String[] aniosVencimiento = new String[13];
		try{
			Fecha fechaNow = new Fecha();
			int anioActual = fechaNow.getAnio();
			
			for (int a = anioActual, i = 0; a <= anioActual+12; a++, i++){
				aniosVencimiento[i] = a+"";
			}
			
			//metemos array al Spinner
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this, 
				android.R.layout.simple_spinner_item, 
				aniosVencimiento
			);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    getCmbFechaVencimientoAnio().setAdapter(adapter);
		}catch(Exception e){
			Sys.log(e);
		}
		
		updateFechaNacimiento(false);
		
		getBtnNewTag().setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ModificarDatosActivity.this, InsertTagActivity.class);
				intent.putExtra("idUser", idUser);
				startActivity(intent);
			}
			
		});
		
		getBtnModTag().setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ModificarDatosActivity.this, ConsTagActivity.class);
				intent.putExtra("idUser", idUser);
				startActivity(intent);
			}
			
		});
	}
	
	private void setTypeface() {
		Typeface type = Typeface.createFromAsset(getAssets(), Font.GANDHISANS_REG);
		((TextView) findViewById(R.id.label_nombre)).setTypeface(type);
		((TextView) findViewById(R.id.label_paterno)).setTypeface(type);
		((TextView) findViewById(R.id.label_materno)).setTypeface(type);
		((TextView) findViewById(R.id.label_nacimiento)).setTypeface(type);
		((TextView) findViewById(R.id.label_sexo)).setTypeface(type);
		((TextView) findViewById(R.id.label_celular)).setTypeface(type);
		((TextView) findViewById(R.id.label_proveedor)).setTypeface(type);
		((TextView) findViewById(R.id.label_correo)).setTypeface(type);
		((TextView) findViewById(R.id.label_casa)).setTypeface(type);
		((TextView) findViewById(R.id.label_oficina)).setTypeface(type);
		((TextView) findViewById(R.id.label_estado)).setTypeface(type);
		((TextView) findViewById(R.id.label_ciudad)).setTypeface(type);
		((TextView) findViewById(R.id.label_exterior)).setTypeface(type);
		((TextView) findViewById(R.id.label_interior)).setTypeface(type);
		((TextView) findViewById(R.id.label_colonia)).setTypeface(type);
		((TextView) findViewById(R.id.label_cp)).setTypeface(type);
		((TextView) findViewById(R.id.label_tarjeta)).setTypeface(type);
		((TextView) findViewById(R.id.label_tipo)).setTypeface(type);
		((TextView) findViewById(R.id.label_cptarjeta)).setTypeface(type);
		((TextView) findViewById(R.id.label_domtarjeta)).setTypeface(type);
		((TextView) findViewById(R.id.label_vigencia)).setTypeface(type);
		((CheckBox) findViewById(R.id.checkCondiciones)).setTypeface(type);
		
	}
	
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtNombre(), "Nombre")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(50)
			.minLength(3);
		
		formValidator.addItem(getTxtApellidoPaterno(), "Apellido Paterno")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(50);
	formValidator.addItem(getTxtApellidoPaterno(), "Apellido Materno")
		.canBeEmpty(true)
		.isRequired(false)
		.maxLength(50);
	
	formValidator.addItem(getCmbSexo(), "Sexo", new Validable(){
		
		public void validate() throws ErrorSys {
			// TODO Auto-generated method stub
			//System.out.println("Sex item pos: "+getCmbSexo().getSelectedItemPosition());
			
			if(getCmbSexo().getSelectedItemPosition()==0){
				throw new ErrorSys("Debe seleccionar su sexo");
			}
		}
	});
	
	/*
	 formValidator.addItem(getTxtCelular(), "Celular")
			.canBeEmpty(false)
			.isRequired(true)
			.isPhoneNumber(true)
			.maxLength(10)
			.minLength(5);
			
	formValidator.addItem(getTxtMail(), "Correo Electr�nico", new Validable(){
			public void validate() throws ErrorSys {
				if (!getTxtMail().getText().toString().equals(getTxtMail2().getText().toString())){
					getTxtMail2().requestFocus();
					throw new ErrorSys("Los correos electr�nicos deben coincidir entre s�");
				}
			}
		}).canBeEmpty(false)
		.isRequired(true)
		.maxLength(60)
		.isEmail(true);
	 */
	
	formValidator.addItem(getTxtTelCasa(), "Tel�fono Casa")
	.canBeEmpty(true)
	.isRequired(false)
	.isPhoneNumber(true)
	.maxLength(10);
	
	formValidator.addItem(getTxtTelOficina(), "Tel�fono Oficina")
	.canBeEmpty(true)
	.isRequired(false)
	.isPhoneNumber(true)
	.maxLength(10);
	
	/*formValidator.addItem(getCmbEstados(), "Estado", new Validable(){

		@Override
		public void validate() throws ErrorSys {
			// TODO Auto-generated method stub
			//System.out.println("Estado item pos: "+getCmbEstados().getSelectedItemPosition());
			if(getCmbEstados().getSelectedItemPosition()==0){
				getCmbEstados().requestFocus();
				throw new ErrorSys("Debe seleccionar su estado");
			}
		}
		
	});*/
	
	formValidator.addItem(getTxtCiudad(), "Ciudad")
	.canBeEmpty(false)
	.isRequired(true)
	.maxLength(50);
	
	formValidator.addItem(getTxtCalle(), "Calle")
	.canBeEmpty(false)
	.isRequired(true)
	.maxLength(50);
	
	formValidator.addItem(getTxtNumExt(), "N�mero exterior")
	.canBeEmpty(false)
	.isRequired(true)
	.isNumeric(true)
	.maxLength(5);
	
	formValidator.addItem(getTxtNumInt(), "N�mero interior")
	.canBeEmpty(true)
	.isRequired(false)
	.maxLength(20);
	
	formValidator.addItem(getTxtColonia(), "Colonia")
	.canBeEmpty(false)
	.isRequired(true)
	.maxLength(50);
	
	formValidator.addItem(getTxtCP(), "C�digo Postal", new Validable(){
		
		public void validate() throws ErrorSys{
			if(getCmbTipoTarjeta().getValorSeleccionado().equals("3")){			
				if(getTxtCP().getText().toString().trim().equals("")){
					throw new ErrorSys("Escriba el C�digo Postal de su estado de cuentaTO AMEX.");
				}
			}
		}
	})
	.canBeEmpty(true)
	.isRequired(false)
	.isNumeric(true)
	.maxLength(6);
	
	formValidator.addItem(getTxtCpAmex(), "C�digo postal Amex", new Validable() {
		
		public void validate() throws ErrorSys {
			if(getCmbTipoTarjeta().getValorSeleccionado().equals("3")){
				
				if(getTxtCpAmex().getText().toString().trim().equals("")){
					throw new ErrorSys("Escriba el c�digo postal de la tarjeta.");
				}
			}
			
		}
	})
	.canBeEmpty(true)
	.isRequired(false)
	.isNumeric(true)
	.maxLength(6);

	formValidator.addItem(getTxtDomFactura(),"Domicilio de Estado de Cuenta", new Validable(){

		
		public void validate() throws ErrorSys {
			if(getCmbTipoTarjeta().getValorSeleccionado().equals("3")){
				
				if(getTxtDomFactura().getText().toString().trim().equals("")){
					throw new ErrorSys("Escriba el domicilio del estado de cuentaTO de la tarjeta.");
				}
			}
		}
		
	}).isRequired(false)
	.canBeEmpty(true);
		/*formValidator.addItem(getTxtTarjeta(), "Tarjeta de cr�dito")
			.canBeEmpty(false)
			.isNumeric(true)
			.maxLength(20)
			.minLength(13);*/
		
		
		formValidator.addItem(getCmbFechaVencimientoMes(), "", new Validable(){
			public void validate() throws ErrorSys {
				Fecha now = new Fecha();
				now.setHora(0);
				now.setMinuto(0);
				now.setSegundo(0);
				now.actualizar();
				
				Fecha fVencimiento = new Fecha();
				fVencimiento.setDia(1);
				fVencimiento.setMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
				fVencimiento.setAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
				fVencimiento.setHora(0);
				fVencimiento.setMinuto(0);
				fVencimiento.setSegundo(0);
				fVencimiento.actualizar();
				
				if (fVencimiento.getTimeStampSeg() <= now.getTimeStampSeg()){
					getCmbFechaVencimientoMes().requestFocus();
					throw new ErrorSys("La fecha de vencimiento debe ser mayor a la fecha actual");
				}
			}
		});
		
		formValidator.addItem(getCheckCondiciones(), "T�rminos y Condiciones")
		.isRequired(true);
		
	}
	
	
	protected void updateFechaNacimiento(boolean updateDatePicker){
		if (fechaNacimiento != null){
			getBtnFechaNacimiento().setText("Seleccionar Fecha [" + fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYYYY) + "]");
			
			if (updateDatePicker){
				getFechaNacimientoDialog().updateFecha(fechaNacimiento);
			}
		}
	}
	
	private String checkaTarjeta() {
		// TODO Auto-generated method stub
		String ret = "";
		String tarjeta = "";
		if(CreditoModificado.equals(getTxtTarjeta().getText().toString())){
			tarjeta = CreditoGuardado;
		}else{
			tarjeta = getTxtTarjeta().getText().toString();
		}
		//System.out.println("Tarjeta: "+tarjeta);
		CreditoFinal = tarjeta;
		if(tarjeta.equals("")){
			ret = "Debe ingresar un n�mero de tarjeta de cr�dito";
		}else if(tarjeta.length() < 13){
			ret = "El m�nimo de n�meros de tarjeta de cr�dito es de 13";
		}else if(tarjeta.length() > 20){
			ret = "El m�ximo de n�meros de tarjeta de cr�dito es de 20";
		}else{
			for (int i = 0; i < tarjeta.length(); i++) {
				char letra = tarjeta.charAt(i);
				if(letra - 48 < 0 && letra-48 > 9){
					
					ret = "S�lo se permiten n�meros en la tarjeta de cr�dito";
					break;
				}
			}
		}
		return ret;
	}
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.MODIFICAR_DATOS;
	}
}