package com.ironbit.mc.activities;

import org.addcel.util.GraphicUtil;

import com.ironbit.mc.R;
import com.ironbit.mc.constant.Categoria;
import com.ironbit.mc.usuario.Usuario;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class RecargasNewActivity extends MenuActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_recargas);
		
		if (isLoggedIn()) {
			View v = findViewById(R.id.button_logout);
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_cerrarsesion2));
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.RECARGAS_NEW;
	}
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		
		View v = findViewById(R.id.button_logout);
		
		if (isLoggedIn()) {
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_cerrarsesion2));
		} else {
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_iniciar_half));
		}
	}
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			
			Intent intent = null;
			
			switch (id) {
			case R.id.button_tae:
				intent = new Intent(RecargasNewActivity.this, TaeNewActivity.class);
				intent.putExtra("clave", Categoria.TAE);
				startActivity(intent);
				break;
			case R.id.button_peaje:
				intent = new Intent(RecargasNewActivity.this, PeajeNewActivity.class);
				intent.putExtra("clave", Categoria.PEAJE);
				startActivity(intent);
				break;
			case R.id.button_back:
				onBackPressed();
				break;
			case R.id.button_menu:
				intent = new Intent(RecargasNewActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				break;
			case R.id.button_logout:
				
				if (isLoggedIn()) {
					Usuario.reset(RecargasNewActivity.this);
					intent = new Intent(RecargasNewActivity.this, MenuAppActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("EXIT", true);
					startActivity(intent);
				} else {
					startActivity(new Intent(RecargasNewActivity.this, IniciarSesionActivity.class));
				}
				break;

			default:
				break;
			}
		}
	}
	
	private boolean isLoggedIn() {
		return !Usuario.getIdUser(RecargasNewActivity.this).isEmpty();
	}

}
