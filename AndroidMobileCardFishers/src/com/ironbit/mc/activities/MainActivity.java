package com.ironbit.mc.activities;

import com.ironbit.mc.activities.events.OnPressBackListener;
import com.ironbit.mc.form.validator.FormValidable;
import com.ironbit.mc.widget.imageloader.ImageLoader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

public abstract class MainActivity extends Activity implements FormValidable{
	/**
	 * Propiedades
	 */
	public static final String ID_APLICACION = "MobileCard";
	public ProgressDialog progressDialog = null;
	public AlertDialog alertProgressDialog = null;
	protected ImageLoader imageLoader = null;
	
	
	
	
	/**
	 * Enums
	 */
	
	public enum Actividades {
		SIN_ID,
		PRINCIPAL,
		INICIO_SESION,
		MENU,
		REGISTRO,
		MODIFICAR_DATOS,
		MODIFICAR_PASSWORD,
		NUEVO,
		RECORDATORIOS,
		CATEGORIAS,
		CATEGORIAS_NEW,
		RECARGAS_NEW,
		SERVICIOS_NEW,
		TAE_NEW,
		PEAJE_NEW,
		PRODUCTOS_NEW,
		PROVEEDORES,
		PRODUCTOS,
		RESUMEN,
		FINAL_COMPRA,
		PROMOCIONES,
		CONDICIONES,
		COMPRAS,
		EDO_CUENTA,
		CONFIGURACIONES,
		COMPRA_FALLIDA,
		ABOUT,
		REC_PASS,
		INVITA_AMIGO,
		MODIFICAR_PASS_MAIL,
		AGREGA_TAG,
		INSERT_TAG,
		CONS_TAG,
		OHL_COMPRA,
		REENVIA_SMS,
		IAVE2,
		VITAMEDICA_INDIVIDUAL,
		VITAMEDICA_COMPRA,
		VITAMEDICA_BENEFICIARIO,
		EDOMEX_LINEA, 
		TENENCIA_COMPRA,
		INTERJET_REFERENCIA,
		INTERJET_REFERENCIA_AMEX,
		ETN_CONFIRMACION,
		FISHERS_MENU,
		FISHERS_CONSULTA,
		FISHERS_DETALLE,
		FISHERS_PAGO,
		FISHERS_PAGO_MOBILECARD,
		FISHERS_CONFIRMACION;
	}
	
	
	/**
	 * Metodos
	 */
	
	abstract public Actividades getIdActividad();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	private AlertDialog getAlertProgressDialog(){
		ImageView im = null;
		
		if (alertProgressDialog == null){
			Builder builder = new AlertDialog.Builder(this);
			
			//creamos imagenes
			im = new ImageView(this);
			im.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			builder.setView(im);
			
			alertProgressDialog = builder.create();
			alertProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		}
		
		imageLoader = new ImageLoader(this, im, "", 8, true);
		return alertProgressDialog;
	}
	
	
	public void mostrarProgressDialog(){
		getAlertProgressDialog().show();
		//progressDialog = ProgressDialog.show(this, "", getString(R.string.app_progress_dialog), true);
	}
	
	
	public void mostrarProgressDialog(final OnPressBackListener listener){
		getAlertProgressDialog().setOnKeyListener(new OnKeyListener() {
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK){
					listener.onPressBack();
					return true;
				}
				
				return false;
			}
		});
		
		getAlertProgressDialog().show();
		
//		progressDialog = ProgressDialog.show(this, "", getString(R.string.app_progress_dialog), true);
//		progressDialog.setOnKeyListener(new OnKeyListener() {
//			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//				if (keyCode == KeyEvent.KEYCODE_BACK){
//					listener.onPressBack();
//					return true;
//				}
//				
//				return false;
//			}
//		});
	}
	
	
	public void ocultarProgressDialog(){
		if (alertProgressDialog != null){
			alertProgressDialog.dismiss();
			
			if (imageLoader != null){
				imageLoader.detener();
				imageLoader = null;
			}
		}
		
//		if (progressDialog != null){
//			progressDialog.dismiss();
//		}
	}
	
	
	public static void setAppConf(Context ctx, String nombre, String valor){
		ConfigActivity.setAppConf(ctx, nombre, valor);
	}
	
	
	public static String getAppConf(Context ctx, String nombre){
		return ConfigActivity.getAppConf(ctx, nombre);
	}
	
	
	public static String getAppConf(Context ctx, String nombre, String defaultVal){
		return ConfigActivity.getAppConf(ctx, nombre, defaultVal);
	}
	
	
	/**
	 * @param layoutResID. id del recurso del layour a mostrar
	 */
	protected void inicializarGUI(int layoutResID){
		setContentView(layoutResID);
		configValidations();
	}
	
	
	/**
	 * Override this to config a series of validations for a form
	 */
	public void configValidations() {}
}
