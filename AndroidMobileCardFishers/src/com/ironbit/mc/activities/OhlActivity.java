package com.ironbit.mc.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.location.LocateUser;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.util.WebUtil;
import com.ironbit.mc.web.webservices.GetCategoriesWSClient;
import com.ironbit.mc.web.webservices.GetComisionWS;
import com.ironbit.mc.web.webservices.GetTagsWSClient;
import com.ironbit.mc.web.webservices.OHLWSClient;
import com.ironbit.mc.web.webservices.data.DataTag;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnGetTagResponseListener;
import com.ironbit.mc.web.webservices.events.OnPurchaseInsertedResponseReceivedListener;

public class OhlActivity extends MenuActivity {
	protected GetComisionWS comisionWS = null;	
	protected LongProcess processCom = null;
	protected LongProcess getTagProcess = null;
	protected GetTagsWSClient wsGet= null;
	
	protected Button btnOk = null;
	protected EditText txtCVV2 = null;
	protected EditText txtTag = null;
	protected EditText txtConfTag = null;
	protected EditText txtPassword = null;
	protected TextView txtResumen = null;
	protected TextView txtMontoPago = null;
	protected TextView txtQueCvv2 = null;
	protected TextView txtQueTag = null;
	protected Spinner cmbFechaVencimientoMes = null;
	protected Spinner cmbFechaVencimientoAnio = null;
	protected Spinner spinEtiqueta = null;
	
	protected Fecha fechaVencimiento = null;
	protected FormValidator formValidator = new FormValidator(this, true);
	public static final String CONF_KEY_CATEGORIA = "adc_resumen_categoria";
	public static final String CONF_KEY_PROVEEDOR = "adc_resumen_proveedor";
	public static final String CONF_KEY_PRODUCTO = "adc_resumen_producto";
	public static final String CONF_KEY_PRODUCTO_CLAVE = "adc_resumen_producto_clave";
	protected LongProcess process = null;
	protected OHLWSClient purchaseWS = null;
	
	protected String comision = null;
	
	protected DataTag[] listaTag;
	
	protected LocateUser locate = null;

	protected EditText getTxtCVV2(){
		if (txtCVV2 == null){
			txtCVV2 = (EditText)findViewById(R.id.txtCvv2);
		}
		
		return txtCVV2;
	}
	
	
	protected EditText getTxtPassword(){
		if (txtPassword == null){
			txtPassword = (EditText)findViewById(R.id.txtPassword);
		}
		
		return txtPassword;
	}
	
	
	protected EditText getTxtTag(){
		if (txtTag == null){
			txtTag = (EditText)findViewById(R.id.txtTag);
		}
		
		return txtTag;
	}
	
	protected EditText getTxtConfTag(){
		if (txtConfTag == null){
			txtConfTag = (EditText)findViewById(R.id.txtConfTag);
		}
		
		return txtConfTag;
	}
	
	
	protected TextView getTxtQueCvv2(){
		if (txtQueCvv2 == null){
			txtQueCvv2 = (TextView)findViewById(R.id.txtQueCvv2);			
		}
		
		return txtQueCvv2;
	}
	
	protected TextView getTxtQueTag(){
		if (txtQueTag == null){
			txtQueTag = (TextView)findViewById(R.id.txtQueTag);			
		}
		
		return txtQueTag;
	}
//	protected EditText getTxtCelularRe(){
//		if (txtCelularRe == null){
//			txtCelularRe = (EditText)findViewById(R.id.txtNoCelularRe);
//		}
//		
//		return txtCelularRe;
//	}
	
	
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	protected TextView getTxtResumen(){
		if (txtResumen == null){
			txtResumen = (TextView)findViewById(R.id.txt_resumen);			
		}
		
		return txtResumen;
	}
	
	protected TextView getTxtMontoPago(){
		if (txtMontoPago == null){
			txtMontoPago = (TextView)findViewById(R.id.txt_monto_pago);			
		}
		
		return txtMontoPago;
	}
	
	protected Spinner getCmbFechaVencimientoAnio(){
		if (cmbFechaVencimientoAnio == null){
			cmbFechaVencimientoAnio = (Spinner)findViewById(R.id.cmbFechaVencimientoAnio);
		}
		
		return cmbFechaVencimientoAnio;
	}
	
	
	protected Spinner getCmbFechaVencimientoMes(){
		if (cmbFechaVencimientoMes == null){
			cmbFechaVencimientoMes = (Spinner)findViewById(R.id.cmbFechaVencimientoMes);
		}
		
		return cmbFechaVencimientoMes;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.ohl_compra);
		//String id = getAppConf(IaveActivity.this, ProvidersActivity);
		
		locate = new LocateUser(OhlActivity.this);
		
		processCom = new LongProcess(this, true, 50, true, "getComision", new LongProcessListener(){
			public void doProcess() {
				comisionWS = new GetComisionWS(OhlActivity.this);
				
				String id = getAppConf(OhlActivity.this, ProductsActivity.CONF_KEY_ID_PROVEEDOR);
				comisionWS.setProveedor(id);
				comisionWS.execute(new OnGeneralWSResponseListener(){

					
					public void onGeneralWSErrorListener(String error) {
						// TODO Auto-generated method stub
						comision = "0.0";
						processCom.processFinished();
						getTagProcess.start();
					}

					
					public void onGeneralWSResponseListener(String response) {
						// TODO Auto-generated method stub
						comision = response;
						TextView tv = (TextView) findViewById(R.id.txtComision);
						
						tv.setText("Todas tus recargas "+
								getAppConf(OhlActivity.this, ResumenCompraActivity.CONF_KEY_PROVEEDOR)+
								" tendr�n una comisi�n de $ "+comision+"0 con IVA INCLUIDO.");
						processCom.processFinished();
						getTagProcess.start();
					}

					
					
				});
			}
			
			public void stopProcess() {
				if (comisionWS != null){
					comisionWS.cancel();
					comisionWS = null;
				}
			}
		});
		
		processCom.start();
		
		process = new LongProcess(this, true, WebUtil.LONG_TIMEOUT, true, "sendPurchase", new LongProcessListener(){
			public void doProcess() {
				purchaseWS = new OHLWSClient(OhlActivity.this);
				purchaseWS.setPassword(getTxtPassword().getText().toString());
				purchaseWS.setCvv2(getTxtCVV2().getText().toString());
				purchaseWS.setProducto(getAppConf(OhlActivity.this, CONF_KEY_PRODUCTO_CLAVE));
				purchaseWS.setAmount(getAppConf(OhlActivity.this, CONF_KEY_PRODUCTO_CLAVE));
				purchaseWS.setTag(getTxtTag().getText().toString());
				purchaseWS.setFechaVencimientoMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
				purchaseWS.setFechaVencimientoAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
				purchaseWS.setX(locate.getLatitud());
				purchaseWS.setY(locate.getLongitud());
				
				purchaseWS.execute(new OnPurchaseInsertedResponseReceivedListener(){
					public void onPurchaseInsertedResponseReceived(String folio, String resultado, String mensaje) {
						if (resultado.trim().equals("1")){
							setAppConf(OhlActivity.this, FinalCompraActivity.CONF_KEY_COMPRA_FOLIO, folio);
							//InfoSys.showMsj(IaveActivity.this, mensaje);
							
							setAppConf(OhlActivity.this, FinalCompraActivity.CONF_COMPRA_CONCEPTO, 
									getAppConf(OhlActivity.this, CONF_KEY_CATEGORIA) + " " + 
									getAppConf(OhlActivity.this, CONF_KEY_PROVEEDOR));
							Intent intent = new Intent(OhlActivity.this, FinalCompraActivity.class);
							intent.putExtra("mensaje", mensaje);
							startActivity(intent);
							finishComprasActivities();
						}else{
							setAppConf(OhlActivity.this, CompraFallidaActivity.CONF_KEY_FALLA_MSG, mensaje);
							startActivity(new Intent(OhlActivity.this, CompraFallidaActivity.class));
							//ErrorSys.showMsj(OhlActivity.this, mensaje);
						}
						
						//avisamos que el proceso ya terminó
						process.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (purchaseWS != null){
					purchaseWS.cancel();
					purchaseWS = null;
				}
			}
		});
		
		getTagProcess = new LongProcess(this, true, 60, true, "GetTag", new LongProcessListener(){

			
			public void doProcess() {
				// TODO Auto-generated method stub
				wsGet = new GetTagsWSClient(OhlActivity.this);
				wsGet.setIdUser(Usuario.getIdUser(OhlActivity.this));
				//String tipo = ((DataTag)spinEtiqueta.getSelectedItem()).getClave();
				wsGet.setIdTipo("2");
				
				wsGet.execute(new OnGetTagResponseListener(){

					
					public void onGetTagResponseListener(ArrayList<DataTag> tags) {
						// TODO Auto-generated method stub
						listaTag = new DataTag[tags.size()];
						for(int i=0; i<tags.size(); i++){
							listaTag[i] = tags.get(i);
						}
						ArrayAdapter<DataTag> adapter = new ArrayAdapter<DataTag>(getApplicationContext(), 
								android.R.layout.simple_spinner_item, 
								listaTag);
						adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						spinEtiqueta.setAdapter(adapter);
						getTagProcess.processFinished();
					}
					
				});
			}
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (wsGet != null){
					wsGet.cancel();
					wsGet = null;
				}
				
			}
			
		});
		
		getTxtQueCvv2().setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es Cvv2?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				View vista = getLayoutInflater().inflate(R.layout.galeria_imagen, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tarjetacvv);
				alert.setView(vista);
				AlertDialog alertDialog = alert.create();
				alertDialog.show();
			}
			
		});
		
		getTxtQueTag().setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 AlertDialog alertDialog;
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_1, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tag_ohl);
			
				
				alert.setView(vista);
				
				alertDialog = alert.create();
				alert.show();
			}
			
		});
		
		
		(findViewById(R.id.pass_sign)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), getString(R.string.pass_chars), Toast.LENGTH_SHORT).show();
				
			}
		});
	}
	
	
	protected void finishComprasActivities(){
		//tratamos de cerrar CategoriesActivity
		try{
			if (CategoriesActivity.instance != null){
				CategoriesActivity.instance.finish();
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		//tratamos de cerrar ProvidersActivity
		try{
			if (ProvidersActivity.instance != null){
				ProvidersActivity.instance.finish();
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		//tratamos de cerrar ProductsActivity
		try{
			if (ProductsActivity.instance != null){
				ProductsActivity.instance.finish();
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		//cerramos esta actividad
		finish();
	}
	
	
	@Override
	protected void onStop() {
		process.stop();
		super.onStop();
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);	
		
		String producto = getAppConf(OhlActivity.this, CONF_KEY_PRODUCTO);
		getTxtResumen().setText( getAppConf(OhlActivity.this, CONF_KEY_CATEGORIA) + " " + 
			getAppConf(OhlActivity.this, CONF_KEY_PROVEEDOR)
		);
		
		getTxtMontoPago().setText("$ " + producto);
		//Año vencimiento
		String[] aniosVencimiento = new String[13];
		try{
			Fecha fechaNow = new Fecha();
			int anioActual = fechaNow.getAnio();
			
			for (int a = anioActual, i = 0; a <= anioActual+12; a++, i++){
				aniosVencimiento[i] = a+"";
			}
			
			//metemos array al Spinner
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this, 
				android.R.layout.simple_spinner_item, 
				aniosVencimiento
			);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    getCmbFechaVencimientoAnio().setAdapter(adapter);
		}catch(Exception e){
			Sys.log(e);
		}
		
		spinEtiqueta = (Spinner) findViewById(R.id.spinTagEtiq);
		
		getBtnOk().setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				if (formValidator.validate()){
					process.start();
				}
			}
		});
		
		spinEtiqueta.setOnItemSelectedListener(new OnItemSelectedListener (){

			
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				getTxtTag().setText("");
				getTxtTag().setEnabled(false);
				
				getTxtConfTag().setText("");
				getTxtConfTag().setEnabled(false);
				
				DataTag tag = (DataTag)parent.getSelectedItem();
				if(Long.toString(tag.getUsuario()).equals( "9999999999999") ){
					getTxtTag().setEnabled(true);
					getTxtConfTag().setEnabled(true);
				}else if(tag.getUsuario()>-1){
					getTxtTag().setText(tag.getNumero());
					getTxtConfTag().setText(tag.getNumero());
				}
			}

			
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				Toast.makeText(OhlActivity.this, "Sin seleccion", Toast.LENGTH_SHORT).show();
			}
			
		});
	}
	
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtPassword(), "Contrase�aa")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(12)
			.minLength(8);
		
		formValidator.addItem(getTxtCVV2(), "CVV2")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(4)
			.minLength(3)
			.isNumeric(true);
		
		formValidator.addItem(getTxtTag(), "N�mero de Tag")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(14)
		.minLength(3)
		.isNumeric(true);
		
		formValidator.addItem(getTxtConfTag(), "Confirmar Tag", new Validable(){
			public void validate() throws ErrorSys {
				
				if (!getTxtConfTag().getText().toString().equals(getTxtTag().getText().toString())){
					getTxtConfTag().requestFocus();
					throw new ErrorSys("Los n�meros de Tag no coinciden");
				}
			}
		});
		
		formValidator.addItem(getCmbFechaVencimientoMes(), "", new Validable(){
			public void validate() throws ErrorSys {
				Fecha now = new Fecha();
				now.setHora(0);
				now.setMinuto(0);
				now.setSegundo(0);
				now.actualizar();
				
				Fecha fVencimiento = new Fecha();
				fVencimiento.setDia(1);
				fVencimiento.setMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
				fVencimiento.setAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
				fVencimiento.setHora(0);
				fVencimiento.setMinuto(0);
				fVencimiento.setSegundo(0);
				fVencimiento.actualizar();
				
				if (fVencimiento.getTimeStampSeg() <= now.getTimeStampSeg()){
					getCmbFechaVencimientoMes().requestFocus();
					throw new ErrorSys("La fecha de vencimiento debe ser mayor a la fecha actual");
				}
			}
		});
		
		
	}
	
	public void enableProviders(){
		locate.stopLocation();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Alerta!")
		.setMessage("Debe activar al menos un proveeder para obtener su localizacion")
		.setPositiveButton("OK", new DialogInterface.OnClickListener(){

			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
			}

		
			
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		if(locate.startLocation()){
			enableProviders();
		}
	}
	@Override
	protected void onDestroy(){
		super.onDestroy();
		
		if(locate != null){
			locate.stopLocation();
		}
	}
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.OHL_COMPRA;
	}

}
