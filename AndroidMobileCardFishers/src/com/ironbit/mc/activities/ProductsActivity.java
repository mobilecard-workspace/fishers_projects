package com.ironbit.mc.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.system.cache.image.ImageCacheManager;
import com.ironbit.mc.web.webservices.GetProductsWSClient;
import com.ironbit.mc.web.webservices.data.DataProduct;
import com.ironbit.mc.web.webservices.events.OnProductsResponseReceivedListener;

public class ProductsActivity extends MenuActivity{
	protected GetProductsWSClient productosWS = null;
	
	public static final String CONF_KEY_CLAVEWS_PROVEEDOR = "adc_clavews_proveedor";
	protected LongProcess process = null;
	
	public static ProductsActivity instance = null;
		protected TextView txtSubtitulos = null;
		protected TextView txtSubtitulos2 = null;
		protected TextView txtSubtitulos3 = null;
		protected ImageView imgSubtitulos = null;
		public static final String CONF_KEY_PROVEEDOR_IMG = "adc_proveedor_img";
		public static final String CONF_KEY_ID_PROVEEDOR = "adc_id_proveedor";
	
	protected TextView getTxtSubtitulos(){
		if (txtSubtitulos == null){
			txtSubtitulos = (TextView)findViewById(R.id.txtSubtitulo);
		}
		
		return txtSubtitulos;
	}
	
	protected TextView getTxtSubtitulos2(){
		if (txtSubtitulos2 == null){
			txtSubtitulos2 = (TextView)findViewById(R.id.txtSubtitulo2);
		}
		
		return txtSubtitulos2;
	}
	
	protected TextView getTxtSubtitulos3(){
		if (txtSubtitulos3 == null){
			txtSubtitulos3 = (TextView)findViewById(R.id.txtSubtitulo3);
		}
		
		return txtSubtitulos3;
	}
	
	protected ImageView getImgSubtitulos(){
		if (imgSubtitulos == null){
			imgSubtitulos = (ImageView)findViewById(R.id.imgSubtitulo);
		}
		
		return imgSubtitulos;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		inicializarGUI(R.layout.categorias);
		
		process = new LongProcess(this, true, 50, true, "getProducts", new LongProcessListener(){
			public void doProcess() {
				productosWS = new GetProductsWSClient(ProductsActivity.this);
				productosWS.setClaveWSProvider(getAppConf(ProductsActivity.this, CONF_KEY_CLAVEWS_PROVEEDOR));
				productosWS.execute(new OnProductsResponseReceivedListener(){
					public void onProductsResponseReceived(ArrayList<DataProduct> products) {
						for (DataProduct product : products){
							final LinearLayout linLayItem = (LinearLayout)getLayoutInflater().inflate(R.layout.item_no_image, null);
							
							//texto
							final TextView txtTitulo = (TextView)linLayItem.findViewById(R.id.item_text);
							if(product.getNombre().equals(""))
								txtTitulo.setText("$ "+product.getDescription());
							else if (product.getDescription().equals("0.00")) {
								txtTitulo.setText(product.getNombre());
							}
							else{
								txtTitulo.setText(product.getNombre() + " $ "+product.getDescription());
							}
							
							//descripcion
							final String descripcion = product.getDescription();
							final String claveWS = product.getClaveWS();
							final String clave = product.getClave();
							
							linLayItem.setOnClickListener(new View.OnClickListener(){
								public void onClick(View v) {
									
									String id = getAppConf(ProductsActivity.this, ProductsActivity.CONF_KEY_ID_PROVEEDOR);
									String desc = getAppConf(ProductsActivity.this, ResumenCompraActivity.CONF_KEY_PROVEEDOR);
									System.out.println("descripcion: "+desc);
									System.out.println("clave: "+id);
									System.out.println("clavews: " + claveWS);
									System.out.println("clave en tabla productosList: " + clave);
									//Integer.parseInt(id)==2
									//System.out.println("Id: "+id);
									
									/** IAVE **/
									if(id.equals("5")){
										setAppConf(ProductsActivity.this, IaveActivity.CONF_KEY_PRODUCTO, descripcion);
										setAppConf(ProductsActivity.this, IaveActivity.CONF_KEY_PRODUCTO_CLAVE, claveWS);
										startActivity(new Intent(ProductsActivity.this, IaveActivity.class));
									}
									/** OHL **/
									else if(id.equals("6")){
										setAppConf(ProductsActivity.this, OhlActivity.CONF_KEY_PRODUCTO, descripcion);
										setAppConf(ProductsActivity.this, OhlActivity.CONF_KEY_PRODUCTO_CLAVE, claveWS);
										startActivity(new Intent(ProductsActivity.this, OhlActivity.class));
									}
									/** Viapass **/
									else if(id.equals("7")){
										setAppConf(ProductsActivity.this, ViapassActivity.CONF_KEY_PRODUCTO, descripcion);
										setAppConf(ProductsActivity.this, ViapassActivity.CONF_KEY_PRODUCTO_CLAVE, claveWS);
										startActivity(new Intent(ProductsActivity.this, ViapassActivity.class));
									}
									/** PASE **/
									else if(id.equals("8")){
										setAppConf(ProductsActivity.this, PaseActivity.CONF_KEY_PRODUCTO, descripcion);
										setAppConf(ProductsActivity.this, PaseActivity.CONF_KEY_PRODUCTO_CLAVE, claveWS);
										startActivity(new Intent(ProductsActivity.this, PaseActivity.class));
									}
									/** Vitamedica **/
									else if(id.equals("9")){
										//String claveProd = getAppConf(ProductsActivity.this, ProductsActivity.conf);
										System.out.println("Clave: "+clave);
										/** Membres�a Individual **/
										if(clave.equals("16") || clave.equals("17")){
											setAppConf(ProductsActivity.this, VitamedicaIndiAct.CONF_KEY_PRODUCTO, descripcion);
											setAppConf(ProductsActivity.this, VitamedicaIndiAct.CONF_KEY_PRODUCTO_CLAVE, clave);
											startActivity(new Intent(ProductsActivity.this, VitamedicaIndiAct.class));
										}
										/** Membres�a Familiar **/
										else if(clave.equals("18") || clave.equals("19")){
											setAppConf(ProductsActivity.this, VitamedicaIndiAct.CONF_KEY_PRODUCTO, descripcion);
											setAppConf(ProductsActivity.this, VitamedicaIndiAct.CONF_KEY_PRODUCTO_CLAVE, clave);
											startActivity(new Intent(ProductsActivity.this, VitamedicaFamAct.class));
										}
									}
									/** Tiempo Aire ***/
									else{
										setAppConf(ProductsActivity.this, ResumenCompraActivity.CONF_KEY_PRODUCTO, descripcion);
										setAppConf(ProductsActivity.this, ResumenCompraActivity.CONF_KEY_PRODUCTO_CLAVE, claveWS);
										startActivity(new Intent(ProductsActivity.this, ResumenCompraActivity.class));
									}
									
								}
							});
							
							linLayItem.setOnTouchListener(new View.OnTouchListener() {
								
								
								public boolean onTouch(View v, MotionEvent event) {
									// TODO Auto-generated method stub
									switch(event.getAction()){
										case MotionEvent.ACTION_DOWN:
											linLayItem.setBackgroundColor(Color.parseColor("#2D7EC1"));
											break;
											
										case MotionEvent.ACTION_OUTSIDE:
											
											break;
											
										case MotionEvent.ACTION_CANCEL:
											linLayItem.setBackgroundColor(Color.parseColor("#414042"));
											break;
											
										case MotionEvent.ACTION_UP:
											linLayItem.setBackgroundColor(Color.parseColor("#414042"));
											break;
									}
									return false;
								}
							});
							
							linLayItem.setOnFocusChangeListener(new OnFocusChangeListener() {
								public void onFocusChange(View v, boolean hasFocus) {
									if (hasFocus){
										linLayItem.setBackgroundColor(Color.parseColor("#2D7EC1"));
									}else{
										linLayItem.setBackgroundColor(Color.parseColor("#414042"));
									}
								}
							});
							
							//lo agregamos a la lista
							((ViewGroup)findViewById(R.id.lista_items)).addView(linLayItem);
						}
						
						process.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (productosWS != null){
					productosWS.cancel();
					productosWS = null;
				}
			}
		});
		
		process.start();
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		String id = getAppConf(ProductsActivity.this, ProductsActivity.CONF_KEY_ID_PROVEEDOR);
		System.out.println("id: "+id);
		if(id.equals("9")){
			getTxtSubtitulos3().setVisibility(View.VISIBLE);
			getTxtSubtitulos3().setOnClickListener(new OnClickListener(){

				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Builder alert = new AlertDialog.Builder(v.getContext());
					alert.setTitle(getString(R.string.sub_vita));
					alert.setIcon(R.drawable.dlg_alert_icon);
					View vista = getLayoutInflater().inflate(R.layout.galeria_imagen, null);
					ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
					imagen.setImageResource(R.drawable.infovm);
					alert.setView(vista);
					AlertDialog alertDialog = alert.create();
					alertDialog.show();
				}
				
			});
		}
		String subtitulo = "";
		subtitulo += getAppConf(this, ResumenCompraActivity.CONF_KEY_CATEGORIA);
		
		String categoria = getAppConf(this, ResumenCompraActivity.CONF_KEY_PROVEEDOR);
		if (subtitulo.trim().length() > 0){
			subtitulo += " " + categoria;
		}
		
		getTxtSubtitulos().setText(subtitulo);
		getTxtSubtitulos2().setText("Cantidad a pagar");
		ImageCacheManager.getImagen(getImgSubtitulos(), getAppConf(this, CONF_KEY_PROVEEDOR_IMG));
		
		ImageView image = (ImageView) findViewById(R.id.footer);
		image.setImageBitmap(null);
	}
	
	
	@Override
	protected void onStop() {
		process.stop();
		super.onStop();
	}
	
	
	@Override
	protected void onDestroy() {
		instance = null;
		super.onDestroy();
	}
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.PRODUCTOS;
	}
}