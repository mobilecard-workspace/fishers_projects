package com.ironbit.mc.activities;

import com.ironbit.mc.R;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.Validador;
import com.ironbit.mc.system.errores.ErrorSys;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ReenviaSmsAct extends MenuActivity {
	protected EditText txtCelular = null;
	protected EditText txtCelular2 = null;
	protected Button btnOk = null;
	protected final FormValidator formValidator = new FormValidator(this, true);

	
	protected EditText getTxtCelular(){
		if (txtCelular == null){
			txtCelular = (EditText)findViewById(R.id.txtNumCel);
		}
		
		return txtCelular;
	}
	
	protected EditText getTxtCelular2(){
		if (txtCelular2 == null){
			txtCelular2 = (EditText)findViewById(R.id.txtNumCel2);
		}
		
		return txtCelular2;
	}
	
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.reenviar_sms);
	}

	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		getBtnOk().setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(formValidator.validate()){
					Toast.makeText(v.getContext(), "Pasa", Toast.LENGTH_SHORT).show();
				}
			}
			
		});
	}
	
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtCelular(), "N�mero de celular", new Validable(){
			public void validate() throws ErrorSys {
				if (!getTxtCelular().getText().toString().equals(getTxtCelular2().getText().toString())){
					getTxtCelular().requestFocus();
					throw new ErrorSys("El n�meros de celular no coinciden");
				}

				if (!Validador.esNumeroCelularMX(getTxtCelular().getText().toString())){
					getTxtCelular().requestFocus();
					throw new ErrorSys("El n�mero de celular debe ser de 10 d�gitos");
				}
			}
		}).canBeEmpty(false)
		.isRequired(true);
	}
	
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.REENVIA_SMS;
	}
}
