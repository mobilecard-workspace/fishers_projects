package com.ironbit.mc.activities;

import org.addcel.util.GraphicUtil;

import com.ironbit.mc.R;
import com.ironbit.mc.constant.Tae;
import com.ironbit.mc.usuario.Usuario;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class TaeNewActivity extends MenuActivity {
	
	int claveCategoria;
	private static final String TAG = "TaeNewActivity";
	private static final String CATEGORIA = "Tiempo Aire";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_tae);
		
		claveCategoria = getIntent().getIntExtra("clave", 0);
		
		if (isLoggedIn()) {
			View v = findViewById(R.id.button_logout);
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_cerrarsesion2));
		}
		
		
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.TAE_NEW;
	}
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		
		View v = findViewById(R.id.button_logout);
		
		if (isLoggedIn()) {
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_cerrarsesion2));
		} else {
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_iniciar_half));
		}
	}
	
	public void executeAction(View v) {
		
		if (null != v) {
			int id = v.getId();
			Intent intent = null;
			
			switch (id) {
			case R.id.button_back:
				onBackPressed();
				break;
				
			case R.id.button_telcel:
				
				if (isLoggedIn()) {
					intent = new Intent(TaeNewActivity.this, ProductosNewActivity.class);
					intent.putExtra("clave_cat",claveCategoria);
					intent.putExtra("cat", CATEGORIA);
					intent.putExtra("clave", Tae.TELCEL);
					intent.putExtra("clave_ws", Tae.TELCEL_WS);
					startActivity(intent);
				} else {
					Toast.makeText(TaeNewActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
				}
				break;

			case R.id.button_iusacell:
				
				if (isLoggedIn()) {
					intent = new Intent(TaeNewActivity.this, ProductosNewActivity.class);
					intent.putExtra("clave_cat",claveCategoria);
					intent.putExtra("cat", CATEGORIA);
					intent.putExtra("clave", Tae.IUSACELL);
					intent.putExtra("clave_ws", Tae.IUSACELL_WS);
					startActivity(intent);
				} else {
					Toast.makeText(TaeNewActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
				}
				
				break;
				
			case R.id.button_movistar:
				
				if (isLoggedIn()) {
					intent = new Intent(TaeNewActivity.this, ProductosNewActivity.class);
					intent.putExtra("clave_cat",claveCategoria);
					intent.putExtra("cat", CATEGORIA);
					intent.putExtra("clave", Tae.MOVISTAR);
					intent.putExtra("clave_ws", Tae.MOVISTAR_WS);
					startActivity(intent);
				} else {

					Toast.makeText(TaeNewActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
				}
				
				break;
			case R.id.button_unefon:
				
				if (isLoggedIn()) {
					intent = new Intent(TaeNewActivity.this, ProductosNewActivity.class);
					intent.putExtra("clave_cat",claveCategoria);
					intent.putExtra("cat", CATEGORIA);
					intent.putExtra("clave", Tae.UNEFON);
					intent.putExtra("clave_ws", Tae.UNEFON_WS);
					startActivity(intent);
				} else {

					Toast.makeText(TaeNewActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
				}
				
				break;
			case R.id.button_menu:
				intent = new Intent(TaeNewActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				break;
			case R.id.button_logout:
				if (isLoggedIn()) {
					Usuario.reset(TaeNewActivity.this);
					intent = new Intent(TaeNewActivity.this, MenuAppActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("EXIT", true);
					startActivity(intent);
					finish();
				} else {
					startActivity(new Intent(TaeNewActivity.this, IniciarSesionActivity.class));
				}
				break;

			default:
				break;
			}
		}
	}
	
	private boolean isLoggedIn() {
		return !Usuario.getIdUser(TaeNewActivity.this).isEmpty();
	}

}
