package com.ironbit.mc.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.location.LocateUser;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.system.errores.InfoSys;
import com.ironbit.mc.web.util.WebUtil;
import com.ironbit.mc.web.webservices.GetCategoriesWSClient;
import com.ironbit.mc.web.webservices.GetComisionWS;
import com.ironbit.mc.web.webservices.PurchaseWSClient;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnPurchaseInsertedResponseReceivedListener;

/**
 * 
 * @author ADDCEL14
 * @version 2.2 MC_REGULAR
 */

public class ResumenCompraActivity extends MenuActivity{
	protected GetCategoriesWSClient categoriasWS = null;
	protected Button btnOk = null;
	protected EditText txtCVV2 = null;
	protected EditText txtPassword = null;
	protected EditText txtCelular = null;
	protected EditText txtCelular2 = null;
	//protected EditText txtCelularRe = null;
	protected TextView txtResumen = null;
	protected TextView txtQueCvv2 = null;
	protected TextView txtMontoPago = null;
	protected Spinner cmbFechaVencimientoMes = null;
	protected Spinner cmbFechaVencimientoAnio = null;
	protected Fecha fechaVencimiento = null;
	protected FormValidator formValidator = new FormValidator(this, true);
	public static final String CONF_KEY_CATEGORIA = "adc_resumen_categoria";
	public static final String CONF_KEY_PROVEEDOR = "adc_resumen_proveedor";
	public static final String CONF_KEY_PRODUCTO = "adc_resumen_producto";
	public static final String CONF_KEY_PRODUCTO_CLAVE = "adc_resumen_producto_clave";
	protected LongProcess process = null;
	protected LongProcess processCom = null;
	protected PurchaseWSClient purchaseWS = null;
	protected GetComisionWS comisionWS = null;	
	protected LocateUser locate = null;
	
	private Button backButton;
	
	private String clave;
	private String claveWS;
	private String descripcion;
	private String categoria;
	private String claveProveedor;
	private String proveedor;
	
	private static final String LOG = "ResumenCompraActivity";
	
	protected Button getBackButton() {
		if (null == backButton) 
			backButton = (Button) findViewById(R.id.button_back);
		
		return backButton;
	}
	
	protected EditText getTxtCVV2(){
		if (txtCVV2 == null){
			txtCVV2 = (EditText)findViewById(R.id.txtCvv2);
		}
		
		return txtCVV2;
	}
	
	
	protected EditText getTxtPassword(){
		if (txtPassword == null){
			txtPassword = (EditText)findViewById(R.id.txtPassword);
		}
		
		return txtPassword;
	}
	
	
	protected EditText getTxtCelular(){
		if (txtCelular == null){
			txtCelular = (EditText)findViewById(R.id.txtNoCelular);
		}
		
		return txtCelular;
	}
	
	protected EditText getTxtCelular2(){
		if (txtCelular2 == null){
			txtCelular2 = (EditText)findViewById(R.id.txtNoCelular2);
		}
		
		return txtCelular2;
	}
	
	
//	protected EditText getTxtCelularRe(){
//		if (txtCelularRe == null){
//			txtCelularRe = (EditText)findViewById(R.id.txtNoCelularRe);
//		}
//		
//		return txtCelularRe;
//	}
	
	
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	
	protected TextView getTxtResumen(){
		if (txtResumen == null){
			txtResumen = (TextView)findViewById(R.id.txt_resumen);			
		}
		
		return txtResumen;
	}
	
	
	protected TextView getTxtMontoPago(){
		if (txtMontoPago == null){
			txtMontoPago = (TextView)findViewById(R.id.txt_monto_pago);			
		}
		
		return txtMontoPago;
	}
	
	
	protected Spinner getCmbFechaVencimientoAnio(){
		if (cmbFechaVencimientoAnio == null){
			cmbFechaVencimientoAnio = (Spinner)findViewById(R.id.cmbFechaVencimientoAnio);
		}
		
		return cmbFechaVencimientoAnio;
	}
	
	
	protected Spinner getCmbFechaVencimientoMes(){
		if (cmbFechaVencimientoMes == null){
			cmbFechaVencimientoMes = (Spinner)findViewById(R.id.cmbFechaVencimientoMes);
		}
		
		return cmbFechaVencimientoMes;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.resumen_compra);
		
		locate = new LocateUser(ResumenCompraActivity.this);
		
		process = new LongProcess(this, true, WebUtil.LONG_TIMEOUT, true, "sendPurchase", new LongProcessListener(){
			public void doProcess() {
				purchaseWS = new PurchaseWSClient(ResumenCompraActivity.this);
				purchaseWS.setPassword(getTxtPassword().getText().toString());
				purchaseWS.setCvv2(getTxtCVV2().getText().toString());
				purchaseWS.setProducto(claveWS);
				purchaseWS.setTelefono(getTxtCelular().getText().toString());
				purchaseWS.setFechaVencimientoMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
				purchaseWS.setFechaVencimientoAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
				purchaseWS.setX(locate.getLatitud());
				purchaseWS.setY(locate.getLongitud());
				purchaseWS.execute(new OnPurchaseInsertedResponseReceivedListener(){
					public void onPurchaseInsertedResponseReceived(String folio, String resultado, String mensaje) {
						if (resultado.trim().equals("1")){
							setAppConf(ResumenCompraActivity.this, FinalCompraActivity.CONF_KEY_COMPRA_FOLIO, folio);
							InfoSys.showMsj(ResumenCompraActivity.this, mensaje);
							
							setAppConf(ResumenCompraActivity.this, FinalCompraActivity.CONF_COMPRA_CONCEPTO, 
									getAppConf(ResumenCompraActivity.this, CONF_KEY_CATEGORIA) + " " + 
									getAppConf(ResumenCompraActivity.this, CONF_KEY_PROVEEDOR) );
							
							Intent intent = new Intent(new Intent(ResumenCompraActivity.this, FinalCompraActivity.class));
							intent.putExtra("mensaje", mensaje);
							startActivity(intent);
							finishComprasActivities();
						}else{
							setAppConf(ResumenCompraActivity.this, CompraFallidaActivity.CONF_KEY_FALLA_MSG, mensaje);
							startActivity(new Intent(ResumenCompraActivity.this, CompraFallidaActivity.class));
							//ErrorSys.showMsj(ResumenCompraActivity.this, mensaje);
						}
						
						//avisamos que el proceso ya terminó
						process.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (purchaseWS != null){
					purchaseWS.cancel();
					purchaseWS = null;
				}
			}
		});
		
		processCom = new LongProcess(this, true, 50, true, "getComision", new LongProcessListener(){
			public void doProcess() {
				comisionWS = new GetComisionWS(ResumenCompraActivity.this);
				
				Log.d(LOG, "CLAVE PRODUCTO");
				String id = claveProveedor;
				comisionWS.setProveedor(id);
				comisionWS.execute(new OnGeneralWSResponseListener(){

					
					public void onGeneralWSErrorListener(String error) {
						// TODO Auto-generated method stub
						processCom.processFinished();
					}

					
					public void onGeneralWSResponseListener(String response) {
						// TODO Auto-generated method stub
						String comision = response;
						TextView tv = (TextView) findViewById(R.id.txtComision);
						
						tv.setText(tv.getText() + comision + "0");
						processCom.processFinished();
						//getTagProcess.start();
					}

					
					
				});
			}
			
			public void stopProcess() {
				if (comisionWS != null){
					comisionWS.cancel();
					comisionWS = null;
				}
			}
		});
		
//		String clave =  getAppConf(ResumenCompraActivity.this, ProductsActivity.CONF_KEY_ID_PROVEEDOR);
		
		if(claveProveedor.equals("3")){
			processCom.start();
		}
		
//		getTxtQueCvv2().setOnClickListener(new OnClickListener(){
//
//			
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				 AlertDialog alertDialog;
//				 
//				Builder alert = new AlertDialog.Builder(v.getContext());
//				alert.setTitle("�Qu� es Cvv2?");
//				alert.setIcon(R.drawable.dlg_alert_icon);
//				View vista = getLayoutInflater().inflate(R.layout.galeria_imagen, null);
//				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
//				imagen.setImageResource(R.drawable.tarjetacvv);
//				alert.setView(vista);
//				alertDialog = alert.create();
//				alert.show();
//			}
//			
//		});
		
//		(findViewById(R.id.pass_sign)).setOnClickListener(new OnClickListener(){
//
//			
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Toast.makeText(v.getContext(), getString(R.string.pass_chars), Toast.LENGTH_SHORT).show();
//				
//			}
//		});
//		(findViewById(R.id.tel_sign)).setOnClickListener(new OnClickListener(){
//
//			
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Toast.makeText(v.getContext(), getString(R.string.tel_chars), Toast.LENGTH_SHORT).show();
//				
//			}
//		});

	}
	
	
	protected void finishComprasActivities(){
		//tratamos de cerrar CategoriesActivity
		try{
			if (CategoriesActivity.instance != null){
				CategoriesActivity.instance.finish();
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		//tratamos de cerrar ProvidersActivity
		try{
			if (ProvidersActivity.instance != null){
				ProvidersActivity.instance.finish();
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		//tratamos de cerrar ProductsActivity
		try{
			if (ProductsActivity.instance != null){
				ProductsActivity.instance.finish();
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		//cerramos esta actividad
		finish();
	}
	
	
	@Override
	protected void onStop() {
		process.stop();
		super.onStop();
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);	
		
		
		clave = getIntent().getStringExtra("clave");
		Log.d(LOG, "CLAVE: " + clave);
		
		claveWS = getIntent().getStringExtra("claveWS");
		Log.d(LOG, "CLAVEWS: " + claveWS);
		
		descripcion = getIntent().getStringExtra("descripcion");
		Log.d(LOG, "DESCRIPCION: " + descripcion);
		
		categoria = getIntent().getStringExtra("cat");
		Log.d(LOG, "CATEGORIA: " + categoria);
		
		proveedor = getIntent().getStringExtra("proveedor");
		Log.d(LOG, "PROVEEDOR: " + proveedor);
		
		claveProveedor = getIntent().getStringExtra("claveProveedor");
		Log.d(LOG, "CLAVE_PROVEEDOR: " + claveProveedor);
		
		String producto = descripcion;
		getTxtResumen().setText("Tiempo Aire " + proveedor);
		getTxtMontoPago().setText("$ " + producto);
		
		getBackButton().setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		//Año vencimiento
		String[] aniosVencimiento = new String[13];
		try{
			Fecha fechaNow = new Fecha();
			int anioActual = fechaNow.getAnio();
			
			for (int a = anioActual, i = 0; a <= anioActual+12; a++, i++){
				aniosVencimiento[i] = a+"";
			}
			
			//metemos array al Spinner
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this, 
				android.R.layout.simple_spinner_item, 
				aniosVencimiento
			);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    getCmbFechaVencimientoAnio().setAdapter(adapter);
		}catch(Exception e){
			Sys.log(e);
		}
		
		getBtnOk().setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				if (formValidator.validate()){
					process.start();
				}
			}
		});
	}
	
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtPassword(), "Contrase�a")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(12)
			.minLength(8);
		
		formValidator.addItem(getTxtCVV2(), "CVV2")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(4)
			.minLength(3)
			.isNumeric(true);
		
		formValidator.addItem(getTxtCelular(), "Celular")
			.canBeEmpty(false)
			.isRequired(true)
			.isPhoneNumber(true)
			.maxLength(10)
			.minLength(10);
		
//		formValidator.addItem(getCmbFechaVencimientoMes(), "", new Validable(){
//			public void validate() throws ErrorSys {
//				Fecha now = new Fecha();
//				now.setHora(0);
//				now.setMinuto(0);
//				now.setSegundo(0);
//				now.actualizar();
//				
//				Fecha fVencimiento = new Fecha();
//				fVencimiento.setDia(1);
//				fVencimiento.setMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
//				fVencimiento.setAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
//				fVencimiento.setHora(0);
//				fVencimiento.setMinuto(0);
//				fVencimiento.setSegundo(0);
//				fVencimiento.actualizar();
//				
//				if (fVencimiento.getTimeStampSeg() <= now.getTimeStampSeg()){
//					getCmbFechaVencimientoMes().requestFocus();
//					throw new ErrorSys("La fecha de vencimiento debe ser mayor a la fecha actual");
//				}
//			}
//		});
		
		formValidator.addItem(getTxtCelular2(), "Celular 2", new Validable(){
			public void validate() throws ErrorSys {
				if (!getTxtCelular2().getText().toString().equals(getTxtCelular().getText().toString())){
					getTxtCelular2().requestFocus();
					throw new ErrorSys("El n�mero telef�nico no coincide con el n�mero de confirmaci�n.");
				}
			}
		})
			.canBeEmpty(false)
		.isPhoneNumber(false)
		.maxLength(10)
			.minLength(10);
	}
	
	public void enableProviders(){
		locate.stopLocation();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Alerta!")
		.setMessage("Debe activar al menos un proveeder para obtener su localizacion")
		.setPositiveButton("OK", new DialogInterface.OnClickListener(){

			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
			}

		
			
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		if(locate.startLocation()){
			enableProviders();
		}
	}
	@Override
	protected void onDestroy(){
		super.onDestroy();
		
//		if(locate != null){
//			locate.stopLocation();
//		}
	}
	
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			
			switch (id) {
			case R.id.button_back:
				onBackPressed();
				break;
			case R.id.button_menu:
				onBackPressed();
				break;
			default:
				break;
			}
		}
	}
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.RESUMEN;
	}
	
	private void setImageBackground(LinearLayout layout, Drawable d) {

		if (Build.VERSION.SDK_INT >= 16)

		    layout.setBackground(d);
		else
		    layout.setBackgroundDrawable(d);
	}
}