package com.ironbit.mc.activities;

import java.util.ArrayList;

import org.addcel.util.GraphicUtil;
import org.apache.http.message.BasicNameValuePair;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.activities.widget.TerminosWidget;
import com.ironbit.mc.constant.Font;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.Fecha.TipoFecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Validador;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.system.errores.InfoSys;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.GetBanksWSClient;
import com.ironbit.mc.web.webservices.GetCardTypesWSClient;
import com.ironbit.mc.web.webservices.GetEstadosWSClient;
import com.ironbit.mc.web.webservices.GetProvidersWSClient;
import com.ironbit.mc.web.webservices.UserInsertWSClient;
import com.ironbit.mc.web.webservices.data.DataProvider;
import com.ironbit.mc.web.webservices.events.OnBanksResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnCardTypesResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnProvidersResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnUserRegisteredResponseReceivedListener;
import com.ironbit.mc.widget.SpinnerValues;
import com.ironbit.mc.widget.SpinnerValuesListener;
import com.ironbit.mc.widget.datepickerdialog.MyDatePickerDialog;

public class RegistroActivity extends MenuActivity {
	protected EditText txtUsuario = null;
	/*
	 * protected EditText txtPassword = null; protected EditText txtPassword2 =
	 * null;
	 */
	protected EditText txtNombre = null;
	protected EditText txtAPat = null;
	protected EditText txtAMat = null;
	protected Button btnFechaNacimiento = null;
	protected EditText txtDireccion = null;
	protected EditText txtCelular = null;
	protected EditText txtCelular2 = null;
	protected EditText txtMail = null;
	protected EditText txtMail2 = null;
	protected EditText txtTelCasa = null;
	protected EditText txtTelOficina = null;
	protected EditText txtCiudad = null;
	protected EditText txtCalle = null;
	protected EditText txtNumExt = null;
	protected EditText txtNumInt = null;
	protected EditText txtColonia = null;
	protected EditText txtCP = null;
	protected EditText txtCPAmex = null;

	protected Spinner cmbCtrlProveedor = null;
	protected SpinnerValues cmbProveedor = null;
	protected EditText txtTarjeta = null;
	protected Spinner cmbCtrlTipoTarjeta = null;
	protected SpinnerValues cmbTipoTarjeta = null;
	protected Spinner cmbCtrlBanco = null;
	protected SpinnerValues cmbBanco = null;
	protected Spinner cmbFechaVencimientoMes = null;
	protected Spinner cmbFechaVencimientoAnio = null;

	protected Spinner cmbSexo = null;
	protected Spinner cmbEstados = null;
	protected SpinnerValues cmbSVEstados = null;

	protected Button cancelarButton;
	protected Button registrarButton;
	protected TextView btnCondiciones = null;
	protected CheckBox checkCondiciones = null;
	protected Button btnAgregaTag = null;
	protected static final int FECHA_NACIMIENTO_DIALOG_ID = 0;
	protected MyDatePickerDialog fechaNacimientoDialog = null;
	protected Fecha fechaNacimiento = null;
	protected Fecha fechaVencimiento = null;
	protected GetProvidersWSClient providersWS = null;
	protected GetEstadosWSClient estadosWS = null;
	protected GetBanksWSClient banksWS = null;
	protected GetCardTypesWSClient cardTypesWS = null;
	protected final FormValidator formValidator = new FormValidator(this, true);
	protected LongProcess sendDataProcess = null;
	protected LongProcess loadDataProcess = null;
	protected UserInsertWSClient userRegisterWS = null;
	protected TerminosWidget terminosWidget = null;

	protected String tagTipo = "", tagNick = "", tagNum = "";
	int tagTipoId = 0, tagVerif = 0;

	protected MyDatePickerDialog getFechaNacimientoDialog() {
		if (fechaNacimientoDialog == null) {
			fechaNacimientoDialog = new MyDatePickerDialog(this,
					new DatePickerDialog.OnDateSetListener() {
						public void onDateSet(DatePicker view, int year,
								int monthOfYear, int dayOfMonth) {
							try {
								fechaNacimiento.setAnio(year);
								fechaNacimiento.setMesBase0(monthOfYear);
								fechaNacimiento.setDia(dayOfMonth);
								fechaNacimiento.actualizar();

								updateFechaNacimiento();
							} catch (ErrorSys e) {
								e.showMsj(RegistroActivity.this);
							}
						}
					}, fechaNacimiento.getAnio(),
					fechaNacimiento.getMesBase0(), fechaNacimiento.getDia());
		}

		return fechaNacimientoDialog;
	}

	protected CheckBox getCheckCondiciones() {
		if (checkCondiciones == null) {
			checkCondiciones = (CheckBox) findViewById(R.id.checkCondiciones);
		}

		return checkCondiciones;
	}

	protected EditText getTxtUsuario() {
		if (txtUsuario == null) {
			txtUsuario = (EditText) findViewById(R.id.txtUsuario);
		}

		return txtUsuario;
	}

	/*
	 * protected EditText getTxtPassword(){ if (txtPassword == null){
	 * txtPassword = (EditText)findViewById(R.id.txtPassword); }
	 * 
	 * return txtPassword; }
	 * 
	 * 
	 * protected EditText getTxtPassword2(){ if (txtPassword2 == null){
	 * txtPassword2 = (EditText)findViewById(R.id.txtPassword2); }
	 * 
	 * return txtPassword2; }
	 */

	protected EditText getTxtMail() {
		if (txtMail == null) {
			txtMail = (EditText) findViewById(R.id.txtEmail);
		}

		return txtMail;
	}

	protected EditText getTxtTelCasa() {
		if (txtTelCasa == null) {
			txtTelCasa = (EditText) findViewById(R.id.txtTelCasa);
		}

		return txtTelCasa;
	}

	protected EditText getTxtTelOficina() {
		if (txtTelOficina == null) {
			txtTelOficina = (EditText) findViewById(R.id.txtTelOficina);
		}

		return txtTelOficina;
	}

	protected EditText getTxtCiudad() {
		if (txtCiudad == null) {
			txtCiudad = (EditText) findViewById(R.id.txtCiudad);
		}

		return txtCiudad;
	}

	protected EditText getTxtCalle() {
		if (txtCalle == null) {
			txtCalle = (EditText) findViewById(R.id.txtCalle);
		}

		return txtCalle;
	}

	protected EditText getTxtColonia() {
		if (txtColonia == null) {
			txtColonia = (EditText) findViewById(R.id.txtColonia);
		}

		return txtColonia;
	}

	protected EditText getTxtCP() {
		if (txtCP == null) {
			txtCP = (EditText) findViewById(R.id.txtCP);
		}

		return txtCP;
	}

	protected EditText getTxtNumExt() {
		if (txtNumExt == null) {
			txtNumExt = (EditText) findViewById(R.id.txtNumExt);
		}

		return txtNumExt;
	}

	protected EditText getTxtNumInt() {
		if (txtNumInt == null) {
			txtNumInt = (EditText) findViewById(R.id.txtNumInt);
		}

		return txtNumInt;
	}

	protected EditText getTxtMail2() {
		if (txtMail2 == null) {
			txtMail2 = (EditText) findViewById(R.id.txtEmail2);
		}

		return txtMail2;
	}

	protected EditText getTxtNombre() {
		if (txtNombre == null) {
			txtNombre = (EditText) findViewById(R.id.txtNombre);
		}

		return txtNombre;
	}

	protected EditText getTxtApellidoPaterno() {
		if (txtAPat == null) {
			txtAPat = (EditText) findViewById(R.id.txtApellidoP);
		}

		return txtAPat;
	}

	protected EditText getTxtApellidoMaterno() {
		if (txtAMat == null) {
			txtAMat = (EditText) findViewById(R.id.txtApellidoM);
		}

		return txtAMat;
	}

	protected Button getBtnFechaNacimiento() {
		if (btnFechaNacimiento == null) {
			btnFechaNacimiento = (Button) findViewById(R.id.btnFechaNacimiento);
		}

		return btnFechaNacimiento;
	}

	protected Button getBtnAgregaTag() {
		if (btnAgregaTag == null) {
			btnAgregaTag = (Button) findViewById(R.id.btnAgregaTag);
		}

		return btnAgregaTag;
	}

	protected EditText getTxtDireccion() {
		if (txtDireccion == null) {
			txtDireccion = (EditText) findViewById(R.id.txtDomicilio);
		}

		return txtDireccion;
	}

	protected EditText getTxtCpAmex() {
		if (txtCPAmex == null) {
			txtCPAmex = (EditText) findViewById(R.id.txtCPAmex);
		}

		return txtCPAmex;
	}

	protected EditText getTxtCelular() {
		if (txtCelular == null) {
			txtCelular = (EditText) findViewById(R.id.txtNoCelular);
		}

		return txtCelular;
	}

	protected EditText getTxtCelular2() {
		if (txtCelular2 == null) {
			txtCelular2 = (EditText) findViewById(R.id.txtNoCelular2);
		}

		return txtCelular2;
	}

	protected Spinner getCmbSexo() {
		if (cmbSexo == null) {
			cmbSexo = (Spinner) findViewById(R.id.spin_sexo);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter
					.createFromResource(this, R.array.sexo,
							android.R.layout.simple_spinner_item);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			cmbSexo.setAdapter(adapter);
		}

		return cmbSexo;
	}

	protected Spinner getCmbEstados() {
		if (cmbEstados == null) {
			cmbEstados = (Spinner) findViewById(R.id.spin_estados);
			/*
			 * ArrayAdapter<CharSequence> adapter =
			 * ArrayAdapter.createFromResource( this, R.array.estados,
			 * android.R.layout.simple_spinner_item);
			 * adapter.setDropDownViewResource
			 * (android.R.layout.simple_spinner_dropdown_item);
			 * cmbEstados.setAdapter(adapter);
			 */
		}

		return cmbEstados;
	}

	protected SpinnerValues getCmbSVEstados() {
		if (cmbSVEstados == null) {
			cmbSVEstados = new SpinnerValues(this, getCmbEstados());
			/*
			 * ArrayAdapter<CharSequence> adapter =
			 * ArrayAdapter.createFromResource( this, R.array.estados,
			 * android.R.layout.simple_spinner_item);
			 * adapter.setDropDownViewResource
			 * (android.R.layout.simple_spinner_dropdown_item);
			 * cmbEstados.setAdapter(adapter);
			 */
		}

		return cmbSVEstados;
	}

	protected Spinner getCmbCtrlProveedor() {
		if (cmbCtrlProveedor == null) {
			cmbCtrlProveedor = (Spinner) findViewById(R.id.cmbProveedor);
		}

		return cmbCtrlProveedor;
	}

	protected SpinnerValues getCmbProveedor() {
		if (cmbProveedor == null) {
			cmbProveedor = new SpinnerValues(this, getCmbCtrlProveedor());
		}

		return cmbProveedor;
	}

	/*
	 * protected Spinner getCmbCtrlBanco(){ if (cmbCtrlBanco == null){
	 * cmbCtrlBanco = (Spinner)findViewById(R.id.cmbBanco); }
	 * 
	 * return cmbCtrlBanco; }
	 */

	/*
	 * protected SpinnerValues getCmbBanco(){ if (cmbBanco == null){ cmbBanco =
	 * new SpinnerValues(this, getCmbCtrlBanco()); }
	 * 
	 * return cmbBanco; }
	 */

	protected Spinner getCmbCtrlTipoTarjeta() {
		if (cmbCtrlTipoTarjeta == null) {
			cmbCtrlTipoTarjeta = (Spinner) findViewById(R.id.cmbTipoTarjeta);
		}

		return cmbCtrlTipoTarjeta;
	}

	protected SpinnerValues getCmbTipoTarjeta() {
		if (cmbTipoTarjeta == null) {
			cmbTipoTarjeta = new SpinnerValues(this, getCmbCtrlTipoTarjeta());
		}

		return cmbTipoTarjeta;
	}

	protected EditText getTxtTarjeta() {
		if (txtTarjeta == null) {
			txtTarjeta = (EditText) findViewById(R.id.txtNoTarjetaCredito);
		}

		return txtTarjeta;
	}

	protected Spinner getCmbFechaVencimientoAnio() {
		if (cmbFechaVencimientoAnio == null) {
			cmbFechaVencimientoAnio = (Spinner) findViewById(R.id.cmbFechaVencimientoAnio);
		}

		return cmbFechaVencimientoAnio;
	}

	protected Spinner getCmbFechaVencimientoMes() {
		if (cmbFechaVencimientoMes == null) {
			cmbFechaVencimientoMes = (Spinner) findViewById(R.id.cmbFechaVencimientoMes);
		}

		return cmbFechaVencimientoMes;
	}

	protected TextView getBtnCondiciones() {
		if (btnCondiciones == null) {
			btnCondiciones = (TextView) findViewById(R.id.btnCondiciones);
		}

		return btnCondiciones;
	}

	protected Button getBtnRegistrar() {
		if (registrarButton == null) {
			registrarButton = (Button) findViewById(R.id.btnRegistrarse);

			System.out.println("ResName: "
					+ getResources().getResourceName(R.id.btnRegistrarse));
		}

		return registrarButton;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.registro);

		loadDataProcess = new LongProcess(this, true, 60, true, "LoadUserData",
				new LongProcessListener() {
					public void doProcess() {
						// inicializamos proveedores
						providersWS = new GetProvidersWSClient(
								RegistroActivity.this);
						providersWS
								.execute(new OnProvidersResponseReceivedListener() {
									public void onProvidersResponseReceived(
											ArrayList<DataProvider> providers) {
										ArrayList<BasicNameValuePair> data = new ArrayList<BasicNameValuePair>();

										for (DataProvider dp : providers) {
											data.add(new BasicNameValuePair(dp
													.getDescription(), dp
													.getClave() + ""));
										}

										getCmbProveedor().setDatos(data);

										// inicializamos bancos
										banksWS = new GetBanksWSClient(
												RegistroActivity.this);
										banksWS.execute(new OnBanksResponseReceivedListener() {
											public void onBanksResponseReceived(
													ArrayList<BasicNameValuePair> bancos) {
												// getCmbBanco().setDatos(bancos);

												// inicializamos tipos de
												// tarjetas
												cardTypesWS = new GetCardTypesWSClient(
														RegistroActivity.this);
												cardTypesWS
														.execute(new OnCardTypesResponseReceivedListener() {
															public void onCardTypesResponseReceived(
																	ArrayList<BasicNameValuePair> cardTypes) {
																getCmbTipoTarjeta()
																		.setDatos(
																				cardTypes);

																// desbloqueamos
																// boton de
																// registro
																getBtnRegistrar()
																		.setEnabled(
																				true);

																estadosWS = new GetEstadosWSClient(
																		RegistroActivity.this);
																estadosWS
																		.execute(new OnBanksResponseReceivedListener() {

																			public void onBanksResponseReceived(
																					ArrayList<BasicNameValuePair> ests) {
																				// TODO
																				// Auto-generated
																				// method
																				// stu
																				getCmbSVEstados()
																						.setDatos(
																								ests);
																				loadDataProcess
																						.processFinished();

																			}

																		});
																// terminamos el
																// proceso

															}
														});
											}
										});
									}
								});
					}

					public void stopProcess() {
						if (providersWS != null) {
							providersWS.cancel();
							providersWS = null;
						}

						if (banksWS != null) {
							banksWS.cancel();
							banksWS = null;
						}

						if (cardTypesWS != null) {
							cardTypesWS.cancel();
							cardTypesWS = null;
						}

						if (estadosWS != null) {
							estadosWS.cancel();
							estadosWS = null;
						}
					}
				});

		sendDataProcess = new LongProcess(this, true, 40, true,
				"processRegistro", new LongProcessListener() {
					public void doProcess() {
						userRegisterWS = new UserInsertWSClient(
								RegistroActivity.this);
						userRegisterWS.setLogin(getTxtUsuario().getText()
								.toString());
						// userRegisterWS.setPassword(getTxtPassword().getText().toString());
						userRegisterWS.setPassword("mCL8m39sJ1");
						userRegisterWS.setNombre(getTxtNombre().getText()
								.toString());
						userRegisterWS.setApellido(getTxtApellidoPaterno()
								.getText().toString());
						userRegisterWS.setMaterno(getTxtApellidoMaterno()
								.getText().toString());
						userRegisterWS.setSexo(""
								+ getCmbSexo().getSelectedItem().toString()
										.charAt(0));
						userRegisterWS.setTelCasa(getTxtTelCasa().getText()
								.toString());
						userRegisterWS.setTelOficina(getTxtTelOficina()
								.getText().toString());
						userRegisterWS.setCiudad(getTxtCiudad().getText()
								.toString());
						userRegisterWS.setCalle(getTxtCalle().getText()
								.toString());
						userRegisterWS.setNumExt(Integer
								.parseInt(getTxtNumExt().getText().toString()));
						userRegisterWS.setNumInt(getTxtNumInt().getText()
								.toString());
						userRegisterWS.setColonia(getTxtColonia().getText()
								.toString());

						if (getCmbTipoTarjeta().getValorSeleccionado().equals(
								"3")) {
							userRegisterWS.setCP(Integer
									.parseInt(getTxtCpAmex().getText()
											.toString()));
						} else {
							userRegisterWS.setCP(Integer.parseInt(getTxtCP()
									.getText().toString()));
						}
						userRegisterWS.setEstado(Integer
								.parseInt(getCmbSVEstados()
										.getValorSeleccionado()));
						userRegisterWS.setFechaNacimiento(fechaNacimiento);
						userRegisterWS.setDireccion(getTxtDireccion().getText()
								.toString());
						userRegisterWS.setDomAmex(getTxtDireccion().getText()
								.toString());
						userRegisterWS.setEmail(getTxtMail().getText()
								.toString());
						userRegisterWS.setCelular(getTxtCelular().getText()
								.toString());
						userRegisterWS.setProveedor(Integer
								.parseInt(getCmbProveedor()
										.getValorSeleccionado()));
						userRegisterWS.setTarjeta(getTxtTarjeta().getText()
								.toString());
						userRegisterWS.setTipoTarjeta(Integer
								.parseInt(getCmbTipoTarjeta()
										.getValorSeleccionado()));
						// userRegisterWS.setBanco(Integer.parseInt(getCmbBanco().getValorSeleccionado()));
						userRegisterWS.setBanco(1);
						userRegisterWS.setFechaVencimientoMes(Fecha
								.mesStringToInt(getCmbFechaVencimientoMes()
										.getSelectedItem().toString()));
						userRegisterWS.setFechaVencimientoAnio(Integer
								.parseInt(getCmbFechaVencimientoAnio()
										.getSelectedItem().toString()));

						userRegisterWS.setEtiqueta(tagNick);
						userRegisterWS.setIdtiporecargatag(tagTipoId);
						userRegisterWS.setNumeroTag(tagNum);
						userRegisterWS.setDv(tagVerif);

						userRegisterWS
								.execute(new OnUserRegisteredResponseReceivedListener() {
									public void onUserRegisteredResponseReceived(
											String resultado, String mensaje) {
										if (resultado.trim().equals("1")) {
											// Guardamos los datos de inicio de
											// sesión para futuras peticiones a
											// los WS
											Usuario.setLogin(
													RegistroActivity.this,
													getTxtUsuario().getText()
															.toString());
											// Usuario.setPass(RegistroActivity.this,
											// getTxtPassword().getText().toString());
											// System.out.println("res: "+resultado+" msj: "+mensaje);
											InfoSys.showMsj(
													RegistroActivity.this,
													mensaje);

											// abrimos pantalla de menú y
											// borramos esta actividad
											IniciarSesionActivity.termina = true;
											// startActivity(new
											// Intent(RegistroActivity.this,
											// MenuAppActivity.class));
											finish();
										} else {
											ErrorSys.showMsj(
													RegistroActivity.this,
													mensaje);
										}

										// avisamos que el proceso ya terminó
										sendDataProcess.processFinished();
									}
								});
					}

					public void stopProcess() {
						if (userRegisterWS != null) {
							userRegisterWS.cancel();
							userRegisterWS = null;
						}
					}
				});

		(findViewById(R.id.user_sign))
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						Toast.makeText(v.getContext(),
								getString(R.string.user_chars),
								Toast.LENGTH_SHORT).show();

					}
				});

		/*
		 * (findViewById(R.id.pass_sign)).setOnClickListener(new
		 * OnClickListener(){
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Toast.makeText(v.getContext(), getString(R.string.pass_chars),
		 * Toast.LENGTH_SHORT).show();
		 * 
		 * } });
		 */
		(findViewById(R.id.tel_sign)).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), getString(R.string.tel_chars),
						Toast.LENGTH_SHORT).show();

			}
		});

		(findViewById(R.id.tel_sign2))
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						Toast.makeText(v.getContext(),
								getString(R.string.tel_chars),
								Toast.LENGTH_SHORT).show();

					}
				});

		loadDataProcess.start();
	}

	@Override
	protected void onStop() {
		if (terminosWidget != null) {
			terminosWidget.onStop();
		}

		sendDataProcess.stop();
		super.onStop();
	}

	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		terminosWidget = new TerminosWidget(this);
		setTypeface();
		// inicializamos fechas
		try {
			fechaNacimiento = new Fecha();
			fechaNacimiento.restarAnios(18);
			fechaVencimiento = new Fecha("1990-01-01", TipoFecha.date);
		} catch (ErrorSys e) {
			e.showMsj(this);
		}

		// Botón Fecha Nacimiento
		getBtnFechaNacimiento().setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// showDialog(FECHA_NACIMIENTO_DIALOG_ID);
				getFechaNacimientoDialog().show();
			}
		});

		// Boton Registrarse
		getBtnRegistrar().setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (formValidator.validate()) {
					sendDataProcess.start();
				}
			}
		});

		// Boton Terminos y Condiciones
		getBtnCondiciones().setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				terminosWidget.show();
			}
		});

		// fecha de nacimiento
		getFechaNacimientoDialog().setMinDate(1, 0, 1911);
		getFechaNacimientoDialog().setMaxDate(fechaNacimiento.getDia(),
				fechaNacimiento.getMesBase0(), fechaNacimiento.getAnio());

		getBtnAgregaTag().setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(RegistroActivity.this,
						AgregaTagActivity.class);
				intent.putExtra("tipo", tagTipo);
				intent.putExtra("nick", tagNick);
				intent.putExtra("num", tagNum);
				intent.putExtra("verif", tagVerif);
				startActivityForResult(intent, 1);
			}
		});

		// Año vencimiento
		String[] aniosVencimiento = new String[13];
		try {
			Fecha fechaNow = new Fecha();
			int anioActual = fechaNow.getAnio();

			for (int a = anioActual, i = 0; a <= anioActual + 12; a++, i++) {
				aniosVencimiento[i] = a + "";
			}

			// metemos array al Spinner
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, aniosVencimiento);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			getCmbFechaVencimientoAnio().setAdapter(adapter);
		} catch (Exception e) {
			Sys.log(e);
		}

		// getTxtDireccion().set
		System.out.println("setListener");
		setCmbCardsListener();

		updateFechaNacimiento();
	}

	public void setCmbCardsListener() {
		getCmbTipoTarjeta().setSpinListener(new SpinnerValuesListener() {

			public void ItemSelected(String texto, String value) {
				// TODO Auto-generated method stub
				if (value.equals("3")) {
					getTxtDireccion().setEnabled(true);
					getTxtDireccion().setText("");
					getTxtCpAmex().setEnabled(true);
					getTxtCpAmex().setText("");
				} else {
					getTxtDireccion().setEnabled(false);
					getTxtDireccion().setText("");
					getTxtCpAmex().setEnabled(false);
					getTxtCpAmex().setText("");
				}
			}

		});
	}

	@Override
	public void configValidations() {
		formValidator.addItem(getTxtUsuario(), "Usuario").canBeEmpty(false)
				.isRequired(true).maxLength(16).minLength(4);

		/*
		 * formValidator.addItem(getTxtPassword(), "Contrase�a", new
		 * Validable(){ public void validate() throws ErrorSys { if
		 * (!getTxtPassword
		 * ().getText().toString().equals(getTxtPassword2().getText
		 * ().toString())){ getTxtPassword2().requestFocus(); throw new
		 * ErrorSys("Las contrase�as deben coincidir entre s�"); } } })
		 * .canBeEmpty(false) .isRequired(true) .maxLength(12) .minLength(8);
		 * 
		 * formValidator.addItem(getTxtPassword2(), "Contrase�a 2")
		 * .canBeEmpty(false) .isRequired(true) .maxLength(12) .minLength(8);
		 */

		formValidator
				.addItem(getTxtMail(), "Correo Electr�nico", new Validable() {
					public void validate() throws ErrorSys {
						if (!getTxtMail().getText().toString()
								.equals(getTxtMail2().getText().toString())) {
							getTxtMail2().requestFocus();
							throw new ErrorSys(
									"Los correos electr�nicos deben coincidir entre s�");
						}
					}
				}).canBeEmpty(false).maxLength(60).isEmail(true);

		formValidator.addItem(getTxtNombre(), "Nombre").canBeEmpty(false)
				.isRequired(true).maxLength(50).minLength(3);

		formValidator.addItem(getTxtApellidoPaterno(), "Apellido Paterno")
				.canBeEmpty(false).isRequired(true).maxLength(50);
		formValidator.addItem(getTxtApellidoPaterno(), "Apellido Materno")
				.canBeEmpty(true).isRequired(false).maxLength(50);

		formValidator
				.addItem(getTxtCelular(), "N�mero de celular", new Validable() {
					public void validate() throws ErrorSys {
						if (!getTxtCelular().getText().toString()
								.equals(getTxtCelular2().getText().toString())) {
							getTxtCelular().requestFocus();
							throw new ErrorSys(
									"N�meros de celular no coinciden");
						}

						if (!Validador.esNumeroCelularMX(getTxtCelular()
								.getText().toString())) {
							getTxtCelular().requestFocus();
							throw new ErrorSys(
									"El n�mero de celular debe ser de 10 d�gitos");
						}
					}
				}).canBeEmpty(false).isRequired(true);

		formValidator.addItem(getTxtTarjeta(), "Tarjeta de cr�dito")
				.canBeEmpty(false).isRequired(true).isNumeric(true)
				.maxLength(20).minLength(13);

		formValidator.addItem(getCmbSexo(), "Sexo", new Validable() {

			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				// System.out.println("Sex item pos: "+getCmbSexo().getSelectedItemPosition());

				if (getCmbSexo().getSelectedItemPosition() == 0) {
					throw new ErrorSys("Debe seleccionar su sexo");
				}
			}

		});

		/*
		 * formValidator.addItem(getCmbEstados(), "Estado", new Validable(){
		 * 
		 * @Override public void validate() throws ErrorSys { // TODO
		 * Auto-generated method stub
		 * //System.out.println("Estado item pos: "+getCmbEstados
		 * ().getSelectedItemPosition());
		 * if(getCmbEstados().getSelectedItemPosition()==0){ throw new
		 * ErrorSys("Debe seleccionar su estado"); } }
		 * 
		 * });
		 */

		formValidator.addItem(getTxtTelCasa(), "Tel�fono Casa")
				.canBeEmpty(true).isRequired(false).isPhoneNumber(true)
				.minLength(8).maxLength(10);

		formValidator.addItem(getTxtTelOficina(), "Tel�fono Oficina")
				.canBeEmpty(true).isRequired(false).isPhoneNumber(true)
				.minLength(8).maxLength(10);

		formValidator.addItem(getTxtCiudad(), "Ciudad").canBeEmpty(false)
				.isRequired(true).maxLength(50);

		formValidator.addItem(getTxtCalle(), "Calle").canBeEmpty(false)
				.isRequired(true).maxLength(50);

		formValidator.addItem(getTxtNumExt(), "N�mero exterior")
				.canBeEmpty(false).isRequired(true).isNumeric(true)
				.maxLength(5);

		formValidator.addItem(getTxtNumInt(), "N�mero interior")
				.canBeEmpty(true).isRequired(false).maxLength(20);

		formValidator.addItem(getTxtColonia(), "Colonia").canBeEmpty(false)
				.isRequired(true).maxLength(50);

		formValidator.addItem(getTxtCP(), "C�digo Postal").canBeEmpty(true)
				.isRequired(false).isNumeric(true).maxLength(6);

		formValidator
				.addItem(getTxtCpAmex(), "C�digo Postal Amex", new Validable() {
					public void validate() throws ErrorSys {
						if (getCmbTipoTarjeta().getValorSeleccionado().equals(
								"3")) {
							if (getTxtCpAmex().getText().toString().trim()
									.equals("")) {
								throw new ErrorSys(
										"Escriba el C�digo Postal de su estado de cuentaTO AMEX");
							}
						}
					}
				}).canBeEmpty(true).isRequired(false).isNumeric(true)
				.maxLength(6);

		formValidator.addItem(getCmbFechaVencimientoMes(),
				"Fecha de Vencimiento", new Validable() {
					public void validate() throws ErrorSys {
						Fecha now = new Fecha();
						now.setHora(0);
						now.setMinuto(0);
						now.setSegundo(0);
						now.actualizar();

						Fecha fVencimiento = new Fecha();
						fVencimiento.setDia(1);
						fVencimiento.setMes(Fecha
								.mesStringToInt(getCmbFechaVencimientoMes()
										.getSelectedItem().toString()));
						fVencimiento.setAnio(Integer
								.parseInt(getCmbFechaVencimientoAnio()
										.getSelectedItem().toString()));
						fVencimiento.setHora(0);
						fVencimiento.setMinuto(0);
						fVencimiento.setSegundo(0);
						fVencimiento.actualizar();

						if (fVencimiento.getTimeStampSeg() <= now
								.getTimeStampSeg()) {
							getCmbFechaVencimientoMes().requestFocus();
							throw new ErrorSys(
									"La fecha de vencimiento debe ser mayor a la fecha actual");
						}
					}
				});

		formValidator
				.addItem(getTxtDireccion(), "Domicilio de Estado de Cuenta",
						new Validable() {

							public void validate() throws ErrorSys {
								if (getCmbTipoTarjeta().getValorSeleccionado()
										.equals("3")) {

									if (getTxtDireccion().getText().toString()
											.trim().equals("")) {
										throw new ErrorSys(
												"Escriba el domicilio del estado de cuentaTO de la tarjeta AMEX.");
									}
								}
							}

						}).isRequired(false).canBeEmpty(true);

		formValidator.addItem(getCheckCondiciones(), "T�rminos y condiciones",
				new Validable() {
					public void validate() throws ErrorSys {
						if (!getCheckCondiciones().isChecked()) {
							getCheckCondiciones().requestFocus();
							throw new ErrorSys(
									"Es necesario Aceptar los Terminos y Condiciones de Uso");
						}
					}
				});

	}

	protected void updateFechaNacimiento() {
		if (fechaNacimiento != null) {
			getBtnFechaNacimiento()
					.setText(
							"Seleccionar Fecha ["
									+ fechaNacimiento
											.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYYYY)
									+ "]");
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {
			tagTipo = data.getStringExtra("tipo");
			tagTipoId = data.getIntExtra("tipoId", 0);
			tagNick = data.getStringExtra("nick");
			tagNum = data.getStringExtra("num");
			tagVerif = data.getIntExtra("verif", 0);
			
			GraphicUtil.setBackgroundDrawableWithBuildValidation(getBtnAgregaTag(),
					getResources().getDrawable(
							R.drawable.selector_addtaggreen));
			
			Toast.makeText(this, "Numero TAG guardado", Toast.LENGTH_LONG)
					.show();
		} else if (resultCode == 2) {
			tagTipo = "";
			tagTipoId = 0;
			tagNick = "";
			tagNum = "";
			tagVerif = 0;

			GraphicUtil.setBackgroundDrawableWithBuildValidation(getBtnAgregaTag(),
					getResources().getDrawable(R.drawable.selector_addtag));

			Toast.makeText(this, "Numero TAG eliminado", Toast.LENGTH_LONG)
					.show();
		}
	}

	@Override
	public Actividades getIdActividad() {
		return Actividades.REGISTRO;
	}

	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();

			switch (id) {
			case R.id.button_back:
				onBackPressed();
				break;
			case R.id.button_cancelar:
				onBackPressed();
				break;
			default:
				break;
			}
		}
	}

	private void setTypeface() {
		Typeface type = Typeface.createFromAsset(getAssets(),
				Font.GANDHISANS_REG);
		((TextView) findViewById(R.id.label_login)).setTypeface(type);
		((TextView) findViewById(R.id.label_nombre)).setTypeface(type);
		((TextView) findViewById(R.id.label_paterno)).setTypeface(type);
		((TextView) findViewById(R.id.label_materno)).setTypeface(type);
		((TextView) findViewById(R.id.label_nacimiento)).setTypeface(type);
		((TextView) findViewById(R.id.label_sexo)).setTypeface(type);
		((TextView) findViewById(R.id.label_celular)).setTypeface(type);
		((TextView) findViewById(R.id.label_confcel)).setTypeface(type);
		((TextView) findViewById(R.id.label_proveedor)).setTypeface(type);
		((TextView) findViewById(R.id.label_correo)).setTypeface(type);
		((TextView) findViewById(R.id.label_confcorreo)).setTypeface(type);
		((TextView) findViewById(R.id.label_casa)).setTypeface(type);
		((TextView) findViewById(R.id.label_oficina)).setTypeface(type);
		((TextView) findViewById(R.id.label_estado)).setTypeface(type);
		((TextView) findViewById(R.id.label_ciudad)).setTypeface(type);
		((TextView) findViewById(R.id.label_exterior)).setTypeface(type);
		((TextView) findViewById(R.id.label_interior)).setTypeface(type);
		((TextView) findViewById(R.id.label_colonia)).setTypeface(type);
		((TextView) findViewById(R.id.label_cp)).setTypeface(type);
		((TextView) findViewById(R.id.label_tarjeta)).setTypeface(type);
		((TextView) findViewById(R.id.label_tipo)).setTypeface(type);
		((TextView) findViewById(R.id.label_cptarjeta)).setTypeface(type);
		((TextView) findViewById(R.id.label_domtarjeta)).setTypeface(type);
		((TextView) findViewById(R.id.label_vigencia)).setTypeface(type);
		((CheckBox) findViewById(R.id.checkCondiciones)).setTypeface(type);

	}

}