package com.ironbit.mc.activities;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.widget.datepickerdialog.MyDatePickerDialog;

public class RecordatorioActivity extends MenuActivity {

	EditText eventoText;
	Button fechaButton, horaButton;
	Fecha eventoDate;
	MyDatePickerDialog eventoDateDialog;
	TimePickerDialog timePickerDialog;
	int hora, minuto;
	
	LongProcess process;

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.RECORDATORIOS;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_recordatorio);
		
		process = new LongProcess(RecordatorioActivity.this, true, 10, true, "record_event", new LongProcessListener() {
			
			public void stopProcess() {
				clean();				
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				Calendar beginTime = Calendar.getInstance();
				beginTime.set(eventoDate.getAnio(), eventoDate.getMesBase0(), eventoDate.getDia());
				
				Intent intent = new Intent(Intent.ACTION_INSERT)
			        .setData(Events.CONTENT_URI)
			        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
	//		        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
			        .putExtra(Events.TITLE, "ALERTA MOBILECARD")
			        .putExtra(Events.DESCRIPTION, eventoText.getText().toString().trim())
			        .putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
				
				startActivity(intent);
		
			process.processFinished();
			}
		});
	}

	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		try {
			eventoDate = new Fecha();
		} catch (ErrorSys e) {
			e.showMsj(this);
		}
		
		eventoText = (EditText) findViewById(R.id.text_evento);
		fechaButton = (Button) findViewById(R.id.button_fecha);
		horaButton = (Button) findViewById(R.id.button_hora);

		fechaButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				getEventoDateDialog().show();
			}
		});
		
		horaButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getTimePickerDialog().show();
			}
		});

	}
	
	public TimePickerDialog getTimePickerDialog() {
		if (timePickerDialog == null) {
			final Calendar c = Calendar.getInstance();
			
			timePickerDialog = new TimePickerDialog(RecordatorioActivity.this, new OnTimeSetListener() {
				
				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					// TODO Auto-generated method stub
					
					try {
						hora = hourOfDay;
						minuto = minute;
						eventoDate.setHora(hora);
						eventoDate.setMinuto(minuto);
	
						eventoDate.actualizar();
						horaButton.setText(hora + ":" + minuto);
					} catch (ErrorSys e) {
						e.showMsj(RecordatorioActivity.this);
					}
				}
			}, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);
		}
		
		return timePickerDialog;
	}

	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();

			switch (id) {
			case R.id.button_grabar:
				if (eventoDate == null || eventoText.getText().toString().trim().isEmpty())
					ErrorSys.showMsj(RecordatorioActivity.this, "Ingrese nombre y fechas del recordatorio");
				else
					process.start();
				break;
			case R.id.button_agregar:
				clean();
				break;

			default:
				onBackPressed();
				break;
			}
		}
	}

	protected MyDatePickerDialog getEventoDateDialog() {
		if (eventoDateDialog == null) {
			eventoDateDialog = new MyDatePickerDialog(this,
					new DatePickerDialog.OnDateSetListener() {
						public void onDateSet(DatePicker view, int year,
								int monthOfYear, int dayOfMonth) {
							try {
								eventoDate.setAnio(year);
								eventoDate.setMesBase0(monthOfYear);
								eventoDate.setDia(dayOfMonth);
								eventoDate.actualizar();

								updateEventoDate();
							} catch (ErrorSys e) {
								e.showMsj(RecordatorioActivity.this);
							}
						}
					}, eventoDate.getAnio(), eventoDate.getMesBase0(),
					eventoDate.getDia());
		}

		return eventoDateDialog;
	}
	
	private void clean() {
		startActivity(new Intent(RecordatorioActivity.this, RecordatorioActivity.class));
		finish();
		
	}

	private void updateEventoDate() {
		if (eventoDate != null) {
			fechaButton
					.setText("Seleccionar Fecha ["
							+ eventoDate
									.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYYYY)
							+ "]");
		}
	}

}
