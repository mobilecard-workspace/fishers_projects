package com.ironbit.mc.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.constant.Font;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.Validador;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.usuario.OnUserRegistradoListener;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.GetRecoveryWS;
import com.ironbit.mc.web.webservices.SetSmsWSClient;
import com.ironbit.mc.web.webservices.UserLoginWSClient;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;

public class IniciarSesionActivity extends MenuActivity{
	protected Button btnOk = null;
	protected Button btnReenvia = null;
	protected Button btnRegistrar = null;
	protected EditText txtUsuario = null; 
	protected EditText txtPassword = null; 
	protected final FormValidator formValidator = new FormValidator(this, true);
	protected UserLoginWSClient login = null;
	protected SetSmsWSClient ws = null;
	protected LongProcess iniciarSesionProcess = null;
	protected LongProcess processSms = null;
	protected TextView recPass = null;
	public static boolean termina = false; 
	public static final int SMS_DIALOG = 1;
	
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	protected Button getBtnReenvia(){
		if (btnReenvia == null){
			btnReenvia = (Button)findViewById(R.id.btnReenviaSms);
		}
		
		return btnReenvia;
	}
	
	protected Button getBtnRegistrar(){
		if (btnRegistrar == null){
			btnRegistrar = (Button)findViewById(R.id.btnRegistrarse);
			
		}
		
		return btnRegistrar;
	}
	
	
	protected EditText getTxtUsuario(){
		if (txtUsuario == null){
			txtUsuario = (EditText)findViewById(R.id.txtUsuario);
		}
		
		return txtUsuario;
	}
	
	
	protected EditText getTxtPassword(){
		if (txtPassword == null){
			txtPassword = (EditText)findViewById(R.id.txtPassword);
		}
		
		return txtPassword;
	}
	
	protected TextView getRecPass(){
		if(recPass == null){
			recPass = (TextView) findViewById(R.id.btnRecuperarPass);
		}
		return recPass;
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		ScrollView root = (ScrollView) findViewById(R.id.root);
		root.requestFocus();
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Display display = getWindowManager().getDefaultDisplay();
		//System.out.println("Screen: "+display.getWidth()+"x"+display.getHeight());
		inicializarGUI(R.layout.inicio_sesion);
		
		Usuario.reset(this);
		
		iniciarSesionProcess = new LongProcess(this, true, 30, true, "iniciarSesion", new LongProcessListener(){
			public void doProcess() {
				Usuario.estaRegistrado(IniciarSesionActivity.this, getTxtUsuario().getText().toString(), getTxtPassword().getText().toString(), 
						new OnUserRegistradoListener(){
					public void onUserRegistradoListener(boolean isRegistrado, String mensaje) {
						if (isRegistrado){
							//Guardamos los datos de inicio de sesión para futuras peticiones a los WS
							
							String[] response = mensaje.split("\\|");
							
							
							String id = response[1];
							
							Usuario.setLogin(IniciarSesionActivity.this, getTxtUsuario().getText().toString());
							Usuario.setPass(IniciarSesionActivity.this, getTxtPassword().getText().toString());
							Usuario.setIdUser(IniciarSesionActivity.this, id);

							Toast t = Toast.makeText(IniciarSesionActivity.this, response[0], Toast.LENGTH_LONG);
							t.setGravity(Gravity.CENTER, 0, 0);
							t.show();
							
							finish();
							
							
//							abrimos menu y cerramos esta pantalla, ahora solo se cerrar�
//							startActivity(new Intent(IniciarSesionActivity.this, MenuAppActivity.class));
							termina = true;
						}else{
							if(mensaje.equals("")){
								mensaje = "Intentelo m�s tarde";
							}
							ErrorSys.showMsj(IniciarSesionActivity.this, mensaje);
						}
						
						iniciarSesionProcess.processFinished();
					}

					
					public void onUserDataUpdate(String mensaje) {
						// TODO Auto-generated method stub
						Usuario.setLogin(IniciarSesionActivity.this, getTxtUsuario().getText().toString());
						Usuario.setPass(IniciarSesionActivity.this, getTxtPassword().getText().toString());
						startActivity(new Intent(IniciarSesionActivity.this, UpdatePassMailActivity.class));
						getTxtUsuario().setText("");
						getTxtPassword().setText("");
						iniciarSesionProcess.processFinished();
					}

					
					public void onUserPassUpdate(String mensaje) {
						// TODO Auto-generated method stub
						Usuario.setLogin(IniciarSesionActivity.this, getTxtUsuario().getText().toString());
						Intent intent = new Intent(IniciarSesionActivity.this, ModificarPassActivity.class);
						intent.putExtra("source", 0);
						startActivity(intent);
						getTxtUsuario().setText("");
						getTxtPassword().setText("");
						iniciarSesionProcess.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				Usuario.cancelarChequeoRegistro(IniciarSesionActivity.this);
			}
		});
		
		(findViewById(R.id.user_sign)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), getString(R.string.user_chars), Toast.LENGTH_SHORT).show();
				
			}
		});
		
		(findViewById(R.id.pass_sign)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), getString(R.string.pass_chars), Toast.LENGTH_SHORT).show();
				
			}
		});
		
		
	}
	
	
	@Override
	protected void onStop() {
		iniciarSesionProcess.stop();
		super.onStop();
	}
	
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		Typeface gandhiSansBold = Typeface.createFromAsset(getAssets(), Font.GANDHISANS_BOLD);
		Typeface gandhiSans = Typeface.createFromAsset(getAssets(), Font.GANDHISANS_REG);
		
		((TextView) findViewById(R.id.label_titulo)).setTypeface(gandhiSansBold);
		((TextView) findViewById(R.id.label_sms)).setTypeface(gandhiSans);
		
		getBtnOk().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				if (formValidator.validate()){
					/*String pass = getTxtPassword().getText().toString();
					System.out.println("pass: "+pass);
					
					String key = Text.parsePass(getTxtPassword().getText().toString());
					System.out.println("key:" +key);
					
					String encrypt = Crypto.aesEncrypt(key, getTxtPassword().getText().toString());
					
					System.out.println("Encrypt: "+encrypt);
					
					String dec = Crypto.aesDecrypt(key, encrypt);
					
					System.out.println("Decrypt: "+dec);*/
					
					iniciarSesionProcess.start();
				}
			}
		});
		
		getBtnRegistrar().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				Intent intent = new Intent(IniciarSesionActivity.this, RegistroActivity.class);
				startActivity(intent);
			}
		});
		
		getRecPass().setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(IniciarSesionActivity.this, RecPassActivity.class);
				startActivity(intent);
			}
			
		});
		
		getBtnReenvia().setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(IniciarSesionActivity.this, ReenviaSmsAct.class);
				startActivity(intent);*/
				showDialog(SMS_DIALOG);
			}
		});
	}
	
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtUsuario(), "Usuario")
			.canBeEmpty(false)
			.maxLength(16)
			.minLength(3);
		
		formValidator.addItem(getTxtPassword(), "Contrase�a")
			.canBeEmpty(false)
			.maxLength(12)
			.minLength(8);
	}
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.INICIO_SESION;
	}
	
	protected Dialog onCreateDialog(int id) {
	    AlertDialog.Builder builder;
	    AlertDialog dialog;
	    final FormValidator validador = new FormValidator(this, true);
	  
	    
	    switch(id) {
	    case SMS_DIALOG:
			//dialog = new AlertDialog(IniciarSesionActivity.this);
	    	Context mContext = IniciarSesionActivity.this;
	    	LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
	    	View layout = inflater.inflate(R.layout.reenviar_sms,
	    			 null);
	    	
			//dialog.setContentView(R.layout.reenviar_sms);
			
			builder = new AlertDialog.Builder(mContext);
			builder.setView(layout);
			dialog = builder.create();
			
			//dialog.setTitle("Reenviar SMS");

			final EditText text = (EditText) layout.findViewById(R.id.txtNumCel);
			
			final EditText text2 = (EditText) layout.findViewById(R.id.txtNumCel2);
			
			Button boton = (Button) layout.findViewById(R.id.btnOk);
			
			boton.setOnClickListener(new OnClickListener(){

				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(validador.validate()){
						processSms.start();
					}
				}
				
			});
			
			validador.addItem(text, "N�mero de celular", new Validable(){
				public void validate() throws ErrorSys {
					if (!text.getText().toString().equals(text2.getText().toString())){
						text.requestFocus();
						throw new ErrorSys("Los n�meros de celular no coinciden");
					}

					if (!Validador.esNumeroCelularMX(text.getText().toString())){
						text.requestFocus();
						throw new ErrorSys("El n�mero de celular debe ser de 10 d�gitos");
					}
				}
			}).canBeEmpty(false)
			.isRequired(true);
			
			processSms = new LongProcess(this, true, 30, true, "processSetSms", new LongProcessListener(){

				
				public void doProcess() {
					// TODO Auto-generated method stub
					ws = new SetSmsWSClient(IniciarSesionActivity.this);
					ws.setNumero(text.getText().toString());
					
					ws.execute(new OnGeneralWSResponseListener(){

						
						public void onGeneralWSErrorListener(String error) {
							// TODO Auto-generated method stub
							Toast.makeText(getApplicationContext(), "Error. Intentelo de nuevo. "+error, Toast.LENGTH_LONG).show();
							processSms.processFinished();
						}
						

						
						public void onGeneralWSResponseListener(String response) {
							// TODO Auto-generated method stub
							processSms.processFinished();
							Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
							removeDialog(SMS_DIALOG);
							
						}
						
					});
				}

				
				public void stopProcess() {
					// TODO Auto-generated method stub
					if (ws != null){
						ws.cancel();
						ws = null;
					}
				}
				
			});
	    	break;
	    	
	    default:
	        dialog = null;
	    }
	    return dialog;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
	}
}