package com.ironbit.mc.activities;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.adapter.NuevoAdapter;
import com.ironbit.mc.web.webservices.GetNuevosWSClient;
import com.ironbit.mc.web.webservices.data.NuevoTO;
import com.ironbit.mc.web.webservices.events.OnNuevoResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnResponseJSONAReceivedListener;

public class NuevoActivity extends MenuActivity {
	
	LongProcess process;
	GetNuevosWSClient client;
	List<NuevoTO> datos;
	ListView datosListView;
	
	private static final String TAG = "NuevoActivity";

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.NUEVO;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_nuevo);
		
		process = new LongProcess(NuevoActivity.this, true, 30, true, "get_nuevo", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (client != null) {
					client.cancel();
					client = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				client = new GetNuevosWSClient(NuevoActivity.this);
				client.execute(new OnNuevoResponseReceivedListener() {
					
					public void onNuevosResponseReceived(List<NuevoTO> nuevos) {
						// TODO Auto-generated method stub
						NuevoAdapter adapter = new NuevoAdapter(NuevoActivity.this, nuevos);
						datosListView.setAdapter(adapter);
						datosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
							
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								// TODO Auto-generated method stub
								NuevoTO nuevo = (NuevoTO) ((NuevoAdapter) arg0.getAdapter()).getItem(arg2);
								
								Log.i(TAG, "Abrimos: " + nuevo.toString());
								
								startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(nuevo.getUrlTienda())));
							}
						});
						
						process.processFinished();
					}
				});
			}
		});
		
		process.start();
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		datosListView = (ListView) findViewById(R.id.list_data);
	}
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			
			switch (id) {
			case R.id.button_menu:
				onBackPressed();
				break;
			case R.id.button_back:
				onBackPressed();
				break;
			default:
				break;
			}
		}
	}

}
