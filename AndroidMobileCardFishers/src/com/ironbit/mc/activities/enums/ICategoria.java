package com.ironbit.mc.activities.enums;

public interface ICategoria {
	public String getNombre();
	
	public int getTipo();
	
	public void setDescargada(boolean descargada);
	
	public boolean isDescargada();
}
