package com.ironbit.mc.activities;

import com.ironbit.mc.R;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CompraFallidaActivity extends MenuActivity {
	protected Button btnOk = null;
	protected TextView txtResumen = null;
	public static final String CONF_KEY_COMPRA_FOLIO = "adc_compra_folio";
	public static final String CONF_KEY_FALLA_MSG = "adc_falla_msg";
	
	protected TextView getTxtResumen(){
		if (txtResumen == null){
			txtResumen = (TextView)findViewById(R.id.txt_resumen);
		}
		
		return txtResumen;
	}
	
	
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.compra_fallida);
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		getTxtResumen().setText( getAppConf(this, CONF_KEY_FALLA_MSG, " "));
		
		getBtnOk().setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				/*Intent intent = new Intent(CompraFallidaActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);*/
				finish();
			}
		});
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.COMPRA_FALLIDA;
	}
	
	
}
