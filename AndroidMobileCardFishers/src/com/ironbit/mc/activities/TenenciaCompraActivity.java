package com.ironbit.mc.activities;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.location.LocateUser;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.TenenciaWSClient;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class TenenciaCompraActivity extends MenuActivity {
	public static final String CONF_KEY_CATEGORIA = "adc_resumen_categoria";
	public static final String CONF_KEY_PROVEEDOR = "adc_resumen_proveedor";
	public static final String CONF_KEY_PRODUCTO = "adc_resumen_producto";
	public static final String CONF_KEY_PRODUCTO_CLAVE = "adc_resumen_producto_clave";
	protected Button btnOk = null;
	
	protected LongProcess process = null;
	protected TenenciaWSClient purchaseWS = null;
	//protected EditText txtPlacas = null;
	protected EditText txtCVV2 = null;
	protected TextView txtQueCvv2 = null;
	protected EditText txtPassword = null;
	
	protected EditText txtVerif= null;
	protected TextView txtMontoPago = null;
	protected TextView txtResumen = null;
	protected TextView txtResumen2 = null;
	protected TextView txtResumenVita = null;
	
	protected Spinner cmbFechaVencimientoMes = null;
	protected Spinner cmbFechaVencimientoAnio = null;
	protected Spinner cmbTipoTarjeta = null;
	long idUser =0;
	String json, placa;
	
	protected LocateUser locate = null;
	
	protected final FormValidator formValidator = new FormValidator(this, true);
	
	protected TextView getTxtMontoPago(){
		if (txtMontoPago == null){
			txtMontoPago = (TextView)findViewById(R.id.txt_monto_pago);			
		}
		
		return txtMontoPago;
	}
	
	protected TextView getTxtResumen(){
		if (txtResumen == null){
			txtResumen = (TextView)findViewById(R.id.txt_resumen);			
		}
		
		return txtResumen;
	}
	protected TextView getTxtResumen2(){
		if (txtResumen2 == null){
			txtResumen2 = (TextView)findViewById(R.id.txt_resumen2);			
		}
		
		return txtResumen2;
	}
	protected TextView getTxtResumenVita(){
		if (txtResumenVita == null){
			txtResumenVita = (TextView)findViewById(R.id.txt_res_vitamedica);			
		}
		
		return txtResumenVita;
	}
	
	protected EditText getTxtCVV2(){
		if (txtCVV2 == null){
			txtCVV2 = (EditText)findViewById(R.id.txtCvv2);
		}
		
		return txtCVV2;
	}
	
	
	protected EditText getTxtVerif(){
		if (txtVerif == null){
			txtVerif = (EditText)findViewById(R.id.txtVerif);
		}
		
		return txtVerif;
	}
	
	protected TextView getTxtQueCvv2(){
		if (txtQueCvv2 == null){
			txtQueCvv2 = (TextView)findViewById(R.id.txtQueCvv2);			
		}
		
		return txtQueCvv2;
	}
	
	protected EditText getTxtPassword(){
		if (txtPassword == null){
			txtPassword = (EditText)findViewById(R.id.txtPassword);
		}
		
		return txtPassword;
	}
	
	protected Spinner getCmbFechaVencimientoMes(){
		if (cmbFechaVencimientoMes == null){
			cmbFechaVencimientoMes = (Spinner)findViewById(R.id.cmbFechaVencimientoMes);
		}
		
		return cmbFechaVencimientoMes;
	}
	
	protected Spinner getCmbFechaVencimientoAnio(){
		if (cmbFechaVencimientoAnio == null){
			cmbFechaVencimientoAnio = (Spinner)findViewById(R.id.cmbFechaVencimientoAnio);
		}
		
		return cmbFechaVencimientoAnio;
	}
	
	protected Spinner getCmbTipoTarjeta(){
		if (cmbTipoTarjeta == null){
			cmbTipoTarjeta = (Spinner)findViewById(R.id.cmbTipoTarjeta);
		}
		
		return cmbTipoTarjeta;
	}
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	/*protected EditText getTxtPlacas(){
		if (txtPlacas == null){
			txtPlacas = (EditText)findViewById(R.id.txtPlacas);
		}
		
		return txtPlacas;
	}*/
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		placa = getIntent().getStringExtra("placa");
		
		inicializarGUI(R.layout.vitamedica_compra);
		locate = new LocateUser(TenenciaCompraActivity.this);
		getBtnOk().setOnClickListener(new OnClickListener(){

			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		process = new LongProcess(this, true, 50, true, "nombreHilo", new LongProcessListener(){
				public void doProcess() {
					 purchaseWS = new TenenciaWSClient(TenenciaCompraActivity.this);
					 purchaseWS.setPlaca(placa);
					 
					 purchaseWS.execute(new OnGeneralWSResponseListener(){

						
						public void onGeneralWSErrorListener(String error) {
							// TODO Auto-generated method stub
							
						}

						
						public void onGeneralWSResponseListener(String response) {
							// TODO Auto-generated method stub
							
						}
						 
					 });
				}
				
				public void stopProcess() {
					
					}
				
			});

		//process.start();
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);	
		
		String producto = getAppConf(TenenciaCompraActivity.this, CONF_KEY_PRODUCTO);
		getTxtResumen().setText( getAppConf(TenenciaCompraActivity.this, CONF_KEY_CATEGORIA)
			
		);
		getTxtResumen2().setText( getAppConf(TenenciaCompraActivity.this, CONF_KEY_PROVEEDOR)
			);
		
		String[] aniosVencimiento = new String[13];
		try{
			getTxtResumenVita().setText(getIntent().getStringExtra("tipo"));
			Fecha fechaNow = new Fecha();
			int anioActual = fechaNow.getAnio();
			
			for (int a = anioActual, i = 0; a <= anioActual+12; a++, i++){
				aniosVencimiento[i] = a+"";
			}
			
			//metemos array al Spinner
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this, 
				android.R.layout.simple_spinner_item, 
				aniosVencimiento
			);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    getCmbFechaVencimientoAnio().setAdapter(adapter);
		}catch(Exception e){
			Sys.log(e);
		}
		
		getTxtMontoPago().setText("$ " + producto);
		
		getTxtQueCvv2().setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es Cvv2?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				View vista = getLayoutInflater().inflate(R.layout.galeria_imagen, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tarjetacvv);
				alert.setView(vista);
				AlertDialog alertDialog = alert.create();
				alertDialog.show();
			}
			
		});
		
		getBtnOk().setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				if (formValidator.validate()){
					process.start();
				}
			}
		});
		
	}
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtPassword(), "Contrase�a")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(12)
			.minLength(8);
		
		formValidator.addItem(getTxtCVV2(), "CVV2")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(4)
			.minLength(3)
			.isNumeric(true);
		
		formValidator.addItem(getCmbFechaVencimientoMes(), "", new Validable(){
			public void validate() throws ErrorSys {
				Fecha now = new Fecha();
				now.setHora(0);
				now.setMinuto(0);
				now.setSegundo(0);
				now.actualizar();
				
				Fecha fVencimiento = new Fecha();
				fVencimiento.setDia(1);
				fVencimiento.setMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
				fVencimiento.setAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
				fVencimiento.setHora(0);
				fVencimiento.setMinuto(0);
				fVencimiento.setSegundo(0);
				fVencimiento.actualizar();
				
				if (fVencimiento.getTimeStampSeg() <= now.getTimeStampSeg()){
					getCmbFechaVencimientoMes().requestFocus();
					throw new ErrorSys("La fecha de vencimiento debe ser mayor a la fecha actual");
				}
			}
		});
		
		formValidator.addItem(getCmbTipoTarjeta(), "", new Validable(){
			public void validate() throws ErrorSys {
				
				if (getCmbTipoTarjeta().getSelectedItemPosition() == 0){
					throw new ErrorSys("Seleccione el tipo de tarjeta");
				}
			}
		});
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		if(locate.startLocation()){
			enableProviders();
		}
	}
	@Override
	protected void onDestroy(){
		super.onDestroy();
		
		if(locate != null){
			locate.stopLocation();
		}
	}
	
	public void enableProviders(){
		locate.stopLocation();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Alerta!")
		.setMessage("Debe activar al menos un proveeder para obtener su localizacion")
		.setPositiveButton("OK", new DialogInterface.OnClickListener(){

			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
			}

		
			
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.TENENCIA_COMPRA;
	}

}
