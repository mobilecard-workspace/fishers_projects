package com.ironbit.mc.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.widget.TextView;

import com.ironbit.mc.R;

public class AboutActivity extends MainActivity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inicializarGUI(R.layout.about);
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		 TextView txt = (TextView) findViewById(R.id.txtVersion);
	     txt.setText("Version "+getVersionName(getApplicationContext(), AboutActivity.class));
	}
	
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.ABOUT;
	}
	
	 public String getVersionName(Context context, Class<AboutActivity> cls) 
	    {
	      try {
	        ComponentName comp = new ComponentName(context, cls);
	        PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
	        return pinfo.versionName;
	      } catch (android.content.pm.PackageManager.NameNotFoundException e) {
	        return null;
	      }
	    }
}
