package com.ironbit.mc.activities;

import java.util.ArrayList;

import org.addcel.util.GraphicUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.adapter.ProductoAdapter;
import com.ironbit.mc.constant.Peaje;
import com.ironbit.mc.constant.Servicios;
import com.ironbit.mc.constant.Tae;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.GetProductsWSClient;
import com.ironbit.mc.web.webservices.data.DataProduct;
import com.ironbit.mc.web.webservices.events.OnProductsResponseReceivedListener;

public class ProductosNewActivity extends MenuActivity {
	
	int categoriaClave;
	int proveedorClave;
	String proveedorWSClave;
	String categoria;
	String proveedor;
	LinearLayout header, subheader;
	ListView productosList;
	GetProductsWSClient productsClient;
	LongProcess productsProcess;
	
	private static final String TAG = "ProductosNewActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_productos);
		
		productsProcess = new LongProcess(ProductosNewActivity.this, true, 30, true, "GetProductos", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != productsClient) {
					productsClient.cancel();
					productsClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				productsClient = new GetProductsWSClient(ProductosNewActivity.this);
				productsClient.setClaveWSProvider(proveedorWSClave);
				productsClient.execute(new OnProductsResponseReceivedListener() {
					
					public void onProductsResponseReceived(ArrayList<DataProduct> products) {
						// TODO Auto-generated method stub
						for (DataProduct dataProduct : products) {
							Log.d(TAG, "PRODUCTO: " + dataProduct.getDescription());
						}
						
						ProductoAdapter adapter = new ProductoAdapter(ProductosNewActivity.this, products);
						productosList.setAdapter(adapter);
						productosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
							
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								// TODO Auto-generated method stub
								DataProduct product = (DataProduct) ((ProductoAdapter) arg0.getAdapter()).getItem(arg2);
								String clave = product.getClave();
								
								if (clave == null) {
									Log.i(TAG, "NO HAY CLAVE");
								} else {
									Log.i(TAG, "CLAVE PRODUCTO: " + clave);
								}
								
								String claveWS = product.getClaveWS();
								String descripcion = product.getDescription();
								Intent intent = null;
								if (categoriaClave == 1) {
									intent = new Intent(ProductosNewActivity.this, ResumenCompraActivity.class);
								} else if (categoriaClave == 2) {
									if (proveedorClave == 5)
										intent = new Intent(ProductosNewActivity.this, IaveActivity.class);
									else if (proveedorClave == 8)
										intent = new Intent(ProductosNewActivity.this, PaseActivity.class);
									
								}else if (categoriaClave == 3) {
									
									Log.i(TAG, "CATEGORIA PRODUCTO: " + categoriaClave);
									Log.i(TAG, "PROVEEDOR CLAVE: " + proveedorClave);
									
									if (proveedorClave == 9) {
										if (clave.equals("16"))
											intent = new Intent(ProductosNewActivity.this, VitamedicaIndiAct.class);
										else if (clave.equals("18"))
											intent = new Intent(ProductosNewActivity.this, VitamedicaFamAct.class);
											
										
									}
								}
								intent.putExtra("clave", clave);
								intent.putExtra("claveWS", claveWS);
								intent.putExtra("descripcion", descripcion);
								intent.putExtra("categoria", categoria);
								intent.putExtra("proveedor", proveedor);
								intent.putExtra("claveProveedor", String.valueOf(proveedorClave));
								startActivity(intent);
							}
						});
						
						productsProcess.processFinished();
					}
				});
			}
		});
		
		productsProcess.start();
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.PRODUCTOS_NEW;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);

		initData();
		header = (LinearLayout) findViewById(R.id.header_titulo);
		subheader = (LinearLayout) findViewById(R.id.header);
		productosList = (ListView) findViewById(R.id.list_productos);
		setHeader();
	}
	
	private void initData() {
		categoria = getIntent().getStringExtra("cat");
		categoriaClave = getIntent().getIntExtra("clave_cat", 0);
		proveedorClave = getIntent().getIntExtra("clave", 0);
		proveedorWSClave = getIntent().getStringExtra("clave_ws");
	}
	
	public void setHeader() {
//		if (categoriaClave == Categoria.TAE) {
			switch (proveedorClave) {
			case Tae.IUSACELL:
				GraphicUtil.setBackgroundDrawableWithBuildValidation(subheader, getResources().getDrawable(R.drawable.iusacell));
				proveedor = "Iusacell";
				break;
			case Tae.MOVISTAR:
				GraphicUtil.setBackgroundDrawableWithBuildValidation(subheader, getResources().getDrawable(R.drawable.movistar));
				proveedor = "Movistar";
				break;
			case Tae.TELCEL:
				GraphicUtil.setBackgroundDrawableWithBuildValidation(subheader, getResources().getDrawable(R.drawable.telcel));
				proveedor = "Telcel";
				break;
			case Tae.UNEFON:
				GraphicUtil.setBackgroundDrawableWithBuildValidation(subheader, getResources().getDrawable(R.drawable.unefon));
				proveedor = "Unefon";
				break;

			case Peaje.IAVE:
				GraphicUtil.setBackgroundDrawableWithBuildValidation(subheader, getResources().getDrawable(R.drawable.sub_iave));
				proveedor = "Iave";
				break;
			case Peaje.PASE:
				GraphicUtil.setBackgroundDrawableWithBuildValidation(subheader, getResources().getDrawable(R.drawable.pase));
				proveedor = "Pase";
				break;
			case Servicios.VITA:
				GraphicUtil.setBackgroundDrawableWithBuildValidation(subheader, getResources().getDrawable(R.drawable.header_vita));
				GraphicUtil.setBackgroundDrawableWithBuildValidation(subheader, getResources().getDrawable(R.drawable.vitamedica));
				proveedor = "Vitamedica";
				break;
			default:
				break;
			}
			
//		} else if (categoriaClave == Categoria.PEAJE) {
//			switch (proveedorClave) {
//			case Peaje.IAVE:
//				
//				break;
//			case Peaje.PASE:
//				
//				break;
//
//			default:
//				break;
//			}
//		}
	}
	
	public void executeAction(View v) {
		if (null != v) {
			
			int id = v.getId();
			Intent intent = null;
			
			switch (id) {
			case R.id.button_back:
				onBackPressed();
				break;
			case R.id.button_menu:
				intent = new Intent(ProductosNewActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				break;
			case R.id.button_logout:
				Usuario.reset(ProductosNewActivity.this);
				intent = new Intent(ProductosNewActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("EXIT", true);
				startActivity(intent);
				finish();
				break;
			default:
				break;
			}
		}
	}

}
