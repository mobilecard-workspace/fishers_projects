//ACTIVIDAD INTERNET AMEX, CREADA 13 DE DICIEMBRE, AGREGAR WIDGETS A XML ASAP!


package com.ironbit.mc.activities.interjet;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.CategoriesActivity;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.activities.ProductsActivity;
import com.ironbit.mc.activities.ProvidersActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.location.LocateUser;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.GetMontoInterjetWSClient;
import com.ironbit.mc.web.webservices.events.OnMontoInterjetResponseListener;


public class InterjetAmexActivity extends MenuActivity {
	protected Button btnOk = null;
	protected Button btnValidate = null;
	protected EditText txtCVV2 = null;
	protected EditText txtPassword = null;
	protected EditText txtReservacion = null;
	protected EditText txtMonto = null;
	protected TextView txtResumen = null;
	protected TextView txtQueCvv2 = null;
	protected Spinner cmbFechaVencimientoMes = null;
	protected Spinner cmbFechaVencimientoAnio = null;
	protected Fecha fechaVencimiento = null;
	protected FormValidator formValidator = new FormValidator(this, true);
	public static final String CONF_KEY_CATEGORIA = "adc_resumen_categoria";
	public static final String CONF_KEY_PROVEEDOR = "adc_resumen_proveedor";
	public static final String CONF_KEY_PRODUCTO = "adc_resumen_producto";
	public static final String CONF_KEY_PRODUCTO_CLAVE = "adc_resumen_producto_clave";
	protected LongProcess processMonto = null;
	protected LongProcess processCom = null; 	
	protected GetMontoInterjetWSClient montoWS = null;
	protected LocateUser locate = null;
	
	protected EditText getTxtCVV2(){
		if(txtCVV2 == null){
			txtCVV2 = (EditText)findViewById(R.id.txtCvv2);
		}
		
		return txtCVV2;
	}
	
	protected EditText getTxtPassword(){
		if(txtPassword == null){
			txtPassword = (EditText)findViewById(R.id.txtPassword);
		}
		
		return txtPassword;
	}
	
	public EditText getTxtReservacion(){
		if(txtReservacion == null){
			txtReservacion = (EditText)findViewById(R.id.txt_ref_interjet_amex);
		}
		
		return txtReservacion;
	}
	
	public EditText getTxtMonto(){
		if(txtMonto == null){
			txtMonto = (EditText)findViewById(R.id.monto_interjet_amex);
		}
		
		return txtMonto;
	}
	
	public Button getBtnOk(){
		if(btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	public TextView getTxtResumen(){
		if (txtResumen == null){
			txtResumen = (TextView)findViewById(R.id.txt_resumen);			
		}
		
		return txtResumen;
	}
	
	public TextView getTxtQueCvv2(){
		if(txtQueCvv2 == null){
			txtQueCvv2 = (TextView)findViewById(R.id.txtQueCvv2);
		}
		
		return txtQueCvv2;
	}
	
	public Button getBtnValidate(){
		if(btnValidate == null){
			btnValidate = (Button)findViewById(R.id.btn_valida_amex);
		}
		
		return btnValidate;
	}
	
	protected Spinner getCmbFechaVencimientoAnio(){
		if (cmbFechaVencimientoAnio == null){
			cmbFechaVencimientoAnio = (Spinner)findViewById(R.id.cmbFechaVencimientoAnio);
		}
		
		return cmbFechaVencimientoAnio;
	}
	
	
	protected Spinner getCmbFechaVencimientoMes(){
		if (cmbFechaVencimientoMes == null){
			cmbFechaVencimientoMes = (Spinner)findViewById(R.id.cmbFechaVencimientoMes);
		}
		
		return cmbFechaVencimientoMes;
	}
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.interjet_compra_amex);
		
		locate = new LocateUser(InterjetAmexActivity.this);
		
		/*FALTA ESPECIFICAR COMO
		 * SERA EL CONSUMO DE SERVICIO
		 * PARA COMPRAR CON AMEX
		 * 
		 *
		 */
		/*processCom = new LongProcess(this, true, WebUtil.LONG_TIMEOUT, true, "sendPurchase", new LongProcessListener(){
			public void doProcess() {
				purchaseWS = new PurchaseWSClient(ResumenCompraActivity.this);
				purchaseWS.setPassword(getTxtPassword().getText().toString());
				purchaseWS.setCvv2(getTxtCVV2().getText().toString());
				purchaseWS.setProducto(getAppConf(ResumenCompraActivity.this, CONF_KEY_PRODUCTO_CLAVE));
				purchaseWS.setTelefono(getTxtCelular().getText().toString());
				purchaseWS.setFechaVencimientoMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
				purchaseWS.setFechaVencimientoAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
				purchaseWS.setX(locate.getLatitud());
				purchaseWS.setY(locate.getLongitud());
				purchaseWS.execute(new OnPurchaseInsertedResponseReceivedListener(){
					public void onPurchaseInsertedResponseReceived(String folio, String resultado, String mensaje) {
						if (resultado.trim().equals("1")){
							setAppConf(ResumenCompraActivity.this, FinalCompraActivity.CONF_KEY_COMPRA_FOLIO, folio);
							InfoSys.showMsj(ResumenCompraActivity.this, mensaje);
							
							setAppConf(ResumenCompraActivity.this, FinalCompraActivity.CONF_COMPRA_CONCEPTO, 
									getAppConf(ResumenCompraActivity.this, CONF_KEY_CATEGORIA) + " " + 
									getAppConf(ResumenCompraActivity.this, CONF_KEY_PROVEEDOR) );
							
							Intent intent = new Intent(new Intent(ResumenCompraActivity.this, FinalCompraActivity.class));
							intent.putExtra("mensaje", mensaje);
							startActivity(intent);
							finishComprasActivities();
						}else{
							setAppConf(ResumenCompraActivity.this, CompraFallidaActivity.CONF_KEY_FALLA_MSG, mensaje);
							startActivity(new Intent(ResumenCompraActivity.this, CompraFallidaActivity.class));
							//ErrorSys.showMsj(ResumenCompraActivity.this, mensaje);
						}
						
						//avisamos que el proceso ya terminó
						process.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (purchaseWS != null){
					purchaseWS.cancel();
					purchaseWS = null;
				}
			}
		});*/
		
		//AGREGAR LONG PROCESS PARA WS DE MONTO
		
		processMonto = new LongProcess(this, true, 60, true, "getMonto", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if(montoWS != null){
					montoWS.cancel();
					montoWS = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				montoWS = new GetMontoInterjetWSClient(InterjetAmexActivity.this);
				montoWS.setReferencia(getTxtReservacion().getText().toString());
				montoWS.execute(new OnMontoInterjetResponseListener() {
					
					public void onMontoInterjetResponseReceived(String status, String monto, String pnr) {
						// TODO Auto-generated method stub
						if(monto != null && !monto.equals("")){
							if(status.equals("Hold")){
								getBtnOk().setEnabled(true);
								processMonto.processFinished();
							}
							else{
								Toast.makeText(InterjetAmexActivity.this, "El periodo de pago para esta reservaci�n ha expirado.", Toast.LENGTH_LONG).show();
								processMonto.processFinished();
							}
						}
						else{
							Toast.makeText(InterjetAmexActivity.this, "La referencia introducida no es v�lida.", Toast.LENGTH_LONG).show();
							processMonto.processFinished();
						}
					}
				});
			}
		});
		
		getTxtQueCvv2().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es Cvv2?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				View vista = getLayoutInflater().inflate(R.layout.galeria_imagen, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tarjetacvv);
				alert.setView(vista);
				AlertDialog alertDialog = alert.create();
				alert.show();
			}
		});
		
		(findViewById(R.id.pass_sign)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), getString(R.string.pass_chars), Toast.LENGTH_SHORT).show();
			}
		});
		
		getBtnValidate().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				processMonto.start();
			}
		});
		
	}
	
	protected void finishComprasActivities(){
		try{
			if(CategoriesActivity.instance != null){
				CategoriesActivity.instance.finish();
			}
			
		}catch(Exception e){
			Sys.log(e);
		}
		
		try{
			if(ProvidersActivity.instance != null){
				ProvidersActivity.instance.finish();
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		try{
			if(ProductsActivity.instance != null){
				ProductsActivity.instance.finish();
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		finish();
	}
	
	protected void onStop(){
		super.onStop();
	}
	
	protected void inicializarGUI(int layoutResID){
		super.inicializarGUI(layoutResID);
		
		getTxtResumen().setText(getAppConf(InterjetAmexActivity.this, CONF_KEY_CATEGORIA) + " " + 
				getAppConf(InterjetAmexActivity.this, CONF_KEY_PROVEEDOR)
				);
		//A�o vencimiento
		String[] aniosVencimiento = new String[13];
		
		try{
			Fecha fechaNow = new Fecha();
			int anioActual = fechaNow.getAnio();
			
			for (int a = anioActual, i = 0; a <= anioActual + 12; a++, i++){
				aniosVencimiento[i] = a+"";
			}
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item,
					aniosVencimiento
					);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			getCmbFechaVencimientoAnio().setAdapter(adapter);
		}catch(Exception e){
			Sys.log(e);
		}
		
		getBtnOk().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(formValidator.validate()){
					
				}
			}
		});
	}
	
	public void configValidations(){
		formValidator.addItem(getTxtPassword(), "Contrase�a")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(12)
		.minLength(8);
	
	formValidator.addItem(getTxtCVV2(), "CVV2")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(4)
		.minLength(3)
		.isNumeric(true);
	
	formValidator.addItem(getTxtReservacion(), "Reservaci�n")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(8)
		.minLength(5);
	
	formValidator.addItem(getTxtMonto(), "Monto")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(6)
		.isNumeric(true);
	
	formValidator.addItem(getCmbFechaVencimientoMes(), "", new Validable(){
		public void validate() throws ErrorSys {
			Fecha now = new Fecha();
			now.setHora(0);
			now.setMinuto(0);
			now.setSegundo(0);
			now.actualizar();
			
			Fecha fVencimiento = new Fecha();
			fVencimiento.setDia(1);
			fVencimiento.setMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
			fVencimiento.setAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
			fVencimiento.setHora(0);
			fVencimiento.setMinuto(0);
			fVencimiento.setSegundo(0);
			fVencimiento.actualizar();
			
			if (fVencimiento.getTimeStampSeg() <= now.getTimeStampSeg()){
				getCmbFechaVencimientoMes().requestFocus();
				throw new ErrorSys("La fecha de vencimiento debe ser mayor a la fecha actual");
			}
		}
	});
	}
	
	public void enableProviders(){
		locate.stopLocation();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Alerta!")
		.setMessage("Debe activar al menos un proveeder para obtener su localizacion")
		.setPositiveButton("OK", new DialogInterface.OnClickListener(){

			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
			}

		
			
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	protected void onStart(){
		super.onStart();
		if(locate.startLocation()){
			enableProviders();
		}
	}
	
	protected void onDestroy(){
		super.onDestroy();
		
		if(locate != null){
			locate.stopLocation();
		}
	}
	
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.INTERJET_REFERENCIA_AMEX;
	}
}
