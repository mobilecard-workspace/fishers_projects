package com.ironbit.mc.activities.interjet;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.activities.web.CompraWebActivity;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.GetMontoInterjetWSClient;
import com.ironbit.mc.web.webservices.GetTransactionIdInterjetWSClient;
import com.ironbit.mc.web.webservices.events.OnIdInterjetResponseListener;
import com.ironbit.mc.web.webservices.events.OnMontoInterjetResponseListener;

public class InterjetActivity extends MenuActivity {
	public static final String CONF_KEY_CATEGORIA = "adc_resumen_categoria";
	public static final String CONF_KEY_PROVEEDOR = "adc_resumen_proveedor";
	public static final String CONF_KEY_PRODUCTO = "adc_resumen_producto";
	public static final String CONF_KEY_PRODUCTO_CLAVE = "adc_resumen_producto_clave";
	
	protected Button btnOK = null;
	protected Button btnValidate = null;
	protected Button backButton;
	protected EditText txtReferencia = null;
	protected EditText txtMonto = null;
	protected TextView txtResumen = null;
	
//	Nuevos gr�ficos
	private TextView montoTextView;
	
	protected LongProcess montoProcess = null;
	protected LongProcess transactionIdProcess = null;
	protected GetMontoInterjetWSClient montoClient = null;
	protected GetTransactionIdInterjetWSClient transactionIdClient = null;
	
	private String idTransaccion;
	private String pnr;
	private String monto;
	private String referencia;
	
	private String clave;
	private String claveWS;
	private String descripcion;
	private String categoria;
	private String claveProveedor;
	private String proveedor;
	
	private static final String LOG = "InterjetActivity";
	
	protected FormValidator formValidator = new FormValidator(this, true);
	
	protected Button getBackButton() {
		if (null == backButton)
			backButton = (Button) findViewById(R.id.button_back);
		
		return backButton;
	}
	
	protected Button getBtnOk(){
		if(btnOK == null){
			btnOK = (Button)findViewById(R.id.btnInterjetOk);
		}
		
		return btnOK;
	}
	
	public TextView getTxtResumen(){
		if (txtResumen == null){
			txtResumen = (TextView)findViewById(R.id.txt_resumen);			
		}
		
		return txtResumen;
	}
	
	protected Button getBtnValidate(){
		if(btnValidate == null){
			btnValidate = (Button)findViewById(R.id.buttonValidar);
		}
		
		return btnValidate;
	}
	
	protected EditText getTxtReferencia(){
		if(txtReferencia == null){
			txtReferencia = (EditText)findViewById(R.id.txtRefInterjet);
		}
		
		return txtReferencia;
	}
	
	public TextView getMontoTextView() {
		if (montoTextView == null) {
			montoTextView = (TextView) findViewById(R.id.textView_monto);
		}
		
		return montoTextView;
	}
	
	public String getPnr() {
		return pnr;
	}
	
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	
	public String getMonto() {
		return monto;
	}
	
	public void setMonto(String monto) {
		this.monto = monto;
	}
	
	public String getReferencia() {
		return referencia;
	}
	
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	public String getIdTransaccion() {
		return idTransaccion;
	}
	
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        inicializarGUI(R.layout.interjet_referencia);          
        
        transactionIdProcess = new LongProcess(this, true, 60, true, "transactionIdClient", new LongProcessListener() {

			public void doProcess() {
				// TODO Auto-generated method stub
				transactionIdClient = new GetTransactionIdInterjetWSClient(InterjetActivity.this);
				transactionIdClient.setUser(Usuario.getLogin(InterjetActivity.this));
				transactionIdClient.setPnr(getPnr());
				transactionIdClient.setMonto(getMonto());
				
				transactionIdClient.execute(new OnIdInterjetResponseListener() {
					
					public void onIdInterjetResponseReceived(String id) {
						// TODO Auto-generated method stub
						if(id.equals("") || id == null) {
							Toast.makeText(InterjetActivity.this, "Transacci�n no v�lida. Intente de nuevo m�s tarde", Toast.LENGTH_LONG).show();
							transactionIdProcess.processFinished();							
						}
						else{	
							setIdTransaccion(id);
							String url = getPurchaseUrl();
							
							if(url.equals("")){
								Toast.makeText(InterjetActivity.this, "Url no v�lida. Intente de nuevo m�s tarde", Toast.LENGTH_LONG).show();
								transactionIdProcess.processFinished();
							}
							else{
								Intent intent = new Intent(InterjetActivity.this, CompraWebActivity.class);
								intent.putExtra("url", url);
								intent.putExtra("tipo_pago", "interjet");
								intent.putExtra("url_regreso", UrlWebServices.URL_WS_INTERJET_REGRESO);
								intent.putExtra("ip_regreso", UrlWebServices.IP_WS_INTERJET_REGRESO);
								startActivity(intent);
								transactionIdProcess.processFinished();
							}
						}
					}
				});
			}

			public void stopProcess() {
				// TODO Auto-generated method stub

				if(transactionIdClient != null){
					transactionIdClient.cancel();
					transactionIdClient= null;
				}
			}
        	
         });
        
        montoProcess = new LongProcess(this, true, 60, true, "validateRes", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if(montoClient != null){
					montoClient.cancel();
					montoClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				montoClient = new GetMontoInterjetWSClient(InterjetActivity.this);
				montoClient.setReferencia(getTxtReferencia().getText().toString());
				montoClient.execute(new OnMontoInterjetResponseListener() {
					
					public void onMontoInterjetResponseReceived(String status, String monto, String pnr) {
						// TODO Auto-generated method stub
						if(monto != null && !monto.equals("")){
							
							if(status.equals("Hold")){
								setPnr(pnr);
								setMonto(monto);
								
								Log.i("pnr", getPnr());
								Log.i("monto", getMonto());
								
								getMontoTextView().setText("$" + monto);
								getMontoTextView().setVisibility(View.VISIBLE);
								
								getBtnOk().setEnabled(true);
								montoProcess.processFinished();
							}
							else if(status.equals("null")){
								Toast.makeText(InterjetActivity.this, "El n�mero de reservaci�n no existe.", Toast.LENGTH_LONG).show();
								montoProcess.processFinished();
							}
							else{
								Toast.makeText(InterjetActivity.this, "El periodo de pago para esta reservaci�n ha expirado.", Toast.LENGTH_LONG).show();
								montoProcess.processFinished();
							}							
						}
						else{
							Toast.makeText(InterjetActivity.this, "El n�mero de reservaci�n introducido no es v�lido.", Toast.LENGTH_LONG).show();
							montoProcess.processFinished();
						}
					}
				});
				
			}
		});
	}
	
	
	public void configValidations(){
		formValidator.addItem(getTxtReferencia(), "Referencia de pago")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(12)
		.minLength(6);
	}
	
	protected void inicializarGUI(int layoutResID){
		super.inicializarGUI(layoutResID); 
		
		clave = getIntent().getStringExtra("clave");
		Log.d(LOG, "CLAVE: " + clave);
		
		claveWS = getIntent().getStringExtra("claveWS");
		Log.d(LOG, "CLAVEWS: " + claveWS);
		
		categoria = getIntent().getStringExtra("cat");
		Log.d(LOG, "CATEGORIA: " + categoria);
		
		proveedor = getIntent().getStringExtra("proveedor");
		Log.d(LOG, "PROVEEDOR: " + proveedor);
		
		
		getTxtResumen().setText(categoria + " " + proveedor);
		
		getBackButton().setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

        
        getBtnValidate().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				if (formValidator.validate()) {
					montoProcess.start();
				}
				
			}
		});
		
		getBtnOk().setEnabled(false);
		getBtnOk().setOnClickListener(new OnClickListener(){
			
			public void onClick(View v) {
				// TODO Auto-generated method stub		
				
				transactionIdProcess.start();
				
			}
		});
	}
	
	protected void onDestroy(){
		super.onDestroy();
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.INTERJET_REFERENCIA;
	}
	
	private String getPurchaseUrl(){
		
		BasicNameValuePair user = new BasicNameValuePair("user", Usuario.getLogin(InterjetActivity.this));
		BasicNameValuePair referencia = new BasicNameValuePair("referencia", getIdTransaccion());
		BasicNameValuePair monto = new BasicNameValuePair("monto", getMonto());
		
		List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
		params.add(user);
		params.add(referencia);
		params.add(monto);
		
		String url = Text.addParamsToUrl(UrlWebServices.URL_WS_INTERJET_GET_SITEPAGO, params);
		
		Log.i("url_pago", url);
		
		return url;
	}
}
