package com.ironbit.mc.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.constant.Peaje;
import com.ironbit.mc.usuario.Usuario;

public class PeajeNewActivity extends MenuActivity {
	
	int claveCategoria;
	private static final String TAG = "PeajeNewActivity";
	private static final String CATEGORIA = "Peaje";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_peaje);
		
		claveCategoria = getIntent().getIntExtra("clave", 0);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.PEAJE_NEW;
	}
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			Intent intent = null;
			
			switch (id) {
			case R.id.button_back:
				onBackPressed();
				break;
			
			case R.id.button_iave:
				
				if (isLoggedIn()) {
					intent = new Intent(PeajeNewActivity.this, ProductosNewActivity.class);
					intent.putExtra("clave_cat",claveCategoria);
					intent.putExtra("cat", CATEGORIA);
					intent.putExtra("clave", Peaje.IAVE);
					intent.putExtra("clave_ws", Peaje.IAVE_WS);
					startActivity(intent);
				} else {
					Toast.makeText(PeajeNewActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
				}
				
				break;
				
			case R.id.button_pase:
				if (isLoggedIn()) {
					intent = new Intent(PeajeNewActivity.this, ProductosNewActivity.class);
					intent.putExtra("clave_cat",claveCategoria);
					intent.putExtra("cat", CATEGORIA);
					intent.putExtra("clave", Peaje.PASE);
					intent.putExtra("clave_ws", Peaje.PASE_WS);
					startActivity(intent);
				} else {
					Toast.makeText(PeajeNewActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
					
				}
				
				break;
			case R.id.button_menu:
				intent = new Intent(PeajeNewActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				break;
			case R.id.button_logout:
				if (isLoggedIn()) {
				
					Usuario.reset(PeajeNewActivity.this);
					intent = new Intent(PeajeNewActivity.this, MenuAppActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("EXIT", true);
					startActivity(intent);
				} else {
					startActivity(new Intent(PeajeNewActivity.this, IniciarSesionActivity.class));
				}
				break;

			default:
				break;
			}
			
		}
	}
	

	
	private boolean isLoggedIn() {
		return !Usuario.getIdUser(PeajeNewActivity.this).isEmpty();
	}

}
