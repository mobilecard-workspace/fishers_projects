package com.ironbit.mc.activities;

import org.addcel.util.GraphicUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.interjet.InterjetActivity;
import com.ironbit.mc.constant.Categoria;
import com.ironbit.mc.constant.Servicios;
import com.ironbit.mc.usuario.Usuario;

public class ServiciosActivity extends MenuActivity {
	
	int claveCategoria;
	private static final String TAG = "ServiciosActivity";
	private static final String CATEGORIA = "Servicios";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_servicios);
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.SERVICIOS_NEW;
	}
	
	private void initData() {
		claveCategoria = getIntent().getIntExtra("clave", 0);
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		initData();
		
		if (isLoggedIn()) {
			View v = findViewById(R.id.button_logout);
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_cerrarsesion2));
		
		}
		
		
	}
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		
		View v = findViewById(R.id.button_logout);
		
		if (isLoggedIn()) {
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_cerrarsesion2));
		} else {
			GraphicUtil.setBackgroundDrawableWithBuildValidation(v, getResources().getDrawable(R.drawable.selector_iniciar_half));
		}
	}
	
	public void executeAction(View v) {
		if (null != v) {
			
			int id = v.getId();
			Intent intent = null;
			switch (id) {
			case R.id.button_back:
				onBackPressed();
				break;
			case R.id.button_luz:
				Toast.makeText(ServiciosActivity.this, "�Pr�ximamente!", Toast.LENGTH_SHORT).show();
				break;
			case R.id.button_telefono:
				Toast.makeText(ServiciosActivity.this, "�Pr�ximamente!", Toast.LENGTH_SHORT).show();
				break;
			case R.id.button_cable:
				Toast.makeText(ServiciosActivity.this, "�Pr�ximamente!", Toast.LENGTH_SHORT).show();
				break;
			case R.id.button_gas:
				Toast.makeText(ServiciosActivity.this, "�Pr�ximamente!", Toast.LENGTH_SHORT).show();
				break;
			case R.id.button_vitamedica:
				if (isLoggedIn()) {
					intent = new Intent(ServiciosActivity.this, ProductosNewActivity.class);
					intent.putExtra("clave_cat",Categoria.SERVICIOS);
					intent.putExtra("cat", "SERVICIOS");
					intent.putExtra("clave", Servicios.VITA);
					intent.putExtra("clave_ws", Servicios.VITA_WS);
					startActivity(intent);
				} else {
					Toast.makeText(ServiciosActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
					
				}
				break;
			case R.id.button_interjet:
				if (isLoggedIn()) {
					intent = new Intent(ServiciosActivity.this, InterjetActivity.class);
					intent.putExtra("clave_cat",Categoria.SERVICIOS);
					intent.putExtra("cat", "SERVICIOS");
					intent.putExtra("clave", Servicios.INTERJET);
					intent.putExtra("clave_ws", Servicios.INTERJET_WS);
					intent.putExtra("proveedor", "INTERJET");
					startActivity(intent);
				} else {
					Toast.makeText(ServiciosActivity.this, "Inicia Sesi�n para tener acceso a este servicio.", Toast.LENGTH_SHORT).show();
					
				}
				break;
			case R.id.button_menu:
				intent = new Intent(ServiciosActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				break;
			case R.id.button_logout:
				
				if (isLoggedIn()) {

					Usuario.reset(ServiciosActivity.this);
					intent = new Intent(ServiciosActivity.this, MenuAppActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("EXIT", true);
					startActivity(intent);
					finish();
				} else {
					startActivity(new Intent(ServiciosActivity.this, IniciarSesionActivity.class));
				}
				break;
			default:
				break;
			}
		}
	}
	

	
	private boolean isLoggedIn() {
		return !Usuario.getIdUser(ServiciosActivity.this).isEmpty();
	}

}
