package com.ironbit.mc.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.web.webservices.GetTagsWSClient;
import com.ironbit.mc.web.webservices.GetTipoRecargaTagWS;
import com.ironbit.mc.web.webservices.RemoveTagWSClient;
import com.ironbit.mc.web.webservices.data.DataTag;
import com.ironbit.mc.web.webservices.data.DataTipoRecargaTag;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mc.web.webservices.events.OnGetTagResponseListener;
import com.ironbit.mc.web.webservices.events.OnTipRecargaTagReceivedListener;

public class ConsTagActivity extends MenuActivity {
	protected LongProcess loadDataProcess = null;
	protected LongProcess getTagProcess = null;
	protected LongProcess removeTagProcess = null;
	
	protected GetTipoRecargaTagWS ws= null;
	protected GetTagsWSClient wsGet= null;
	protected RemoveTagWSClient wsRemove= null;
	
	protected DataTipoRecargaTag[] lista = null;
	protected DataTag[] listaTag = null;

	protected Spinner spinTipos = null, spinEtiqueta;
	protected EditText txtNum = null, txtVerif=null ;
	protected TextView lblTag, lblVerif;
	protected long idUser = 0;

	protected Button btnElim = null;
	
	protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			inicializarGUI(R.layout.cons_tag);
			
			idUser = getIntent().getLongExtra("idUser", 0);
			
			spinTipos = (Spinner) findViewById(R.id.tagTipo);
			spinEtiqueta = (Spinner) findViewById(R.id.tagNick);
			txtNum = (EditText) findViewById(R.id.tagNum);
			txtVerif = (EditText) findViewById(R.id.tagVerif);
			btnElim = (Button) findViewById(R.id.btnTagCancelar);
			
			lblTag = (TextView) findViewById(R.id.tagLblNum);
			lblVerif = (TextView) findViewById(R.id.tagLblVerif);
			
			spinTipos.setOnItemSelectedListener(new OnItemSelectedListener (){

			
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					txtNum.setText("");
					txtVerif.setText("");
					DataTipoRecargaTag item = (DataTipoRecargaTag)parent.getSelectedItem();
					System.out.println(item.getNombre());
					
					if(item.getClave().trim().equals("2")){
						txtVerif.setEnabled(false);
						lblVerif.setText(getString(R.string.confTag));
						addOhlListener();
					}else if(item.getClave().trim().equals("3")){
						lblVerif.setText(getString(R.string.num_cliente));
						addViapassListener();
					}else{
						txtVerif.setEnabled(true);

						lblVerif.setText(getString(R.string.nverif));
						addIaveListener();
					}
					getTagProcess.start();
				}

			
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub
					Toast.makeText(ConsTagActivity.this, "Sin seleccion", Toast.LENGTH_SHORT).show();
				}
				
			});
			
			
			
			spinEtiqueta.setOnItemSelectedListener(new OnItemSelectedListener (){

			
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					DataTag tag = (DataTag)(parent.getItemAtPosition(position));
					txtNum.setText(tag.getNumero());
					txtVerif.setText(""+tag.getDv());
				}

				
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub
					
				}
				
			});
			
			btnElim.setOnClickListener(new OnClickListener(){

				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(!txtNum.getText().toString().trim().equals("")){
						String eti = ((DataTag) spinEtiqueta.getSelectedItem()).getEtiqueta();
						AlertDialog.Builder builder = new AlertDialog.Builder(ConsTagActivity.this);
						builder.setMessage("�Seguro quiere elimnar el tag "+eti+"?")
						       .setCancelable(true)
						       .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
						           public void onClick(DialogInterface dialog, int id) {
						        	  dialog.cancel();
						        	  removeTagProcess.start();
						        	
						           }
						       
						       })
						       .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

						
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.dismiss();
								}
						    	   
						       });
						
						AlertDialog alert = builder.create();
				        alert.show();
					}
					
				}
				
			});
			
			(findViewById(R.id.tagLblEti)).setOnClickListener(new OnClickListener(){

			
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(arg0.getContext(), getString(R.string.eti_help), Toast.LENGTH_LONG).show();
				}
				
			});
			
			(findViewById(R.id.tagLblNum)).setOnClickListener(new OnClickListener(){

			
				public void onClick(View v) {
					// TODO Auto-generated method stub
					AlertDialog alertDialog;
					 
					Builder alert = new AlertDialog.Builder(v.getContext());
					alert.setTitle("�Qu� es N�mero de Tag?");
					alert.setIcon(R.drawable.dlg_alert_icon);
					
					View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
					ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
					imagen.setImageResource(R.drawable.tagiave);
					
					ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
					imagen2.setImageResource(R.drawable.iave);
					
					alert.setView(vista);
					
					alertDialog = alert.create();
					alert.show();
				}
				
			});
			
			(findViewById(R.id.tagLblVerif)).setOnClickListener(new OnClickListener(){

			
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 AlertDialog alertDialog;
					 
						Builder alert = new AlertDialog.Builder(v.getContext());
						alert.setTitle("�Qu� es N�mero Verificador?");
						alert.setIcon(R.drawable.dlg_alert_icon);
						
						View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
						ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
						imagen.setImageResource(R.drawable.dviave);
						
						ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
						imagen2.setImageResource(R.drawable.iave);
						
						alert.setView(vista);
						alertDialog = alert.create();
						alert.show();
				}
				
			});
			loadDataProcess = new LongProcess(this, true, 60, true, "LoadTipoRecargaTag", new LongProcessListener(){
				public void doProcess() {
					//inicializamos proveedores
					ws = new GetTipoRecargaTagWS(ConsTagActivity.this);
					ws.execute(new OnTipRecargaTagReceivedListener(){
						
						
					
						public void onTipoRecargaTagReceivedListener(
								ArrayList<DataTipoRecargaTag> listaTipos) {
							// TODO Auto-generated method stub
							
							lista = new DataTipoRecargaTag[listaTipos.size()];
							
							for (int i = 0; i < listaTipos.size(); i++) {
								lista[i] = listaTipos.get(i);
							}
							
							ArrayAdapter<DataTipoRecargaTag> adapter = new ArrayAdapter<DataTipoRecargaTag>(getApplicationContext(), 
									android.R.layout.simple_spinner_dropdown_item, 
									lista);
							
							spinTipos.setAdapter(adapter);
							loadDataProcess.processFinished();
						}
					});
				}
				
				public void stopProcess() {
					if (ws != null){
						ws.cancel();
						ws = null;
					}
					
					if (ws != null){
						ws.cancel();
						ws = null;
					}
					
					if (ws != null){
						ws.cancel();
						ws = null;
					}
				}
			});
			
			getTagProcess = new LongProcess(this, true, 60, true, "GetTag", new LongProcessListener(){

				
				public void doProcess() {
					// TODO Auto-generated method stub
					wsGet = new GetTagsWSClient(ConsTagActivity.this);
					wsGet.setIdUser(Long.toString(idUser));
					String tipo = ((DataTipoRecargaTag)spinTipos.getSelectedItem()).getClave();
					wsGet.setIdTipo(tipo);
					
					wsGet.execute(new OnGetTagResponseListener(){

					
						public void onGetTagResponseListener(ArrayList<DataTag> tags) {
							// TODO Auto-generated method stub
							listaTag = new DataTag[tags.size()-2];
							
							int count = 0;
							for(int i=0; i<tags.size(); i++){
								if(!Long.toString(tags.get(i).getUsuario()).equals( "9999999999999")
										&& tags.get(i).getUsuario() != -1){
									listaTag[count] = tags.get(i);
									count++;
								}
								
								
							}
							ArrayAdapter<DataTag> adapter = new ArrayAdapter<DataTag>(getApplicationContext(), 
									android.R.layout.simple_spinner_dropdown_item, 
									listaTag);
							
							spinEtiqueta.setAdapter(adapter);
							getTagProcess.processFinished();
						}
						
					});
				}

			
				public void stopProcess() {
					// TODO Auto-generated method stub
					if (wsGet != null){
						wsGet.cancel();
						wsGet = null;
					}
					
				}
				
			});
			
			removeTagProcess = new LongProcess(this, true, 60, true, "RemoveTag", new LongProcessListener(){

			
				public void doProcess() {
					// TODO Auto-generated method stub
					wsRemove = new RemoveTagWSClient(ConsTagActivity.this);
					DataTag tag = (DataTag)spinEtiqueta.getSelectedItem();
					
					wsRemove.setDV(tag.getDv());
					wsRemove.setEtiqueta(tag.getEtiqueta());
					wsRemove.setNumero(tag.getNumero());
					wsRemove.setTipotag(tag.getTipotag());
					wsRemove.setUsuario(tag.getUsuario());
					
					wsRemove.execute(new OnGeneralWSResponseListener(){

						
						public void onGeneralWSErrorListener(String error) {
							// TODO Auto-generated method stub
							removeTagProcess.processFinished();
							Toast.makeText(ConsTagActivity.this, error, Toast.LENGTH_LONG).show();
						}

						public void onGeneralWSResponseListener(String response) {
							// TODO Auto-generated method stub
							removeTagProcess.processFinished();
							finish();
							Toast.makeText(ConsTagActivity.this, response, Toast.LENGTH_LONG).show();
						}
						
					});
				}

				public void stopProcess() {
					// TODO Auto-generated method stub
					
				}
				
			});
			
			loadDataProcess.start();
		}
	
	private void addIaveListener() {
		
		lblVerif.setOnClickListener(new OnClickListener(){

			public void onClick(View v) {
				// TODO Auto-generated method stub
				 AlertDialog alertDialog;
				 
					Builder alert = new AlertDialog.Builder(v.getContext());
					alert.setTitle("�Qu� es N�mero Verificador?");
					alert.setIcon(R.drawable.dlg_alert_icon);
					
					View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
					ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
					imagen.setImageResource(R.drawable.dviave);
					
					ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
					imagen2.setImageResource(R.drawable.iave);
					
					alert.setView(vista);
					alertDialog = alert.create();
					alert.show();
			}
			
		});
	
		(findViewById(R.id.tagLblNum)).setOnClickListener(new OnClickListener(){

			public void onClick(View v) {
				// TODO Auto-generated method stub
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tagiave);
				
				ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
				imagen2.setImageResource(R.drawable.iave);
				
				alert.setView(vista);
				
				AlertDialog alertDialog = alert.create();
				alert.show();
			}
			
		});
		
	}
	
	private void addOhlListener() {
		lblVerif.setOnClickListener(null);
		
		(findViewById(R.id.tagLblNum)).setOnClickListener(new OnClickListener(){

			public void onClick(View v) {
				// TODO Auto-generated method stub
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_1, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tag_ohl);
								
				alert.setView(vista);
				
				AlertDialog alertDialog = alert.create();
				alert.show();
			}
			
		});
		
	}
	
	private void addViapassListener() {
		lblVerif.setOnClickListener(null);
		
		
		(findViewById(R.id.tagLblNum)).setOnClickListener(new OnClickListener(){

			public void onClick(View v) {
				// TODO Auto-generated method stub
				 AlertDialog alertDialog;
				 
				Builder alert = new AlertDialog.Builder(v.getContext());
				alert.setTitle("�Qu� es N�mero de Tag?");
				alert.setIcon(R.drawable.dlg_alert_icon);
				
				View vista = getLayoutInflater().inflate(R.layout.imagenes_2, null);
				ImageView imagen = (ImageView)vista.findViewById(R.id.galeria_imagen);
				imagen.setImageResource(R.drawable.tagviapass1);
			
				ImageView imagen2 = (ImageView)vista.findViewById(R.id.galeria_imagen2);
				imagen2.setImageResource(R.drawable.tagviapass2);
				
				alert.setView(vista);
				
				alertDialog = alert.create();
				alert.show();
			}
			
		});
		
	}
	
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.CONS_TAG;
	}
}
