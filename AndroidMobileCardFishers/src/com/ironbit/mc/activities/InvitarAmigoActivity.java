package com.ironbit.mc.activities;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.InvitaWSClient;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class InvitarAmigoActivity extends MainActivity {
	
	protected FormValidator formValidator = new FormValidator(this, true);
	protected EditText txtTuNombre, txtNombreAmigo, txtCelular;
	
	private LongProcess process;
	private InvitaWSClient client;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.invitar_amigo);
		
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		findViewById(R.id.btnInvitar).setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (formValidator.validate()){
					
					process = new LongProcess(InvitarAmigoActivity.this, true, 30, true, "invita", new LongProcessListener() {
						
						public void stopProcess() {
							// TODO Auto-generated method stub
							if (null != client) {
								client.cancel();
								client = null;
							}
						}
						
						public void doProcess() {
							// TODO Auto-generated method stub
							client = new InvitaWSClient(InvitarAmigoActivity.this);
							client.setLogin(getTxtTuNombre().getText().toString().trim());
							client.setNombre(getTxtNombreAmigo().getText().toString().trim());
							client.setTelefono(getTxtCelular().getText().toString().trim());
							client.executeWS(new OnResponseJSONReceivedListener() {
								
								public void onResponseJSONReceived(JSONObject jsonResponse) {
									// TODO Auto-generated method stub
									String id = jsonResponse.optString("id", "-1");
									String mensaje = jsonResponse.optString("msgError");
									
									Toast.makeText(InvitarAmigoActivity.this, mensaje, Toast.LENGTH_LONG).show();
									
									if ("0".equals(id))
										finish();
									
									process.processFinished();
								}
								
								public void onResponseJSONAReceived(
										JSONArray jsonResponse) {
									// TODO Auto-generated method stub
									
								}
								
								public void onResponseStringReceived(
										String jsonresponse) {
									// TODO Auto-generated method stub
									
								}
							});
						}
					});
					
					process.start();
					
				}
			}
		});
		
		getTxtCelular().setOnFocusChangeListener(new OnFocusChangeListener(){

			
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(hasFocus){
					Toast.makeText(v.getContext(), getString(R.string.tel_chars), Toast.LENGTH_LONG).show();
				}
			}
			
		});
		
		getTxtTuNombre().setText(Usuario.getLogin(InvitarAmigoActivity.this));
	}
	
	protected EditText getTxtTuNombre(){
		if (txtTuNombre == null){
			txtTuNombre = (EditText)findViewById(R.id.txtTuNombre);
		}
		
		return txtTuNombre;
	}
	
	protected EditText getTxtNombreAmigo(){
		if (txtNombreAmigo == null){
			txtNombreAmigo = (EditText)findViewById(R.id.txtNombreAmigo);
		}
		
		return txtNombreAmigo;
	}
	
	protected EditText getTxtCelular(){
		if (txtCelular == null){
			txtCelular = (EditText)findViewById(R.id.txtEmailAmigo);
		}
		
		return txtCelular;
	}
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtNombreAmigo(), "Nombre de tu Amigo")
			.canBeEmpty(false);
		
		formValidator.addItem(getTxtTuNombre(), "Tu Nombre")
			.canBeEmpty(false);
		
		formValidator.addItem(getTxtCelular(), "Tel�fono")
		.canBeEmpty(false)
		.isNumeric(true)
		.minLength(10);
	}
	
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.INVITA_AMIGO;
	}
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			
			switch (id) {
			case R.id.button_back:
				onBackPressed();
				break;
			case R.id.button_cancelar:
				onBackPressed();
				break;
			default:
				break;
			}
		}
	}

}
