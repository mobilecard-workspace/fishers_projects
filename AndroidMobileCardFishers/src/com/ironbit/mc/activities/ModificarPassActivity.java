package com.ironbit.mc.activities;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.system.errores.InfoSys;
import com.ironbit.mc.usuario.Usuario;
import com.ironbit.mc.web.webservices.UserPassUpdateWSClient;
import com.ironbit.mc.web.webservices.events.OnUserPassUpdatedResponseReceivedListener;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ModificarPassActivity extends MenuActivity{
	protected EditText txtPasswordActual = null;
	protected EditText txtPassword = null;
	protected EditText txtPassword2 = null;
	protected Button btnModificar = null;
	protected final FormValidator formValidator = new FormValidator(this, true);
	protected LongProcess sendDataProcess = null;
	protected UserPassUpdateWSClient userPassUpdateWS = null;
	
	
	protected EditText getTxtPasswordActual(){
		if (txtPasswordActual == null){
			txtPasswordActual = (EditText)findViewById(R.id.txtPasswordActual);
		}
		
		return txtPasswordActual;
	}
	
	
	protected EditText getTxtPassword(){
		if (txtPassword == null){
			txtPassword = (EditText)findViewById(R.id.txtPassword);
		}
		
		return txtPassword;
	}
	
	
	protected EditText getTxtPassword2(){
		if (txtPassword2 == null){
			txtPassword2 = (EditText)findViewById(R.id.txtPassword2);
		}
		
		return txtPassword2;
	}
	
	
	protected Button getBtnModificar(){
		if (btnModificar == null){
			btnModificar = (Button)findViewById(R.id.btnModificarDatos);
		}
		
		return btnModificar;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.modificar_pass);
		
		sendDataProcess = new LongProcess(this, true, 30, true, "processUpdatePass", new LongProcessListener(){
			public void doProcess() {
				userPassUpdateWS = new UserPassUpdateWSClient(ModificarPassActivity.this);
				userPassUpdateWS.setActualPassword(getTxtPasswordActual().getText().toString());
				userPassUpdateWS.setNewPassword(getTxtPassword().getText().toString());
				
				userPassUpdateWS.execute(new OnUserPassUpdatedResponseReceivedListener(){
					public void onUserPassUpdatedResponseReceived(String resultado, String mensaje) {
						if (resultado.trim().equals("1")){
							//Actualizamos password en la app
							Usuario.setPass(ModificarPassActivity.this, getTxtPassword().getText().toString());
							
							InfoSys.showMsj(ModificarPassActivity.this, mensaje);
							finish();
							
							if(getIntent().getIntExtra("source", 1) == 0){
								IniciarSesionActivity.termina = true;
								startActivity(new Intent(ModificarPassActivity.this, MenuAppActivity.class));
							}
						}else{
							ErrorSys.showMsj(ModificarPassActivity.this, mensaje);
						}
						
						//avisamos que el proceso ya terminó
						sendDataProcess.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (userPassUpdateWS != null){
					userPassUpdateWS.cancel();
					userPassUpdateWS = null;
				}
			}
		});
	}
	
	
	@Override
	protected void onStop() {
		sendDataProcess.stop();
		super.onStop();
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		//Boton Registrarse
		getBtnModificar().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				if (formValidator.validate()){
					sendDataProcess.start();
				}
			}
		});
	}
	
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtPasswordActual(), "Contrase�a Actual")
			.canBeEmpty(false)
			.maxLength(12)
			.minLength(8);
		
		formValidator.addItem(getTxtPassword(), "Contrase�a", new Validable(){
			public void validate() throws ErrorSys {
				if (!getTxtPassword().getText().toString().equals(getTxtPassword2().getText().toString())){
					getTxtPassword2().requestFocus();
					throw new ErrorSys("Las contrase�as deben coincidir entre s�");
				}
			}
		})
			.canBeEmpty(false)
			.maxLength(12)
			.minLength(8);
		
		formValidator.addItem(getTxtPassword2(), "Contrase�a 2")
			.canBeEmpty(false)
			.maxLength(12)
			.minLength(8);
	}
	
	public void executeAction(View v) {
		if (null != v) {
			
			int id = v.getId();
			switch (id) {
			case R.id.button_cancelar:
				finish();
				break;

			default:
				break;
			}
		}
	}
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.MODIFICAR_PASSWORD;
	}
}