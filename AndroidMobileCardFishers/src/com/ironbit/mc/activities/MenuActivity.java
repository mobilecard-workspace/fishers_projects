package com.ironbit.mc.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public abstract class MenuActivity extends MainActivity{
	/**
	 * Propiedades
	 */
	public static final int MENU_TRIVIA = 0;
	public static final int MENU_INICIO = 1;
	public static final int MENU_CONFIGURACIONES = 2;
	public static final int MENU_NOTICIAS = 3;
	public static final int MENU_GALERIA = 4;
	
	
	/**
	 * Metodos
	 */
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID){
		super.inicializarGUI(layoutResID);
	}
	
	 
    /* 
	 * Llenamos el menu con las opciones necesarias
	 */
	public boolean onCreateOptionsMenu(Menu menu){
		onCreateOptionsMenuStatic(menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	
	/* 
	 * Llenamos el menu con las opciones necesarias<br/>
	 * Esta funci�n est� dise�ada para ser llamada desde actividades que no extiendan esta actividad
	 */
	public static void onCreateOptionsMenuStatic(Menu menu){
//		menu.add(0, MENU_CONFIGURACIONES, 0, R.string.menu_configuraciones).setIcon(R.drawable.ic_menu_configuraciones);
//		menu.add(0, MENU_TRIVIA, 1, "Trivia");
//		menu.add(0, MENU_NOTICIAS, 2, "Noticias");
//		menu.add(0, MENU_GALERIA, 3, "Galeria");
//		menu.add(0, MENU_INICIO, 3, "Inicio");
	}
	
	
	@Override
	/**
	 * Realizamos cualquier operaci�n necesario al seleccionar una opci�n del men� principal
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		onOptionsItemSelectedStatic(item, getIdActividad(), this);
		return super.onOptionsItemSelected(item);
	}
	
	
	/**
	 * Realizamos cualquier operaci�n necesario al seleccionar una opci�n del men� principal
	 */
	public static void onOptionsItemSelectedStatic(MenuItem item, Actividades idActividad, Context ctx) {
		switch(item.getItemId()){
//			case MENU_CONFIGURACIONES:
//				if (idActividad != Actividades.CONFIGURACIONES){
//					ctx.startActivity(new Intent(ctx, ConfigAppActivity.class));
//				}
//				break;
//			case MENU_NOTICIAS:
//				if (idActividad != Actividades.NOTICIAS){
//					ctx.startActivity(new Intent(ctx, NoticiasActivity.class));
//				}
//				break;
//			case MENU_GALERIA:
//				if (idActividad != Actividades.GALERIAS){
//					ctx.startActivity(new Intent(ctx, GaleriasActivity.class));
//				}
//				break;
//			case MENU_INICIO:
//				if (idActividad != Actividades.PRINCIPAL){
//					ctx.startActivity(new Intent(ctx, InicioActivity.class));
//				}
//				break;
		}
	}
}
