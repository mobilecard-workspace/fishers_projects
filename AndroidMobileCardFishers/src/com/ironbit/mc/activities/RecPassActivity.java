package com.ironbit.mc.activities;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.web.webservices.GetRecoveryWS;
import com.ironbit.mc.web.webservices.events.OnGeneralWSResponseListener;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RecPassActivity extends MainActivity {
	protected Button btnOk = null;
	protected EditText txtUsuario = null;
	protected TextView recusuario = null;
	protected final FormValidator formValidator = new FormValidator(this, true);
	protected LongProcess sendDataProcess = null;
	protected GetRecoveryWS recoveryWS = null;
	protected EditText getTxtUsuario(){
		if (txtUsuario == null){
			txtUsuario = (EditText)findViewById(R.id.txtUsuario);
		}
		
		return txtUsuario;
	}
	
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	protected TextView getBtnRecUsuario(){
		if (recusuario == null){
			recusuario = (TextView)findViewById(R.id.btnRecuperarUsuario);
		}
		
		return recusuario;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inicializarGUI(R.layout.recuperar_pass);
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		sendDataProcess = new LongProcess(this, true, 30, true, "processUpdatePass", new LongProcessListener(){

			
			public void doProcess() {
				// TODO Auto-generated method stub
				recoveryWS = new GetRecoveryWS(RecPassActivity.this);
				recoveryWS.setUser(getTxtUsuario().getText().toString());
				
				recoveryWS.execute(new OnGeneralWSResponseListener(){

					
					public void onGeneralWSErrorListener(String error) {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(), "Error. Int�ntelo de nuevo. "+error, Toast.LENGTH_LONG).show();
						sendDataProcess.processFinished();
					}

					
					public void onGeneralWSResponseListener(String response) {
						// TODO Auto-generated method stub
						System.out.println("**** Respuesta: "+response);
						if(response.equals("0")){
							Toast.makeText(getApplicationContext(), "La contrase�a a sido enviada a su correo electr�nico.", Toast.LENGTH_LONG).show();
							getTxtUsuario().setText("");
						}else if(response.equals("-2")){
							Toast.makeText(getApplicationContext(), "No se encontr� registrado el correo electr�nico.", Toast.LENGTH_LONG).show();
						}else if(response.equals("-1")){
							Toast.makeText(getApplicationContext(), "Error. Int�ntelo de nuevo", Toast.LENGTH_LONG).show();
						}else{
							Toast.makeText(getApplicationContext(), "Error. Int�ntelo de nuevo", Toast.LENGTH_LONG).show();
						}
						sendDataProcess.processFinished();
					}
					
				});
			}

			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (recoveryWS != null){
					recoveryWS.cancel();
					recoveryWS = null;
				}
			}
			
		});
		
		getBtnOk().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				if (formValidator.validate()){
					sendDataProcess.start();
				}
			}
		});
		
		getBtnRecUsuario().setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(RecPassActivity.this, RecUsuarioActivity.class));
			}
			
		});
	}
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtUsuario(), "Usuario")
			.canBeEmpty(false)
			.maxLength(10)
			.minLength(3);
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.REC_PASS;
	}
}
