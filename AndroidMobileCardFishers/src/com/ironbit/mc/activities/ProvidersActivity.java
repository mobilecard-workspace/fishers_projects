package com.ironbit.mc.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.interjet.InterjetActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.system.cache.image.ImageCacheManager;
import com.ironbit.mc.web.webservices.GetProvidersWSClient;
import com.ironbit.mc.web.webservices.data.DataProvider;
import com.ironbit.mc.web.webservices.events.OnProvidersResponseReceivedListener;

public class ProvidersActivity extends MenuActivity{
	protected GetProvidersWSClient proveedoresWS = null;
	public static final String CONF_KEY_CLAVE_CATEGORIA = "adc_clave_categoria";
	protected LongProcess process = null;
	public static ProvidersActivity instance = null;
	protected TextView txtSubtitulos = null;
	protected ImageView imgSubtitulos = null;
	public static final String CONF_KEY_CATEGORIA_IMG = "adc_categoria_img";
	
	
	protected TextView getTxtSubtitulos(){
		if (txtSubtitulos == null){
			txtSubtitulos = (TextView)findViewById(R.id.txtSubtitulo);
		}
		
		return txtSubtitulos;
	}
	
	
	protected ImageView getImgSubtitulos(){
		if (imgSubtitulos == null){
			imgSubtitulos = (ImageView)findViewById(R.id.imgSubtitulo);
		}
		
		return imgSubtitulos;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		inicializarGUI(R.layout.categorias);
		
		process = new LongProcess(this, true, 50, true, "getProviders", new LongProcessListener(){
			public void doProcess() {
				proveedoresWS = new GetProvidersWSClient(ProvidersActivity.this);
				proveedoresWS.setClaveCategoria(getAppConf(ProvidersActivity.this, CONF_KEY_CLAVE_CATEGORIA));
				proveedoresWS.execute(new OnProvidersResponseReceivedListener(){
						public void onProvidersResponseReceived(ArrayList<DataProvider> providers) {
							
							
						//if(providers.size() > 1){
							int cont = 0;
							LinearLayout tabla = (LinearLayout)((ViewGroup)findViewById(R.id.lista_items));
							LinearLayout row = null;
							
							for (DataProvider provider : providers){
								final String path = provider.getPath();
								final String claveWS = provider.getClaveWS(); 
								final String descripcion = provider.getDescription(); 
								final int clave = provider.getClave();
								final int comp = provider.getCompatible();
								final int tipoTarjeta = provider.getTipoTarjeta();
								final LinearLayout linLayItem = (LinearLayout)getLayoutInflater().inflate(R.layout.item2, null);
								
								if(cont%2 == 0){
									row = new LinearLayout(instance);
									row.setLayoutParams(new LinearLayout.LayoutParams(
									LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
									row.setOrientation(LinearLayout.HORIZONTAL);
									//row.setPadding(0, 20, 0, 0);
									row.setGravity(Gravity.LEFT);
									tabla.addView(row);
								}
								//texto
								final TextView txtTitulo = (TextView)linLayItem.findViewById(R.id.item_text);
								txtTitulo.setText(provider.getDescription());
								
								//imagen
								ImageCacheManager.getImagen((ImageView)linLayItem.findViewById(R.id.item_img), provider.getPath(), 67);
								
								DisplayMetrics metrics = new DisplayMetrics();
							    getWindowManager().getDefaultDisplay().getMetrics(metrics);
							    linLayItem.setWeightSum(1.0f);
								linLayItem.setMinimumWidth(metrics.widthPixels/2);
								//linLayItem.setMinimumHeight(metrics.widthPixels/3);
								
								
								linLayItem.setOnClickListener(new View.OnClickListener(){
									public void onClick(View v) {
										if(comp==1){
											setAppConf(ProvidersActivity.this, ProductsActivity.CONF_KEY_PROVEEDOR_IMG, path);
											setAppConf(ProvidersActivity.this, ResumenCompraActivity.CONF_KEY_PROVEEDOR, descripcion);
											setAppConf(ProvidersActivity.this, ProductsActivity.CONF_KEY_CLAVEWS_PROVEEDOR, claveWS);
											setAppConf(ProvidersActivity.this, ProductsActivity.CONF_KEY_ID_PROVEEDOR, Integer.toString(clave));
											
											//modoficaci�n aurelio 2012/09/17
											if(clave==11){
//												if(tipoTarjeta == 3){
//
//													setAppConf(ProvidersActivity.this, InterjetAmexActivity.CONF_KEY_PRODUCTO, descripcion);
//													setAppConf(ProvidersActivity.this, InterjetAmexActivity.CONF_KEY_PRODUCTO_CLAVE, Integer.toString(clave));
//													startActivity(new Intent(ProvidersActivity.this, InterjetAmexActivity.class));
//												}
//												else{
													setAppConf(ProvidersActivity.this, InterjetActivity.CONF_KEY_PRODUCTO, descripcion);
													setAppConf(ProvidersActivity.this, InterjetActivity.CONF_KEY_PRODUCTO_CLAVE, Integer.toString(clave));
													startActivity(new Intent(ProvidersActivity.this, InterjetActivity.class));
//												}										
												
											}
											else{
												startActivity(new Intent(ProvidersActivity.this, ProductsActivity.class));	
											}
											
										}else{
											String msg="";
											System.out.println("Tipo Tarjeta: "+tipoTarjeta);
											if(tipoTarjeta==1){
												msg = "Lo sentimos, este producto no est� disponible en este momento para tarjetas Visa/MasterCard ";
											}else{
												msg = "Lo sentimos, este producto no est� disponible en este momento para tarjetas AMEX";
											}
											AlertDialog.Builder builder = new AlertDialog.Builder(ProvidersActivity.this);
											builder.setMessage(msg)
											       .setCancelable(false)
											       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
											           public void onClick(DialogInterface dialog, int id) {
											        	  dialog.cancel();											        	
											           }
											       
											       });
											
											AlertDialog alert = builder.create();
									        alert.show();
										}
									}
								});
								
								linLayItem.setOnTouchListener(new View.OnTouchListener() {
									
									
									public boolean onTouch(View v, MotionEvent event) {
										// TODO Auto-generated method stub
										switch(event.getAction()){
											case MotionEvent.ACTION_DOWN:
												linLayItem.setBackgroundColor(Color.parseColor("#2D7EC1"));
												break;
												
											case MotionEvent.ACTION_OUTSIDE:
												
												break;
												
											case MotionEvent.ACTION_CANCEL:
												linLayItem.setBackgroundColor(Color.parseColor("#414042"));
												break;
												
											case MotionEvent.ACTION_UP:
												linLayItem.setBackgroundColor(Color.parseColor("#414042"));
												break;
										}
										return false;
									}
								});
	
								linLayItem.setOnFocusChangeListener(new OnFocusChangeListener() {
									public void onFocusChange(View v, boolean hasFocus) {
										if (hasFocus){
											linLayItem.setBackgroundColor(Color.parseColor("#2D7EC1"));
											//txtTitulo.setTextColor(Color.WHITE);
										}else{
											linLayItem.setBackgroundColor(Color.parseColor("#414042"));
											//txtTitulo.setTextColor(Color.parseColor("#fff"));
										}
									}
								});
								
								//lo agregamos a la lista
								//((ViewGroup)findViewById(R.id.lista_items)).addView(linLayItem);
								if(row!=null)
									row.addView(linLayItem);
								cont++;
							
							}
							
							process.processFinished();
						//}
						
						/*if(providers.size() == 1){
							DataProvider provider = providers.get(0);
							
							 String path = provider.getPath();
							 String claveWS = provider.getClaveWS(); 
							 String descripcion = provider.getDescription();
							 int clave = provider.getClave();
							 
							 finish();
							 setAppConf(ProvidersActivity.this, ProductsActivity.CONF_KEY_PROVEEDOR_IMG, path);
							setAppConf(ProvidersActivity.this, ResumenCompraActivity.CONF_KEY_PROVEEDOR, descripcion);
							setAppConf(ProvidersActivity.this, ProductsActivity.CONF_KEY_CLAVEWS_PROVEEDOR, claveWS);
							setAppConf(ProvidersActivity.this, ProductsActivity.CONF_KEY_ID_PROVEEDOR, Integer.toString(clave));
							startActivity(new Intent(ProvidersActivity.this, ProductsActivity.class));
						}*/
					}
				});
			}
			
			public void stopProcess() {
				if (proveedoresWS != null){
					proveedoresWS.cancel();
					proveedoresWS = null;
				}
			}
		});
		
		process.start();
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		getTxtSubtitulos().setText(getAppConf(this, ResumenCompraActivity.CONF_KEY_CATEGORIA));
		ImageCacheManager.getImagen(getImgSubtitulos(), getAppConf(this, CONF_KEY_CATEGORIA_IMG));
	}
	
	
	@Override
	protected void onStop() {
		process.stop();
		super.onStop();
	}
	
	
	@Override
	protected void onDestroy() {
		instance = null;
		super.onDestroy();
	}
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.PROVEEDORES;
	}
}