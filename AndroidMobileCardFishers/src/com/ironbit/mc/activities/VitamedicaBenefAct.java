package com.ironbit.mc.activities;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.form.validator.FormValidator;
import com.ironbit.mc.form.validator.Validable;
import com.ironbit.mc.system.Fecha;
import com.ironbit.mc.system.Fecha.FormatoFecha;
import com.ironbit.mc.system.Fecha.TipoFecha;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.webservices.GetEdoCivilWSClient;
import com.ironbit.mc.web.webservices.GetParentescoWSClient;
import com.ironbit.mc.web.webservices.GetUserWSClient;
import com.ironbit.mc.web.webservices.events.OnBanksResponseReceivedListener;
import com.ironbit.mc.web.webservices.events.OnUserGetResponseReceivedListener;
import com.ironbit.mc.widget.SpinnerValues;
import com.ironbit.mc.widget.datepickerdialog.MyDatePickerDialog;


public class VitamedicaBenefAct extends MenuActivity {
	public static final int CONF_DIALOG = 1;
	private static int nBenef = 0;
	private JSONObject[] arrBenef;
	protected FormValidator formValidator = null;
	protected MyDatePickerDialog fechaNacimientoDialog = null;
	protected Fecha fechaNacimiento = null;
	protected Button btnFecha;
	/*


"beneficiario":[{"FecNac":"01\/01\/1990","apMat":"PEREZ","apPat":"LOPEZ","id_edoc":"1","id_gen":"M","id_par":"4",
"nombre":"PACO"},
{"FecNac":"25\/12\/1985","apMat":"PEREZ","apPat":"LOPEZ","id_edoc":"2","id_gen":"F","id_par":"4","nombre":"ROCIO"}],

{"nombre":"manu","bParentesco":"","FecNac":"1986-04-01","id_par":3,"bEdoCivil":"","id_gen":1,"id_edoc":1,"bGenero":"M",
"apMat":"lara","apPat":"glez"}
	 */
	public static final String BNOMBRE = "nombre";
	public static final String BAPAT = "apPat";
	public static final String BAMAT = "apMat";
	
	public static final String BGENERO = "id_gen";
	public static final String BEDO_CIVIL = "id_edoc";
	public static final String BPARENT = "id_par";
	
	public static final String BGENERO_INDEX = "genero_index";
	public static final String BEDO_CIVIL_INDEX = "edocivil_index";
	public static final String BPARENT_INDEX = "parent_index";
	
	public static final String BNACIM = "FecNac";
	ArrayAdapter<CharSequence> adapterGen, adapterEdoCivil, adapterParent;
	protected Spinner cmbEdoCivil, cmbParentesco = null;
	protected SpinnerValues cmbSVEstadoC, cmbSVParentesco= null;
	
	protected LongProcess loadDataProcess = null;
	protected GetEdoCivilWSClient edoCivilGet = null;
	protected GetParentescoWSClient parentGet = null;
	
	ArrayList<BasicNameValuePair> estadosData, parentData;
	SpinnerValues SVEdoCivil, SVParent;
	String json;
	
	private String clave;
	private String claveWS;
	private String descripcion;
	private String categoria;
	private String claveProveedor;
	private String proveedor;
	
	private static final String TAG = "VitamedicaBenefAct";
	
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.VITAMEDICA_BENEFICIARIO;
	}
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		estadosData = new ArrayList<BasicNameValuePair>();
		parentData = new ArrayList<BasicNameValuePair>();
		json = getIntent().getStringExtra("json");
		
		inicializarGUI(R.layout.vitamedica_beneficiarios);
		
	}
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		clave = getIntent().getStringExtra("clave");
		Log.d(TAG, "CLAVE: " + clave);
		
		claveWS = getIntent().getStringExtra("claveWS");
		Log.d(TAG, "CLAVEWS: " + claveWS);
		
		descripcion = getIntent().getStringExtra("descripcion");
		Log.d(TAG, "DESCRIPCION: " + descripcion);
		
		categoria = getIntent().getStringExtra("categoria");
		Log.d(TAG, "CATEGORIA: " + categoria);
		
		proveedor = getIntent().getStringExtra("proveedor");
		Log.d(TAG, "PROVEEDOR: " + proveedor);
		
		claveProveedor = getIntent().getStringExtra("claveProveedor");
		Log.d(TAG, "CLAVE_PROVEEDOR: " + claveProveedor);
		
		//jsonArray = new JSONArray();
		int numFam = getIntent().getIntExtra("numFam", 0);
		String texto = getResources().getQuantityString(R.plurals.beneficiarios, numFam, numFam);
		((TextView)findViewById(R.id.txtBenefs)).setText(texto);
		
		arrBenef = new JSONObject[numFam];
		
		adapterGen = ArrayAdapter.createFromResource(
	            this, R.array.sexo, android.R.layout.simple_spinner_item);
		adapterGen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    
		/*adapterEdoCivil = ArrayAdapter.createFromResource(
	            this, R.array.edo_civil, android.R.layout.simple_spinner_item);
		adapterEdoCivil.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		/*adapterParent = ArrayAdapter.createFromResource(
	            this, R.array.parentesco, android.R.layout.simple_spinner_item);
		adapterParent.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
	    
		LinearLayout ly = (LinearLayout)findViewById(R.id.lyBenefs);
		LayoutInflater inflater = getLayoutInflater();
		
		for(int i=0; i<numFam; i++){
			View v = inflater.inflate(R.layout.beneficiario_item, null);
			
			Button boton = (Button)v.findViewById(R.id.btnBenef);
			boton.setText("Beneficiario "+(i+1));
			boton.setTag(i);
			boton.setOnClickListener(new OnClickListener(){

				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					nBenef = (Integer)v.getTag();
					System.out.println("Tag: "+nBenef);
					showDialog(CONF_DIALOG);
				}
				
			});
			ly.addView(v);
		}
		
		(findViewById(R.id.button_back)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		(findViewById(R.id.button_menuprincipal)).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(VitamedicaBenefAct.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
		
		(findViewById(R.id.btnOk)).setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout ly = (LinearLayout)findViewById(R.id.lyBenefs);
				
				int count = 0;
				for(int i=0; i<ly.getChildCount(); i++){
					View child = ly.getChildAt(i);
				
					CheckBox check = (CheckBox) child.findViewById(R.id.chkBenef);
					
					if(!check.isChecked())
						break;
					count++;
				}
				
				if(count == ly.getChildCount()){
					String json = getIntent().getStringExtra("json");
					JSONObject obj = null;
					try {
						
						obj = new JSONObject(json);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JSONArray array = new JSONArray();
					for(int i=0; i< arrBenef.length; i++){
						arrBenef[i].remove(BEDO_CIVIL_INDEX);
						arrBenef[i].remove(BGENERO_INDEX);
						arrBenef[i].remove(BPARENT_INDEX);
						array.put(arrBenef[i]);
					}
					try {
						obj.put("beneficiario", array);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("JSON: "+obj);
					Intent intent = new Intent(VitamedicaBenefAct.this, VitamedicaCompraAct.class);
					intent.putExtra("json", obj.toString());
					intent.putExtra("tipo", "FAMILIAR");					
					intent.putExtra("clave", clave);
					intent.putExtra("claveWS", claveWS);
					intent.putExtra("descripcion", descripcion);
					intent.putExtra("categoria", categoria);
					intent.putExtra("proveedor", proveedor);
					intent.putExtra("claveProveedor", "9");
					startActivity(intent);
				}else{
					Toast.makeText(VitamedicaBenefAct.this, "No ha agregado a todos los beneficiarios", Toast.LENGTH_SHORT).show();
				}
			}
			
		});
		
		loadDataProcess = new LongProcess(this, true, 60, true, "LoadUserData", new LongProcessListener(){
			public void doProcess() {
				edoCivilGet = new GetEdoCivilWSClient(VitamedicaBenefAct.this);
				edoCivilGet.execute(new OnBanksResponseReceivedListener(){

					
					public void onBanksResponseReceived(
							ArrayList<BasicNameValuePair> ests) {
						estadosData = ests;
						// TODO Auto-generated method stub
						parentGet = new GetParentescoWSClient(VitamedicaBenefAct.this);
						parentGet.execute(new OnBanksResponseReceivedListener(){

							
							public void onBanksResponseReceived(
									ArrayList<BasicNameValuePair> parents) {
								// TODO Auto-generated method stub
								parentData = parents;
								loadDataProcess.processFinished();
							}
							
						});
						
					}
					
				});
						
			}

			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (edoCivilGet != null){
					edoCivilGet.cancel();
					edoCivilGet = null;
				}
			}
				
			
		});
		
		loadDataProcess.start();
	}
	

	protected MyDatePickerDialog getFechaNacimientoDialog(){
		if (fechaNacimientoDialog == null){
			fechaNacimientoDialog = new MyDatePickerDialog(this, new DatePickerDialog.OnDateSetListener(){
				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
					try{
						fechaNacimiento.setAnio(year);
						fechaNacimiento.setMesBase0(monthOfYear);
						fechaNacimiento.setDia(dayOfMonth);
						fechaNacimiento.actualizar();
						
						updateFechaNacimiento();
					}catch (ErrorSys e) {
						e.showMsj(VitamedicaBenefAct.this);
					}
				}
			},fechaNacimiento.getAnio() ,fechaNacimiento.getMesBase0(), fechaNacimiento.getDia());
		}
		return fechaNacimientoDialog;
	}
	
	protected void updateFechaNacimiento(){
		if (btnFecha != null){
			
			btnFecha.setText("Seleccionar Fecha [" + fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYY) + "]");
		}
	}
	
	protected Dialog onCreateDialog(int id) {
		System.out.println("Create Dialog");
	    AlertDialog.Builder builder;
	    AlertDialog alertDialog = null;
	    switch(id) {
	    case CONF_DIALOG:
	    	Context mContext = VitamedicaBenefAct.this;
	    	LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
	    	View layout = inflater.inflate(R.layout.beneficiario_form,
	    	                              null );

	    	/*TextView text = (TextView) layout.findViewById(R.id.text);
	    	text.setText("Hello, this is a custom dialog!");
	    	ImageView image = (ImageView) layout.findViewById(R.id.image);
	    	image.setImageResource(R.drawable.android);*/
	    	formValidator  = new FormValidator(this, true);
	    	EditText txtNombre = (EditText) layout.findViewById(R.id.txtNombre);
	    	EditText txtAPat = (EditText) layout.findViewById(R.id.txtApellidoP);
	    	EditText txtAmat = (EditText) layout.findViewById(R.id.txtApellidoM);
	    	
	    	formValidator.addItem(txtNombre, "Nombre")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(50)
			.minLength(3);
		
			formValidator.addItem(txtAPat, "Apellido Paterno")
				.canBeEmpty(false)
				.isRequired(true)
				.maxLength(50);
			formValidator.addItem(txtAmat, "Apellido Materno")
				.canBeEmpty(true)
				.isRequired(false)
				.maxLength(50);
		
			final Spinner spinSexo = (Spinner)layout.findViewById(R.id.spin_sexo);
			spinSexo.setAdapter(adapterGen);
			
			final Spinner spinEdoCivil = (Spinner)layout.findViewById(R.id.spin_estado_civil);
			SVEdoCivil=  new SpinnerValues(this, spinEdoCivil);
			SVEdoCivil.setDatos(estadosData);
			//spinEdoCivil.setAdapter(adapterEdoCivil);
			
			final Spinner spinParent = (Spinner)layout.findViewById(R.id.spin_parentesco);
			SVParent= new SpinnerValues(this, spinParent);
			SVParent.setDatos(parentData);
			//spinParent.setAdapter(adapterParent);
			
			formValidator.addItem(spinSexo, "Sexo", new Validable(){

				
				public void validate() throws ErrorSys {
					// TODO Auto-generated method stub
					//System.out.println("Sex item pos: "+getCmbSexo().getSelectedItemPosition());
					
					if(spinSexo.getSelectedItemPosition()==0){
						throw new ErrorSys("Debe seleccionar su sexo");
					}
				}
				
			});
			
			Button btnNacimiento = (Button)layout.findViewById(R.id.btnFechaNacimiento);
			btnFecha = btnNacimiento;
			btnNacimiento.setOnClickListener(new OnClickListener(){

				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					getFechaNacimientoDialog().updateFecha(fechaNacimiento);
					getFechaNacimientoDialog().show();
				}
				
			});
			
			if(arrBenef[nBenef]!= null){
				System.out.println("*** NO NULL ***");
				try {
					txtNombre.setText(arrBenef[nBenef].getString(BNOMBRE));
					txtAPat.setText(arrBenef[nBenef].getString(BAPAT));
					txtAmat.setText(arrBenef[nBenef].getString(BAMAT));
					
					spinSexo.setSelection(arrBenef[nBenef].getInt(BGENERO_INDEX));
					spinEdoCivil.setSelection(arrBenef[nBenef].getInt(BEDO_CIVIL_INDEX));
					spinParent.setSelection(arrBenef[nBenef].getInt(BPARENT_INDEX));
					
					try {
						fechaNacimiento = new Fecha();
					} catch (ErrorSys e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					btnNacimiento.setText(arrBenef[nBenef].getString("BNOMBRE"));
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
	    	builder = new AlertDialog.Builder(mContext);
	    	builder.setView(layout);
	    	builder.setCancelable(true)
	    	
	    	.setPositiveButton("Confirmar",null)
	    	.setNegativeButton("Cancelar", null);
	    	
	    	alertDialog = builder.create();
	    	alertDialog.setOnShowListener(new MyOnShow(alertDialog));
	    	
	    	break;
	    default:
	    	break;
	    }
	    
	    return alertDialog;
	}
	
	protected void onPrepareDialog (int id, Dialog dialog){
		System.out.println("Prepare dialog:");
		EditText txtNombre = (EditText) dialog.findViewById(R.id.txtNombre);
    	EditText txtAPat = (EditText) dialog.findViewById(R.id.txtApellidoP);
    	EditText txtAmat = (EditText) dialog.findViewById(R.id.txtApellidoM);
    	
    	Spinner spinSexo = (Spinner)dialog.findViewById(R.id.spin_sexo);
		Spinner spinEdoCivil = (Spinner)dialog.findViewById(R.id.spin_estado_civil);
		Spinner spinParent = (Spinner)dialog.findViewById(R.id.spin_parentesco);
		
		Button btnNacimiento = (Button)dialog.findViewById(R.id.btnFechaNacimiento);
		
		if(arrBenef[nBenef]!= null){
			System.out.println("*** NO NULL ***");
			System.out.println(arrBenef[nBenef]);
			try {
				txtNombre.setText(arrBenef[nBenef].getString(BNOMBRE));
				txtAPat.setText(arrBenef[nBenef].getString(BAPAT));
				txtAmat.setText(arrBenef[nBenef].getString(BAMAT));
				
				spinSexo.setSelection(arrBenef[nBenef].getInt(BGENERO_INDEX));
				spinEdoCivil.setSelection(arrBenef[nBenef].getInt(BEDO_CIVIL_INDEX));
				spinParent.setSelection(arrBenef[nBenef].getInt(BPARENT_INDEX));
				
				try {
					System.out.println("Fecha: "+arrBenef[nBenef].getString(BNACIM));
					System.out.println("Replace: "+arrBenef[nBenef].getString(BNACIM).replace("\\/", "/"));
					fechaNacimiento = new Fecha(arrBenef[nBenef].getString(BNACIM).replace("\\/", "/"), TipoFecha.date);
				} catch (ErrorSys e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				btnNacimiento.setText(arrBenef[nBenef].getString(BNACIM));
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			try {
				fechaNacimiento = new Fecha();
				fechaNacimiento.restarAnios(18);
			} catch (ErrorSys e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			txtNombre.setText("");
			txtAPat.setText("");
			txtAmat.setText("");
			
			spinSexo.setSelection(0);
			spinEdoCivil.setSelection(0);
			spinParent.setSelection(0);
			
			btnNacimiento.setText("Seleccionar Fecha");
		}
		
		txtNombre.requestFocus();
	}
	/*private class MiDialogListener implements DialogInterface.OnClickListener{

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			System.out.println("which: "+which);
			switch(which){
			
			case DialogInterface.BUTTON_NEGATIVE:
				System.out.println("Negative");
				break;
			case DialogInterface.BUTTON_NEUTRAL:
				System.out.println("Neutral");
				break;
			case DialogInterface.BUTTON_POSITIVE:
				System.out.println("Positive");
				if(formValidator.validate()){
					System.out.println("Valida");
					return;
				}
				break;
			}
		}
		
	}*/

    private class MyOnShow implements DialogInterface.OnShowListener{
    	AlertDialog d;
    	
    	Button fechaButton;
    	
    	public MyOnShow(AlertDialog dialog){
    		super();
    		d = dialog;
    	}
    	protected void updateFechaNacimiento(){
    		if (fechaNacimiento != null){
    			fechaButton.setText("Seleccionar Fecha [" + fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYY) + "]");
    		}
    	}
    	
		
		public void onShow(final DialogInterface dialog) {
			// TODO Auto-generated method stub
	          
	          Button positive = d.getButton(AlertDialog.BUTTON_POSITIVE);
	          positive.setOnClickListener(new View.OnClickListener() {
	
	              
	              public void onClick(View view) {
	                  // TODO Do something
	            	  if(formValidator.validate()){
	  					System.out.println("Valida");
	  					EditText txtNombre = (EditText) d.findViewById(R.id.txtNombre);
	  			    	EditText txtAPat = (EditText) d.findViewById(R.id.txtApellidoP);
	  			    	EditText txtAmat = (EditText) d.findViewById(R.id.txtApellidoM);
	  			    	Spinner spinSexo = (Spinner)d.findViewById(R.id.spin_sexo);
	  					Spinner spinEdoCivil = (Spinner)d.findViewById(R.id.spin_estado_civil);
	  					Spinner spinParent = (Spinner)d.findViewById(R.id.spin_parentesco);
	  					
	  					JSONObject json = new JSONObject();
	  					
	  					try {
							json.put(BNOMBRE, txtNombre.getText().toString());
							json.put(BAPAT, txtAPat.getText().toString());
							json.put(BAMAT, txtAmat.getText().toString());
							json.put(BGENERO, ""+spinSexo.getSelectedItem().toString().charAt(0));
							json.put(BEDO_CIVIL, SVEdoCivil.getValorSeleccionado());
							json.put(BPARENT, SVParent.getValorSeleccionado());
							json.put(BGENERO_INDEX, spinSexo.getSelectedItemPosition());
							json.put(BEDO_CIVIL_INDEX, spinEdoCivil.getSelectedItemPosition());
							json.put(BPARENT_INDEX, spinParent.getSelectedItemPosition());
							
							json.put(BNACIM, fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYY) );
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						arrBenef[nBenef] = json;
						System.out.println(arrBenef[nBenef]);
	  					LinearLayout ly = (LinearLayout)findViewById(R.id.lyBenefs);
	  					
	  					
	  					View child = ly.getChildAt(nBenef);
	  					CheckBox check = (CheckBox) child.findViewById(R.id.chkBenef);
	  					check.setChecked(true);
	  					dialog.dismiss();
	  				}
	                  //Dismiss once everything is OK.
	                  
	              }
	          });
		}
		
		
	}
}
