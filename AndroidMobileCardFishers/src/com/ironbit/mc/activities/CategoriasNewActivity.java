package com.ironbit.mc.activities;

import org.addcel.fishers.view.FishersConsultaActivity;
import org.addcel.fishers.view.FishersMenuActivity;

import com.ironbit.mc.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class CategoriasNewActivity extends MenuActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_categorias);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.CATEGORIAS_NEW;
	}
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			
			switch (id) {
			case R.id.button_recarga:
				startActivity(new Intent(CategoriasNewActivity.this, RecargasNewActivity.class));
				break;
			case R.id.button_servicios:
				startActivity(new Intent(CategoriasNewActivity.this, ServiciosActivity.class));
				break;
			case R.id.button_donaciones:
				break;
			case R.id.button_fishers:
				startActivity(new Intent(CategoriasNewActivity.this, FishersMenuActivity.class));
				break;
				
			case R.id.button_menu:
				onBackPressed();
				break;
			case R.id.button_back:
				onBackPressed();
				break;

			default:
				break;
			}
		}
	}

}
