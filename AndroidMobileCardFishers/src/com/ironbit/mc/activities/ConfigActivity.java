package com.ironbit.mc.activities;

import com.ironbit.mc.activities.MainActivity.Actividades;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;

public abstract class ConfigActivity extends PreferenceActivity{
	
	/**
	 * Metodos    
	 */
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
    
    abstract public Actividades getIdActividad();
    
    
    /**
     * @param ctx. Contexto donde se desea usar la configuraci�n
     * @param nombre. Nombre de la configuraci�n
     * @param defaultVal. Valor por default (si esque no hay a�n configuraci�n)
     * @return String. valor de la configuraci�n
     */
    public static String getConf(Context ctx, String nombre, String defaultVal){
    	return PreferenceManager.getDefaultSharedPreferences(ctx).getString(nombre, defaultVal);
    }
    
    
    public static String getConf(Context ctx, String nombre){
    	return getConf(ctx, nombre, "");
    }
    
    
    /**
     * @param ctx Contexto
     * @param nombre. nombre de la configuraci�n a obtener
     * @param defaultVal.valor a regresar si la configuraci�n no se encontr�
     * @return Valor de la configuraci�n
     */
    public static String getAppConf(Context ctx, String nombre, String defaultVal){
    	return ctx.getSharedPreferences(MainActivity.ID_APLICACION, 0).getString(nombre, defaultVal);
    }
    
    
    public static String getAppConf(Context ctx, String nombre){
    	return getAppConf(ctx, nombre, "");
    }
    
    
    /**
     * @description
     * guarda una configuraci�n de toda la aplicaci�n
     */
    public static void setAppConf(Context ctx, String nombre, String valor){
    	ctx.getSharedPreferences(MainActivity.ID_APLICACION, 0)
    		.edit()
    		.putString(nombre, valor)
    		.commit();
    }
    
    
    public static String getActivityConf(Context ctx, Actividades actividad, String nombre, String defaultVal){
    	return ctx.getSharedPreferences(actividad.name(), 0).getString(nombre, defaultVal);
    }
    
    
    public static String getActivityConf(Context ctx, Actividades actividad, String nombre){
    	return getActivityConf(ctx, actividad, nombre, "");
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuActivity.onCreateOptionsMenuStatic(menu);
    	return super.onCreateOptionsMenu(menu);
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	MenuActivity.onOptionsItemSelectedStatic(item, getIdActividad(), this);
    	return super.onOptionsItemSelected(item);
    }
}
