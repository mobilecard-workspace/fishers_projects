package com.ironbit.mc.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.events.OnTouchSlideListener;
import com.ironbit.mc.system.events.TouchSlide;
import com.ironbit.mc.web.webservices.GetPromotionsWSClient;
import com.ironbit.mc.web.webservices.data.DataPromotion;
import com.ironbit.mc.web.webservices.events.OnPromotionsResponseReceivedListener;
import com.ironbit.mc.widget.galeria.Galeria;
import com.ironbit.mc.widget.galeria.OnImagenObservadaListener;
import com.ironbit.mc.widget.galeria.OnImagenSeleccionadaListener;

public class PromocionesActivity extends MenuActivity{
	protected GetPromotionsWSClient promocionesWS = null;
	protected LongProcess process = null;
	protected Galeria galeria = null;
	protected static int resIdGaleriaLayout = R.layout.galeria_imagenes;
	protected static int resIdGaleriaItemLayout = R.layout.galeria_imagen;
	protected LinearLayout linLayGaleria = null;
	protected LinearLayout linLayRoot = null;
	protected TextView txtContador = null;
	protected PromocionesActivity instance;
	
	
	protected TextView getTxtContador(){
		if (txtContador == null){
			txtContador = (TextView)findViewById(R.id.galeria_contador);
		}
		
		return txtContador;
	}
	
	
	protected LinearLayout getLinLayGaleria(){
		if (linLayGaleria == null){
			linLayGaleria = (LinearLayout)findViewById(R.id.linLay_galeria);
		}
		
		return linLayGaleria;
	}
	
	
	protected LinearLayout getLinLayRoot(){
		if (linLayRoot == null){
			linLayRoot = (LinearLayout)findViewById(R.id.galeria_layout_root);
		}
		
		return linLayRoot;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		inicializarGUI(R.layout.promociones);
		
		process = new LongProcess(this, true, 50, true, "getPromotions", new LongProcessListener(){
			public void doProcess() {
				promocionesWS = new GetPromotionsWSClient(PromocionesActivity.this);
				promocionesWS.execute(new OnPromotionsResponseReceivedListener() {
					
					public void onPromotionsResponseReceived(ArrayList<DataPromotion> promotions) {
						//inicializamos galeria
						
						if(promotions.size() > 0){
							galeria = new Galeria(PromocionesActivity.this, getLinLayGaleria(), resIdGaleriaLayout, resIdGaleriaItemLayout, new OnImagenSeleccionadaListener() {
								public void onImagenSeleccionada(int posicion) {
									Sys.log("onImagenSeleccionada: " + posicion);
									System.out.println("onImagenSeleccionada: " + posicion);
								}
							});
							galeria.setOnImagenObservadaListener(new OnImagenObservadaListener() {
								
								public void onImagenObservada(int posicion) {
									getTxtContador().setText((posicion+1) + " de " + galeria.getTotalImagenes());
								}
							});
							
							inicializarControlesGaleria();
							
							for (DataPromotion promotion : promotions){
								try{
									galeria.agregarImagen(
										promotion.getPath(), 
										"",
										new String(promotion.getDescription().getBytes(), "UTF-8")
									);
								}catch (Exception e) {}
							}
						}else{
							
							//Toast.makeText(getApplicationContext(), "No existen promociones en este momento.", Toast.LENGTH_LONG).show();
							AlertDialog.Builder builder = new AlertDialog.Builder(instance);
							builder.setMessage("No existen promociones por el momento.")
							       .setCancelable(false)
							       .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
							           public void onClick(DialogInterface dialog, int id) {
							        	  dialog.cancel();
							        	  instance.finish();
							           }
							       
							       });
							AlertDialog alert = builder.create();
					        alert.show();
						}
						process.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (promocionesWS != null){
					promocionesWS.cancel();
					promocionesWS = null;
				}
			}
		});
		
		process.start();
	}
	
	
	@Override
	protected void onStop() {
		process.stop();
		super.onStop();
	}
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.PROMOCIONES;
	}
	
	
	protected void inicializarControlesGaleria(){
		// controles touch
		TouchSlide.registerOnTouchSlideListener(getLinLayGaleria(), new OnTouchSlideListener(){
			public void onTouchSlideLeft() {
				Sys.log("onTouchSlideLeft()");
				verImagenSiguiente();
			}
			
			
			public void onTouchSlideRight() {
				Sys.log("onTouchSlideRight()");
				verImagenAnterior();
			}
		});
		
		//controles de teclado
		getLinLayRoot().setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				boolean consumido = false;
				
				if (event.getAction() == KeyEvent.ACTION_UP){
					switch (keyCode){
						case KeyEvent.KEYCODE_DPAD_RIGHT:
							verImagenSiguiente();
							consumido = true;
							break;
						case KeyEvent.KEYCODE_DPAD_LEFT:
							verImagenAnterior();
							consumido = true;
							break;
					}
				}
				
				return consumido;
			}
		});
	}
	
	
	protected void verImagenSiguiente(){
		if (galeria != null){
			galeria.verImagenSiguiente();
		}
	}
	
	
	protected void verImagenAnterior(){
		if (galeria != null){
			galeria.verImagenAnterior();
		}
	}
}