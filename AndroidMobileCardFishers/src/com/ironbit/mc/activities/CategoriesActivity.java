package com.ironbit.mc.activities;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ironbit.mc.activities.scan.QR_reader_actividad;
import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.system.cache.image.ImageCacheManager;
import com.ironbit.mc.web.webservices.GetCategoriesWSClient;
import com.ironbit.mc.web.webservices.data.DataCategory;
import com.ironbit.mc.web.webservices.events.OnCategoriesResponseReceivedListener;

public class CategoriesActivity extends MenuActivity{
	protected GetCategoriesWSClient categoriasWS = null;
	protected LongProcess process = null;
	public static CategoriesActivity instance = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		inicializarGUI(R.layout.categorias_noimg);

		process = new LongProcess(this, true, 50, true, "getCategorias", new LongProcessListener(){
			public void doProcess() {
				categoriasWS = new GetCategoriesWSClient(CategoriesActivity.this);
				categoriasWS.execute(new OnCategoriesResponseReceivedListener(){
					public void onCategoriesResponseReceived(ArrayList<DataCategory> categories) {
						int cont = 0;
							LinearLayout tabla = (LinearLayout)((ViewGroup)findViewById(R.id.lista_items));
							LinearLayout row = null;
						for (DataCategory categoria : categories){
							final LinearLayout linLayItem = (LinearLayout)getLayoutInflater().inflate(R.layout.item2, null);
							linLayItem.setGravity(1);
							if(cont%2 == 0){
								row = new LinearLayout(instance);
								row.setLayoutParams(new LinearLayout.LayoutParams(
										LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
								row.setOrientation(LinearLayout.HORIZONTAL);
								//row.setPadding(0, 20, 0, 0);
								row.setGravity(Gravity.LEFT);
								
								tabla.addView(row);
							}
							//texto
							final TextView txtTitulo = (TextView)linLayItem.findViewById(R.id.item_text);
							txtTitulo.setText(categoria.getDescription());
							//imagen
							ImageCacheManager.getImagen((ImageView)linLayItem.findViewById(R.id.item_img), categoria.getPath());
							DisplayMetrics metrics = new DisplayMetrics();
						    getWindowManager().getDefaultDisplay().getMetrics(metrics);
							
						    ImageView iv = (ImageView) linLayItem.findViewById(R.id.item_img);
							linLayItem.setMinimumWidth(metrics.widthPixels/2);
							linLayItem.setWeightSum(1.0f);
							//linLayItem.setMinimumHeight(metrics.widthPixels/3);
							
							final String path = categoria.getPath();
							final String clave = categoria.getClave();
							final String descripcion = categoria.getDescription();
							
							System.out.println(":::PATH, CLAVE, DESCRIPCI�N:::" + path + "-"+ clave +"-" + descripcion);
							
							linLayItem.setOnClickListener(new View.OnClickListener(){
								public void onClick(View v) {

									setAppConf(CategoriesActivity.this, ProvidersActivity.CONF_KEY_CATEGORIA_IMG, path);
									setAppConf(CategoriesActivity.this, ResumenCompraActivity.CONF_KEY_CATEGORIA, descripcion);
									setAppConf(CategoriesActivity.this, ProvidersActivity.CONF_KEY_CLAVE_CATEGORIA, clave);
									startActivity(new Intent(CategoriesActivity.this, ProvidersActivity.class));
									
								}
							});
							
							linLayItem.setOnTouchListener(new View.OnTouchListener() {
								
							
								public boolean onTouch(View v, MotionEvent event) {
									// TODO Auto-generated method stub
									switch(event.getAction()){
										case MotionEvent.ACTION_DOWN:
											//System.out.println("Action Down");
											linLayItem.setBackgroundColor(Color.parseColor("#2D7EC1"));
											break;
											
										case MotionEvent.ACTION_OUTSIDE:
											//System.out.println("Action Ouside");
											
											break;
											
										case MotionEvent.ACTION_CANCEL:
											//System.out.println("Action Cancel");
											linLayItem.setBackgroundColor(Color.parseColor("#414042"));
											break;
											
										case MotionEvent.ACTION_UP:
											//System.out.println("Action Up");
											linLayItem.setBackgroundColor(Color.parseColor("#414042"));
											break;
									}
									return false;
								}
							});
							
							linLayItem.setOnFocusChangeListener(new OnFocusChangeListener() {
								public void onFocusChange(View v, boolean hasFocus) {
									if (hasFocus){
										linLayItem.setBackgroundColor(Color.parseColor("#2D7EC1"));
										//txtTitulo.setTextColor(Color.WHITE);
									}else{
										linLayItem.setBackgroundColor(Color.parseColor("#414042"));
										//txtTitulo.setTextColor(Color.parseColor("#fff"));
									}
								}
							});
							
							//lo agregamos a la lista
							//((ViewGroup)findViewById(R.id.lista_items)).addView(linLayItem);
							if(row!=null)
								row.addView(linLayItem);
							cont++;
						}
						
						/*while(cont%3 != 0 && row != null){
							View v = new View();
							cont++;
						}*/
						process.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (categoriasWS != null){
					categoriasWS.cancel();
					categoriasWS = null;
				}
			}
		});
		
		process.start();
	}
	
	
	@Override
	protected void onStop() {
		process.stop();
		super.onStop();
	}
	
	
	@Override
	protected void onDestroy() {
		instance = null;
		super.onDestroy();
	} 
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.CATEGORIAS;
	}
}