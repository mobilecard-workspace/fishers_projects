package com.ironbit.mc.activities;

import java.util.ArrayList;

import com.ironbit.mc.R;
import com.ironbit.mc.adapter.OperacionAdapter;
import com.ironbit.mc.web.webservices.data.DataOperation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class EstadoCuentaActivity extends MenuActivity {
	
	ArrayList<DataOperation> operaciones;
	ListView operacionesList;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_edocuenta);
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.EDO_CUENTA;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		initData();
		operacionesList = (ListView) findViewById(R.id.list_operaciones);
		OperacionAdapter adapter = new OperacionAdapter(EstadoCuentaActivity.this, operaciones);
		operacionesList.setAdapter(adapter);
	}
	
	private void initData() {
		operaciones = getIntent().getParcelableArrayListExtra("operaciones");
	}

	public void executeAction(View v) {
		if (null != v) {
			
			int id = v.getId();
			
			switch (id) {
			case R.id.button_back:
				onBackPressed();
				break;
			case R.id.button_footer:
				Intent intent = new Intent(EstadoCuentaActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				break;

			default:
				break;
			}
		}
	}
}
