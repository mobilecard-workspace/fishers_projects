package com.ironbit.mc.activities.web;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.MenuAppActivity;
public class CompraWebActivity extends Activity {
	private WebView web;
	private String url;
	private String urlRegreso;
	private String ipRegreso;
	private String tipoPago;
	private ProgressDialog dialog;
	private Button finalizar;
	
	public void onCreate(Bundle bundle){
		setContentView(R.layout.web_vista);
		super.onCreate(bundle);
		
		web = (WebView)findViewById(R.id.webInfo);

		setDialog(ProgressDialog.show(CompraWebActivity.this, "", "Cargando...", true));
		
		getFinalizar().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(CompraWebActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
		 
		web.getSettings().setBuiltInZoomControls(true);
		web.getSettings().setJavaScriptEnabled(true);
		web.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		web.getSettings().setLoadsImagesAutomatically(true);
		web.setWebViewClient(new Callback());  
		 
		web.clearSslPreferences();
		web.loadUrl(getUrl());
		
	}
	
	public Button getFinalizar() {
		if (finalizar == null) {
			finalizar = (Button) findViewById(R.id.web_finalizar_button);
		}
		
		return finalizar;
	}
	
	public ProgressDialog getDialog() {
		return dialog;
	}
	
	public void setDialog(ProgressDialog dialog) {
		this.dialog = dialog;
	}
	
	public String getTipoPago() {
		if (tipoPago == null) {
			tipoPago = getIntent().getStringExtra("tipo_pago");
		}
		
		return tipoPago;
	}
	
	public String getUrl(){
		if(url == null){
			url = getIntent().getStringExtra("url");
		}
		
		return url;
	}
	
	public String getUrlRegreso() {
		if (urlRegreso == null) {
			urlRegreso = getIntent().getStringExtra("url_regreso");
		}
		
		return urlRegreso;
	}
	
	public String getIpRegreso() {
		if (ipRegreso == null) {
			ipRegreso = getIntent().getStringExtra("ip_regreso");
		}
		
		return ipRegreso;
	}
	
	private class Callback extends WebViewClient{
		
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			
			if (!getDialog().isShowing() && getDialog() != null) {
				getDialog().show();
			}
		}
		
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			
			if (getDialog().isShowing() && getDialog() != null) {
				getDialog().dismiss();
			}
			
			if (getTipoPago().equals("interjet")) { 
				if (url.equals(getUrlRegreso()) || url.equals(getIpRegreso())) {
					getFinalizar().setVisibility(View.VISIBLE);
				}
			} else if (url.equals(getUrlRegreso())) {
				getFinalizar().setVisibility(View.VISIBLE);
			}
		}

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }
    	
    	@Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error){
//    	    super.onReceivedSslError(view, handler, error);
    	    System.out.println(error.toString());
    		handler.proceed();
    	}

    }
	
}
