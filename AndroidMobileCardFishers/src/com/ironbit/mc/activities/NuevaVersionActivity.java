package com.ironbit.mc.activities;

import com.ironbit.mc.R;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class NuevaVersionActivity extends MainActivity {

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inicializarGUI(R.layout.nueva_version);
		
		String prior = getIntent().getStringExtra("prioridad");
		final String url = getIntent().getStringExtra("url");
		
		Button desc = (Button) findViewById(R.id.btnDescargar);
		Button ignor = (Button) findViewById(R.id.btnIgnorar);
		
		
		desc.setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
			}
			
		});
		
		ignor.setOnClickListener(new OnClickListener(){

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(NuevaVersionActivity.this, IniciarSesionActivity.class));
			}
			
		});
		
		if(prior.equals("1"))
			ignor.setVisibility(Button.INVISIBLE);
		
		
		
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		 
	}
	
	@Override
	protected void onRestart(){
		super.onRestart();
		finish();
	}

}
