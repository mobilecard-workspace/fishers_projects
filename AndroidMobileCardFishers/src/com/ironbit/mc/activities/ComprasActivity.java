package com.ironbit.mc.activities;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ironbit.mc.R;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.web.webservices.GetPurchasesWSClient;
import com.ironbit.mc.web.webservices.GetPurchasesWSClient.Periodo;
import com.ironbit.mc.web.webservices.data.DataOperation;
import com.ironbit.mc.web.webservices.events.OnOperationsResponseReceivedListener;

public class ComprasActivity extends MenuActivity{
	protected GetPurchasesWSClient purchasesWS = null;
	protected LongProcess process = null;

	private static final String TAG = "ComprasActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_edocuenta_options);
		
	}
	
	public void runPeriodoProcess(final Periodo periodo) {

		process = new LongProcess(this, true, 60, true, "getPurchases", new LongProcessListener(){
			public void doProcess() {
				purchasesWS = new GetPurchasesWSClient(ComprasActivity.this);
				purchasesWS.setPeriodo(periodo);
				purchasesWS.execute(new OnOperationsResponseReceivedListener(){
					public void onOperationsResponseReceived(ArrayList<DataOperation> operations) {
						Log.i(TAG, operations.toString());
						//borramos antiguos items
						if (null != operations && operations.size() > 0) {
							Intent i = new Intent(ComprasActivity.this, EstadoCuentaActivity.class);
							i.putParcelableArrayListExtra("operaciones", operations);
							startActivity(i);
						} else {
							Toast.makeText(ComprasActivity.this, "No existen compras en el historial", Toast.LENGTH_SHORT).show();
						}
						
						process.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (purchasesWS != null){
					purchasesWS.cancel();
					purchasesWS = null;
				}
			}
		});
		
		process.start();
	}
	
		
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		

	}
	
	
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			
			switch (id) {
			case R.id.button_hoy:
				runPeriodoProcess(Periodo.hoy);
				break;
			case R.id.button_semana:
				runPeriodoProcess(Periodo.semana);
				break;
			case R.id.button_mes:
				runPeriodoProcess(Periodo.mes);
				break;
			case R.id.button_mes_anterior:
				runPeriodoProcess(Periodo.mesAnterior);
				break;
			case R.id.button_back:
				onBackPressed();
				break;
			case R.id.button_footer:
				onBackPressed();
				break;
			default:
				break;
			}
		}
	}

	
	@Override
	public Actividades getIdActividad() {
		return Actividades.COMPRAS;
	}
}