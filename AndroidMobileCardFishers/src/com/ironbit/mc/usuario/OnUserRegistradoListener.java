package com.ironbit.mc.usuario;

public interface OnUserRegistradoListener {
	public void onUserRegistradoListener(boolean isRegistrado, String mensaje);
	public void onUserDataUpdate(String mensaje);
	public void onUserPassUpdate(String mensaje);
}