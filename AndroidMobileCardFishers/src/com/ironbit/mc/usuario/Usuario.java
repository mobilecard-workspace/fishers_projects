package com.ironbit.mc.usuario;

import android.app.Activity;

import com.ironbit.mc.activities.ConfigActivity;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.web.webservices.UserLoginWSClient;
import com.ironbit.mc.web.webservices.events.OnLoginResponseReceivedListener;

public class Usuario {
	protected static UserLoginWSClient loginWS = null;
	protected static boolean isRegistrado = false;
	public static String nombre = "";
	public static String apellidos = "";
	public static String fechaNacimiento = "";
	public static String direccion = "";
	public static String celular = "";
	public static int proveedor = 0;
	public static String tarjeta = "";
	public static int tipoTarjeta = 0;
	public static int banco = 0;
	public static int fechaVencimientoMes = 0;
	public static int fechaVencimientoAnio = 0;
	private static final String CONF_KEY_LOGIN = "adc_login";
	private static final String CONF_KEY_PASS = "adc_pass";
	public static final String CONF_KEY_IDUSER = "adc_id_user";
	
	
	public static void setLogin(Activity ctx, String login){
		ConfigActivity.setAppConf(ctx, CONF_KEY_LOGIN, login);
	}
	
	public static void setIdUser(Activity ctx, String id){
		ConfigActivity.setAppConf(ctx, CONF_KEY_IDUSER, id);
	}
	
	public static String getIdUser(Activity ctx){
		return ConfigActivity.getAppConf(ctx, CONF_KEY_IDUSER, "");
	}
	
	
	public static String getLogin(Activity ctx){
		return ConfigActivity.getAppConf(ctx, CONF_KEY_LOGIN, "");
	}
	
	
	public static void setPass(Activity ctx, String pass){
		ConfigActivity.setAppConf(ctx, CONF_KEY_PASS, pass);
	}
	
	
	public static String getPass(Activity ctx){
		return ConfigActivity.getAppConf(ctx, CONF_KEY_PASS, "");
	}
	
	
	public static void reset(Activity ctx){
		setLogin(ctx, "");
		setPass(ctx, "");
		setIdUser(ctx, "");
	}
	
	
	protected static UserLoginWSClient getLoginWS(Activity ctx){
		if (loginWS == null){
			loginWS = new UserLoginWSClient(ctx);
		}
		
		return loginWS;
	}
	
	
	public static void estaRegistrado(final Activity ctx, String usuario, String password, final OnUserRegistradoListener listener){
		//si esta función ya había deducido que ya se había logueado, entonces NO consulta el server otra vez
		try{
			getLoginWS(ctx).setLogin(usuario);
			getLoginWS(ctx).setPassword(password);
			getLoginWS(ctx).execute(new OnLoginResponseReceivedListener(){
				public void onLoginResponseReceived(String resultado, String mensaje) {
					isRegistrado = resultado.trim().equals("1");
					if(resultado.trim().equals("99")){
						listener.onUserDataUpdate(mensaje);
					} else if(resultado.trim().equals("98")){
						listener.onUserPassUpdate(mensaje);
					}
					else{
						listener.onUserRegistradoListener(isRegistrado, mensaje);
					}
						
				}
			});
		}catch(Exception e){
			Sys.log(e);
			Sys.log("user NO esta registrado");
			listener.onUserRegistradoListener(false, "Algo fall�, intente de nuevo");
		}
	}
	
	
	public static void cancelarChequeoRegistro(Activity ctx){
		getLoginWS(ctx).cancel();
	}
	
	
	
	public static boolean isRegistrado(){
		return isRegistrado;
	}
}