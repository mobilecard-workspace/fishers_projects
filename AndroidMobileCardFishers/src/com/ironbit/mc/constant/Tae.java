package com.ironbit.mc.constant;

public class Tae {
	public static final int IUSACELL = 1;
	public static final int MOVISTAR = 2;
	public static final int TELCEL = 3;
	public static final int UNEFON = 4;
	
	public static final String IUSACELL_WS = "IUSACELL";
	public static final String MOVISTAR_WS = "MOVISTAR";
	public static final String TELCEL_WS = "TELCELFORWTS";
	public static final String UNEFON_WS = "UNEFON";
}
