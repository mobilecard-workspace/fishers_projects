package com.ironbit.mc;

public class UrlWebServices {
	
//	private final static String URL_WS_BASE = 			"http://www.mobilecard.mx:8080/AddCelBridge/Servicios/";
	private final static String URL_WS_BASE = 			"http://50.57.192.214:8080/AddCelBridge/Servicios/";
//	private final static String URL_WS_BASE = 			"http://www.mobilecard.mx:8080/AddCelBridge_/Servicios/";
	
	public final static String URL_WS_USER_LOGIN = 			URL_WS_BASE + "adc_userLogin.jsp";
	public final static String URL_WS_USER_INSERT = 		URL_WS_BASE + "adc_userInsert.jsp";
	public final static String URL_WS_USER_UPDATE = 		URL_WS_BASE + "adc_userUpdate.jsp";
	public final static String URL_WS_USER_UPDATE_PASS = 	URL_WS_BASE + "adc_userPasswordUpdate.jsp";
	public final static String URL_WS_USER_GET = 			URL_WS_BASE + "adc_getUserData.jsp";
	public final static String URL_WS_CARD_TYPE_GET = 		URL_WS_BASE + "adc_getCardType.jsp";
	public final static String URL_WS_BANKS_GET = 			URL_WS_BASE + "adc_getBanks.jsp";
	public final static String URL_WS_PROVIDERS_GET = 		URL_WS_BASE + "adc_getProviders.jsp";
	public final static String URL_WS_CATEGORIAS_GET = 		URL_WS_BASE + "adc_getCategoris.jsp";
	public final static String URL_WS_PRODUCTOS_GET = 		URL_WS_BASE + "adc_getProducts.jsp";
	public final static String URL_WS_PURCHASES_GET = 		URL_WS_BASE + "adc_getUserPurchases.jsp";
	public final static String URL_WS_PURCHASES_INSERT = 	URL_WS_BASE + "adc_purchase.jsp";
	public final static String URL_WS_IAVE_INSERT = 		URL_WS_BASE + "adc_purchase_iave.jsp";
	public final static String URL_WS_PROMOTIONS_GET = 		URL_WS_BASE + "adc_getPromotions.jsp";
	public final static String URL_WS_CONDITIONS_GET = 		URL_WS_BASE + "adc_getConditions.jsp";
	
	public final static String URL_WS_ESTADOS_GET = 		URL_WS_BASE + "adc_getEstados.jsp";
	public final static String URL_WS_EDO_CIVIL_GET = 		URL_WS_BASE + "adc_getEstadoCivil.jsp";
	public final static String URL_WS_PARENTESCO_GET = 		URL_WS_BASE + "adc_getParentesco.jsp";
	
	public final static String URL_WS_COMISION_GET = 			URL_WS_BASE + "adc_getComision.jsp";
	public final static String URL_WS_RECOVERY_GET = 			URL_WS_BASE + "adc_RecoveryUP.jsp";
	public static final String URL_WS_USER_PASS_MAIL = 			URL_WS_BASE + "adc_userPasswordUpdateMail.jsp";
	public static final String URL_WS_GET_TIPO_RECARGA_TAG =	URL_WS_BASE + "adc_getTipoRecargaTag.jsp";
	public static final String URL_WS_SET_TAG =					URL_WS_BASE + "adc_setTag.jsp";
	public static final String URL_WS_GET_TAG =					URL_WS_BASE + "adc_getTags.jsp";
	public static final String URL_WS_REMOVE_TAG =				URL_WS_BASE + "adc_removeTag.jsp";
	public static final String URL_WS_PURCHASE_OHL =			URL_WS_BASE + "adc_purchase_ohl.jsp";
	public static final String URL_WS_PURCHASE_VIAPASS = 		URL_WS_BASE + "adc_purchase_viapass.jsp";
	public static final String URL_WS_PURCHASE_VITA = 			URL_WS_BASE + "adc_purchase_vitamedica.jsp";
	public static final String URL_WS_SET_SMS = 				URL_WS_BASE + "adc_setSMS.jsp";
	public final static String URL_WS_IAVE_PASE = 				URL_WS_BASE + "adc_purchase_pase.jsp";
	
//	http://mobilecard.mx:8080/AddCelBridge/invita
	
	public final static String URL_WS_INVITA = 					"http://www.mobilecard.mx:8080/AddCelBridge/invita";
	public final static String URL_WS_NUEVO = 					"http://www.mobilecard.mx:8080/AddcelUtilServicios/getUrlModuloApp";


																
	//URL's INTERJET
	public final static String URL_WS_INTERJET_GET_MONTO = 		"http://mobilecard.mx:8080/InterjetWeb/getPNR3"; 
	public final static String URL_WS_INTERJET_GET_ID = 		"http://mobilecard.mx:8080/InterjetWeb/getInterjetTransaction"; 
	public final static String URL_WS_INTERJET_GET_SITEPAGO = 	"http://mobilecard.mx:8080/ProCom/comercio_fin.jsp";
	public final static String URL_WS_INTERJET_REGRESO = 		"http://mobilecard.mx:8080/ProCom/comercio_con.jsp";

	public final static String IP_WS_INTERJET_REGRESO  = 		"http://50.57.192.210:8080/ProCom/comercio_con.jsp";
	
	//URL's EDOMEX
	public final static String URL_WS_EDOMEX_LINEA_GET            = "http://50.57.192.210:8595/GobEstado/Consumer";
	public final static String URL_WS_EDOMEX_GET_SITEPAGO         = "http://mobilecard.mx:8080/EdoMexWeb/envio.jsp";
	public final static String URL_WS_EDOMEX_SET_ID				  = "http://mobilecard.mx:8080/InterjetWeb/getId.aspx";
	public final static String URL_WS_EDOMEX_REGRESO			  = "http://mobilecard.mx:8080/EdoMexWeb/regreso.jsp";
	//URL's SCAN AND PAY
	public final static String URL_WS_SCAN_PAY = URL_WS_BASE + "prosaScanAndPay.jsp";
	
//	URL INSERCIÓN EN BITACORA
	public final static String URL_WS_BITACORA_AGREGA = "http://www.mobilecard.mx:8080/AddCelBridge/ClienteBitacoraAgrega";

}