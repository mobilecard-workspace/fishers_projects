package com.ironbit.mc;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

import com.ironbit.mc.activities.IniciarSesionActivity;
import com.ironbit.mc.activities.MenuActivity;
import com.ironbit.mc.activities.MenuAppActivity;
import com.ironbit.mc.activities.NuevaVersionActivity;
import com.ironbit.mc.activities.process.LongProcess;
import com.ironbit.mc.activities.process.LongProcessListener;
import com.ironbit.mc.system.contador.Contador;
import com.ironbit.mc.system.contador.OnContadorTerminadoListener;
import com.ironbit.mc.web.webservices.GetVersionWSClient;
import com.ironbit.mc.web.webservices.events.OnResponseJSONReceivedListener;

public class InicioActivity extends MenuActivity{
    public static final int WAIT_TIME = 30000;
    private GetVersionWSClient versionClient ;
    private LongProcess versionProcess;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.inicio);
	}
	
	
	@Override
	protected void onStart() {
		super.onStart();
		
		versionProcess = new LongProcess(InicioActivity.this, true, 30, false, "Version", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (versionClient != null) {
					versionClient.cancel();
					versionClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				versionClient = new GetVersionWSClient(InicioActivity.this);
				versionClient.execute(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						String appVersion = getVersionName(getApplicationContext(), InicioActivity.class);
						
						try {
							String[] servVersion = jsonResponse.getString("version").split("[.]");
							String[] currVersion = appVersion.split("[.]");
							
							boolean pasa = true;
							
							for (int i = 0; i < servVersion.length; i++) {
								
								if (i < currVersion.length) {
									int max = Integer.parseInt(servVersion[i]);
									int min = Integer.parseInt(currVersion[i]);
									if (max > min) {
										pasa = false;
										break;
									}
								} else {
									break;
								}
							}
							
							if (pasa) {
								startActivity(new Intent(InicioActivity.this, MenuAppActivity.class));
							} else {
								String prior = jsonResponse.getString("tipo");
								final String url = jsonResponse.getString("url");
								Intent intent = new Intent(InicioActivity.this, NuevaVersionActivity.class);
								intent.putExtra("prioridad", prior);
								intent.putExtra("url", url);
								startActivity(intent);
							}
						} catch(JSONException e) {
							startActivity(new Intent(InicioActivity.this, MenuAppActivity.class));
						} finally {
							versionProcess.processFinished();
						}
						
					
					}
					
					public void onResponseJSONAReceived(JSONArray jsonResponse) {
						// TODO Auto-generated method stub
						
					}
					
					public void onResponseStringReceived(String jsonresponse) {
						// TODO Auto-generated method stub
						
					}
				});
			}
		});
		
		new Contador(new Handler(), new OnContadorTerminadoListener(){
			public void onContadorTerminado() {
				versionProcess.start();
			}
		},2, "splashInicial");
		
	}
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.PRINCIPAL;
	}
	
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	public String getVersionName(Context context, Class<InicioActivity> cls) 
	    {
	      try {
	        ComponentName comp = new ComponentName(context, cls);
	        PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
	        return pinfo.versionName;
	      } catch (android.content.pm.PackageManager.NameNotFoundException e) {
	        return null;
	      }
	    }
}