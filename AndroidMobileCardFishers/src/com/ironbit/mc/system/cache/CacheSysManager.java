package com.ironbit.mc.system.cache;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.ironbit.mc.system.Sys;
import com.ironbit.mc.util.FileUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class CacheSysManager {
	/**
	 * Maximo tama�o de la memoria cache <4 MB>
	 */
	public static final long MAX_SIZE_KB = 4096L;
	
	
	public static long getCacheDirSizeInBytes(Context ctx){
		return FileUtils.getDirSizeInBytes(new File(ctx.getCacheDir().getAbsolutePath()));
	}
	
	
	public static long getCacheDirSizeInMegabytes(Context ctx){
		return FileUtils.getDirSizeInMegabytes(new File(ctx.getCacheDir().getAbsolutePath()));
	}
	
	
	public static long getCacheDirSizeInKilobytes(Context ctx){
		return FileUtils.getDirSizeInKilobytes(new File(ctx.getCacheDir().getAbsolutePath()));
	}
	
	
	public static void saveFile(Context ctx, String fileName, byte[] bytes){
		try{
			FileOutputStream fos = new FileOutputStream(new File(ctx.getCacheDir().getAbsolutePath(), fileName));
			fos.write(bytes);
			fos.flush();
			fos.close();
		}catch(Exception e){
			Sys.log(e);
		}
	}
	
	
	public static void deleteFile(Context ctx, String fileName){
		try{
			new File(ctx.getCacheDir().getAbsolutePath(), fileName).delete();
		}catch(Exception e){
			Sys.log(e);
		}
	}
	
	
	public static void saveImage(Context ctx, String fileName, Bitmap imagen){
		try{
			FileOutputStream fos = new FileOutputStream(new File(ctx.getCacheDir().getAbsolutePath(), fileName));
			imagen.compress(Bitmap.CompressFormat.PNG, 90, fos);
	        
			fos.flush();
			fos.close();
		}catch(Exception e){
			Sys.log(e);
		}
	}
	
	
	public static void deleteImage(Context ctx, String fileName){
		try{
			new File(ctx.getCacheDir().getAbsolutePath(), fileName).delete();
		}catch(Exception e){
			Sys.log(e);
		}
	}
	
	
	public static Bitmap getImage(Context ctx, String fileName){
		Bitmap img = null;
		
		try{
			FileInputStream fIn = new FileInputStream(new File(ctx.getCacheDir().getAbsoluteFile(), fileName));
			img = BitmapFactory.decodeStream(fIn);
	        fIn.close();
		}catch(Exception e){
			Sys.log(e);
		}
		
        return img;
	}
	
	
	public static String getFile(Context ctx, String fileName){
		StringBuffer sb = new StringBuffer();
		int ch;
		
		try{
			FileInputStream fis = new FileInputStream(new File(ctx.getCacheDir().getAbsolutePath(), fileName));
			
			while ((ch = fis.read()) != -1){
				sb.append((char)ch);
			}
			
			fis.close();
		}catch(Exception e){
			Sys.log(e);
		}
		
		return sb.toString();
	}
	
	
	public static void cleanCacheToMaxSize(Context ctx){
		cleanDirToMaxSize(ctx.getCacheDir(), getCacheDirSizeInKilobytes(ctx));
	}
	
	
	/**
	 * Reduce el tama�o de un directorio (normalmente el directorio de cache) 
	 * a la mitad del maximo tama�o permitido <CacheManager.MAX_SIZE_KB>
	 * 
	 * @param directory. File. Directorio a limpiar
	 * @param actualSizeKb. long. tama�o actual del directorio
	 */
	public static void cleanDirToMaxSize(File directory, long actualSizeKb){
		File[] files = directory.listFiles();
		
		for (File file : files){
			if (actualSizeKb > (MAX_SIZE_KB / 2)){
				if (file.isFile()){
					if (file.exists()){
						try{
							actualSizeKb -= (file.length() / 1024);
							file.delete();
							Sys.log("cache file deleted (actualSizeKb:"+actualSizeKb+")");
						}catch(Exception e){
							Sys.log(e);
						}
					}
				}else{
					cleanDirToMaxSize(file, actualSizeKb);
				}
			}else{
				return;
			}
		}
	}
}