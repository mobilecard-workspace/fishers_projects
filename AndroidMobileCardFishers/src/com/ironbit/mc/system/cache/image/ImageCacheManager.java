package com.ironbit.mc.system.cache.image;


import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.cache.CacheSysManager;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.system.image.ImageUtil;

public class ImageCacheManager {
	public static Context ctx = null;
	private static Hashtable<String, Thread> threads = null;
	
	
	public static Hashtable<String, Thread> getThreads(){
		if (threads == null){
			threads = new Hashtable<String, Thread>();
		}
		
		return threads;
	}
	
	
	public static void getImagen(final ImageView imgView, final String urlImg){
		getImagen(imgView, urlImg, 0);
	}
	
	
	public static void getImagen(final ImageView imgView, final String urlImg, int width){
		getImagen(imgView.getContext(), new OnImagenObtenidaListener() {
			public void onImagenObtenida(final Bitmap imagen) {
				Sys.log("onImagenObtenida()");
				imgView.setImageBitmap(imagen);
			}
		}, urlImg, width);
	}
	
	
	public static void getImagen(final Context context, final OnImagenObtenidaListener listener, final String urlImg){
		getImagen(context, listener, urlImg, 0);
	}
	
	
	public static void getImagen(final Context context, final OnImagenObtenidaListener listener, final String urlImg, final int width){
		ctx = context;
		String key = Crypto.md5(urlImg);
		
		getThreads().put(key, new Thread(new Runnable(){
			public void run(){
				try{
					Bitmap img = getSyncImagen(context, urlImg, width);
					
					//colocamos imagen en ImageView (si esque si existe la imagen)
					if (img != null){
						final Bitmap imgAux = img;
						((Activity)context).runOnUiThread(new Runnable(){
							public void run(){
								listener.onImagenObtenida(imgAux);
							}
						});
					}
				}catch(Exception e){
					Sys.log(e);
				}
			}
		}));
		
		//iniciamos hilo
		getThreads().get(key).start();
	}
	
	
	public static Bitmap getSyncImagen(final Context context, final String urlImg){
		return getSyncImagen(context, urlImg, 0);
	}
	
	
	public static Bitmap getSyncImagen(final Context context, final String urlImg, final int width){
		Bitmap img = null;
		ctx = context;
		
		try{
			String imgName = Crypto.md5(urlImg)+"."+Bitmap.CompressFormat.PNG.name();
			
			if (existsLocalImage(imgName)){
				//obtenemos imagen en cacheDiro
				Sys.log("img si existe, la obtenemos");
				
				img = CacheSysManager.getImage(ctx, imgName);
			}else{
				Sys.log("img no existe, la descargamos y guardamos");
				
				//descargamos imagen de internet
				img = ImageUtil.descargarImagen(urlImg);
				
				//almacenamos imagen en archivo (si esque si se descargó correctamente)
				if (img != null){
					//limpiamos cache
					CacheSysManager.cleanCacheToMaxSize(ctx);
					
					if (width > 0){
						//redimensionamos imagen
						img = ImageUtil.resizeImageToWidth(img, width);
					}
					
					//guardamos esta imagen
					CacheSysManager.saveImage(ctx, imgName, img);
				}
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		return img;
	}
	
	
	public static boolean existsLocalImage(String imgName){
		try{
			return new File(ctx.getCacheDir(), imgName).exists();
		}catch(Exception e){
			return false;
		}
	}
	
	
	public static void cancelarObtencionImagen(String urlImagen){
		String key = Crypto.md5(urlImagen);
		
		if (getThreads().get(key) != null){
			try{
				getThreads().get(key).interrupt();
				getThreads().remove(key);
			}catch(Exception e){
				Sys.log(e);
			}
		}
	}
	
	
	public static void cancelarObtencionImagenes(){
		Hashtable<String, Thread> threads = getThreads();
		Enumeration<String> keys = threads.keys();
		String key = null;
		
		while (keys.hasMoreElements()){
			key = keys.nextElement();
			
			try{
				threads.get(key).interrupt();
				threads.remove(key);
			}catch(Exception e){
				Sys.log(e);
			}
		}
	}
}