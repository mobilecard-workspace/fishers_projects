package com.ironbit.mc.system.events;

public interface OnTouchSlideListener {
	public void onTouchSlideLeft();
	
	public void onTouchSlideRight();
}