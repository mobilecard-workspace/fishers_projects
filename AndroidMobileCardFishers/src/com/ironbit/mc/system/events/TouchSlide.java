package com.ironbit.mc.system.events;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class TouchSlide {
	public static void registerOnTouchSlideListener(View vista, final OnTouchSlideListener listener){
		vista.setOnTouchListener(new OnTouchListener() {
			float downXValue = 0;
			
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						downXValue = event.getX();
						
						break;
					case MotionEvent.ACTION_UP:
						float currentX = event.getX();
	
						if (downXValue < currentX) {
							listener.onTouchSlideRight();
						} else if (downXValue > currentX) {
							listener.onTouchSlideLeft();
						}
						
						break;
				}

				return true;
			}
		});
	}
	
	
	/**
	 * Registra varias vistas 4m
	 * @param listener
	 * @param vistas
	 */
	public static void registerOnTouchSlideListener(final OnTouchSlideListener listener, View... vistas){
		for (View v : vistas){
			registerOnTouchSlideListener(v, listener);
		}
	}
}