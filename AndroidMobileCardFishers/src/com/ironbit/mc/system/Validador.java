package com.ironbit.mc.system;


public class Validador {
	
	/**
	 * @param valor. String. numero a verificar 
	 * @return boolean. True si es numero, de lo contrario regresa false
	 * @example
	 *  234		-> true
	 *  233.44	-> true
	 *  34sad3	-> false
	 *  sda		-> false
	 *  .233	-> false
	 *  23.		-> false
	 */
	public static boolean esNumerico(String valor){
		valor = valor.trim();
		boolean esNum = true;
		int numPuntos = 0;
		
		if (valor.length() == 0){
			esNum = false;
		}else{
			if (valor.charAt(0) == '.' || valor.charAt(valor.length()-1) == '.'){
				esNum = false;
			}else{
				for (char c : valor.toCharArray()){
					if (!Character.isDigit(c)){
						if (c != '.'){
							esNum = false;
							break;
						}else{
							numPuntos++;
							
							if (numPuntos > 1){
								esNum = false;
								break;
							}
						}
					}
				}
			}
		}
		
		return esNum;
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de 7 o 10 digitos: 
	 * espacios, guiones o puntos
	 * @note: Funciona para numeros en los M�xico.
	 * @param telefono
	 */
	public static boolean esNumeroEntero(String numero){
		return (numero.matches("\\b\\d+\\b"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de 7 o 10 digitos con extenciones permitidas, delimitadores son: 
	 * espacios, guiones o puntos
	 * @note: Funciona para numeros en los Estados Unidos.
	 * @param telefono
	 */
	public static boolean esNumeroTelefonicoEU(String telefono){
		return (telefono.matches("^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?$"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de 7 o 10 digitos: 
	 * espacios, guiones o puntos
	 * @note: Funciona para numeros en los M�xico.
	 * @param telefono
	 */
	public static boolean esNumeroTelefonicoMX(String telefono){
		return (esNumeroEntero(telefono) && (telefono.length() == 7 || telefono.length() == 10));
	}
	
	
	public static boolean esNumeroCelularMX(String telefono){
		return (esNumeroEntero(telefono) && telefono.length() == 10);
	}
	
	
	/**
	 * Verifica si un texto es una IP valida
	 * @param ip
	 */
	public static boolean esDireccionIP(String ip){
		return (ip.matches("\\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de tarjeta de cr�dito valido (entre 13 y 16 digitos)
	 * @param tarjetaCredito Numero de la tarjeta de credito a validar
	 */
	public static boolean esTarjetaDeCredito(String tarjetaCredito){
		return (tarjetaCredito.matches("^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\\d{3})\\d{11})$"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de tarjeta de cr�dito VISA valido (13 o 16 digitos)
	 * @param tarjetaCredito Numero de la tarjeta de credito VISA a validar
	 */
	public static boolean esTarjetaDeCreditoVisa(String tarjetaCredito){
		return (tarjetaCredito.matches("^4[0-9]{12}(?:[0-9]{3})?$"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de tarjeta de cr�dito MASTER CARD valido (16 digitos)
	 * @param tarjetaCredito Numero de la tarjeta de credito MASTER CARD a validar
	 */
	public static boolean esTarjetaDeCreditoMasterCard(String tarjetaCredito){
		return (tarjetaCredito.matches("^5[1-5][0-9]{14}$"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de tarjeta de cr�dito AMERICAN EXPRESS valido (15 digitos)
	 * @param tarjetaCredito Numero de la tarjeta de credito AMERICAN EXPRESS a validar
	 */
	public static boolean esTarjetaDeCreditoAmericanExpress(String tarjetaCredito){
		return (tarjetaCredito.matches("^3[47][0-9]{13}$"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de tarjeta de cr�dito DINERS CLUB valido (14 digitos)
	 * @param tarjetaCredito Numero de la tarjeta de credito DINERS CLUB a validar
	 */
	public static boolean esTarjetaDeCreditoDinersClub(String tarjetaCredito){
		return (tarjetaCredito.matches("^3(?:0[0-5]|[68][0-9])[0-9]{11}$"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de tarjeta de cr�dito DISCOVER valido (16 digitos)
	 * @param tarjetaCredito Numero de la tarjeta de credito DISCOVER a validar
	 */
	public static boolean esTarjetaDeCreditoDiscover(String tarjetaCredito){
		return (tarjetaCredito.matches("^6(?:011|5[0-9]{2})[0-9]{12}$"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de tarjeta de cr�dito JCB valido (15 o 16 digitos)
	 * @param tarjetaCredito Numero de la tarjeta de credito JCB a validar
	 */
	public static boolean esTarjetaDeCreditoJCB(String tarjetaCredito){
		return (tarjetaCredito.matches("^(?:2131|1800|35\\d{3})\\d{11}$"));
	}
	
	
	/**
	 * Verifica si un texto dado, es correo o no
	 * @return boolean.
	 */
	public static boolean esCorreo(String correo){
		return (correo.matches("^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\\.)*[a-zA-Z0-9-]{2,200}\\.[a-zA-Z]{2,6}$"));
	}
	
	
	/**
	 * Verifica si una cadena esta vacia o no. Una serie de espacios en blanco se considera como cadena vacia
	 * @return boolean
	 */
	public static boolean estaVacio(String q){
		if (q == null){ 
			return true;
		}
		
		for (int i = 0; i < q.length(); i++){
			if (q.charAt(i) != ' '){ 
				return false;
			}
		}
		
		return true;
	}
}