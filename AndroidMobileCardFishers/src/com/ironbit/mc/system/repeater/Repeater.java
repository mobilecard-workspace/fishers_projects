package com.ironbit.mc.system.repeater;

import com.ironbit.mc.system.Sys;

import android.os.Handler;
import android.os.Looper;

/**
 * @author angel
 * @description
 * Clase que se utiliza para realizar alg�n proceso en un determinado tiempo en el futuro.
 * @note
 * La clase que instancie un objeto de esta clase tendr� que pasar un listener para recivir el aviso de que<br/>
 * el tiempo especificado ya termin�
 */
public class Repeater extends Thread{
	/**
	 * Propiedades
	 */
	public Handler handler = null;
	private RepetibleWrapper listener = null;
	
	/**
	 * Tiempo que espera para revisar si ya lleg� al tiempo solicitado
	 */
	public float segundosEspera = 0;
	public String nombre = "";
	private boolean running = false;
	private boolean ejecutarAlInicio = false;
	
	
	/**
	 * Constructor
	 */
	
	public Repeater(Handler handler, long segundosEspera, boolean ejecutarAlInicio, Repetible listener){
		this(handler, segundosEspera, "", ejecutarAlInicio, listener);
	}
	
	
	public Repeater(Handler handler, long segundosEspera, String nombre, boolean ejecutarAlInicio, Repetible listener){
		this(handler, (float)segundosEspera, nombre, ejecutarAlInicio, listener);
	}

	
	public Repeater(Handler handler, float segundosEspera, String nombre, boolean ejecutarAlInicio, Repetible listener){
		this.nombre = nombre;
		this.handler = handler;
		this.listener = new RepetibleWrapper(listener);
		this.segundosEspera = segundosEspera;
		this.ejecutarAlInicio = ejecutarAlInicio;
		running = true;
		
		//iniciamos el contador
		start();
	}
	
	
	public Repeater setSegundosEspera(long segundosEspera){
		this.segundosEspera = segundosEspera;
		return this;
	}
	
	
	public void detener(){
		running = false;
	}
	
	
	/**
	 * Metodos
	 */
	
	@Override
	public void run(){
		Looper.prepare();
		
		try{
			if (ejecutarAlInicio){
				handler.post(listener);
			}
			
			while(running){
				sleep((long)(segundosEspera * 1000));
						
				if (running){
					handler.post(listener);
				}
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		Looper.loop();
	}
}