package com.ironbit.mc.system.contador;

import com.ironbit.mc.system.Sys;

import android.os.Handler;
import android.os.Looper;

/**
 * @author angel
 * @description
 * Clase que se utiliza para realizar alg�n proceso en un determinado tiempo en el futuro.
 * @note
 * La clase que instancie un objeto de esta clase tendr� que pasar un listener para recivir el aviso de que<br/>
 * el tiempo especificado ya termin�
 */
public class Contador implements Runnable{
	public Handler handler = null;
	private OnContadorTerminadoListenerWrapper listener = null;
	public long segundosEspera = 0;
	public String nombre = "";
	private boolean running = false;
	protected Thread thread = null;
	
	
	public void setOnContadorTerminadoListener(OnContadorTerminadoListener listener){
		this.listener = new OnContadorTerminadoListenerWrapper(listener);
	}
	
	
	protected Thread getThread(){
		if (thread == null){
			thread = new Thread(this);
		}
		
		return thread;
	}
	
	
	public Contador(Handler handler, OnContadorTerminadoListener listener, long segundosEspera){
		this(handler, listener, segundosEspera, "");
	}
	
	
	/**
	 * Este constructor se usa para crear un contador pero sin iniciar el conteo
	 * Requiere la posterior llamada a iniciar(OnContadorTerminadoListener listener);
	 * @param handler
	 * @param nombre
	 * @param segundosEspera
	 */
	public Contador(Handler handler, String nombre, int segundosEspera){
		this.nombre = nombre;
		this.handler = handler;
		this.segundosEspera = segundosEspera;
		running = false;
	}
	
	
	public Contador(Handler handler, OnContadorTerminadoListener listener, long segundosEspera, String nombre){
		this(handler, listener, segundosEspera, nombre, true);
	}
	
	
	public Contador(Handler handler, OnContadorTerminadoListener listener, long segundosEspera, String nombre, boolean iniciar){
		this.nombre = nombre;
		this.handler = handler;
		this.segundosEspera = segundosEspera;
		this.listener = new OnContadorTerminadoListenerWrapper(listener);
		
		if (iniciar){
			iniciar();
		}
	}
	
	
	public void iniciar(OnContadorTerminadoListener listener){
		this.listener = new OnContadorTerminadoListenerWrapper(listener);;
		iniciar();
	}
	
	
	public void iniciar(){
		if (getThread().isAlive()){
			getThread().interrupt();
			thread = null;
		}
		
		running = true;
		getThread().start();
	}
	
	
	public void cancelar(){
		Sys.log("cancelando contador "+nombre);
		running = false;
		getThread().interrupt();
		thread = null;
	}
	
	
	public void run(){
		try{
			Looper.prepare();
			
			try{
				Thread.sleep(segundosEspera * 1000);
						
				if (running && listener != null){
					handler.post(listener);
				}
			}catch(Exception e){
				Sys.log(e);
			}
			
			Looper.loop();
		}catch(Exception ee){
			//Sys.log(ee);
		}finally{
			thread = null;
		}
	}
}