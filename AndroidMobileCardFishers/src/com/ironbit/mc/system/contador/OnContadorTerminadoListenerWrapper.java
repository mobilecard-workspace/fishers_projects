package com.ironbit.mc.system.contador;


public class OnContadorTerminadoListenerWrapper implements Runnable{
	protected OnContadorTerminadoListener listener = null;
	
	public OnContadorTerminadoListenerWrapper(OnContadorTerminadoListener listener){
		this.listener = listener;
	}
	
	
	public void run(){
		listener.onContadorTerminado();
	}
}
