package com.ironbit.mc.system.contador;

public interface OnContadorTerminadoListener {
	public void onContadorTerminado();
}
