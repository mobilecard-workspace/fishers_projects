package com.ironbit.mc.system;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * @author angel
 *
 */
public class Text {
	/**
	 * Constantes
	 */
	
	public static final String newLine = "\n";
	public static final String tab = "\t";
	
	
	/**
	 * elimina espacios en blanco, saltos de linea y tabuladores del inicio y final del string recibido
	 */
	public static String trimAll(String txt){
		return txt.trim().replaceAll("\\n", "").replaceAll("\\t", "");
	}
	
	
	public static String trimNewLine(String txt){
		return txt.trim().replaceAll("\\r", "");
	}
	
	
	/**
	 * convierte un numero de un decimal a uno de dos
	 * @param num. Numero a convertir
	 * @return numero con formato NN
	 * @example
	 *  4 -> 04
	 *  10 -> 10
	 */
	public static String dosDigitos(String num){
		if (num.length() == 1){
			return '0'+num;
		}else{
			return num;
		}
	}
	
	
	public static String dosDigitos(int num){
		return dosDigitos(String.valueOf(num));
	}
	
	
	public static String dosDigitos(long num){
		return dosDigitos(String.valueOf(num));
	}
	
	
	public static String dosDigitos(double num){
		return dosDigitos(String.valueOf(num));
	}
	
	
	public static String dosDigitos(float num){
		return dosDigitos(String.valueOf(num));
	}
	
	
	public static String limpiarTexto(String txt){
		return addSlashes(quitarComillas(txt.trim()));
	}
	
	
	public static String addSlashes(String txt){
		return txt.replace("'", "\'").replace('"', '\"').replace("\\", "\\\\");
	}
	
	
	/**
	 *Recorta un texto hasta numMax y si sobrepasa numMax, a txt se le agregan 3 puntos al final
	 *@param txt. String. texto a recortar
	 *@param numMax. Int. Numero máximo permitido de caracteres
	 */
	public static String recortarTexto(String txt, int numMax){
		if (txt == null){
			txt = "";
		}else if (txt.length() > numMax){
			txt = txt.substring(0, numMax - 3)+"...";
		}
		
		return txt;
	}
	
	
	/**
	 * @param txt. Texto al que se le eliminarán las comillas
	 * @return String. Texto sin comillas
	 */
	public static String quitarComillas(String txt){
		return txt.replace("\"", "``").replace("'", "`");
	}
	
	
	/**
	 * Funcion similar a String.replace(), con la diferencia de que acepta arrays de palabras.
	 * @param String[] viejasPalabras. palabras a reemplazar
	 * @param String[] nuevasPalabras. palabras nuevas
	 * @param String texto. texto en donde se buscará
	 * @return texto con las nuevas palabras en vez de las viejas
	 */
	public static String replace(String[] viejasPalabras, String[] nuevasPalabras, String texto){
		int minLen = (viejasPalabras.length > nuevasPalabras.length) ? nuevasPalabras.length : viejasPalabras.length;
		
		for (int i = 0; i < minLen; i++){
			texto = texto.replace(viejasPalabras[i], nuevasPalabras[i]);
		}
		
		return texto;
	}
	
	
	/**
	 * To convert the InputStream to String we use the BufferedReader.readLine()
	 * method. We iterate until the BufferedReader return null which means
	 * there's no more data to read. Each line will appended to a StringBuilder
	 * and returned as String.
	 */
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (Exception e) {}

		return sb.toString();
	}
}
