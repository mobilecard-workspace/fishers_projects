package com.ironbit.mc.system.errores;

import android.content.Context;

public class InfoSys extends ErrorSys{
	
	/*
	 * Propiedades
	 */
	
	private static final long serialVersionUID = 1L;

	/*
	 * Contructores
	 */
	
	public InfoSys(String msj, String msjOpcional){
		super(msj, msjOpcional);
		tipoMsj = TipoMsj.info;
	}
	
	
	public InfoSys(String msj) {
		super(msj);
		tipoMsj = TipoMsj.info;
	}
	
	
	public InfoSys(Context c, int msj, int msjOpcional){
		super(c.getString(msj));
		tipoMsj = TipoMsj.info;
		
		this.setMsjOpcional(c.getString(msjOpcional));
	}
	
	
	public InfoSys(Context c, int msj){
		super(c.getString(msj));
		tipoMsj = TipoMsj.info;
	}
	
	
	public InfoSys(int msj, int msjOpcional){		
		super(msj, msjOpcional);
		tipoMsj = TipoMsj.info;
	}
	
	
	public InfoSys(int msj, String msjOpcional){		
		super(msj, msjOpcional);
		tipoMsj = TipoMsj.info;
	}
	
	
	public InfoSys(int msj){
		super(msj);
		tipoMsj = TipoMsj.info;
	}
	
	
	public static void showMsj(Context c, String msj){
		new InfoSys(msj).showMsj(c);
	}
	
	
	public static void showMsj(Context c, int msj){
		new InfoSys(msj).showMsj(c);
	}
}
