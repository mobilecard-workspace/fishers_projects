package com.ironbit.mc.system.errores;

import android.content.Context;

public class AlertaSys extends ErrorSys{
	
	/*
	 * Propiedades
	 */
	
	private static final long serialVersionUID = 1L;

	/*
	 * Contructores
	 */
	
	public AlertaSys(String msj, String msjOpcional){
		super(msj, msjOpcional);
		tipoMsj = TipoMsj.alerta;
	}
	
	
	public AlertaSys(String msj) {
		super(msj);
		tipoMsj = TipoMsj.alerta;
	}
	
	
	public AlertaSys(Context c, int msj, int msjOpcional){
		super(c.getString(msj));
		tipoMsj = TipoMsj.alerta;
		
		this.setMsjOpcional(c.getString(msjOpcional));
	}
	
	
	public AlertaSys(Context c, int msj){
		super(c.getString(msj));
		tipoMsj = TipoMsj.alerta;
	}
	
	
	public AlertaSys(int msj, int msjOpcional){		
		super(msj, msjOpcional);
		tipoMsj = TipoMsj.alerta;
	}
	
	
	public AlertaSys(int msj, String msjOpcional){		
		super(msj, msjOpcional);
		tipoMsj = TipoMsj.alerta;
	}
	
	
	public AlertaSys(int msj){
		super(msj);
		tipoMsj = TipoMsj.alerta;
	}
	
	
	public static void showMsj(Context c, String msj){
		new AlertaSys(msj).showMsj(c);
	}
}
