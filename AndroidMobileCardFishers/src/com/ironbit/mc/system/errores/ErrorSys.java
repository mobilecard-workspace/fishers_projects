package com.ironbit.mc.system.errores;

import com.ironbit.mc.R;
import com.ironbit.mc.system.Sys;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.widget.Toast;

/**
 * 
 * @author angel
 * Clase que env�a mensajes de error/alerta/informacion al UI
 * 
 * @example
 * 		new ErrorSys(R.string.dlg_msj_prueba, R.string.dlg_msj_submsj_prueba)
 * 			.setBotonDialogNeutro(new DialogInterface.OnClickListener(){
 *				public void onClick(DialogInterface dialog, int which) {
 *					//presion� boton neutro
 *				}
 *   		}).setBotonDialogCancel(new DialogInterface.OnClickListener(){
 *				public void onClick(DialogInterface dialog, int which) {
 *					//presion� boton cancelar
 *				}
 *   		}).showDialog(this);
 */

public class ErrorSys extends Exception{
	/*
	 * Enums 
	 */
	
	protected enum TipoMsj{
		error(R.string.dlg_titulo_error),
		alerta(R.string.dlg_titulo_alerta),
		info(R.string.dlg_titulo_info);

		private int resTitulo;
		
		TipoMsj(int resTitulo){
			this.resTitulo = resTitulo;
		}
		
		public String getTitulo(Context c){
			return c.getString(resTitulo);
		}
	}
	
		
	public enum Duracion{
		corto,
		largo
	}
	
	
	/*
	 * Propiedades
	 */
	
	private static final long serialVersionUID = 1L;
	protected String msjOpcional = null;
	protected TipoMsj tipoMsj = TipoMsj.error; 
	protected DialogInterface.OnClickListener onClickBtnSi = null;
	protected DialogInterface.OnClickListener onClickBtnNo = null;
	protected DialogInterface.OnClickListener onClickBtnCancel = null;
	protected DialogInterface.OnClickListener onClickBtnNeutro = null;
	protected int msjRef = 0;
	protected int msjOpcionalRef = 0;
	protected boolean preparado = true;
	protected Context context = null;
	protected boolean mostrarStackTrace = false;


	public String getMsjOpcional(){
		return this.msjOpcional;
	}


	public ErrorSys setMsjOpcional(String msjOpcional){
		this.msjOpcional = msjOpcional;
		return this;
	}
	
	
	public ErrorSys mostrarStackTrace(){
		this.mostrarStackTrace = true;
		return this;
	}
	
	
	protected int duracionToToastTime(Duracion duracion){
		int d = Toast.LENGTH_SHORT;
		
		if (duracion == Duracion.largo){
			d = Toast.LENGTH_LONG;
		}
		
		return d;
	}


	/*
	 * Contructores
	 */
	
	public ErrorSys(String msj, String msjOpcional){
		super(msj);
		
		this.setMsjOpcional(msjOpcional);
	}
	
	
	public ErrorSys(String msj){
		super(msj);
	}
	
	
	public ErrorSys(Context c, int msj, int msjOpcional){
		super(c.getString(msj));
		
		this.setMsjOpcional(c.getString(msjOpcional));
	}
	
	
	public ErrorSys(Context c, int msj){
		super(c.getString(msj));
	}
	
	
	public ErrorSys(int msj, int msjOpcional){		
		super("");
		
		msjRef = msj;
		msjOpcionalRef = msjOpcional;
		preparado = false;
	}
	
	
	public ErrorSys(int msj, String msjOpcional){		
		super("");
		
		msjRef = msj;
		this.setMsjOpcional(msjOpcional);
		preparado = false;
	}
	
	
	public ErrorSys(int msj){
		super("");
		
		msjRef = msj;
		preparado = false;
	}


	/*
	 * Metodos
	 */
	
	public ErrorSys setBotonDialogSi(DialogInterface.OnClickListener onClickListenerSi){
		this.onClickBtnSi = onClickListenerSi;
		
		return this;
	}
	
	
	public ErrorSys setBotonDialogNo(DialogInterface.OnClickListener onClickListenerNo){
		this.onClickBtnNo = onClickListenerNo;
		
		return this;
	}
	
	
	public ErrorSys setBotonDialogCancel(DialogInterface.OnClickListener onClickListenerCancel){
		this.onClickBtnCancel = onClickListenerCancel;
		
		return this;
	}
	
	
	public ErrorSys setBotonDialogNeutro(DialogInterface.OnClickListener onClickListenerNeutro){
		this.onClickBtnNeutro = onClickListenerNeutro;
		
		return this;
	}
	
	
	public String getStackTraceString(){
		String stack = "";
		
		for (int a = 0; a < getStackTrace().length; a++){
			stack += getStackTrace()[a].toString()+"\n";
		}
		
		return stack;
	}
	

	public String toString(){
		String txt = null;
		
		if (preparado){
			txt = this.getMessage()+(getMsjOpcional() != null ? ("\n\n" + getMsjOpcional()) : "");
		}else{
			if (context != null){
				txt = context.getString(msjRef)+(msjOpcionalRef != 0 ? ("\n\n" + context.getString(msjOpcionalRef)) : ((this.getMsjOpcional() != null) ? "\n\n" + this.getMsjOpcional() : ""));
			}
		}
		
		if (mostrarStackTrace){
			txt += "\n\n"+getStackTraceString(); 
		}
		
		return txt;
	}
	
	
	public void showMsj(Context c, Duracion duracion){
		if (c != null){
			context = c;
			
			try{
				Toast toast = Toast.makeText(c, tipoMsj.getTitulo(c)+": "+toString(), duracionToToastTime(duracion));
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}catch(Exception ee){
				Sys.log(ee);
			}
		}
	}
	
	
	public void showMsj(Context c){
		showMsj(c, Duracion.largo);
	}
	
	
	public void showDialog(Context c){
		if (c != null){
			context = c;
			
			AlertDialog alert = new AlertDialog.Builder(c)
				.setTitle(tipoMsj.getTitulo(c))
				.setMessage(toString())
				.create();
			
			if (onClickBtnSi != null){
				alert.setButton(AlertDialog.BUTTON_POSITIVE, c.getString(R.string.btn_dialog_si), onClickBtnSi);
			}
			if (onClickBtnNo != null){
				alert.setButton(AlertDialog.BUTTON_NEGATIVE, c.getString(R.string.btn_dialog_no), onClickBtnNo);
			}
			if (onClickBtnCancel != null){
				alert.setButton(c.getString(R.string.btn_dialog_cancel), onClickBtnCancel);
			}
			if (onClickBtnNeutro != null || (onClickBtnSi == null && onClickBtnNo == null && onClickBtnCancel == null)){
				alert.setButton(AlertDialog.BUTTON_NEUTRAL, c.getString(R.string.btn_dialog_ok), onClickBtnNeutro);
			}
			
			try{
				alert.show();
			}catch(Exception e){
				Sys.log(e);
			}
		}
	}
	
	
	public static void showMsj(Context c, Exception e){
		if (c != null){
			try{
				Toast toast = Toast.makeText(c, e.toString(), Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}catch(Exception ee){
				Sys.log(ee);
			}
		}
	}
	
	
	public static void showMsj(Context c, String msj){
		new ErrorSys(msj).showMsj(c);
	}
	
	
	public static void showDialog(Context c, Exception e){
		if (c != null){
			try{
				new AlertDialog.Builder(c)
				.setTitle("Error")
				.setMessage(e.toString())
				.show();
			}catch(Exception ee){
				Sys.log(ee);
			}
		}
	}
}
