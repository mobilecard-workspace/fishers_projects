package com.ironbit.mc.system.email;

import com.ironbit.mc.R;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.Validador;
import com.ironbit.mc.system.errores.ErrorSys;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.provider.Contacts.People;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class Email {
	public static Email instancia = null;
	public static Activity ctx = null;
	private LinearLayout linLayFormInsertEmails = null;
	private EditText txtEmails = null;
	private Button btnEnviar = null;
	private AlertDialog alertDialog = null;
	protected String msjTitulo = "";
	protected String msjMensaje = "";
	public static final int REQUEST_CODE_PICK_CONTACT = 1;
	protected final int layoutId = 0; //R.layout.dialog_insertar_emails;
	protected final int editTextEmailId = 0; //R.id.txt_dialog_emails;
	protected final int buttonEnviarId = 0; //R.id.dlg_btn_enviar_email;
	
	
	public static Email getInstancia(Activity ctx){
		if (instancia == null || ctx != Email.ctx){
			instancia = new Email(ctx);
		}
		
		return instancia;
	}
	
	
	private Email(Activity ctx){
		Email.ctx = ctx;
	}
	
	
	public void enviarEmail(String[] emails, String titulo, String mensaje){
		if (emails != null && emails.length > 0){
			Intent i = new Intent(Intent.ACTION_SEND);
			//i.setType("text/plain"); //use this line for testing in the emulator
			i.setType("message/rfc822"); // use from live device
			i.putExtra(Intent.EXTRA_EMAIL, emails);
			i.putExtra(Intent.EXTRA_SUBJECT, titulo);
			i.putExtra(Intent.EXTRA_TEXT, mensaje);
			ctx.startActivity(Intent.createChooser(i, ctx.getString(R.string.lbl_selecionar_email_app)));
		}
	}
	
	
	protected AlertDialog getAlertDialog(){
		if (alertDialog == null){
			AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
			builder.setTitle(R.string.dlg_titulo_inserta_emails);
			builder.setView(getLinLayFormInsertEmails());
			alertDialog = builder.create();
		}
		
		return alertDialog;
	}
	
	
	protected LinearLayout getLinLayFormInsertEmails(){
		if (linLayFormInsertEmails == null){
			linLayFormInsertEmails = (LinearLayout)ctx.getLayoutInflater().inflate(layoutId, null);
		}
		
		return linLayFormInsertEmails;
	}
	
	
	protected EditText getTxtEmails(){
		if (txtEmails == null){
			txtEmails = (EditText)getLinLayFormInsertEmails().findViewById(editTextEmailId);
		}
		
		return txtEmails;
	}
	
	
	protected Button getBtnEnviar(){
		if (btnEnviar == null){
			btnEnviar = (Button)getLinLayFormInsertEmails().findViewById(buttonEnviarId);
		}
		
		return btnEnviar;
	}
	
	
	public void mostrarDialogInsertarEmails(final String titulo, final String mensaje){
		getBtnEnviar().setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String txt = getTxtEmails().getText().toString();
				if (Validador.estaVacio(txt)){
					new ErrorSys("Introduce los emails separados por coma").showMsj(ctx);
				}else if (txt.indexOf(",") == -1 && !Validador.esCorreo(txt)){
					new ErrorSys("Introduce un email valido").showMsj(ctx);
				}else{
					enviarEmail(parsearEmails(txt), titulo, mensaje);
					getAlertDialog().dismiss();
				}
			}
		});
		
		getAlertDialog().show();
	}
	
	
	protected String[] parsearEmails(String emails){
		String[] emailsArr = new String[0];
		
		try{
			if (emails.indexOf(",") != -1){
				String[] auxEmailsArr = emails.trim().split(",");
				String coma = "";
				String emailsOk = "";
				
				for (String email : auxEmailsArr){
					email = Text.trimAll(email);
					
					if (email != null && Validador.esCorreo(email)){
						emailsOk += coma + email;
						coma = ",";
					}
				}
				
				if (emailsOk.trim().length() > 0){
					emailsArr = emailsOk.split(",");
				}
			}else if (Validador.esCorreo(emails)){
				emailsArr = new String[]{emails};
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		return emailsArr;
	}
	
	
	public void enviarEmailAContactoDeAgenda(String titulo, String mensaje){
		ctx.startActivityForResult(
			new Intent(Intent.ACTION_PICK, People.CONTENT_URI), 
			REQUEST_CODE_PICK_CONTACT
		);
	}
	
	
	public void onContactoSeleccionado(int resultCode, Intent data){
		if (resultCode == Activity.RESULT_OK){
			Cursor c = ctx.managedQuery(data.getData(), null, null, null, null);
			int index = 0;
			String email = "";
			
			if (c.moveToFirst() && (index = c.getColumnIndex(People.IM_ACCOUNT)) != -1){
				email = c.getString(index);
				
				if (Validador.esCorreo(email)){
					enviarEmail(new String[]{email}, msjTitulo, msjMensaje);
				}else{
					new ErrorSys("El email el contacto seleccionado es invalido").showMsj(ctx);
				}
			}
		}
	}
}