package com.ironbit.mc.system;

import com.ironbit.mc.R;
import com.ironbit.mc.system.errores.ErrorSys;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;

/**
 * @author angel
 *
 */
public class Fecha implements Cloneable{
	
	/*
	 * Propiedades
	 */

	private Date fecha = null;
	private Calendar calendar = null;
	private int dia = 0;
	private int mes = 0;
	private int anio = 0;
	private int hora = 0;
	private int minuto = 0;
	private int segundo = 0;
	private String amPm = null;
	private long timeStampMili = 0;
	private long timeStampSeg = 0;
	private Idioma idioma = Idioma.espaniol;
	private int diaSemana = 1; //Lunes
	private int diaAnio = 0;
	protected Hashtable<Idioma, Hashtable<TipoArray, String[]>> nombresFechas = new Hashtable<Idioma, Hashtable<TipoArray, String[]>>();
	protected String[] diasCortosEsp = {"Lun","Mar","Mie","Jue","Vie","Sab","Dom"};
	protected String[] diasLargosEsp = {"Lunes","Martes","Mi�rcoles","Jueves","Viernes","S�bado","Domingo"};
	protected String[] diasCortosEng = {"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};
	protected String[] diasLargosEng = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
	protected String[] mesesCortosEsp = {"Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"};
	protected String[] mesesLargosEsp = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	protected String[] mesesCortosEng = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	protected String[] mesesLargosEng = {"January","February","March","April","May","June","July","August","September","October","November","December"};
	

	/**
	 * Getters & setters
	 */
	
	public Date getFecha() {
		return fecha;
	}


	public Fecha setFecha(Date fecha) {
		this.fecha = fecha;
		return this;
	}
	
	
	public Calendar getCalendar() {
		return calendar;
	}


	public Fecha setCalendar(Calendar calendar) {
		this.calendar = calendar;
		return this;
	}
	
	
	public int getDia() {
		return dia;
	}


	public Fecha setDia(int dia) {
		this.dia = dia;
		return this;
	}


	/**
	 * @return mes. Numero de mes [1 - 12]
	 */
	public int getMes() {
		return mes;
	}


	/**
	 * @param mes. Numero de mes [1 - 12]
	 * @return misma instancia
	 */
	public Fecha setMes(int mes) {
		this.mes = mes;
		return this;
	}
	
	
	/**
	 * @description
	 * funci�n util cuando se maneja con Calendar o DatePicker de android
	 * @return mes. Numero de mes [0 - 11]
	 */
	public int getMesBase0() {
		return getMes()-1;
	}


	/**
	 * @description
	 * funci�n util cuando se maneja con Calendar o DatePicker de android
	 * @param mes. Numero de mes [0 - 11]
	 * @return misma instancia
	 */
	public Fecha setMesBase0(int mes) {
		return setMes(mes+1);
	}


	public int getAnio() {
		return anio;
	}


	public Fecha setAnio(int anio) {
		this.anio = anio;
		return this;
	}


	public int getHora() {
		return hora;
	}


	public Fecha setHora(int hora) {
		this.hora = hora;
		return this;
	}


	public int getMinuto() {
		return minuto;
	}


	public Fecha setMinuto(int minuto) {
		this.minuto = minuto;
		return this;
	}


	public int getSegundo() {
		return segundo;
	}


	public Fecha setSegundo(int segundo) {
		this.segundo = segundo;
		return this;
	}


	public String getAmPm() {
		return amPm;
	}


	public Fecha setAmPm(String amPm) {
		this.amPm = amPm;
		return this;
	}


	/**
	 * @return int. Milisegundos desde el 1 de enero de 1970
	 */
	public long getTimeStampMili() {
		return timeStampMili;
	}


	public Fecha setTimeStampMili(long timeStampMili) {
		this.timeStampMili = timeStampMili;
		return this;
	}
	
	
	/**
	 * @return int. Segundos desde el 1 de enero de 1970
	 */
	public long getTimeStampSeg() {
		return timeStampSeg;
	}


	public Fecha setTimeStampSeg(long timeStampSeg) {
		this.timeStampSeg = timeStampSeg;
		return this;
	}


	public Idioma getIdioma() {
		return idioma;
	}


	public Fecha setIdioma(Idioma idioma) {
		this.idioma = idioma;
		return this;
	}


	/**
	 * @return dia de la semana del 1 al 7 donde 1 = domingo, 2 = lunes, etc.
	 */
	public int getDiaSemana() {
		return diaSemana;
	}


	public Fecha setDiaSemana(int diaSemana) {
		this.diaSemana = diaSemana;
		return this;
	}


	public int getDiaAnio() {
		return diaAnio;
	}


	public Fecha setDiaAnio(int diaAnio) {
		this.diaAnio = diaAnio;
		return this;
	}


	
	/**
	 * Enums
	 */
	
	protected enum TipoArray{
		diasCortos,
		diasLargos,
		mesesCortos,
		mesesLargos
	}
	
	
	public enum TipoFecha{
		/**
		 * @var YYYY-MM-DDTHH:MM:SS  |  YY-MM-DD HH:MM:SS  |  YYYYMMDDHHMMSS  |  YYMMDDHHMMSS
		 */
		dateTime,
		/**
		 * @var YYYY-MM-DD  |  YY-MM-DD  |  YYYYMMDD  |  YYMMDD
		 */
		date,
		/**
		 * @var Milisegundos desde el 01-01-1970
		 */
		timeStamp,
		/**
		 * @var Fecha y Hora actuales
		 */
		now,
		/**
		 * @var  HH:MM:SS  |  HHMMSS  |  HH:MM  |  MMSS  |  SS
		 */
		time,
		/**
		 * @var YYYY  |  YY
		 */
		year
	}
	
	
	/**
	 * Inner Classes
	 */
	
	public class FormatoFecha{
		/**
		 * FormatoFecha.diaLargo+", "+FormatoFecha.diaNumero+" de "+FormatoFecha.mesLargo+
			 *		" del "+FormatoFecha.anioCorto+". "+FormatoFecha.hora12+":"+FormatoFechaminuto+
			 *		":"+FormatoFecha.segundo+" "+FormatoFechaamPmMayusculas
		 */
		public static final String diaCorto = 		"ffDC"; //Lun
		public static final String diaLargo = 		"ffDL"; //Lunes
		public static final String diaNumero = 		"ffDN"; //02
		public static final String mesCorto = 		"ffMC"; //Mar
		public static final String mesLargo = 		"ffML"; //Marzo
		public static final String mesNumero = 		"ffMN"; //03
		public static final String anioCorto = 		"ffAC"; //09
		public static final String anioLargo = 		"ffAL"; //2009
		public static final String hora = 			"ffHH"; //14
		public static final String hora12 = 		"ff12"; //02
		public static final String minuto = 		"ffMM"; //28
		public static final String segundo = 		"ffSS"; //13
		public static final String amPmMinusculas = "ffPm"; //pm
		public static final String amPmMayusculas = "ffPM"; //AM
		/*Formatos predefinidos*/
		public static final String fechaEsp_DDDDD_DD_de_MMMMM_de_YYYY = "ffDL ffDN de ffML de ffAL";
		public static final String fechaEsp_DD_de_MMMMM_de_YYYY = 		"ffDN de ffML de ffAL";
		public static final String fechaEsp_DD_de_MMMMM_de_YYYY_HH_mm_am = "ffDN de ffML de ffAL ff12:ffMM ffPm";
		public static final String fechaEsp_DD_de_MMM_de_YYYY = 		"ffDN de ffMC de ffAL";
		public static final String fechaEsp_DDguionMMMguionYYYY = 		"ffDN/ffMC/ffAL";
		public static final String fechaEsp_DD_de_MMMMM = 				"ffDN de ffML";
		public static final String fechaEsp_MMMMM_del_YY = 				"ffML del ffAC";
		public static final String fechaEsp_DDguionMMguionYYYY = 		"ffDN/ffMN/ffAL";
		public static final String fechaEsp_DDguionMMguionYY = 			"ffDN/ffMN/ffAC";
		public static final String fechaEsp_YYYYdashMMdashDD = 		"ffAL-ffMN-ffDN";
		public static final String horaEsp_HH_MM_am = 					"ff12:ffMM ffPm";
		
		public static final String fechaEng_DDDDD_MMMMM_DD_YYYY = 		"ffDL, ffML ffDN, ffAL";
		public static final String fechaEng_MMMMM_DD_YYYY = 			"ffML ffDN, ffAL";
		public static final String fechaEng_MMM_DD_YYYY = 				"ffMC ffDN, ffAL";
		public static final String fechaEng_MMguionDDguionYY = 			"ffMN/ffDN/ffAC";
		public static final String horaEng_HH_MM_AM = 					"ff12:ffMM ffPM";
	}


	/*
	 * Contructor
	 */
	
	public Fecha() throws ErrorSys{
		this(new Date().getTime());
	}
	
	
	
	/**
	 * @param tiempo. Milisegundos desde el Unix Epoch
	 * @throws ErrorSys
	 */
	public Fecha(long tiempoMilisegundos) throws ErrorSys{
		inicializarNombresFechas();
		
		actualizarDatosFecha(tiempoMilisegundos);

		if (!this.esValida()){
			throw new ErrorSys(R.string.err_fechaNoValida);
		}
	}
	
	
	public Fecha(String fecha, TipoFecha tipo) throws ErrorSys{
		inicializarNombresFechas();
		
		switch (tipo){
			case now:
				actualizarDatosFecha(new Date().getTime());
				break;
			case dateTime:
				if (fecha.length() < 20){
					if (fecha.length() == 19){
						//YYYY-MM-DDTHH:MM:SS
						actualizarDatosFecha(mkTime(fecha.substring(11, 13),fecha.substring(14, 16),fecha.substring(17, 19),fecha.substring(5, 7),fecha.substring(8, 10),fecha.substring(0, 4)));
					}else if (fecha.length() == 17){
						//YY-MM-DD HH:MM:SS
						actualizarDatosFecha(mkTime(fecha.substring(9, 11),fecha.substring(12, 14),fecha.substring(15, 17),fecha.substring(3, 5),fecha.substring(6, 8),fecha.substring(0, 2)));
					}else if (fecha.length() == 14){
						//YYYYMMDDHHMMSS
						actualizarDatosFecha(mkTime(fecha.substring(8, 10),fecha.substring(10, 12),fecha.substring(12, 14),fecha.substring(4, 6),fecha.substring(6, 8),fecha.substring(0, 4)));
					}else if (fecha.length() == 12){
						//YYMMDDHHMMSS
						actualizarDatosFecha(mkTime(fecha.substring(6, 8),fecha.substring(8, 10),fecha.substring(10, 12),fecha.substring(2, 4),fecha.substring(4, 6),fecha.substring(0, 2)));
					}else{
						throw new ErrorSys(R.string.err_fechaDateTimeInvalida, "Fecha: "+fecha);
					}
				}else{
					throw new ErrorSys(R.string.err_fechaDateTimeInvalida, "Fecha: "+fecha);
				}
	
				break;
			case date:
				if (fecha.length() < 11){
					if (fecha.length() == 10){
						//YYYY-MM-DD
						actualizarDatosFecha(mkTime(0,0,0,Integer.parseInt(fecha.substring(5, 7)),Integer.parseInt(fecha.substring(8, 10)),Integer.parseInt(fecha.substring(0, 4))));
					}else if (fecha.length() == 8 && !Validador.esNumerico(fecha.substring(2, 3))){
						//YY-MM-DD
						actualizarDatosFecha(mkTime(0,0,0,Integer.parseInt(fecha.substring(3, 5)),Integer.parseInt(fecha.substring(6, 8)),Integer.parseInt(fecha.substring(0, 2))));
					}else if (fecha.length() == 8 && Validador.esNumerico(fecha.substring(2, 3))){
						//YYYYMMDD
						actualizarDatosFecha(mkTime(0,0,0,Integer.parseInt(fecha.substring(4, 6)),Integer.parseInt(fecha.substring(6, 8)),Integer.parseInt(fecha.substring(0, 4))));
					}else if (fecha.length() == 6){
						//YYMMDD
						actualizarDatosFecha(mkTime(0,0,0,Integer.parseInt(fecha.substring(2, 4)),Integer.parseInt(fecha.substring(4, 6)),Integer.parseInt(fecha.substring(0, 2))));
					}else{
						throw new ErrorSys(R.string.err_fechaDateInvalida, "Fecha: "+fecha);
					}
				}else{
					throw new ErrorSys(R.string.err_fechaDateInvalida, "Fecha: "+fecha);
				}
	
				break;
			case timeStamp:
				if (Validador.esNumerico(fecha) && Long.parseLong(fecha) > -1){
					actualizarDatosFecha(Long.parseLong(fecha));
				}else{
					throw new ErrorSys(R.string.err_timeStampInvalido, "Fecha: "+fecha);
				}
	
				break;
			case time:
				if (fecha.length() < 9){
					if (fecha.length() == 8){
						//HH:MM:SS
						actualizarDatosFecha(mkTime(fecha.substring(0, 2),fecha.substring(3, 5),fecha.substring(6, 8)));
					}else if (fecha.length() == 6){
						//HHMMSS
						actualizarDatosFecha(mkTime(fecha.substring(0, 2),fecha.substring(2, 4),fecha.substring(4, 6)));
					}else if (fecha.length() == 5){
						//HH:MM
						actualizarDatosFecha(mkTime(Integer.parseInt(fecha.substring(0, 2)),Integer.parseInt(fecha.substring(3, 5)), 0));
					}else if (fecha.length() == 4){
						//MMSS
						actualizarDatosFecha(mkTime(0,Integer.parseInt(fecha.substring(0, 2)),Integer.parseInt(fecha.substring(2, 4))));
					}else if (fecha.length() == 2){
						//SS
						actualizarDatosFecha(mkTime(0,0,Integer.parseInt(fecha)));
					}else{
						throw new ErrorSys(R.string.err_fechaTimeInvalida, "Fecha: "+fecha);
					}
				}else{
					throw new ErrorSys(R.string.err_fechaTimeInvalida, "Fecha: "+fecha);
				}
	
				break;
			case year:
				if (fecha.length() < 5){
					actualizarDatosFecha(mkTime(0,0,0,0,0,Integer.parseInt(fecha)));
				}else{
					throw new ErrorSys(R.string.err_fechaYearInvalida, "Fecha: "+fecha);
				}
	
				break;
		}
		

		if (!esValida()){
			throw new ErrorSys(R.string.err_fechaNoValida);
		}
	}
	
	
	/*
	 * M�todos
	 */
	
	protected void inicializarNombresFechas(){
		Hashtable<TipoArray, String[]> arrDatos;
		
		arrDatos = new Hashtable<TipoArray, String[]>();
		arrDatos.put(TipoArray.diasCortos, diasCortosEsp);
		arrDatos.put(TipoArray.diasLargos, diasLargosEsp);
		arrDatos.put(TipoArray.mesesCortos, mesesCortosEsp);
		arrDatos.put(TipoArray.mesesLargos, mesesLargosEsp);
		
		nombresFechas.put(Idioma.espaniol, arrDatos);
		
		arrDatos = new Hashtable<TipoArray, String[]>();
		arrDatos.put(TipoArray.diasCortos, diasCortosEng);
		arrDatos.put(TipoArray.diasLargos, diasLargosEng);
		arrDatos.put(TipoArray.mesesCortos, mesesCortosEng);
		arrDatos.put(TipoArray.mesesLargos, mesesLargosEng);
		
		nombresFechas.put(Idioma.ingles, arrDatos);
		arrDatos = null;
	}
	
	
	/**
	 * @description
	 * Actualiza los datos de la clase despues de haber utilizado algun metodo set.
	 * @return Misma instancia
	 * @example
	 * Fecha f = new Fecha();
	 * f.setDia(13)
	 * .setMes(2)
	 * .actualizar();
	 * @throws ErrorSys
	 */
	public Fecha actualizar() throws ErrorSys{
		actualizarDatosFecha(mkTime(getHora(), getMinuto(), getSegundo(), getMes(), getDia(), getAnio()));
		return this;
	}
	

	public Fecha actualizarDatosFecha(long timeStamp)throws ErrorSys{
		try{
			setFecha(new Date(timeStamp));
			setCalendar(Calendar.getInstance());
			getCalendar().setTime(getFecha());

			setDia(getCalendar().get(Calendar.DAY_OF_MONTH));
			setMes(getCalendar().get(Calendar.MONTH)+1);
			setAnio(getCalendar().get(Calendar.YEAR));
			setHora(getCalendar().get(Calendar.HOUR_OF_DAY));
			setMinuto(getCalendar().get(Calendar.MINUTE));
			setSegundo(getCalendar().get(Calendar.SECOND));
			setDiaSemana(getCalendar().get(Calendar.DAY_OF_WEEK));
			setDiaAnio(getCalendar().get(Calendar.DAY_OF_YEAR));
			setTimeStampMili(timeStamp);
			setTimeStampSeg(timeStamp / 1000);
			setAmPm(((getHora() < 12) ? "am" : "pm"));
		}catch(Exception e){
			throw new ErrorSys(R.string.err_errorEnFecha, e.getMessage());
		}
		
		return this;
	}
	
	
	/**
	 * Regresa los segundos transcurridos desde el "Unix Epoch" (1 del 1 de 1970 a las 00:00:00)
	 * @return
	 */
	public static long mkTimeSeg(int hora, int minuto, int segundo, int mes, int dia, int anio){
		return mkTime(hora, minuto, segundo, mes, dia, anio)/1000;
	}
	
	
	public static long mkTimeSeg(String hora, String minuto, String segundo, String mes, String dia, String anio){
		return mkTime(hora, minuto, segundo, mes, dia, anio)/1000;
	}
	
	
	/**
	 * @param mes. numero del mes [1 - 12]
	 * Regresa los milisegundos transcurridos desde el "Unix Epoch" (1 del 1 de 1970 a las 00:00:00)
	 * @return
	 */
	public static long mkTime(int hora, int minuto, int segundo, int mes, int dia, int anio){
		Calendar cal = Calendar.getInstance();
		cal.set(anio, mes-1, dia, hora, minuto, segundo);
		
		return cal.getTime().getTime();
	}
	
	
	/**
	 * Regresa los milisegundos transcurridos desde el "Unix Epoch" (1 del 1 de 1970 a las 00:00:00)
	 * @return
	 */
	public static long mkTime(String hora, String minuto, String segundo, String mes, String dia, String anio){
		return mkTime(Integer.parseInt(hora), Integer.parseInt(minuto), Integer.parseInt(segundo), Integer.parseInt(mes), Integer.parseInt(dia), Integer.parseInt(anio));
	}
	
	
	public static long mkTime(int hora, int minuto, int segundo){
		Calendar cal = Calendar.getInstance();
		
		return mkTime(hora, minuto, segundo, cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR));
	}
	
	
	public static long mkTime(String hora, String minuto, String segundo){
		Calendar cal = Calendar.getInstance();
	
		return mkTime(Integer.parseInt(hora), Integer.parseInt(minuto), Integer.parseInt(segundo), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR));
	}
	
	
	public static long mkTime(int hora, int minuto){
		Calendar cal = Calendar.getInstance();
		
		return mkTime(hora, minuto, cal.get(Calendar.SECOND), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR));
	}
	
	
	public static long mkTime(String hora, String minuto){
		Calendar cal = Calendar.getInstance();
		
		return mkTime(Integer.parseInt(hora), Integer.parseInt(minuto), cal.get(Calendar.SECOND), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR));
	}
	

	/**
	 * Verifica si la fecha es correcta
	 * @return boolean. true si es valida, de lo contrario false.
	 * @example
	 *  04/31/2005 -> false
	 *  02/29/2004 -> true
	 */
	public boolean esValida(){
		String fecha = Text.dosDigitos(getDia())+"/"+
			Text.dosDigitos(getMes())+"/"+
			getAnio()+" "+
			Text.dosDigitos(getHora())+":"+
			Text.dosDigitos(getMinuto());
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date testDate = null;

		try{
			testDate = sdf.parse(fecha);
		}catch (Exception e){
			return false; 
		}

		if (!sdf.format(testDate).equals(fecha)) {
			return false;
		}

		return true;
	}
	
	
	public static boolean esValida(int dia, int mes, int anio){
		String fecha = Text.dosDigitos(dia)+"/"+Text.dosDigitos(mes)+"/"+anio;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date testDate = null;
	
		try{
			testDate = sdf.parse(fecha);
		}catch (Exception e){
			return false; 
		}
	
		if (!sdf.format(testDate).equals(fecha)) {
			return false;
		}
	
		return true;
	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * @example
	 * 	23/08/2009 06:34 pm
	 */
	public String toString(){
		return toFechaFormateada(
				FormatoFecha.diaNumero 	+ "/" +
				FormatoFecha.mesNumero 	+ "/" +
				FormatoFecha.anioLargo 	+ " " +
				FormatoFecha.hora12 	+ ":" +
				FormatoFecha.minuto 	+ " " +
				FormatoFecha.amPmMinusculas
		);
	}


	/**
	 * Regresa la fecha formateada como DateTime para sql
	 * @return YYYY-MM-DDTHH:MM:SS
	 */
	public String toSqlDateTime(){
		return getAnio()+"-"+Text.dosDigitos(getMes())+"-"+Text.dosDigitos(getDia())+"T"+Text.dosDigitos(getHora())+":"+Text.dosDigitos(getMinuto())+":"+Text.dosDigitos(getSegundo());
	}


	/**
	 * Regresa la fecha formateada como TimeStamp para sql
	 * @return YYYY-MM-DDTHH:MM:SS
	 */
	public String toSqlTimeStamp(){
		return toSqlDateTime();
	}


	/**
	 * Regresa la fecha formateada como Date para sql
	 * @return YYYY-MM-DD
	 */
	public String toSqlDate(){
		return getAnio()+"-"+Text.dosDigitos(getMes())+"-"+Text.dosDigitos(getDia());
	}


	/**
	 * Regresa la fecha formateada como Time para sql
	 * @return HH:MM:SS
	 */
	public String toSqlTime(){
		return Text.dosDigitos(getHora())+":"+Text.dosDigitos(getMinuto())+":"+Text.dosDigitos(getSegundo());
	}


	/**
	 *Regresa la fecha formateada con las especificaciones enviadas
	 *Ejemplo:
	 *	toFechaFormateada(
	 *		FormatoFecha.diaLargo+", "+FormatoFecha.diaNumero+" de "+FormatoFecha.mesLargo+
	 *		" del "+FormatoFecha.anioCorto+". "+FormatoFecha.hora12+":"+FormatoFechaminuto+
	 *		":"+FormatoFecha.segundo+" "+FormatoFechaamPmMayusculas
	 *	)
	 *	//Regresa: Lunes 06 de Julio del 09. 02:12:00 PM
	 */
	public String toFechaFormateada(String formato){
		String[] patrones = {"ffDC","ffDL","ffDN","ffMC","ffML","ffMN","ffAC","ffAL","ffHH","ff12","ffMM","ffSS","ffPm","ffPM"};
		
		String[] reemplazo = {
			getFormatoDiaCorto(),
			getFormatoDiaLargo(),
			Text.dosDigitos(getDia()),
			getFormatoMesCorto(),
			getFormatoMesLargo(),
			Text.dosDigitos(getMes()),
			String.valueOf(getAnio()).substring(2, 4),
			String.valueOf(getAnio()),
			Text.dosDigitos(getHora()),
			Text.dosDigitos(toHora12(getHora())),
			Text.dosDigitos(getMinuto()),
			Text.dosDigitos(getSegundo()),
			getAmPm(),
			getAmPm().toUpperCase()
		};

		return Text.replace(patrones, reemplazo, formato);
	}


	public String getFormatoDiaCorto(){
		return nombresFechas.get(idioma).get(TipoArray.diasCortos)[getDiaSemana()-1];
	}


	public String getFormatoDiaLargo(){
		return nombresFechas.get(idioma).get(TipoArray.diasLargos)[getDiaSemana()-1];
	}


	public String getFormatoMesCorto(){
		return nombresFechas.get(idioma).get(TipoArray.mesesCortos)[getMes()-1];
	}


	public String getFormatoMesLargo(){
		return nombresFechas.get(idioma).get(TipoArray.mesesLargos)[getMes()-1];
	}
	

	public Fecha agregarDias(int numeroDias, boolean sobreescribir) throws ErrorSys{
		return agregar(Calendar.DAY_OF_MONTH, numeroDias, sobreescribir);
	}
	
	
	/**
	 * Esta funcion agrega dias y lo reemplaza en el objeto actual
	 * @param numeroDias
	 * @return referencia a la misma clase
	 */
	public Fecha agregarDias(int numeroDias) throws ErrorSys{
		return agregarDias(numeroDias, true);
	}
	
	
	public Fecha restarDias(int numeroDias, boolean sobreescribir) throws ErrorSys{
		return restar(Calendar.DAY_OF_MONTH, numeroDias, sobreescribir);
	}
	
	
	/**
	 * Esta funcion resta dias y lo reemplaza en el objeto actual
	 * @param numeroDias
	 * @return referencia a la misma clase
	 */
	public Fecha restarDias(int numeroDias) throws ErrorSys{
		return restarDias(numeroDias, true);
	}
	
	
	public Fecha agregarMeses(int numeroMeses, boolean sobreescribir) throws ErrorSys{
		return agregar(Calendar.MONTH, numeroMeses, sobreescribir);
	}
	
	
	/**
	 * Esta funcion agrega meses y lo reemplaza en el objeto actual
	 * @param numeroMeses
	 * @return referencia a la misma clase
	 */
	public Fecha agregarMeses(int numeroMeses) throws ErrorSys{
		return agregarMeses(numeroMeses, true);
	}
	
	
	public Fecha restarMeses(int numeroMeses, boolean sobreescribir) throws ErrorSys{
		return restar(Calendar.MONTH, numeroMeses, sobreescribir);
	}
	
	
	/**
	 * Esta funcion resta meses y lo reemplaza en el objeto actual
	 * @param numeroMeses
	 * @return referencia a la misma clase
	 */
	public Fecha restarMeses(int numeroMeses) throws ErrorSys{
		return restarMeses(numeroMeses, true);
	}

	
	public Fecha agregarAnios(int numeroAnios, boolean sobreescribir) throws ErrorSys{
		return agregar(Calendar.YEAR, numeroAnios, sobreescribir);
	}
	
	
	/**
	 * Esta funcion agrega a�os y lo reemplaza en el objeto actual
	 * @param numeroAnios
	 * @return referencia a la misma clase
	 */
	public Fecha agregarAnios(int numeroAnios) throws ErrorSys{
		return agregarAnios(numeroAnios, true);
	}
	
	
	public Fecha restarAnios(int numeroAnios, boolean sobreescribir) throws ErrorSys{
		return restar(Calendar.YEAR, numeroAnios, sobreescribir);
	}
	
	
	/**
	 * Esta funcion resta a�os y lo reemplaza en el objeto actual
	 * @param numeroAnios
	 * @return referencia a la misma clase
	 */
	public Fecha restarAnios(int numeroAnios) throws ErrorSys{
		return restarAnios(numeroAnios, true);
	}
	
	
	public Fecha agregarHoras(int numeroHoras, boolean sobreescribir) throws ErrorSys{
		return agregar(Calendar.HOUR, numeroHoras, sobreescribir);
	}
	
	
	/**
	 * Esta funcion agrega horas y lo reemplaza en el objeto actual
	 * @param numeroHoras
	 * @return referencia a la misma clase
	 */
	public Fecha agregarHoras(int numeroHoras) throws ErrorSys{
		return agregarHoras(numeroHoras, true);
	}
	
	
	public Fecha restarHoras(int numeroHoras, boolean sobreescribir) throws ErrorSys{
		return restar(Calendar.HOUR, numeroHoras, sobreescribir);
	}
	
	
	/**
	 * Esta funcion resta horas y lo reemplaza en el objeto actual
	 * @param numeroHoras
	 * @return referencia a la misma clase
	 */
	public Fecha restarHoras(int numeroHoras) throws ErrorSys{
		return restarHoras(numeroHoras, true);
	}
	
	
	public Fecha agregarMinutos(int numeroMinutos, boolean sobreescribir) throws ErrorSys{
		return agregar(Calendar.MINUTE, numeroMinutos, sobreescribir);
	}
	
	
	/**
	 * Esta funcion agrega minutos y lo reemplaza en el objeto actual
	 * @param numeroMinutos
	 * @return referencia a la misma clase
	 */
	public Fecha agregarMinutos(int numeroMinutos) throws ErrorSys{
		return agregarMinutos(numeroMinutos, true);
	}
	
	
	public Fecha restarMinutos(int numeroMinutos, boolean sobreescribir) throws ErrorSys{
		return restar(Calendar.MINUTE, numeroMinutos, sobreescribir);
	}
	
	
	/**
	 * Esta funcion resta minutos y lo reemplaza en el objeto actual
	 * @param numeroMinutos
	 * @return referencia a la misma clase
	 */
	public Fecha restarMinutos(int numeroMinutos) throws ErrorSys{
		return restarMinutos(numeroMinutos, true);
	}


	public Fecha agregarSegundos(int numeroSegundos, boolean sobreescribir) throws ErrorSys{
		return agregar(Calendar.SECOND, numeroSegundos, sobreescribir);
	}
	
	
	/**
	 * Esta funcion agrega segundos y lo reemplaza en el objeto actual
	 * @param numeroSegundos
	 * @return referencia a la misma clase
	 */
	public Fecha agregarSegundos(int numeroSegundos) throws ErrorSys{
		return agregarSegundos(numeroSegundos, true);
	}
	
	
	public Fecha restarSegundos(int numeroSegundos, boolean sobreescribir) throws ErrorSys{
		return restar(Calendar.SECOND, numeroSegundos, sobreescribir);
	}
	
	
	/**
	 * Esta funcion resta segundos y lo reemplaza en el objeto actual
	 * @param numeroSegundos
	 * @return referencia a la misma clase
	 */
	public Fecha restarSegundos(int numeroSegundos) throws ErrorSys{
		return restarSegundos(numeroSegundos, true);
	}
	
	
	public Fecha restar(int campo, int valor, boolean sobreescribir) throws ErrorSys{
		return agregar(campo, -1 * valor, sobreescribir);
	}
	
	
	public Fecha agregar(int campo, int valor, boolean sobreescribir) throws ErrorSys{
		Fecha fechaTemp;
		
		if (sobreescribir){
			fechaTemp = this;
		}else{
			fechaTemp = (Fecha)this.clone();
		}
		
		fechaTemp.getCalendar().add(campo, valor);
		fechaTemp.actualizarDatosFecha(fechaTemp.getCalendar().getTimeInMillis());
		
		return fechaTemp;
	}


	public static String fechaToFechaHora(String fecha) throws ErrorSys{
		if (fecha.length() == 10 || (fecha.length() == 8 && !Validador.esNumerico(fecha.substring(2, 3)))){
			//YYYY-MM-DD
			fecha += "T00:00:00";
		}else if ((fecha.length() == 8 && Validador.esNumerico(fecha.substring(2, 3))) || fecha.length() == 6){
			//YYYYMMDD
			fecha += "000000";
		}else{
			throw new ErrorSys(R.string.err_fechaDateInvalida, "Fecha: "+fecha);
		}

		return fecha;
	}


	public int toHora12(int hora){
		if (this.getAmPm().equals("pm") && hora != 12){
			hora -= 12;
		}else if (this.getAmPm().equals("am") && hora == 0){
			hora = 12;
		}

		return hora;
	}
	
	
	public Object clone() {
		Fecha f = null;
		
		try{
			f = new Fecha(this.getTimeStampMili());
		}catch(Exception e){
			try{
				f = new Fecha();
			}catch(Exception ee){
				Sys.log(ee);
			}
		}
		
		return f;
	}
	
	
	public static Fecha fechaFromFormat(String fecha, String formato){
		Fecha newFecha = null;
		
		try {
			SimpleDateFormat format = new SimpleDateFormat(formato, Locale.US);
            newFecha = new Fecha(format.parse(fecha).getTime());
        }catch(Exception e) {
        	Sys.log(e);
        	
        	try{
        		newFecha = new Fecha();
        	}catch(Exception ee){
        		Sys.log(ee);
        	}
        }
		
		return newFecha;
	}
	
	
	public static int mesStringToInt(String mes){
		int mesInt = 1;
		
		if (mes != null){
			if (mes.equals("Febrero")){
				mesInt = 2;
			}else if (mes.equals("Marzo")){
				mesInt = 3;
			}else if (mes.equals("Abril")){
				mesInt = 4;
			}else if (mes.equals("Mayo")){
				mesInt = 5;
			}else if (mes.equals("Junio")){
				mesInt = 6;
			}else if (mes.equals("Julio")){
				mesInt = 7;
			}else if (mes.equals("Agosto")){
				mesInt = 8;
			}else if (mes.equals("Septiembre")){
				mesInt = 9;
			}else if (mes.equals("Octubre")){
				mesInt = 10;
			}else if (mes.equals("Noviembre")){
				mesInt = 11;
			}else if (mes.equals("Diciembre")){
				mesInt = 12;
			}
		}
		
		return mesInt;
	}
}
