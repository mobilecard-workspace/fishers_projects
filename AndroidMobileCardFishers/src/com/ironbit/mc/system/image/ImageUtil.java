package com.ironbit.mc.system.image;


import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.ironbit.mc.system.Sys;

public class ImageUtil {
	public static Bitmap descargarImagen(String urlImagen){
		Bitmap img = null;
		
		try {
			HttpGet httpRequest = new HttpGet(urlImagen);
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = (HttpResponse) httpclient.execute(httpRequest);

            HttpEntity entity = response.getEntity();
            BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
            InputStream instream = bufHttpEntity.getContent();
            img = BitmapFactory.decodeStream(instream);
		} catch (Exception e) {
			Sys.log(e);
		}

		return img;
	}
	
	
	/**
	 * @note METODO NO PROBADO <Suceptible a errores>
	 */
	public static String guardarImagen(Activity ctx, Bitmap imagen, String nombreImg, Bitmap.CompressFormat tipo){
		try{
			OutputStream fOut = null;
	        File file = new File(Environment.getDataDirectory().toString(), nombreImg + tipo.name());
	        fOut = new FileOutputStream(file);
	
	        imagen.compress(tipo, 90, fOut);
	        fOut.flush();
	        fOut.close();
	
	        return MediaStore.Images.Media.insertImage(ctx.getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
		}catch(Exception e){
			Sys.log(e);
		}
		
		return "";
	}
	
	
	public static String guardarImagen(Activity ctx, Bitmap imagen, String nombreImg){
		return guardarImagen(ctx, imagen, nombreImg, Bitmap.CompressFormat.PNG);
	}
	
	
	/**
	 * @note METODO NO PROBADO <Suceptible a errores>
	 */
	public static Bitmap obtenerImagen(Activity ctx, String imgName){
		Bitmap img = null;
		
		try{
			img = MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), Uri.parse(imgName));
		}catch(Exception e){
			Sys.log(e);
		}
		
		return img;
	}
	
	
	public static Bitmap resizeImageToHeight(Bitmap bitmapOrg, int newHeight){
		return resizeImage(bitmapOrg, newHeight * bitmapOrg.getWidth() / bitmapOrg.getHeight(), newHeight);
	}
	
	
	public static Bitmap resizeImageToWidth(Bitmap bitmapOrg, int newWidth){
		return resizeImage(bitmapOrg, newWidth, newWidth * bitmapOrg.getHeight() / bitmapOrg.getWidth());
	}
	
	
	public static Bitmap resizeImage(Bitmap bitmapOrg, int newWidth, int newHeight){
	    int width = bitmapOrg.getWidth();
        int height = bitmapOrg.getHeight();
        
        // calculate the scale - in this case = 0.4f
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
       
        // createa matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
 
        // recreate the new Bitmap
        return Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
	}
}