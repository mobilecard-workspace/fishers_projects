package com.ironbit.mc.system;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;

import com.ironbit.mc.activities.ConfigActivity;
import com.ironbit.mc.util.LogUtils;

public class Sys {
	public static final boolean DEPURANDO = false;
	private static final boolean LOG_TO_FILE = false;
	public static String IMEI = null;
	private static final String KEY = "1234567890ABCDEF0123456789ABCDEF";
	private static String telefono = "NULO";
	
	public static String getKey(){
		return KEY;
	}
	
	public static void setPTelefono(String tel){
		telefono = tel;
	}
	
	public static String getTelefono(){
		return telefono;
	}
	
	/**
     * @param ctx Contexto
     * @param nombre. nombre de la configuración a obtener
     * @param defaultVal.valor a regresar si la configuración no se encontró
     * @return Valor de la configuración
     */
    public static String getAppConf(Context ctx, String nombre, String defaultVal){
    	return ConfigActivity.getAppConf(ctx, nombre, defaultVal);
    }
    
    
    public static String getAppConf(Context ctx, String nombre){
    	return ConfigActivity.getAppConf(ctx, nombre);
    }
    
    
    /**
     * @description
     * guarda una configuración de toda la aplicación
     */
    public static void setAppConf(Context ctx, String nombre, String valor){
    	ConfigActivity.setAppConf(ctx, nombre, valor);
    }
    
    
    public static class Dispositivo{
    	
    	/**
    	 * Orientación del dispositivo
    	 */
    	public enum Orientacion{
    		portrait,
    		landscape,
    		cuadrado
    	}
    	
    	
    	public enum ScreenSize{
    		otro,
    		_480x854,
    		_480x800,
    		_320x480
    	}
    	
    	
    	public static Orientacion getOrientacion(Activity ctx){
    		Orientacion or = Orientacion.cuadrado;
    		Display display = ctx.getWindowManager().getDefaultDisplay();

			Configuration config = ctx.getResources().getConfiguration();
			int orientation = config.orientation;
			
			if (orientation == Configuration.ORIENTATION_UNDEFINED) {
				// if height and widht of screen are equal then
				// it is square orientation
				if (display.getWidth() == display.getHeight()) {
					or = Orientacion.cuadrado;
				} else { // if widht is less than height than it is portrait
					if (display.getWidth() < display.getHeight()) {
						or = Orientacion.portrait;
					} else { // if it is not any of the above it will // defineitly be landscape
						or = Orientacion.landscape;
					}
				}
			}else if (orientation == Configuration.ORIENTATION_LANDSCAPE){
				or = Orientacion.landscape;
			}else if (orientation == Configuration.ORIENTATION_PORTRAIT){
				or = Orientacion.portrait;
			}
			
			return or;
    	}
    	
    	
    	public static ScreenSize getScreenSize(Activity ctx){
    		ScreenSize scr = ScreenSize.otro;
    		
    		Display display = ctx.getWindowManager().getDefaultDisplay(); 
    		int width = display.getWidth();
    		int height = display.getHeight();
    		
    		if ((width == 480 && height == 854) || (width == 854 && height == 480)){
    			scr = ScreenSize._480x854;
    		}else if ((width == 480 && height == 800) || (width == 800 && height == 480)){
    			scr = ScreenSize._480x800;
    		}else if ((width == 320 && height == 480) || (width == 480 && height == 320)){
    			scr = ScreenSize._320x480;
    		}
    		
    		return scr;
    	}
    	
    	
    	public static int getWidthPortrait(Activity ctx){
    		Display display = ctx.getWindowManager().getDefaultDisplay();
    		return Math.min(display.getWidth(), display.getHeight());
    	}
    	
    	
    	public static int getHeightPortrait(Activity ctx){
    		Display display = ctx.getWindowManager().getDefaultDisplay();
    		return Math.max(display.getWidth(), display.getHeight());
    	}
    	
    	
    	public static int dipToPx(Context ctx, float dip){
    		return (int) (dip * ctx.getResources().getDisplayMetrics().density + 0.5f);    		
    	}
    }
    
    
	public static void log(String texto){
		Sys.log("INFO", texto);
	}
	
	
	public static void log(String tag, String texto){
		if (DEPURANDO){
			Log.i(tag, texto);
			
			if (LOG_TO_FILE){
				LogUtils.logToFile("<" + tag + ">:\t\t" + texto);
			}
		}
	}
	
	
	public static void log(Exception e){
		if (DEPURANDO){
			e.printStackTrace();
			
			if (LOG_TO_FILE){
				LogUtils.logToFile(e);
			}
		}
	}
	
	public static String getIMEI(Context ctx){
		if (IMEI == null){
			String imei = null;
			
			try{
				imei = ((TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
				
				if (imei == null){
					imei = Settings.System.getString(ctx.getContentResolver(), Settings.System.ANDROID_ID);
				}
			}catch(Exception e){
				imei = Settings.System.getString(ctx.getContentResolver(), Settings.System.ANDROID_ID);
				Sys.log(e);
			}
			
			IMEI = imei;
		}
		
		//Sys.log("IMEI: " + IMEI);
		//IMEI = "105692483920141";
		return IMEI;
	}
	
	public static String getSWVersion(){
		return Build.VERSION.RELEASE;
	}
	
	public static String getModel(){
		return Build.MODEL;
	}
	
	public static String getTipo(){
		return Build.MANUFACTURER;
	}
	
	
}