package com.ironbit.mc.system;

public class Convertidor {
	public static String segToSegMinHrs(long numeroSegundos){  	
		String seMinHrs = "";
		
		if (numeroSegundos < 60){ //solo segundos
			seMinHrs = numeroSegundos+" seg"; 
		}else if (numeroSegundos < 3600){ //solo minutos
			seMinHrs = round(numeroSegundos / 60)+" min"; 
		}else{ 
			seMinHrs = round(numeroSegundos / 3600)+""; 
			
			if (seMinHrs.equals("1")){ 
				seMinHrs += " hr"; 
			}else{ 
				seMinHrs += " hrs"; 
			}
		}
		
		return seMinHrs;
	}
	
	
	public static double round(double numero){
		return Math.round(numero * 100) / 100;
	}
	
	
	public static double celciusToFarenheit(double gradosCelcius){
		return round(gradosCelcius * 9 / 5 + 32);
	}
	
	
	public static double kmToMillas(double km){
		return round(km * 0.621371192);
	}
}
