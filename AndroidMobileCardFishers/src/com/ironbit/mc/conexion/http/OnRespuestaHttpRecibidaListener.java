package com.ironbit.mc.conexion.http;

import com.ironbit.mc.widget.ProgressBarHandler;

public interface OnRespuestaHttpRecibidaListener {
	/**
	 * Propiedades
	 */
	public ProgressBarHandler barraHandler = new ProgressBarHandler();
	
	
	/**
	 * Metodos
	 */
	public void onRespuestaHttpRecibida(int idRespuesta, String respuesta);
}