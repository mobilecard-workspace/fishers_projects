package com.ironbit.mc.conexion.http;


public class RespuestaListenerWrapper implements Runnable{
	protected String respuesta = null;
	protected OnRespuestaHttpRecibidaListener listener = null;
	protected int idRespuesta = -1;
	
	public RespuestaListenerWrapper(OnRespuestaHttpRecibidaListener listener, int idRespuesta){
		this.idRespuesta = idRespuesta;
		this.listener = listener;
	}
	
	
	public void setRespuesta(String respuesta){
		this.respuesta = respuesta;
	}
	
	
	public void run(){
		listener.onRespuestaHttpRecibida(idRespuesta, respuesta);
	}
}
