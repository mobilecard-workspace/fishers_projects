package com.ironbit.mc.conexion.http;


import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.Handler;

import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.util.ListUtils;
import com.ironbit.mc.web.util.Url;

/**
 * @author angel
 * @description
 * Realiza conexiones hacia Internet mediante el protocolo HTTP
 * Las conexiones pueden ser sincronas (usando los metodos estaticos) o asincronas(instancian la clase 
 * <br/>e iniciando el hilo)
 * @example
 * <b>Sincrono</b><br/>
 * String pagina = HttpConexion.conectar("http://www.page.com/index.php?nombre=juan&edad=12", Metodo.get);
 * <br/><br/>
 * <b>Asincrono</b><br/>
 * new HttpConexion(handler, miActividad, idRespuesta, url, metodo, parametros).start();
 * <br/><br/>La respuesta será recibida en miActividad.onRespuestaRecibida(int idRespuesta, JSONObject json) 
 * y uno puede identificar a que solicitud corresponde esta respuesta con idRespuesta.
 */
public class HttpConexion extends Thread{
	/**
	 * Propiedades 
	 */
	protected static HttpClient httpClient = null;
	protected static HttpGet httpGet = null;
	protected static HttpPost httpPost = null;
	protected static HttpResponse httpResponse = null;
	protected static HttpEntity httpEntity = null;
	protected Handler handler = null;
	protected RespuestaListenerWrapper listenerWrapper = null;
	protected String url = null;
	protected Metodo metodo = Metodo.get;
	protected Charset charsetRequest = Charset.UTF_8;
	protected Charset charsetResponse = Charset.UTF_8;
	protected Hashtable<String, String> parametros = null;
	protected Hashtable<String, String> files = null;
	
	
	/**
	 * Enums
	 */
	
	public enum Metodo{
		get,
		post
	}
	
	
	public enum Charset{
		UTF_8("UTF-8"),
		ISO_8859_1("ISO-8859-1");
		
		private String charset = "";
		
		private Charset(String charset){
			this.charset = charset;
		}
		
		
		public String getCharsetString(){
			return charset;
		}
	}
	
	
	/**
	 * Constructor
	 */
	
	public HttpConexion(Handler handler, OnRespuestaHttpRecibidaListener listener, int idRespuesta, String url, Metodo metodo, 
			Hashtable<String, String> parametros){
		this(handler, listener, idRespuesta, url, metodo, parametros, null);
	}
	
	
	public HttpConexion(Handler handler, OnRespuestaHttpRecibidaListener listener, int idRespuesta, String url, Metodo metodo, 
			Hashtable<String, String> parametros, Charset charsetRequest, Charset charsetResponse){
		this(handler, listener, idRespuesta, url, metodo, parametros, null, charsetRequest, charsetResponse);
	}
	
	
	public HttpConexion(Handler handler, OnRespuestaHttpRecibidaListener listener, int idRespuesta, String url, Metodo metodo, 
			Hashtable<String, String> parametros, Hashtable<String, String> files){
		this(handler, listener, idRespuesta, url, metodo, parametros, files, null, null);
	}
	
	
	public HttpConexion(Handler handler, OnRespuestaHttpRecibidaListener listener, int idRespuesta, String url, Metodo metodo, 
			Hashtable<String, String> parametros, Hashtable<String, String> files, Charset charsetRequest, Charset charsetResponse){
		this.handler = handler;
		this.listenerWrapper = new RespuestaListenerWrapper(listener, idRespuesta);
		this.url = url;
		this.metodo = metodo;
		this.parametros = parametros;
		this.files = files;
		if (charsetRequest != null){
			this.charsetRequest = charsetRequest;
		}
		if (charsetResponse != null){
			this.charsetResponse = charsetResponse;
		}
	}
	
	
	/**
	 * Metodos
	 */
	
	public void run(){
		String valor = "";
		
		try{
			DefaultHttpClient thisHttpClient = new DefaultHttpClient();
			HttpResponse thisHttpResponse;
			
			if (metodo == Metodo.get){
				if (parametros != null){
					url = Url.setUrl(url).agregarParametros(parametros).getUrl();
				}
				
				Sys.log("HttpConexion_Get", "Url[" + url + "]");
				HttpGet thisHttpGet = new HttpGet(url);
				synchronized(thisHttpClient){
					thisHttpResponse = thisHttpClient.execute(thisHttpGet);
				}
			}else{
				Sys.log("HttpConexion_Post", "Url[" + url + "]");
				HttpPost thisHttpPost = new HttpPost(url);
			
				//guardamos archivos (si esque hay archivos)
				if (files != null){
					MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
					
					//guardamos parametros de texto enviados al server (si esque hay parametros)
					if (parametros != null){
						Enumeration<String> enumeration = parametros.keys();
						String key = "";
						while (enumeration.hasMoreElements()){
							key = enumeration.nextElement();
							entity.addPart(key, new StringBody(parametros.get(key)));
							Sys.log("HttpConexion_Post", "PostVar[" + key + "] = " + parametros.get(key));
						}
					}
					
					Enumeration<String> enumeration = files.keys();
					String key = "";
					StringBuilder sbFiles = new StringBuilder("PostFile[");
					String separador = "";
					
					while (enumeration.hasMoreElements()){
						key = enumeration.nextElement();
						
						try{
							File file = new File(files.get(key));
							if (file.exists()){
								entity.addPart(key, new FileBody(file));
							}
						}catch(Exception ee){
							Sys.log(ee);
						}
						
						sbFiles.append(separador);
						sbFiles.append(key);
						separador = ",";
					}
					
					if (files.size() > 0){
						Sys.log("HttpConexion_Post", sbFiles.toString());
					}
					
					thisHttpPost.setEntity(entity);
				}else if (parametros != null){
					//si solo está mandando variables pero no archivos:
					UrlEncodedFormEntity u = new UrlEncodedFormEntity(
						ListUtils.HashtableToListNameValuePair(parametros)//, 
						//charsetRequest.getCharsetString()
					);
					thisHttpPost.setEntity(u);
					
					//imprimimos Log de parametros
					for (Entry<String, String> entry : parametros.entrySet()){
						Sys.log("HttpConexion_Post", "PostVar[" + entry.getKey() + "] = " + entry.getValue());
					}
				}
				
				synchronized(thisHttpClient){
					thisHttpResponse = thisHttpClient.execute(thisHttpPost);
				}
			}
			
			HttpEntity thisHttpEntity = thisHttpResponse.getEntity();
			if (thisHttpEntity != null){
				valor = EntityUtils.toString(thisHttpEntity);
			}
			
			//imprimimos respuesta
			if (metodo == Metodo.get){
				Sys.log("HttpConexion_Get_Response", valor);
			}else{
				Sys.log("HttpConexion_Post_Response", valor);
			}
			
			//enviamos respuesta al handler para que se lo haga llegar al RespuestaDBListener
			listenerWrapper.setRespuesta(valor);
			handler.post(listenerWrapper);
		}catch(Exception e){
			Sys.log("HttpConexionThread cancelled");
			Sys.log(e);
		}
	}
	

	/**
	 * @param url. dirección a donde se quiere conectar
	 * @param metodo. metodo mediante el cual se quieren enviar datos
	 * @param parametros. variables que se desean enviar
	 * @return String. respuesta regresada por el servidor
	 * @example
	 * 
	 * Hashtable<String, String> vars = new Hashtable<String, String>();
	 * vars.put("nombre", "juan");
	 * vars.put("edad", "12");
	 *	
	 * String pagina = HttpConexion.conectar("http://www.ejemplo.com/pagina.php", Metodo.post, vars);
	 */
	public static String conectar(String url, Metodo metodo, Hashtable<String, String> parametros) throws ErrorSys{
		String valor = "";
		
		try{
			httpClient = new DefaultHttpClient();
			
			if (metodo == Metodo.get){
				if (parametros != null){
					url = Url.setUrl(url).agregarParametros(parametros).getUrl();
				}
				
				httpGet = new HttpGet(url);
				httpResponse = httpClient.execute(httpGet);
			}else{
				httpPost = new HttpPost(url);
				
				if (parametros != null){
					httpPost.setEntity(new UrlEncodedFormEntity(ListUtils.HashtableToListNameValuePair(parametros)));
				}
				
				httpResponse = httpClient.execute(httpPost);
			}
			
			httpEntity = httpResponse.getEntity();
			if (httpEntity != null){
				valor = Text.convertStreamToString(httpEntity.getContent());
			}
		}catch(Exception e){
			throw new ErrorSys(e.getMessage());
		}
		
		return valor;
	}
	
	
	/**
	 * @param url. dirección a donde se quiere conectar
	 * @param metodo. metodo mediante el cual se quieren enviar datos
	 * @return String. respuesta regresada por el servidor
	 * @example
	 * 
	 * String pagina = HttpConexion.conectar("http://www.page.com/index.php?nombre=juan&edad=12", Metodo.get);
	 */
	public static String conectar(String url, Metodo metodo) throws ErrorSys{
		try {
			return conectar(url, metodo, null);
		} catch (Exception e) {
			throw new ErrorSys(e.getMessage());
		}
	}
	
	
	/**
	 * @param url. dirección a donde se quiere conectar
	 * @param metodo. metodo mediante el cual se quieren enviar datos
	 * @param parametros. variables que se desean enviar
	 * @return JSONObject. respuesta regresada por el servidor en forma de JSON. NULL si algo salió mal
	 * @example
	 * 
	 * Hashtable<String, String> vars = new Hashtable<String, String>();
	 * vars.put("nombre", "juan");
	 * vars.put("edad", "12");
	 *	
	 * JSONObject datos = HttpConexion.conectar("http://www.ejemplo.com/pagina.php", Metodo.post, vars);
	 */
	public static JSONObject conectarJson(String url, Metodo metodo, Hashtable<String, String> parametros) throws ErrorSys{
		JSONObject json = null;
		
		try{
			json = new JSONObject(conectar(url, metodo, parametros)); 
		}catch(Exception e){
			throw new ErrorSys(e.getMessage());
		}
		
		return json;
	}
	
	
	/**
	 * Se conecta a una web mediante el método Get y regresa la respuesta como String
	 * @param url. dirección a donde se quiere conectar
	 * @param parametros. variables que se desean enviar
	 * @return String. respuesta regresada por el servidor
	 * @example
	 * 
	 * Hashtable<String, String> vars = new Hashtable<String, String>();
	 * vars.put("nombre", "juan");
	 * vars.put("edad", "12");
	 *	
	 * String pagina = HttpConexion.get("http://www.ejemplo.com/pagina.php", vars);
	 */
	public static String get(String url, Hashtable<String, String> parametros) throws ErrorSys{
		try {
			return conectar(url, Metodo.get, parametros);
		} catch (Exception e) {
			throw new ErrorSys(e.getMessage());
		}
	}
	
	
	public static JSONObject getJson(String url, Hashtable<String, String> parametros) throws ErrorSys{
		try {
			return conectarJson(url, Metodo.get, parametros);
		} catch (Exception e) {
			throw new ErrorSys(e.getMessage());
		}
	}
	
	
	/**
	 * Se conecta a una web mediante el método Get y regresa la respuesta como String
	 * @param url. dirección a donde se quiere conectar
	 * @return String. respuesta regresada por el servidor
	 * @example
	 * 
	 * String pagina = HttpConexion.get("http://www.ejemplo.com/pagina.php?edad=13");
	 */
	public static String get(String url) throws ErrorSys{
		try {
			return get(url, null);
		} catch (Exception e) {
			throw new ErrorSys(e.getMessage());
		}
	}
	
	
	public static JSONObject getJson(String url) throws ErrorSys{
		try {
			return getJson(url, null);
		} catch (Exception e) {
			throw new ErrorSys(e.getMessage());
		}
	}
	
	
	/**
	 * Se conecta a una web mediante el método Post y regresa la respuesta como String
	 * @param url. dirección a donde se quiere conectar
	 * @param parametros. variables que se desean enviar
	 * @return String. respuesta regresada por el servidor
	 * @example
	 * 
	 * Hashtable<String, String> vars = new Hashtable<String, String>();
	 * vars.put("nombre", "juan");
	 * vars.put("edad", "12");
	 *	
	 * String pagina = HttpConexion.post("http://www.ejemplo.com/pagina.php", vars);
	 */
	public static String post(String url, Hashtable<String, String> parametros) throws ErrorSys{
		try {
			return conectar(url, Metodo.post, parametros);
		} catch (Exception e) {
			throw new ErrorSys(e.getMessage());
		}
	}
	
	
	public static JSONObject postJson(String url, Hashtable<String, String> parametros) throws ErrorSys{
		try {
			return conectarJson(url, Metodo.post, parametros);
		} catch (Exception e) {
			throw new ErrorSys(e.getMessage());
		}
	}
	
	
	/**
	 * Se conecta a una web mediante el método Post y regresa la respuesta como String
	 * @param url. dirección a donde se quiere conectar
	 * @return String. respuesta regresada por el servidor
	 * @example
	 * 
	 * String pagina = HttpConexion.get("http://www.ejemplo.com/pagina.php?edad=13");
	 */
	public static String post(String url) throws ErrorSys{
		try {
			return post(url, null);
		} catch (Exception e) {
			throw new ErrorSys(e.getMessage());
		}
	}
	
	
	public static JSONObject postJson(String url) throws ErrorSys{
		try {
			return postJson(url, null);
		} catch (Exception e) {
			throw new ErrorSys(e.getMessage());
		}
	}
}
