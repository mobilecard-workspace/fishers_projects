package com.ironbit.mc.mbd;

import java.util.ArrayList;

import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

public class PagosDataSource {
	
	private SQLiteDatabase 	basedatos2;
	private Mysqlite sqlWizard2;
	private String[] columnas_pagos = {
									   Mysqlite.COLUMN_ID, 
									   Mysqlite.COLUMN_AFI, 
									   Mysqlite.COLUMN_NOM,
									   Mysqlite.COLUMN_MON,
									   Mysqlite.COLUMN_PRO,
									   Mysqlite.COLUMN_EST,
									   Mysqlite.COLUMN_TD
									   };
	/*
	 * constructor 
	 */
	public PagosDataSource (Context context){
		sqlWizard2 = new Mysqlite(context);
	}
	
	public void Open() throws SQLException{
		basedatos2 = sqlWizard2.getWritableDatabase();
	}
	
	public void Close(){
		sqlWizard2.close();
	}//cierra conexion
	
	/*
	 * Insersion de pagos en el telefono
	 */
	public boolean InsertPayments (long afiliacion, String negocio,
								   Double monto, Double propina, 
								   long estatus, String fechahora){
		long insertada = 0;
		boolean flag = false;
		
		ContentValues valores = new ContentValues();
		
		valores.put(Mysqlite.COLUMN_AFI, afiliacion);
		valores.put(Mysqlite.COLUMN_NOM, negocio);
		valores.put(Mysqlite.COLUMN_MON, monto);
		valores.put(Mysqlite.COLUMN_PRO, propina);
		valores.put(Mysqlite.COLUMN_EST, estatus);
		valores.put(Mysqlite.COLUMN_TD, fechahora);
		
		insertada = basedatos2.insert(Mysqlite.TABLE_PAGOS, Mysqlite.COLUMN_ID, valores);
		
		if (insertada != 0){
			flag = true;
		}
						
		return flag;
		
	}
	/*
	 * Consulta de datos sobre pagos
	 */
	public ArrayList<pagos> PaymentsList(){
		ArrayList<pagos> paymentList = new ArrayList<pagos>();
		
		Cursor cursor = basedatos2.query(Mysqlite.TABLE_PAGOS, columnas_pagos, 
										 null, null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			pagos payments = cursorTopayments (cursor);
			paymentList.add(payments);
			cursor.moveToNext();
		}
		cursor.close();
		return paymentList;
	}
	/*
	 * Consulta para el detalle del pago
	 */
	
	public pagos DetallePago(long id){
		pagos detalle = new pagos();
		
		Cursor cursor = basedatos2.query(Mysqlite.TABLE_PAGOS,columnas_pagos,"_id="+id,null,null,null,null);		
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()){
			detalle = cursorTopayments(cursor);
			cursor.moveToNext();
		}
		cursor.close();
		return detalle;
	}
	/*
	 * seteo del objeto pagos
	 */
	public pagos cursorTopayments (Cursor cursor){
		pagos paytemp = new pagos();
		paytemp.setId(cursor.getLong(0));
		paytemp.setAfiliacion(cursor.getLong(1));
		paytemp.setEstablecimiento(cursor.getString(2));
		paytemp.setMonto(cursor.getDouble(3));
		paytemp.setPropina(cursor.getDouble(4));
		paytemp.setEstatus(cursor.getLong(5));
		paytemp.setFechahora(cursor.getString(6));
		
		return paytemp;
	}

}
