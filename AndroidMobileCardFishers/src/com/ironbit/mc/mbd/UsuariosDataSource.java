package com.ironbit.mc.mbd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


public class UsuariosDataSource {
	
	private SQLiteDatabase 	basedatos;
	private Mysqlite sqlWizard;
	private String[] columnas = {Mysqlite.COLUMN_ID, Mysqlite.COLUMN_NAME, Mysqlite.COLUMN_PASS };
	
	public UsuariosDataSource (Context context){
		sqlWizard = new Mysqlite(context);
	}//Constructor
	
	/*
	 * escritura de metodos para abrir y cerrar la conexion
	 */
	
	public void Open () throws SQLException {
		basedatos = sqlWizard.getWritableDatabase();
	}//abrir conexion
	
	public void Close(){
		sqlWizard.close();
	}//cierra conexion
	
	/*
	 * Manejo de insrtes updates y recuperación de información
	 */
	
	public long insertaNuevo (String user, String pass){
		long insertada = 0;
		ContentValues valores = new ContentValues();
		
		valores.put(Mysqlite.COLUMN_NAME, user);
		valores.put(Mysqlite.COLUMN_PASS, pass);
		
		insertada = basedatos.insert(Mysqlite.TABLE_USERS,Mysqlite.COLUMN_ID, valores);
						
		return insertada;
	}//inserta nuevo usuario
	
	public boolean verificaExiste(){
		boolean flag = false;
		Cursor cursor = basedatos.query(Mysqlite.TABLE_USERS, columnas, null, null, null, null, null);
		
		if (cursor.getCount() > 0){
			flag = true;
		}
			System.out.println("LOS REGISTROS QUE ENCONTRO: "+cursor.getCount());	
		return flag;
	}//verifica si existe un usuario en la tabla
	
	public usuarios obtenUsr(){
		Cursor cursor = basedatos.query(Mysqlite.TABLE_USERS, columnas, null, null, null, null, null);
		cursor.moveToFirst();
		usuarios nuevoUser = CursorAResultado(cursor);
		cursor.close();
		
		return nuevoUser;
	}
	
	public usuarios CursorAResultado(Cursor cursor){
		usuarios existUser = new usuarios();
		
		existUser.setId(cursor.getLong(0));
		existUser.setUser(cursor.getString(1));
		existUser.setPass(cursor.getString(2));		
		return existUser;
	}//obtiene datos del registro de la tabla
}//clase
