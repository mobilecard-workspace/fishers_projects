package com.ironbit.mc.mbd;

public class pagos {
	private long id;
	private long afiliacion;
	private String establecimiento;
	private Double monto;
	private Double propina;
	private long estatus;
	private String fechahora;
	private String everyThing;
	
	
	/*
	 * Setters de la aplicacion
	 */
	public void setId(long ids){
		this.id = ids;
	}
	public void setAfiliacion (long af){
		this.afiliacion = af;
	}
	public void setMonto (Double mon){
		this.monto = mon;
	}
	public void setPropina (Double pro){
		this.propina = pro;
	}
	public void setEstatus (long est){
		this.estatus = est;
	}
	public void setFechahora (String fh){
		this.fechahora = fh;
	}
	public void setEstablecimiento (String es){
		this.establecimiento = es;
	}
	
	/*
	 * Getters de la aplicacion
	 */
	public long getAfiliacion(){
		return afiliacion;
	}
	public Double getMonto(){
		return monto;
	}
	public Double getPropina(){
		return propina;
	}
	public long getEstatus(){
		return estatus;
	}
	public String getFechahora(){
		return fechahora;
	}
	public String getEstablecimiento (){
		return establecimiento;
	}
	public long getId(){
		return id;
	}
	 @Override
	  public String toString() {
		 
		 
		 everyThing = "Compra en: "+establecimiento				 	  
				 	  +"\n"
				 	  +"Fecha: "+fechahora
				 	  +"\n"
				 	  +"Monto: $"+monto
				 	  +"\n"
				 	  +"Propina: $"+propina;
	    return everyThing;
	  }
}
