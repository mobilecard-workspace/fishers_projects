package com.ironbit.mc.mbd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class Mysqlite extends SQLiteOpenHelper {
	
	public static final String TABLE_USERS = "usuarios";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "nombre";
	public static final String COLUMN_PASS = "password";
	
	public static final String TABLE_PAGOS = "pagos";
	public static final String COLUMN_AFI = "afiliacion";
	public static final String COLUMN_NOM = "negocio";
	public static final String COLUMN_MON = "monto";
	public static final String COLUMN_PRO = "propina";
	public static final String COLUMN_EST = "estatus";
	public static final String COLUMN_TD = "fechahora";

	private static final String DATABASE_NAME = "scanandpay.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "
		      	+ TABLE_USERS + "(" + COLUMN_ID
		      	+ " integer primary key autoincrement, " + COLUMN_NAME
		      	+ " text not null, " + COLUMN_PASS + " text not null);";
	
	private static final String DATABASE_PAGOS ="CREATE TABLE IF NOT EXISTS "
				+TABLE_PAGOS+"( "+COLUMN_ID+" integer primary key autoincrement, "
				+COLUMN_AFI+" integer not null, "+COLUMN_NOM+" text not null, "
				+COLUMN_MON+" double not null, "+COLUMN_PRO+" double not null,"+COLUMN_EST+" int not null, "
				+COLUMN_TD+" text not null)";
	
	public Mysqlite(Context context) {
	    	super(context, DATABASE_NAME, null, DATABASE_VERSION);	    	
	  	}//constructor

	  @Override
	  public void onCreate(SQLiteDatabase database) {
		  database.execSQL(DATABASE_CREATE);
		  database.execSQL(DATABASE_PAGOS);
	  }//OnCreate para crear si no existe

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    Log.w(Mysqlite.class.getName(),
	        "Upgrading database from version " + oldVersion + " to "
	            + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
	    db.execSQL("DROP TABLE IF EXISTS" + TABLE_PAGOS);
	    onCreate(db);
	  }//actualizador de tabla


}
