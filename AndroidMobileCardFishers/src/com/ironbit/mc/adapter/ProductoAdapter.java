package com.ironbit.mc.adapter;

import java.util.List;

import com.ironbit.mc.R;
import com.ironbit.mc.constant.Font;
import com.ironbit.mc.web.webservices.data.DataProduct;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ProductoAdapter extends BaseAdapter {
	
	Context con;
	List<DataProduct> data;
	LayoutInflater inflater;
	Typeface type;
	
	public ProductoAdapter(Context _con, List<DataProduct> _data) {
		con = _con;
		data = _data;
		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		type = Typeface.createFromAsset(con.getAssets(), Font.GANDHISANS_REG);
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder viewHolder;
		
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.item_producto, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.montoView = (TextView) convertView.findViewById(R.id.view_monto);
			viewHolder.separador = convertView.findViewById(R.id.separador);
			
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		DataProduct product = data.get(position);
		
		if (null != product) {
			
			if (product.getClaveWS().equals("800"))
				viewHolder.montoView.setText(product.getNombre() + " " + product.getDescription());
			else
				viewHolder.montoView.setText(product.getDescription());
				
			viewHolder.montoView.setTypeface(type);
			
			if (position == (data.size() - 1))
				viewHolder.separador.setVisibility(View.GONE);
		}
		
		return convertView;
	}
	
	private class ViewHolder {
		TextView montoView;
		View separador;
	}

}
