package com.ironbit.mc.adapter;

import java.util.List;

import com.ironbit.mc.R;
import com.ironbit.mc.web.webservices.data.DataOperation;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class OperacionAdapter extends BaseAdapter {
	
	private Context con;
	private List<DataOperation> data;
	private LayoutInflater inflater;
	
	public OperacionAdapter(Context _con, List<DataOperation> _data) {
		con = _con;
		data = _data;
		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		ViewHolder viewHolder;
		
		if (null == arg1) {
			arg1 = inflater.inflate(R.layout.item_compra, arg2, false);
			viewHolder = new ViewHolder();
			viewHolder.conceptoView = (TextView) arg1.findViewById(R.id.txtCompraConcepto);
			viewHolder.fechaView = (TextView) arg1.findViewById(R.id.txtCompraFecha);
			viewHolder.montoView = (TextView) arg1.findViewById(R.id.txtCompraMonto);
			
			arg1.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) arg1.getTag();
		}
		
		DataOperation operacion = data.get(arg0);
		
		if (null != operacion) {
			
			Log.d("OperacionAdapter", operacion.toString());
			
			if (null != operacion.getConcepto())
				viewHolder.conceptoView.setText(operacion.getConcepto());
			else
				viewHolder.conceptoView.setText("");
				
			
			viewHolder.fechaView.setText(operacion.getFecha());
			viewHolder.montoView.setText(operacion.getMonto());
		}
		
		return arg1;
	}
	
	private class ViewHolder {
		TextView conceptoView;
		TextView fechaView;
		TextView montoView;
	}

}
