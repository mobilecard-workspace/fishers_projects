package com.ironbit.mc.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ironbit.mc.R;
import com.ironbit.mc.system.cache.image.ImageCacheManager;
import com.ironbit.mc.system.cache.image.OnImagenObtenidaListener;
import com.ironbit.mc.web.webservices.data.NuevoTO;

public class NuevoAdapter extends BaseAdapter {
	
	Context context;
	List<NuevoTO> data;
	LayoutInflater inflater;
	Bitmap images[];
	
	public NuevoAdapter(Context _context, List<NuevoTO> _data) {
		context = _context;
		data = _data;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		images = new Bitmap[data.size()];
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.item_nuevo, arg2, false);

		NuevoTO nuevo = data.get(arg0);
		
		if (null != nuevo) {
			final ImageView  nuevoImageView = (ImageView) view.findViewById(R.id.image_nuevo);
			
			if (null == images[arg0]) {
				
				final int auxIndex = arg0;

				DisplayMetrics metrics = context.getResources().getDisplayMetrics();
				ImageCacheManager.getImagen(context, new OnImagenObtenidaListener() {
					
					public void onImagenObtenida(Bitmap imagen) {
						// TODO Auto-generated method stub
						nuevoImageView.setImageBitmap(imagen);
						images[auxIndex] = imagen;
						
					}
				}, nuevo.getUrlImagen(), metrics.widthPixels);
			} else {
				nuevoImageView.setImageBitmap(images[arg0]);
			}
		}
		
		return view;
	}

}
