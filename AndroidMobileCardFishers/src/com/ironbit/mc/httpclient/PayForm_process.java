package com.ironbit.mc.httpclient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import com.ironbit.mc.activities.scan.Formulario_pago_actividad;
import com.ironbit.mc.activities.scan.QR_reader_actividad;
import com.ironbit.mc.R;
import com.ironbit.mc.UrlWebServices;
import com.ironbit.mc.httpclient.HttpManager.OnExecuteHttpPostAsyncListener;

import com.ironbit.mc.mbd.PagosDataSource;
import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.Text;
import com.ironbit.mc.system.crypto.Crypto;
import com.ironbit.mc.usuario.Usuario;


public class PayForm_process  {

	private long afiliacion;
	private Double monto;
	private Double propina;
	private String referencia;
	private String establecimiento;
	private String password;
	private String cvv;
	private boolean response;
	private PagosDataSource DSpagos;
	private long oafiliacion;
	
	public void setoAfiliacion(long afi){
		this.oafiliacion = afi;
	}
	public long getoAfiliacion(){
		return oafiliacion;
	}
	public void setAfiliacion(long af){
		this.afiliacion = af;
	}
	public void setMonto(Double mon){
		this.monto = mon;
	}
	public void setPropina(Double pro){
		this.propina = pro;
	}
	public void setReferencia(String ref){
		this.referencia = ref;
	}
	public void setEstablecimiento(String est){
		this.establecimiento = est;
	}
	public void setPass(String pass){
		this.password = pass;
	}
	
	public void setCvv(String cvv){
		this.cvv = cvv;
	}
	
	public void setResponse(boolean res){
		this.response = res;
	}
	
	public long getAfiliacion(){
		return afiliacion;
	}
	public Double getMonto(){
		return monto;
	}
	public Double getPropina(){
		return propina;
	}
	public String getReferencia(){
		return referencia;
	}
	public String getEstablecimiento(){
		return establecimiento;		
	}
	public String getPassword(){
		return password;
	}
	
	public String getCvv(){
		return cvv;
	}
	
	public boolean getResponse(){
		return response;
	}
	
	private static String KEY_SUCCESS = "autorizacion";
	private static String AES_KEY = "xCL8m39sJ1";
	
	public boolean makePayment (final Activity activity){		
		HttpManager httpclient = new HttpManager(activity);
		DSpagos = new PagosDataSource(activity);
		
	    JSONObject payment = new JSONObject ();
	    try{
	    	payment.put("id_comercio", getAfiliacion());
	    	payment.put("comercio",getEstablecimiento());
	    	payment.put("afil", getoAfiliacion());
	    	payment.put("importe",getMonto());
	    	payment.put("prop", getPropina());
	    	payment.put("referencia", getReferencia());
	    	payment.put("pass", getPassword());
	    	payment.put("cvv", getCvv());
	    	payment.put("user", Usuario.getLogin(activity));
	    	
	    	/*
 *                                  request.getParameter("pass").toString(),
                                   request.getParameter("afil").toString(),
                                    request.getParameter("cvv").toString(),
                                    request.getParameter("monto").toString(),
                                    request.getParameter("prop").toString()
	    	 * */

	    }catch(JSONException e){
	    	System.out.println(e.toString());
	    }
	    String postData = payment.toString();	    
		String encripPado = Crypto.aesEncrypt(Text.parsePass(AES_KEY), postData);		
	    System.out.println(encripPado);
	    System.out.println(postData);
	    //httpclient.addNameValue("payment", payment.toString());	    
		
	    //httpclient.addNameValue("json", postData);
		httpclient.addNameValue("pass", getPassword());
		httpclient.addNameValue("ref", getReferencia());
		httpclient.addNameValue("afil", Long.toString(getoAfiliacion()));
		httpclient.addNameValue("cvv", getCvv());
		httpclient.addNameValue("monto", Double.toString(getMonto()));
		httpclient.addNameValue("prop", Double.toString(getPropina()));
		httpclient.addNameValue("usr", Usuario.getLogin(activity));
		
		
		
	    httpclient.setOnExecuteHttpPostAsyncListener(new OnExecuteHttpPostAsyncListener() {
	    public void onExecuteHttpPostAsyncListener(String ResponseBody) {
	    	 System.out.println("Mi respuesta "+ResponseBody);
	         try {
	        	 //String respuesta = Crypto.aesDecrypt(Text.parsePass(AES_KEY), ResponseBody);
	        	 
	        	 JSONObject json = new JSONObject(ResponseBody);

	        	
	        	 if (json.getBoolean("isAuthorized")) {
	        		 if((Integer.parseInt(json.getString(KEY_SUCCESS)) != 0)){	        			 	        			 
	        			 setResponse(true);
	        			 DSpagos.Open();
	        			 long estatus = Long.parseLong(json.getString(KEY_SUCCESS));	        			 
	        			 Date ahora = new Date();
	        			 SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");	        			
	        			 Calendar cal = new GregorianCalendar();

	        			 Date date = cal.getTime();
	        			 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	        			 String formatteDate = df.format(date);
	        			 String fecha = formatteDate+" "+formateador.format(ahora);
	        			 /*
	        			  * Muestra cuadro de dialogo y manda a la captura de codigo
	        			  *        			 
	        			  */
	        			 AlertDialog.Builder Alerta = new AlertDialog.Builder(activity);
	        			 Alerta.setTitle(R.string.pago_detail);
	        			 Alerta.setMessage(R.string.alert1_tag);
	        			 Alerta.setPositiveButton(R.string.ok_tag, new DialogInterface.OnClickListener() {
	        		           public void onClick(DialogInterface dialog, int id) {
	        		        	   Intent consulta = new Intent (activity,QR_reader_actividad.class);											
									consulta.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);								
									activity.startActivity(consulta);
	        		           }
	        		       });
	        			 Alerta.create();
	        			 Alerta.show();
	        			 //Fin del alert
	        			 try{
	        				 DSpagos.InsertPayments(getAfiliacion(), getEstablecimiento(), 
	        					 				getMonto(), getPropina(), estatus, fecha);
	        				 
	        			 	}catch (Exception e){
	        				 			System.out.println(e.toString());
	        			
	        			 }
	        			 
	        			 
	        			 DSpagos.Close();
	        			 
	        		 } else {
	        			 	
	        			 listenerAsyncPost.onErrorAsyncPost("Fallo la realizacion del pago");
	        			 /*
	        			  * Muestra cuadro de dialogo y manda a la captura de codigo
	        			  *        			 
	        			  */
	        			 AlertDialog.Builder Alerta = new AlertDialog.Builder(activity);
	        			 Alerta.setTitle(R.string.pago_detail);
	        			 Alerta.setMessage(R.string.alert1_tag);
	        			 Alerta.setPositiveButton(R.string.cont_tag, new DialogInterface.OnClickListener() {
					           public void onClick(DialogInterface dialog, int id) {
					        	   Intent consulta = new Intent (activity,QR_reader_actividad.class);											
									consulta.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);								
									activity.startActivity(consulta);
					           }
					       });
	        			 Alerta.create();
	        			 Alerta.show();
	        			 //Fin del alert
	        			 
	        		 }
	        	 } else {
	        		 listenerAsyncPost.onErrorAsyncPost("Servicio no disponible temporalmente");
	        		 /*
        			  * Muestra cuadro de dialogo y manda a la captura de codigo
        			  *        			 
        			  */
        			 AlertDialog.Builder Alerta = new AlertDialog.Builder(activity);
        			 Alerta.setTitle(R.string.pago_detail);
        			 Alerta.setMessage(R.string.alert2_tag);
        			 Alerta.setPositiveButton(R.string.ok_tag, new DialogInterface.OnClickListener() {
        		           public void onClick(DialogInterface dialog, int id) {
        		        	   Intent consulta = new Intent (activity,QR_reader_actividad.class);											
								consulta.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);								
								activity.startActivity(consulta);
        		           }
        		       });
        			 Alerta.create();
        			 Alerta.show();
        			 //Fin del alert
	        	 }
	         } catch (JSONException e) {      
	        	 listenerAsyncPost.onErrorAsyncPost("Se ha producido un error al conectar con el servicio: "+e.toString());
	        	 /*
    			  * Muestra cuadro de dialogo y manda a la captura de codigo
    			  *        			 
    			  */
    			 AlertDialog.Builder Alerta = new AlertDialog.Builder(activity);
    			 Alerta.setTitle(R.string.pago_detail);
    			 Alerta.setMessage(R.string.alert3_tag);
    			 Alerta.setPositiveButton(R.string.ok_tag, new DialogInterface.OnClickListener() {
    		           public void onClick(DialogInterface dialog, int id) {
    		        	   Intent consulta = new Intent (activity,QR_reader_actividad.class);											
							consulta.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);								
							activity.startActivity(consulta);
    		           }
    		       });
    			 Alerta.create();
    			 Alerta.show();
    			 //Fin del alert
	         	}     
	        }

	        public void onErrorHttpPostAsyncListener(String message) {
	        	/*
   			  * Muestra cuadro de dialogo y manda a la captura de codigo
   			  *        			 
   			  */
   			 AlertDialog.Builder Alerta = new AlertDialog.Builder(activity);
   			 Alerta.setTitle(R.string.pago_detail);
   			 Alerta.setMessage(R.string.alert2_tag);
   			 Alerta.setPositiveButton(R.string.ok_tag, new DialogInterface.OnClickListener() {
   		           public void onClick(DialogInterface dialog, int id) {
   		        	Intent consulta = new Intent (activity, QR_reader_actividad.class);											
					consulta.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);								
					activity.startActivity(consulta);
   		           }
   		       });
   			 Alerta.create();
   			 Alerta.show();
   			 //Fin del alert	
	         listenerAsyncPost.onErrorAsyncPost("Se ha producido un error ");
	        }
	       });      
	          //httpclient.executeHttpPost("http://192.168.1.105/androidws/procesapagojson.php");	    
	    	  //httpclient.executeHttpPost("http://201.161.23.42:8085/addcel_ws/adc_payment.jsp");
	    	 //httpclient.executeHttpPost("http://192.168.1.117:8084/addcel_ws/adc_payment.jsp");
	    	//httpclient.executeHttpPost("http://201.161.23.42:8084/addcel_ws/adc_payment.jsp");
	    	httpclient.executeHttpPost(UrlWebServices.URL_WS_SCAN_PAY);
	    	return getResponse();   
	}
	
	 public interface OnExecuteAsyncPostListener{ void onFinishAsyncPost(String message); void onErrorAsyncPost(String message); }
	 public OnExecuteAsyncPostListener listenerAsyncPost;
	 public void setOnExecuteAsyncPostListener(OnExecuteAsyncPostListener l){listenerAsyncPost = l; }
	 
}
