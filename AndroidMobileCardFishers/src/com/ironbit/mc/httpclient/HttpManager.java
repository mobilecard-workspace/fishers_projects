package com.ironbit.mc.httpclient;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.ironbit.mc.system.Sys;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;

public class HttpManager {

	/*
	 * definicion de variables
	 */
	private Activity activity;
    private ArrayList<NameValuePair> nameValuePairs;
    private String responseBody = "";
 
    private ProgressDialog m_ProgressDialog = null; 
    private Runnable viewOrders;
    private String UrlService = "";
    private String strMessageHeadLoading = "Por favor espera";
    private String strMessageBodyLoading  = "Enviando informaci�n...";
    
    /*
     * metodos de la clase
     */
    public HttpManager(Activity activity){
    	this.activity = activity;
    	nameValuePairs = new ArrayList<NameValuePair>();
    }//constructor
    
    //permite saber si la actividad tiene permisos de acceeso a la red
    // estos permisos deben ser definidos en el manifest
    
    private boolean isInternetAllowed(Activity activity){
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni!=null && ni.isAvailable() && ni.isConnected()) {
            return true;
        } else {
            return false; 
        }    
    }
    
    public String getStrMessageHeadLoading () { 
    	return strMessageHeadLoading; 
    	}
    
    public void setStrMessageHeadLoading (String message) { 
    	strMessageHeadLoading = message; 
    	}
    
   
    public String getStrMessageBodyLoading () { 
    	return strMessageBodyLoading; 
    	}
    
    public void setStrMessageBodyLoading(String message){ 
    	strMessageBodyLoading = message; 
    	}    
    
    private String getUrlService () { 
    	return UrlService;
    	}
    
    private void setUrlService (String UrlService){
    	this.UrlService = UrlService;
    	}
 
    public String getResponseBody (){
    	return responseBody;
    	} 
    
    private void setResponseBody (String ResponseBody){
    	responseBody = ResponseBody;
    	}
    //envio de parejas de datos (etiqueta valor)
    public void addNameValue(String name, String value){
    	nameValuePairs.add(new BasicNameValuePair(name,value));
    }
    	 
    public void executeHttpPost(String UrlService){
    	 System.out.println(":::ENTRANNDO A URL SERVICE:::");
    	 System.out.println(":::URL SERVICIO:::" + UrlService);
    	 setUrlService(UrlService);    	  
    	 viewOrders = new Runnable(){         
    	 public void run() {
		 	try{
		 		Looper.prepare();
	              try {
	            	  	executeHttpPostAsync(activity, getUrlService());
	              } catch (Exception e) {
	            	  System.out.println(e.getMessage());
	              }
	         }catch(Exception e){
	        	 Sys.log(e);
	         }
	 		Looper.loop();
    	 }
     };
        Thread thread =  new Thread(null, viewOrders, "Background");
    	thread.start();
    	m_ProgressDialog = ProgressDialog.show(activity, getStrMessageHeadLoading(), getStrMessageBodyLoading(), true);
    	  
    }
    
    private void executeHttpPostAsync(Activity activity, String UrlService){
    	 if(isInternetAllowed(activity)){
    	    try{
    	    	   HttpClient httpclient = new DefaultHttpClient();
    	    	   HttpPost httppost = new HttpPost(UrlService);  
    	    	   httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
    	    	   System.out.println(httppost.getParams().toString());
    	    
    	    	   ResponseHandler<String> responseHandler = new BasicResponseHandler();
    	    	   setResponseBody(httpclient.execute(httppost, responseHandler));
    	    
    	    	   activity.runOnUiThread(returnRes);
    	     }catch(HttpResponseException hre){
    	    	 ListenerExecuteHttpPostAsync.onErrorHttpPostAsyncListener("Se ha producido un error al conectar con el servidor");
    	    }catch(Exception e){         
    	      ListenerExecuteHttpPostAsync.onErrorHttpPostAsyncListener("Se ha producido un error");

    	      System.out.println(":::EXCEPCION POST:::");
    	      System.out.println(e.getMessage());
    	    } 
    	 } else{
    		 ListenerExecuteHttpPostAsync.onErrorHttpPostAsyncListener("No es posible realizar la conexi�n. Comprueba tu conexi�n de datos.");
    	 }
     }
    
    private Runnable returnRes = new Runnable() {
    	 public void run() {
    		 	m_ProgressDialog.dismiss();
    		 	ListenerExecuteHttpPostAsync.onExecuteHttpPostAsyncListener(getResponseBody());
    	 }
    };
    
    public interface OnExecuteHttpPostAsyncListener{
    	 void onExecuteHttpPostAsyncListener(String ResponseBody);
    	 void onErrorHttpPostAsyncListener(String message);
    	    }
    	 
    	    private OnExecuteHttpPostAsyncListener ListenerExecuteHttpPostAsync;
    	 
    	    public void setOnExecuteHttpPostAsyncListener(OnExecuteHttpPostAsyncListener l){ListenerExecuteHttpPostAsync = l;}
    
}
