package com.ironbit.mc.form.validator;

import com.ironbit.mc.system.errores.ErrorSys;

public interface Validable {
	public void validate() throws ErrorSys;
}
