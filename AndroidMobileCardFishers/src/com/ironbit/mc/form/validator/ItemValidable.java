package com.ironbit.mc.form.validator;

import com.ironbit.mc.R;
import com.ironbit.mc.system.Validador;
import com.ironbit.mc.system.errores.ErrorSys;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class ItemValidable implements Validable{
	protected Context ctx = null;
	protected View itemView = null;
	/**
	 * Utilizado si se quiere validar que este item sea igual en valor a otro del mismo tipo
	 */
	protected View itemViewEqual = null;
	protected String name = "";
	protected String nameEqual = "";
	protected boolean isRequired = false;
	protected boolean isEmail = false;
	protected boolean isNumeric = false;
	protected boolean isPhoneNumber = false;
	protected boolean isIpAddress = false;
	protected boolean isCreditCardNumber = false;
	protected boolean isCreditCardNumberVisa = false;
	protected boolean isCreditCardNumberMasterCard = false;
	protected boolean isCreditCardNumberAmericanExpress = false;
	protected boolean isCreditCardNumberDinersClub = false;
	protected boolean isCreditCardNumberDiscover = false;
	protected boolean isCreditCardNumberJCB = false;
	protected boolean canBeEmpty = false;
	protected int maxLength = 0;
	protected int minLength = 0;
	protected int exactLength = 0;
	protected Validable extraValidation = null;
	protected static int ID_MSJ_ERROR_IS_REQUIRED = R.string.form_err_is_required;
	protected static int ID_MSJ_ERROR_IS_EMAIL = R.string.form_err_is_email;
	protected static int ID_MSJ_ERROR_IS_NUMERIC = R.string.form_err_is_numeric;
	protected static int ID_MSJ_ERROR_IS_PHONE_NUMBER = R.string.form_err_is_phone_number;
	protected static int ID_MSJ_ERROR_IS_IP_ADDRESS = R.string.form_err_is_ip_address;
	protected static int ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER = R.string.form_err_is_credit_card_number;
	protected static int ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_VISA = R.string.form_err_is_credit_card_number_visa;
	protected static int ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_MASTER_CARD = R.string.form_err_is_credit_card_number_master_card;
	protected static int ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_AMERICAN_EXPRESS = R.string.form_err_is_credit_card_number_american_express;
	protected static int ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_DINERS_CLUB = R.string.form_err_is_credit_card_number_diners_club;
	protected static int ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_DISCOVER = R.string.form_err_is_credit_card_number_discover;
	protected static int ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_JCB = R.string.form_err_is_credit_card_number_jcb;
	protected static int ID_MSJ_ERROR_CANT_BE_EMPTY = R.string.form_err_cant_be_empty;
	protected static int ID_MSJ_ERROR_MAX_LENGTH = R.string.form_err_max_length;
	protected static int ID_MSJ_ERROR_MIN_LENGTH = R.string.form_err_min_length;
	protected static int ID_MSJ_ERROR_EXACT_LENGTH = R.string.form_err_exact_length;
	protected static int ID_MSJ_ERROR_EQUAL_TO = R.string.form_err_equal_to;
	
	
	public ItemValidable isRequired(boolean is){
		isRequired = is;
		return this;
	}
	
	
	public ItemValidable isEmail(boolean is){
		isEmail = is;
		return this;
	}
	
	
	public ItemValidable isNumeric(boolean is){
		isNumeric = is;
		return this;
	}
	
	
	public ItemValidable isPhoneNumber(boolean is){
		isPhoneNumber = is;
		return this;
	}
	
	
	public ItemValidable isIPAddress(boolean is){
		isIpAddress = is;
		return this;
	}
	
	
	public ItemValidable isCreditCardNumber(boolean is){
		isCreditCardNumber = is;
		return this;
	}
	
	
	public ItemValidable isCreditCardNumberVisa(boolean is){
		isCreditCardNumberVisa = is;
		return this;
	}
	
	
	public ItemValidable isCreditCardNumberMasterCard(boolean is){
		isCreditCardNumberMasterCard = is;
		return this;
	}
	
	
	public ItemValidable isCreditCardNumberAmericanExpress(boolean is){
		isCreditCardNumberAmericanExpress = is;
		return this;
	}
	
	
	public ItemValidable isCreditCardNumberDinersClub(boolean is){
		isCreditCardNumberDinersClub = is;
		return this;
	}
	
	
	public ItemValidable isCreditCardNumberDiscover(boolean is){
		isCreditCardNumberDiscover = is;
		return this;
	}
	
	
	public ItemValidable isCreditCardNumberJCB(boolean is){
		isCreditCardNumberJCB = is;
		return this;
	}
	
	
	public ItemValidable canBeEmpty(boolean can){
		canBeEmpty = can;
		return this;
	}
	
	
	public ItemValidable maxLength(int length){
		maxLength = length;
		return this;
	}
	
	
	public ItemValidable minLength(int length){
		minLength = length;
		return this;
	}
	
	
	public ItemValidable exactLength(int length){
		exactLength = length;
		return this;
	}
	
	
	/**
	 * Especif�ca otro elemento para validar que este elemento y el que se est� recibiendo sean iguales en valor.
	 * @param otherItemView
	 * @return
	 */
	public ItemValidable equalTo(View otherItemView, String otherItemName){
		itemViewEqual = otherItemView;
		nameEqual = otherItemName;
		return this;
	}
	
	
	public ItemValidable(Context ctx, View itemView, String name, Validable extraValidation){
		this.ctx = ctx;
		this.itemView = itemView;
		this.name = name;
		this.extraValidation = extraValidation;
	}
	
	
	public ItemValidable(Context ctx, View itemView, String name){
		this(ctx, itemView, name, null);
	}
	
	
	public void validate() throws ErrorSys {
		if (itemView instanceof EditText){
			EditText itemEditText = (EditText)itemView;
			String strItem = itemEditText.getText().toString().trim();
				if ((!canBeEmpty || isRequired) && Validador.estaVacio(strItem)){
					itemView.requestFocus();
					throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_CANT_BE_EMPTY, name));
				}else if( (canBeEmpty && strItem.length() > 0) ||  isRequired){
					
					if (isEmail && !Validador.esCorreo(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_EMAIL, name));
					}else if (isNumeric && !Validador.esNumerico(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_NUMERIC, name));
					}else if (isIpAddress && !Validador.esDireccionIP(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_IP_ADDRESS, name));
					}else if (isPhoneNumber && !Validador.esNumeroTelefonicoMX(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_PHONE_NUMBER, name));
					}else if (isCreditCardNumber && !Validador.esTarjetaDeCredito(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER, name));
					}else if (isCreditCardNumberVisa && !Validador.esTarjetaDeCreditoVisa(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_VISA, name));
					}else if (isCreditCardNumberMasterCard && !Validador.esTarjetaDeCreditoMasterCard(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_MASTER_CARD, name));
					}else if (isCreditCardNumberAmericanExpress && !Validador.esTarjetaDeCreditoAmericanExpress(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_AMERICAN_EXPRESS, name));
					}else if (isCreditCardNumberDinersClub && !Validador.esTarjetaDeCreditoDinersClub(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_DINERS_CLUB, name));
					}else if (isCreditCardNumberDiscover && !Validador.esTarjetaDeCreditoDiscover(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_DISCOVER, name));
					}else if (isCreditCardNumberJCB && !Validador.esTarjetaDeCreditoJCB(strItem)){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_CREDIT_CARD_NUMBER_JCB, name));
					}else if (maxLength != 0 && strItem.length() > maxLength){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_MAX_LENGTH, name, maxLength));
					}else if (minLength != 0 && strItem.length() < minLength){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_MIN_LENGTH, name, minLength));
					}else if (exactLength != 0 && strItem.length() != exactLength){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_EXACT_LENGTH, name, exactLength));
					}else if (itemViewEqual != null && itemViewEqual instanceof EditText && 
							!itemEditText.getText().toString().equals(((EditText)itemViewEqual).getText().toString())){
						itemView.requestFocus();
						throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_EQUAL_TO, name, nameEqual));
					}
			}
		}else if (itemView instanceof CheckBox){
			CheckBox itemCheckBox = (CheckBox)itemView;
			if (isRequired && !itemCheckBox.isChecked()){
				itemView.requestFocus();
				throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_IS_REQUIRED, name));
			}else if (itemViewEqual != null && itemViewEqual instanceof CheckBox && 
					 itemCheckBox.isChecked() != ((CheckBox)itemViewEqual).isChecked()){
				itemView.requestFocus();
				throw new ErrorSys(ctx.getString(ID_MSJ_ERROR_EQUAL_TO, name, nameEqual));
			}
		}
		
		//validaci�n personalizada
		if (extraValidation != null){
			extraValidation.validate();
		}
	}
}