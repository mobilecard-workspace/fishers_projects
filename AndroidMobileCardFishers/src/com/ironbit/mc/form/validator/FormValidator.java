package com.ironbit.mc.form.validator;


import java.util.ArrayList;

import com.ironbit.mc.system.errores.ErrorSys;

import android.content.Context;
import android.view.View;


public class FormValidator {
	private final ArrayList<ItemValidable> items = new ArrayList<ItemValidable>();
	protected boolean showErrorMessages = true;
	protected Context ctx = null;
	
	
	public void setShowErrorMessages(boolean showErrorMessages){
		this.showErrorMessages = showErrorMessages;
	}
	
	
	public FormValidator(Context ctx, boolean showErrorMessages){
		this.ctx = ctx;
		this.showErrorMessages = showErrorMessages;
	}
	
	
    public ItemValidable addItem(View itemView, String name, Validable extraValidation){
    	return addItem(new ItemValidable(ctx, itemView, name, extraValidation));
    }
    
    
    public ItemValidable addItem(View itemView, String name){
    	return addItem(itemView, name, null);
    }
    
    
    public ItemValidable addItem(ItemValidable item){
    	if (item != null){
    		items.add(item);
    	}
    	
    	return item;
    }
    
    
    public FormValidator removeItemForm(ItemValidable item){
    	items.remove(item);
    	return this;
    }


	public boolean validate() {
		int size = items.size();
		ItemValidable item = null;
		
		try{
			for (int a = 0; a < size; a++){
				item = items.get(a);
				item.validate();
			}
		}catch(ErrorSys e){
			if (showErrorMessages){
				e.showMsj(ctx);
			}
			
			return false;
		}
		
		return true;
	}
}