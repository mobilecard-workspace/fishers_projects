package com.ironbit.mc.location;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;

public class LocateUser {
	private LocationManager lm;
	private MyLocationListener ll;
	private Location loc;
	private Context ctx;
	 
	public LocateUser(Context ctx){
		this.ctx = ctx;
		lm =  (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        ll = new MyLocationListener();
      
	    //lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, ll);
	    loc = null;
	}
	
	public boolean startLocation(){
		boolean ret = false;
		System.out.println("Start Location");
		  lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, ll);
		  lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, ll);
		  
		  int n1 =  lm.getAllProviders().size();
		  List<String> lista = lm.getProviders(true);
		  int n2 = lm.getProviders(true).size();
		  System.out.println("n1: "+n1+" n2:"+n2);
		
		  if(n2 == 1 && lista.get(0).contains("passive")){
			  ret = true;
		  }
		 
		  
		  return ret;
	}
	
	public double getLatitud(){
		if(loc == null){
			 loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		 }
		if(loc == null){
			 loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		 }
		
		try{
			return loc.getLatitude();
		}catch(Exception e){
			return 0.0;
		}
	}
	
	public double getLongitud(){
		if(loc == null){
			 loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		 }
		if(loc == null){
			 loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		 }
		
		try{
			return loc.getLongitude();
		}catch(Exception e){
			return 0.0;
		}
	}
	
	
	
	
	public void stopLocation(){
		System.out.println("Stop Location");
		System.out.println("Lat:");
		System.out.println(getLatitud());
		System.out.println("lon: ");
		System.out.println(getLongitud());
		lm.removeUpdates(ll);
	}
}
