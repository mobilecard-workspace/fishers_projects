package com.ironbit.mc.location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

public class MyLocationListener implements LocationListener{

	
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		  if (location != null) {
		        Log.d("LOCATION CHANGED", location.getLatitude() + "");
		        Log.d("LOCATION CHANGED", location.getLongitude() + "");
		  }
	}

	
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		Log.d("Provider: ", provider);
	}

	
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

}
