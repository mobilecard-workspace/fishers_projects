package com.ironbit.mc.util;


import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ironbit.mc.system.Sys;
import com.ironbit.mc.system.errores.ErrorSys;
import com.ironbit.mc.web.util.WebUtil;

public class JsonUtils {
	
	/**
	 * Convierte un Hashtable a una representaci�n textual JSON. Normalmente usado para enviar multiples SqlQueries a un server
	 * @param hash. Hashtable con querys/comandos SQL y sus respectivos nombres
	 * @return String. una representaci�n textual de un json con formato {"query1":"select * from users", "query2":"select count(*) from horas"}
	 * @example
	 * Hashtable<String, String> queries = new Hashtable<String, String>();<br/>
	 * queries.add("personas", "SELECT * FROM personas");<br/>
	 * queries.add("camiones", "SELECT * FROM camiones");<br/><br/>
	 * hashtableToJsonString(queries);
	 * @throws ErrorSys
	 */
	public static String hashtableToJsonString(Hashtable<String, String> hash) throws ErrorSys{
		JSONObject json = new JSONObject();
		
		Enumeration<String> en = hash.keys();
		String key;
		
		try{
			while (en.hasMoreElements()){
				key = en.nextElement();
				json.put(key, WebUtil.encriptarParaQuery(hash.get(key)));
			}
		}catch(Exception e){
			throw new ErrorSys(e.getMessage());
		}
		
		return json.toString();
	}
	
	
	public static ArrayList<String> jsonArrayToArrayList(JSONArray jsonArray){
		ArrayList<String> array = new ArrayList<String>();
		
		try{
			int largo = jsonArray.length();
			for (int a = 0; a < largo; a++){
				array.add(jsonArray.getString(a));
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		return array;
	}
	
	
	public static JSONArray arrayListToJsonArray(ArrayList<String> arrayList){
		JSONArray jsonArray = new JSONArray();
		
		try{
			int largo = arrayList.size();
			for (int a = 0; a < largo; a++){
				jsonArray.put(arrayList.get(a));
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonArray;
	}
}
