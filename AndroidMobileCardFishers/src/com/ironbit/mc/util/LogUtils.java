package com.ironbit.mc.util;

import android.util.Log;

public class LogUtils {
	public static void logToFile(String text){
		Log.d("LOG_TO_FILE", text);
	}
	
	
	public static void logToFile(Exception e){
		try{
			StringBuffer sb = new StringBuffer(e.toString() + "\n");
			StackTraceElement[] stElements = e.getStackTrace();
			String newLine = "";
			
			for (StackTraceElement stElement : stElements){
				sb.append(newLine);
				sb.append("\tat ");
				sb.append(stElement.toString());
				newLine = "\n";
			}
			
			logToFile(sb.toString());
		}catch(Exception ee){
			e.printStackTrace();
		}
	}
}