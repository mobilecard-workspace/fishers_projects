package com.ironbit.mc.util;


import java.io.File;

import com.ironbit.mc.system.Sys;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

public class FileUtils {
	public static long getDirSizeInBytes(File dir) {
		long size = 0;
		if (dir.isFile()) {
			size = dir.length();
		} else {
			File[] subFiles = dir.listFiles();

			for (File file : subFiles) {
				if (file.isFile()) {
					size += file.length();
				} else {
					size += getDirSizeInBytes(file);
				}

			}
		}

		return size;
	}
	
	
	public static long getDirSizeInKilobytes(File dir) {
		return getDirSizeInBytes(dir) / 1024;
	}
	

	public static long getDirSizeInMegabytes(File dir) {
		return getDirSizeInBytes(dir) / 1024 / 1024;
	}
	
	
	public static String getRealPathFromURI(Activity ctx, Uri contentUri) {
		try{
			String[] proj = { MediaStore.Images.Media.DATA };
			Cursor cursor = ctx.managedQuery(
				contentUri, proj, // Which columns to return
				null, // WHERE clause; which rows to return (all rows)
				null, // WHERE clause selection arguments (none)
				null  // Order-by clause (ascending by name)
			); 
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
	
			return cursor.getString(column_index);
		}catch(Exception e){
			Sys.log(e);
			return "";
		}
	}
}