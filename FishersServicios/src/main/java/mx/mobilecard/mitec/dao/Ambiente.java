package mx.mobilecard.mitec.dao;

public enum Ambiente {
	DEV("https://dev.mitec.com.mx"),
	QA("https://qa3.mitec.com.mx"),
	PROD("https://ssl.e-pago.com.mx");  
	private final String ventaDirectaBandaMOTO="/pgs/cobroXml";
	private final String cancelacion="/pgs/CancelacionXml";
	private final String checkInBandaMOTO="/pgs/CheckInXml";
	private final String checkOut="/pgs/CheckOutXml";
	private final String cierrePreventa="/pgs/CierrePreventaXml";
	private final String preventa="/pgs/PreventaXml";
	private final String reAutorizacion ="/pgs/ReAutorizacionXml";
	private final String reImpresion ="/pgs/reImpresionXml";
	private final String ventaForzada ="/pgs/VentaForzadaXml";
	private final String threeds ="/ws3dsecure/Auth3dsecure";
	private final String url;
	Ambiente(String url){
		this.url=url;
	}
	public String getUrlVentaDirecta(){
		return this.url+ventaDirectaBandaMOTO;
	}
	public String getUrlCancelacion(){
		return this.url+cancelacion;
	}
	public String getUrlCheckInBandaMOTO(){
		return this.url+checkInBandaMOTO;
	}
	public String getUrlCheckOut(){
		return this.url+checkOut;
	}
	public String getUrlCierrePreventa(){
		return this.url+cierrePreventa;
	}
	public String getUrlPreventa(){
		return this.url+preventa;
	}
	public String getUrlReAutorizacion(){
		return this.url+reAutorizacion;
	}
	public String getUrlReImpresion(){
		return this.url+reImpresion;
	}
	public String getUrl3DS(){
		return this.url+threeds;
	}
}
