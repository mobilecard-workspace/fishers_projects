package mx.mobilecard.mitec.dao;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public class ConsumeServicioDAO {
    private Logger log = Logger.getLogger(ConsumeServicioDAO.class);
    

    public String consumeServicio(String urlServicio, HashMap parametros) {
        InputStreamReader isr = null;
        DataOutputStream writer;
        BufferedReader br = null;
        HttpURLConnection conn = null;
        URL url = null;
        String inputLine = null;
        Map<String, List<String>> map = null;
        StringBuffer sbf = new StringBuffer();
        String envio=null;
        try {
        	log.info("Consumiendo : "+urlServicio);
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                //conn.connect();
        	    //conn=(HttpURLConnection)url.openConnection();
        	    conn.setConnectTimeout(20000);
                conn.setRequestMethod("POST"); 
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                if (parametros != null) {
                    writer = new DataOutputStream(conn.getOutputStream());
                    envio=this.getQuery(parametros);
                    writer.writeBytes(envio);
                    writer.flush();
                    writer.close();
                }
                isr = new InputStreamReader(conn.getInputStream());
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }          
                map = conn.getHeaderFields();
            }
        } catch (Exception e) {
            log.fatal("Error al leer la url " + url, e);
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }
        }
        return sbf.toString();
    }

    private String getQuery(HashMap params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator it = params.entrySet().iterator();
        String p = null;
        Map.Entry pairs=null;
        while (it.hasNext()) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }
            pairs = (Map.Entry) it.next();
            log.info(pairs.getKey() + " = " + pairs.getValue());
            it.remove(); // avoids a ConcurrentModificationException
            result.append(pairs.getKey());
            result.append("=");
            result.append(pairs.getValue());
        }
        return result.toString();
    }
}
