package mx.mobilecard.mitec.service;

//import java.util.HashMap;
import java.util.HashMap;

import mx.mobilecard.mitec.crypto.TripleDESEncryption;
import mx.mobilecard.mitec.crypto.rc4;
import mx.mobilecard.mitec.dao.Ambiente;
import mx.mobilecard.mitec.dao.ConsumeServicioDAO;
import mx.mobilecard.mitec.operaciones.CENTEROFPAYMENTS;
import mx.mobilecard.mitec.operaciones.MitTransacction;
import mx.mobilecard.mitec.operaciones.RESPONSETX3DS;
import mx.mobilecard.mitec.operaciones.VMCAMEXM;
import mx.mobilecard.mitec.utils.Tarjeta;
//import mx.mobilecard.mitec.dao.BitacoraMitDAO;
//import mx.mobilecard.mitec.dao.ConsumeServicioDAO;
//import mx.mobilecard.mitec.operaciones.CENTEROFPAYMENTS;
//import mx.mobilecard.mitec.operaciones.MitTransacction;
import mx.mobilecard.mitec.operaciones.VMCAMEXAUTH3DS;
//import mx.mobilecard.mitec.operaciones.VMCAMEXM;
//import mx.mobilecard.mitec.operaciones.VMCAMEXMCANCELACION;
//import mx.mobilecard.mitec.operaciones.threeds.RESPONSETX3DS;
//import mx.mobilecard.mitec.operaciones.threeds.TRANSACTION3DS;
import mx.mobilecard.mitec.utils.Utils;
import mx.mobilecard.mitec.vo.AuthBusinessVO;
import mx.mobilecard.mitec.vo.AuthCompraVO;
import mx.mobilecard.mitec.vo.AuthCreditCardVO;
import mx.mobilecard.mitec.vo.AuthTransactionVO;
import mx.mobilecard.mitec.vo.BusinessVO;
import mx.mobilecard.mitec.vo.CompraVO;
import mx.mobilecard.mitec.vo.CreditCardVO;
import mx.mobilecard.mitec.vo.RespuestaVO;
import mx.mobilecard.mitec.vo.TransacctionVO;
//import mx.mobilecard.mitec.vo.BusinessVO;
//import mx.mobilecard.mitec.vo.CancelacionVO;
//import mx.mobilecard.mitec.vo.CompraVO;
//import mx.mobilecard.mitec.vo.CreditCardVO;
//import mx.mobilecard.mitec.vo.RespuestaVO;
//import mx.mobilecard.mitec.vo.TransacctionVO;

import org.apache.log4j.Logger;

import com.addcel.fishers.model.mapper.BitacorasMapper;
import com.addcel.fishers.model.vo.SucursalDatosMitecVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraMitAuthVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraMitVO;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

public class Operaciones {
    private Logger log = Logger.getLogger(Operaciones.class);
	private static Operaciones instance;
	private static Ambiente ambiente;
	private ConsumeServicioDAO consumeServicio=new ConsumeServicioDAO();
	private rc4 crpt=new rc4();
	//private BitacoraMitDAO bitacoraMitDAO;
	private static final String llave = "516883805150505753525754"; 

	public String obtenXMLAutenticacion(AuthCompraVO compra, BitacorasMapper bm){
		String encodedStrParametersXml = null;
		try{
			AuthBusinessVO authBusinessVO = this.getAuthBusiness3d(compra.getDatosCon());
			AuthCreditCardVO acc = new AuthCreditCardVO();
			acc.setCc_cvv(compra.getCreditCardCvv());
			acc.setCc_expMonth(compra.getCreditCardMonth());
			acc.setCc_expYear(compra.getCreditCardYear());
			acc.setCc_name(QuitaAcentos(compra.getCreditCardName()));
			acc.setCc_number(compra.getCreditCardNumber());
			AuthTransactionVO at = new AuthTransactionVO();
			at.setCreditcard(acc);
			at.setTx_amount(compra.getMonto());
			at.setTx_cobro("0");
			at.setTx_currency(compra.getCurrency());
			at.setTx_merchant(compra.getMerchant());
			at.setTx_reference(compra.getReference());
			at.setTx_urlResponse(compra.getUrlResponse());
			
			VMCAMEXAUTH3DS operacion = new VMCAMEXAUTH3DS();
			
			operacion.setBusiness(authBusinessVO);
			operacion.setTransacction(at);

			XStream xstream=new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("_-", "_")));
			xstream.autodetectAnnotations(true);
			String xml=xstream.toXML(operacion);
			xml = xml.replace("\n", "");
			xml = xml.replace(">  <", "><");
			xml = xml.replace(">    <", "><");							   
			xml = xml.replace(">      <", "><");
			log.info(xml);
			TBitacoraMitAuthVO ta = new TBitacoraMitAuthVO(authBusinessVO.getBs_idCompany(),authBusinessVO.getBs_idBranch(),authBusinessVO.getBs_country(), authBusinessVO.getBs_user(), authBusinessVO.getBs_pwd(),at.getTx_merchant(),at.getTx_reference(),at.getTx_amount(),at.getTx_currency(),at.getCreditcard().getCc_name(),at.getCreditcard().getCc_expMonth(),at.getCreditcard().getCc_expYear());
			bm.addMitBitacoraAuth(ta);
			encodedStrParametersXml = TripleDESEncryption.encrypt(xml, llave);
			
		} catch(Exception e){
			log.error(e.getMessage());
		}
		return encodedStrParametersXml;
	}
	public String validateAuth(RESPONSETX3DS res, BitacorasMapper bm){
		String validate = "false";
		boolean loop = true;
		try{
			if(res!=null){
				bm.updateMitBitacoraAuth(res);
				int i = 0;
				while(loop){
					String validCode = bm.getParametro("@FishersAuthValidCode"+i);
					if(validCode!=null){
						if(validCode.equals(res.getR3ds_responseCode())){
							validate = "true";
							loop = false;
						}
						i++;	
					}else
						loop = false;
				}
				if(i==0){
					log.info("No hay codigo valido definido");
					validate = "true";
				}					
			}
		}catch(Exception e){
			log.info("Error al determinar el resultado de la autenticacion");
		}
		return validate;
	}
	public RESPONSETX3DS respuestaAuth(String respXML){
		try{
			XStream xStream = new XStream(new DomDriver());
			xStream.alias("RESPONSETX3DS", RESPONSETX3DS.class);
			RESPONSETX3DS res = (RESPONSETX3DS)xStream.fromXML(respXML);
			return res;
		} catch(Exception e){
			log.error(e.getMessage());
			return null;
		}
	}
	
	public RespuestaVO ejecutaVMCAMEXM(CompraVO compra, String reference, BitacorasMapper bm){
		BusinessVO businessVO=null;
		TransacctionVO transacctionVO=null;
		CreditCardVO creditCardVO=null;
		VMCAMEXM vmcamexm=null;
		CENTEROFPAYMENTS respuestaMit=null;
		RespuestaVO respuestaVO=null;		
		try{
			businessVO=this.getBusiness(compra.getDatosCon());
			creditCardVO=new CreditCardVO();
			creditCardVO.setCrypto(compra.getDatosCon().getCrypto());
			creditCardVO.setType(compra.getCreditCardType().getTipo());
			creditCardVO.setName(crpt.StringToHexString(crpt.Salaa(QuitaAcentos(compra.getCreditCardName()),compra.getDatosCon().getSemilla())));
			creditCardVO.setExpmonth(crpt.StringToHexString(crpt.Salaa(compra.getCreditCardMonth(),compra.getDatosCon().getSemilla())));
			creditCardVO.setExpyear(crpt.StringToHexString(crpt.Salaa(compra.getCreditCardYear(),compra.getDatosCon().getSemilla())));
			creditCardVO.setNumber(crpt.StringToHexString(crpt.Salaa(compra.getCreditCardNumber(),compra.getDatosCon().getSemilla())));
			creditCardVO.setCvvcsc(crpt.StringToHexString(crpt.Salaa(compra.getCreditCardCvv(),compra.getDatosCon().getSemilla())));
			transacctionVO=new TransacctionVO();
			if(compra.getCreditCardType().equals(Tarjeta.VISA_MASTERCARD))
				transacctionVO.setMerchant(compra.getDatosCon().getAfiliacion());
			else
				transacctionVO.setMerchant(compra.getDatosCon().getAfiliacionAmex());			
			transacctionVO.setReference(reference);
			transacctionVO.setTp_operation(compra.getDatosCon().getTp_operation());
			transacctionVO.setAmount(compra.getMonto());
			transacctionVO.setCurrency(compra.getDatosCon().getCurrency());
			transacctionVO.setCreditcard(creditCardVO);
			transacctionVO.setUsrtransacction("");
			transacctionVO.setVersion(compra.getDatosCon().getVersion());
			vmcamexm=new VMCAMEXM();
			vmcamexm.setBusiness(businessVO);
			vmcamexm.setTransacction(transacctionVO);
			TBitacoraMitVO tbm = getTbm(vmcamexm); 
			bm.addMitBitacora(tbm);
			respuestaMit=enviaDatosMIT(vmcamexm,ambiente.getUrlVentaDirecta());
			bm.updateMitBitacora(getTbmR(respuestaMit,tbm.getIdBitacoraMIT().intValue()));
			respuestaVO=new RespuestaVO();
			respuestaVO.setAuth(respuestaMit.getAuth());
			respuestaVO.setCd_error(respuestaMit.getCd_error());
			respuestaVO.setDate(respuestaMit.getDate());
			respuestaVO.setFoliocpagos(respuestaMit.getFoliocpagos());
			respuestaVO.setNb_error(respuestaMit.getNb_error());
			respuestaVO.setReference(respuestaMit.getReference());
			respuestaVO.setResponse(respuestaMit.getResponse());
			respuestaVO.setTime(respuestaMit.getTime());
			log.info("respuestaVO: " + respuestaVO.toString());
		}catch(Exception e){
			log.error("Error en ejecutaVMCAMEXB : ",e);
		}
		return respuestaVO;
	}
	private TBitacoraMitVO getTbm(VMCAMEXM mitTransacction){
		TBitacoraMitVO tbm = new TBitacoraMitVO();
		try{
			tbm.setId_company(mitTransacction.getBusiness().getId_company());
			tbm.setId_branch(mitTransacction.getBusiness().getId_branch());
			tbm.setCountry(mitTransacction.getBusiness().getCountry());
			tbm.setUser(mitTransacction.getBusiness().getUser());
			tbm.setPwd(mitTransacction.getBusiness().getPwd());
			tbm.setMerchant(mitTransacction.getTransacction().getMerchant());
			tbm.setReference(mitTransacction.getTransacction().getReference());
			tbm.setTp_operation(mitTransacction.getTransacction().getTp_operation());
			tbm.setCrypto(mitTransacction.getTransacction().getCreditcard().getCrypto());
			tbm.setType(mitTransacction.getTransacction().getCreditcard().getType());
			tbm.setName(mitTransacction.getTransacction().getCreditcard().getName());
			tbm.setExpmonth(mitTransacction.getTransacction().getCreditcard().getExpmonth());
			tbm.setExpyear(mitTransacction.getTransacction().getCreditcard().getExpyear());
			tbm.setAmount(mitTransacction.getTransacction().getAmount());
			tbm.setCurrency(mitTransacction.getTransacction().getCurrency());
			tbm.setUsrtransacction(mitTransacction.getTransacction().getUsrtransacction());
			tbm.setVersion(mitTransacction.getTransacction().getVersion());
			tbm.setAuth(mitTransacction.getTransacction().getAuth());
			tbm.setOperacion(mitTransacction.getOperacion());
			tbm.setNo_operacion(mitTransacction.getTransacction().getNo_operacion());			
		}catch(Exception e){
			log.error("Error en getTbm : ",e);
		}
		return tbm;
	}
	private TBitacoraMitVO getTbmR(CENTEROFPAYMENTS resultado, int IdBitacora){
		TBitacoraMitVO tbm = new TBitacoraMitVO();
		try{
			tbm.setResponse(resultado.getResponse());
			tbm.setFoliocpagos(resultado.getFoliocpagos());
			tbm.setAuth(resultado.getAuth());
			tbm.setCd_response(resultado.getCd_response());
			tbm.setCd_error(resultado.getCd_error());
			tbm.setTime(resultado.getTime());
			tbm.setDate(resultado.getDate());
			tbm.setNb_error(resultado.getNb_error());
			tbm.setIdBitacoraMIT(new Long(IdBitacora));

		}catch(Exception e){
			log.error("Error en getTbmR : ",e);
		}
		return tbm;
	}

/*	
	public RespuestaVO ejecutaVMCAMEXMCANCELACION(CancelacionVO cancelacion){
		BusinessVO businessVO=null;
		TransacctionVO transacctionVO=null;
		VMCAMEXMCANCELACION vmcamexmcancelacion=null;
		CENTEROFPAYMENTS respuestaMit=null;
		RespuestaVO respuestaVO=null;
		int idBitacora=0;
		try{
			businessVO=this.getBusiness();
			transacctionVO=new TransacctionVO();
			transacctionVO.setAmount(cancelacion.getAmount());
			transacctionVO.setAuth(cancelacion.getAuth());
			transacctionVO.setNo_operacion(cancelacion.getNo_operacion());
			transacctionVO.setCrypto("3");
			
			vmcamexmcancelacion=new VMCAMEXMCANCELACION();
			vmcamexmcancelacion.setBusiness(businessVO);
			vmcamexmcancelacion.setTransacction(transacctionVO);
			idBitacora=bitacoraMitDAO.guardaBItacora(vmcamexmcancelacion);
			respuestaMit=enviaDatosMIT(vmcamexmcancelacion,ambiente.getUrlCancelacion());
			bitacoraMitDAO.actualizaBitacora(respuestaMit,idBitacora);
			respuestaVO=new RespuestaVO();
			respuestaVO.setAuth(respuestaMit.getAuth());
			respuestaVO.setCd_error(respuestaMit.getCd_error());
			respuestaVO.setDate(respuestaMit.getDate());
			respuestaVO.setFoliocpagos(respuestaMit.getFoliocpagos());
			respuestaVO.setNb_error(respuestaMit.getNb_error());
			respuestaVO.setReference(respuestaMit.getReference());
			respuestaVO.setResponse(respuestaVO.getResponse());
			respuestaVO.setTime(respuestaVO.getTime());
		}catch(Exception e){
			log.error("Error en ejecutaVMCAMEXMCANCELACION : ",e);
		}
		return respuestaVO;
	}
*/	
	
	private Operaciones(Ambiente ambiente){
		this.ambiente=ambiente;
		//bitacoraMitDAO=new BitacoraMitDAO();
	}
	public static Operaciones getInstance(Ambiente ambiente_){
		if(ambiente_!=ambiente){
			synchronized(Operaciones.class) {
                if (ambiente_!=ambiente) { 
                	instance=null;
                }
            }
		}
		if(instance==null){
			synchronized(Operaciones.class) {
                // En la zona sincronizada ser�a necesario volver
                // a comprobar que no se ha creado la instancia
                if (instance == null) { 
                	instance=new Operaciones(ambiente_);
                }
            }
		}
		return instance;
	}
	public static void main(String args[]){
		/*
		Ambiente a=Ambiente.QA;
		Ambiente b=null;
		System.out.print(a.getUrlCancelacion());
		System.out.print(b==Ambiente.QA);
		*/
		//String xml = "<RESPONSETX3DS><r3ds_reference>123456</r3ds_reference><r3ds_xid>564678</r3ds_xid><r3ds_eci>05</r3ds_eci><r3ds_cavv>1656151519961</r3ds_cavv><r3ds_authResult>Y</r3ds_authResult><r3ds_responseCode>R01</r3ds_responseCode><r3ds_responseDescription>Autenticacion exitosa</r3ds_responseDescription></RESPONSETX3DS>";
		//RESPONSETX3DS res = respuestaAuth(xml);		
	}
	//El m�todo "clone" es sobreescrito por el siguiente que arroja una excepci�n:
	public Object clone() throws CloneNotSupportedException {
	    	throw new CloneNotSupportedException(); 
	}
	private CENTEROFPAYMENTS enviaDatosMIT(MitTransacction operacion,String url){
		XStream xstream=null;
		CENTEROFPAYMENTS respuesta=null;
		String respuestaServicio=null;
		String xml=null;
		HashMap<String,String> parametros=null;
		try{
			xstream=new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("_-", "_")));
			xstream.autodetectAnnotations(true);
			xml=xstream.toXML(operacion);
			parametros=new HashMap<String,String>();
			log.info("ENVIO : "+xml);
			parametros.put("xml",xml);
			respuestaServicio=consumeServicio.consumeServicio(url, parametros);
			log.info("RECIBO : "+respuestaServicio);
			xstream.alias("CENTEROFPAYMENTS", CENTEROFPAYMENTS.class);
			respuesta = (CENTEROFPAYMENTS)xstream.fromXML(respuestaServicio);
		}catch(Exception e){
			log.info("Error al enviar los datos a MIT : ",e);
		}
		return respuesta;
	}

	private BusinessVO getBusiness(SucursalDatosMitecVO datosCon){
		BusinessVO businessVO=new BusinessVO();
		businessVO.setCountry(datosCon.getCountry());
		businessVO.setId_company(datosCon.getId_company());
		businessVO.setId_branch(datosCon.getId_branch());
		businessVO.setPwd(crpt.StringToHexString(crpt.Salaa(datosCon.getPwd(),datosCon.getSemilla())));
		businessVO.setUser(datosCon.getUser());
		return businessVO;
	}
	private AuthBusinessVO getAuthBusiness3d(SucursalDatosMitecVO datosCon){
		AuthBusinessVO authBusinessVO=new AuthBusinessVO();
		authBusinessVO.setBs_country(datosCon.getCountry3D());
		authBusinessVO.setBs_idCompany(datosCon.getId_company3D());
		authBusinessVO.setBs_idBranch(datosCon.getId_branch3D());
		authBusinessVO.setBs_pwd(datosCon.getPwd3D());
		authBusinessVO.setBs_user(datosCon.getUser3D());
		return authBusinessVO;
	}	
    private String QuitaAcentos(String cad) {
        String Res = cad;

        Res = Res.replace('á', 'a');
        Res = Res.replace('é', 'e');
        Res = Res.replace('í', 'i');
        Res = Res.replace('ó', 'o');
        Res = Res.replace('ú', 'u');
        Res = Res.replace('Á', 'A');
        Res = Res.replace('É', 'E');
        Res = Res.replace('Í', 'I');
        Res = Res.replace('Ó', 'O');
        Res = Res.replace('Ú', 'U');

        return Res;
    }

}
