package mx.mobilecard.mitec.operaciones;

public class RESPONSETX3DS {
	private String r3ds_reference;
	private String r3ds_xid;
	private String r3ds_eci;
	private String r3ds_cavv;
	private String r3ds_authResult;
	private String r3ds_responseCode;
	private String r3ds_responseDescription;
	public String getR3ds_reference() {
		return r3ds_reference;
	}
	public void setR3ds_reference(String r3ds_reference) {
		this.r3ds_reference = r3ds_reference;
	}
	public String getR3ds_xid() {
		return r3ds_xid;
	}
	public void setR3ds_xid(String r3ds_xid) {
		this.r3ds_xid = r3ds_xid;
	}
	public String getR3ds_eci() {
		return r3ds_eci;
	}
	public void setR3ds_eci(String r3ds_eci) {
		this.r3ds_eci = r3ds_eci;
	}
	public String getR3ds_cavv() {
		return r3ds_cavv;
	}
	public void setR3ds_cavv(String r3ds_cavv) {
		this.r3ds_cavv = r3ds_cavv;
	}
	public String getR3ds_authResult() {
		return r3ds_authResult;
	}
	public void setR3ds_authResult(String r3ds_authResult) {
		this.r3ds_authResult = r3ds_authResult;
	}
	public String getR3ds_responseCode() {
		return r3ds_responseCode;
	}
	public void setR3ds_responseCode(String r3ds_responseCode) {
		this.r3ds_responseCode = r3ds_responseCode;
	}
	public String getR3ds_responseDescription() {
		return r3ds_responseDescription;
	}
	public void setR3ds_responseDescription(String r3ds_responseDescription) {
		this.r3ds_responseDescription = r3ds_responseDescription;
	}
	@Override
	public String toString(){
		String res = "r3ds_reference: " + r3ds_reference +
		",r3ds_xid: " + r3ds_xid +
		", r3ds_eci: " + r3ds_eci + 
		", r3ds_cavv: " + r3ds_cavv +
		", r3ds_authResult: " + r3ds_authResult +
		", r3ds_responseCode: " + r3ds_responseCode +
		", r3ds_responseDescription	: " + r3ds_responseDescription;
		return res;
	}
}
