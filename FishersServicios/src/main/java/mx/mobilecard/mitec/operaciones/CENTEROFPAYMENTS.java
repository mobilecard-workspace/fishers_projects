package mx.mobilecard.mitec.operaciones;

public class CENTEROFPAYMENTS {
	private String reference;
	private String response;
	private String foliocpagos;
	private String auth;
	private String cd_response;
	private String cd_error;
	private String nb_error;
	private String time;
	private String date;
	private String nb_company;
	private String nb_merchant;
	private String nb_street;
	private String cc_type;
	private String tp_operation;
	private String cc_name;
	private String cc_number;
	private String cc_expmonth;
	private String cc_expyear;
	private String amount;
	private String cc_desc;
	private String voucher;
	private String avsauth;
    private String tokenb5;
    private String tokenb6;
    private String voucher_comercio;
    private String voucher_cliente;
    private String friendly_response;
    private String emv_key_date;
    private String icc_csn;
    private String icc_atc;
    private String icc_arpc;
    private String icc_issuer_script;
    private String authorized_amount;
    private String account_balance_1;
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getFoliocpagos() {
		return foliocpagos;
	}
	public void setFoliocpagos(String foliocpagos) {
		this.foliocpagos = foliocpagos;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public String getCd_response() {
		return cd_response;
	}
	public void setCd_response(String cd_response) {
		this.cd_response = cd_response;
	}
	public String getCd_error() {
		return cd_error;
	}
	public void setCd_error(String cd_error) {
		this.cd_error = cd_error;
	}
	public String getNb_error() {
		return nb_error;
	}
	public void setNb_error(String nb_error) {
		this.nb_error = nb_error;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNb_company() {
		return nb_company;
	}
	public void setNb_company(String nb_company) {
		this.nb_company = nb_company;
	}
	public String getNb_merchant() {
		return nb_merchant;
	}
	public void setNb_merchant(String nb_merchant) {
		this.nb_merchant = nb_merchant;
	}
	public String getNb_street() {
		return nb_street;
	}
	public void setNb_street(String nb_street) {
		this.nb_street = nb_street;
	}
	public String getCc_type() {
		return cc_type;
	}
	public void setCc_type(String cc_type) {
		this.cc_type = cc_type;
	}
	public String getTp_operation() {
		return tp_operation;
	}
	public void setTp_operation(String tp_operation) {
		this.tp_operation = tp_operation;
	}
	public String getCc_name() {
		return cc_name;
	}
	public void setCc_name(String cc_name) {
		this.cc_name = cc_name;
	}
	public String getCc_number() {
		return cc_number;
	}
	public void setCc_number(String cc_number) {
		this.cc_number = cc_number;
	}
	public String getCc_expmonth() {
		return cc_expmonth;
	}
	public void setCc_expmonth(String cc_expmonth) {
		this.cc_expmonth = cc_expmonth;
	}
	public String getCc_expyear() {
		return cc_expyear;
	}
	public void setCc_expyear(String cc_expyear) {
		this.cc_expyear = cc_expyear;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCc_desc() {
		return cc_desc;
	}
	public void setCc_desc(String cc_desc) {
		this.cc_desc = cc_desc;
	}
	public String getVoucher() {
		return voucher;
	}
	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}
	public String getAvsauth() {
		return avsauth;
	}
	public void setAvsauth(String avsauth) {
		this.avsauth = avsauth;
	}
	public String getTokenb5() {
		return tokenb5;
	}
	public void setTokenb5(String tokenb5) {
		this.tokenb5 = tokenb5;
	}
	public String getTokenb6() {
		return tokenb6;
	}
	public void setTokenb6(String tokenb6) {
		this.tokenb6 = tokenb6;
	}
	public String getVoucher_comercio() {
		return voucher_comercio;
	}
	public void setVoucher_comercio(String voucher_comercio) {
		this.voucher_comercio = voucher_comercio;
	}
	public String getVoucher_cliente() {
		return voucher_cliente;
	}
	public void setVoucher_cliente(String voucher_cliente) {
		this.voucher_cliente = voucher_cliente;
	}
	public String getFriendly_response() {
		return friendly_response;
	}
	public void setFriendly_response(String friendly_response) {
		this.friendly_response = friendly_response;
	}
	public String getEmv_key_date() {
		return emv_key_date;
	}
	public void setEmv_key_date(String emv_key_date) {
		this.emv_key_date = emv_key_date;
	}
	public String getIcc_csn() {
		return icc_csn;
	}
	public void setIcc_csn(String icc_csn) {
		this.icc_csn = icc_csn;
	}
	public String getIcc_atc() {
		return icc_atc;
	}
	public void setIcc_atc(String icc_atc) {
		this.icc_atc = icc_atc;
	}
	public String getIcc_arpc() {
		return icc_arpc;
	}
	public void setIcc_arpc(String icc_arpc) {
		this.icc_arpc = icc_arpc;
	}
	public String getIcc_issuer_script() {
		return icc_issuer_script;
	}
	public void setIcc_issuer_script(String icc_issuer_script) {
		this.icc_issuer_script = icc_issuer_script;
	}
	public String getAuthorized_amount() {
		return authorized_amount;
	}
	public void setAuthorized_amount(String authorized_amount) {
		this.authorized_amount = authorized_amount;
	}
	public String getAccount_balance_1() {
		return account_balance_1;
	}
	public void setAccount_balance_1(String account_balance_1) {
		this.account_balance_1 = account_balance_1;
	}

}
