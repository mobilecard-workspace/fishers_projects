package mx.mobilecard.mitec.operaciones;

import mx.mobilecard.mitec.vo.AuthBusinessVO;
import mx.mobilecard.mitec.vo.AuthTransactionVO;

public abstract class AuthMitTransacction {
	private AuthBusinessVO business;
	private AuthTransactionVO transaction;
	public AuthBusinessVO getBusiness() {
		return business;
	}
	public void setBusiness(AuthBusinessVO business) {
		this.business = business;
	}
	public AuthTransactionVO getTransacction() {
		return transaction;
	}
	public void setTransacction(AuthTransactionVO transacction) {
		this.transaction = transacction;
	}
	public abstract String getOperacion();
}
