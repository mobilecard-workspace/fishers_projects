package mx.mobilecard.mitec.operaciones;

import mx.mobilecard.mitec.vo.BusinessVO;
import mx.mobilecard.mitec.vo.TransacctionVO;

public abstract class MitTransacction {
	private BusinessVO business;
	private TransacctionVO transacction;
	public BusinessVO getBusiness() {
		return business;
	}
	public void setBusiness(BusinessVO business) {
		this.business = business;
	}
	public TransacctionVO getTransacction() {
		return transacction;
	}
	public void setTransacction(TransacctionVO transacction) {
		this.transacction = transacction;
	}
	public abstract String getOperacion();
}
