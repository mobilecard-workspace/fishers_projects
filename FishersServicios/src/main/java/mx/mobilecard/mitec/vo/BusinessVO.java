package mx.mobilecard.mitec.vo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("business")
public class BusinessVO {
	@XStreamAlias("id_company")
	private String id_company;
	@XStreamAlias("id_branch")
	private String id_branch;
	@XStreamAlias("country")
	private String country;
	@XStreamAlias("user")
	private String user;
	@XStreamAlias("pwd")
	private String pwd;
	public String getId_company() {
		return id_company;
	}
	public void setId_company(String id_company) {
		this.id_company = id_company;
	}
	public String getId_branch() {
		return id_branch;
	}
	public void setId_branch(String id_branch) {
		this.id_branch = id_branch;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
