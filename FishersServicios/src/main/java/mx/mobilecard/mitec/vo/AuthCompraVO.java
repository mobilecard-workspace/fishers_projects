package mx.mobilecard.mitec.vo;

import com.addcel.fishers.model.vo.SucursalDatosMitecVO;

public class AuthCompraVO extends CompraVO {
	private String merchant;
	private String reference;
	private String currency;
	private String urlResponse;
	private SucursalDatosMitecVO datosCon;
	public String getMerchant() {
		return merchant;
	}
	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getUrlResponse() {
		return urlResponse;
	}
	public void setUrlResponse(String urlResponse) {
		this.urlResponse = urlResponse;
	}
	public SucursalDatosMitecVO getDatosCon() {
		return datosCon;
	}
	public void setDatosCon(SucursalDatosMitecVO datosCon) {
		this.datosCon = datosCon;
	}
}
