package mx.mobilecard.mitec.vo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("transaction")
public class AuthTransactionVO {
	@XStreamAlias("tx_merchant")
	private String tx_merchant;
	@XStreamAlias("tx_reference")
	private String tx_reference;
	@XStreamAlias("tx_amount")
	private String tx_amount;
	@XStreamAlias("tx_currency")
	private String tx_currency;
	private AuthCreditCardVO creditcard;
	@XStreamAlias("tx_urlResponse")
	private String tx_urlResponse;
	@XStreamAlias("tx_cobro")
	private String tx_cobro;
	
	public String getTx_merchant() {
		return tx_merchant;
	}
	public void setTx_merchant(String tx_merchant) {
		this.tx_merchant = tx_merchant;
	}
	public String getTx_reference() {
		return tx_reference;
	}
	public void setTx_reference(String tx_reference) {
		this.tx_reference = tx_reference;
	}
	public String getTx_amount() {
		return tx_amount;
	}
	public void setTx_amount(String tx_amount) {
		this.tx_amount = tx_amount;
	}
	public String getTx_currency() {
		return tx_currency;
	}
	public void setTx_currency(String tx_currency) {
		this.tx_currency = tx_currency;
	}
	public AuthCreditCardVO getCreditcard() {
		return creditcard;
	}
	public void setCreditcard(AuthCreditCardVO creditcard) {
		this.creditcard = creditcard;
	}
	public String getTx_urlResponse() {
		return tx_urlResponse;
	}
	public void setTx_urlResponse(String tx_urlResponse) {
		this.tx_urlResponse = tx_urlResponse;
	}
	public String getTx_cobro() {
		return tx_cobro;
	}
	public void setTx_cobro(String tx_cobro) {
		this.tx_cobro = tx_cobro;
	}
	
}
