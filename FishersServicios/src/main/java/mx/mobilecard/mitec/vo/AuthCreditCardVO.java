package mx.mobilecard.mitec.vo;

import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("creditcard")
public class AuthCreditCardVO {
	@XStreamAlias("cc_name")
	private String cc_name;
	@XStreamAlias("cc_number")
	private String cc_number;
	@XStreamAlias("cc_expMonth")
	private String cc_expMonth;
	@XStreamAlias("cc_expYear")
	private String cc_expYear;
	@XStreamAlias("cc_cvv")
	private String cc_cvv;
	public String getCc_name() {
		return cc_name;
	}
	public void setCc_name(String cc_name) {
		this.cc_name = cc_name;
	}
	public String getCc_number() {
		return cc_number;
	}
	public void setCc_number(String cc_number) {
		this.cc_number = cc_number;
	}
	public String getCc_expMonth() {
		return cc_expMonth;
	}
	public void setCc_expMonth(String cc_expMonth) {
		this.cc_expMonth = cc_expMonth;
	}
	public String getCc_expYear() {
		return cc_expYear;
	}
	public void setCc_expYear(String cc_expYear) {
		this.cc_expYear = cc_expYear;
	}
	public String getCc_cvv() {
		return cc_cvv;
	}
	public void setCc_cvv(String cc_cvv) {
		this.cc_cvv = cc_cvv;
	}

}
