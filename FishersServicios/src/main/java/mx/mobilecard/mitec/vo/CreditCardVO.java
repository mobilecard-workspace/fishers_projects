package mx.mobilecard.mitec.vo;

import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("creditcard")
public class CreditCardVO {
	@XStreamAlias("crypto")
	private String crypto;
	@XStreamAlias("type")
	private String type;
	@XStreamAlias("name")
	private String name;
	@XStreamAlias("number")
	private String number;
	@XStreamAlias("expmonth")
	private String expmonth;
	@XStreamAlias("expyear")
	private String expyear;
	@XStreamAlias("cvv-csc")
	private String cvvcsc;


	public String getCrypto() {
		return crypto;
	}
	public void setCrypto(String crypto) {
		this.crypto = crypto;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getExpmonth() {
		return expmonth;
	}
	public void setExpmonth(String expmonth) {
		this.expmonth = expmonth;
	}
	public String getExpyear() {
		return expyear;
	}
	public void setExpyear(String expyear) {
		this.expyear = expyear;
	}
	public String getCvvcsc() {
		return cvvcsc;
	}
	public void setCvvcsc(String cvvcsc) {
		this.cvvcsc = cvvcsc;
	}
	
}
