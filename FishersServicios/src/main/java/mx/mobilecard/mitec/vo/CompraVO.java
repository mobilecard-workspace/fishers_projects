package mx.mobilecard.mitec.vo;

import com.addcel.fishers.model.vo.SucursalDatosMitecVO;

import mx.mobilecard.mitec.utils.Tarjeta;

public class CompraVO {
	private String creditCardNumber;
	private String creditCardMonth;
	private String creditCardYear;
	private String creditCardName;
	private String creditCardCvv;
	private Tarjeta creditCardType;
	private String monto;
	SucursalDatosMitecVO datosCon;
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	public String getCreditCardMonth() {
		return creditCardMonth;
	}
	public void setCreditCardMonth(String creditCardMonth) {
		this.creditCardMonth = creditCardMonth;
	}
	public String getCreditCardYear() {
		return creditCardYear;
	}
	public void setCreditCardYear(String creditCardYear) {
		this.creditCardYear = creditCardYear;
	}
	public String getCreditCardName() {
		return creditCardName;
	}
	public void setCreditCardName(String creditCardName) {
		this.creditCardName = creditCardName;
	}
	public String getCreditCardCvv() {
		return creditCardCvv;
	}
	public void setCreditCardCvv(String creditCardCvv) {
		this.creditCardCvv = creditCardCvv;
	}
	public Tarjeta getCreditCardType() {
		return creditCardType;
	}
	public void setCreditCardType(Tarjeta creditCardType) {
		this.creditCardType = creditCardType;
	}
	public SucursalDatosMitecVO getDatosCon() {
		return datosCon;
	}
	public void setDatosCon(SucursalDatosMitecVO datosCon) {
		this.datosCon = datosCon;
	}
	
}
