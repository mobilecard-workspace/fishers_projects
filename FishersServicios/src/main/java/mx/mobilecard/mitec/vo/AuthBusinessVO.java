package mx.mobilecard.mitec.vo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("business")
public class AuthBusinessVO {
	@XStreamAlias("bs_idCompany")
	private String bs_idCompany;
	@XStreamAlias("bs_idBranch")
	private String bs_idBranch;
	@XStreamAlias("bs_country")
	private String bs_country;
	@XStreamAlias("bs_user")
	private String bs_user;
	@XStreamAlias("bs_pwd")
	private String bs_pwd;
	public String getBs_idCompany() {
		return bs_idCompany;
	}
	public void setBs_idCompany(String bs_idCompany) {
		this.bs_idCompany = bs_idCompany;
	}
	public String getBs_idBranch() {
		return bs_idBranch;
	}
	public void setBs_idBranch(String bs_idBranch) {
		this.bs_idBranch = bs_idBranch;
	}
	public String getBs_country() {
		return bs_country;
	}
	public void setBs_country(String bs_country) {
		this.bs_country = bs_country;
	}
	public String getBs_user() {
		return bs_user;
	}
	public void setBs_user(String bs_user) {
		this.bs_user = bs_user;
	}
	public String getBs_pwd() {
		return bs_pwd;
	}
	public void setBs_pwd(String bs_pwd) {
		this.bs_pwd = bs_pwd;
	}

}
