package mx.mobilecard.mitec.vo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
@XStreamAlias("transacction")
public class TransacctionVO {
	@XStreamAlias("merchant")
	private String merchant;
	@XStreamAlias("reference")
	private String reference;
	@XStreamAlias("tp_operation")
	private String tp_operation;
	
	private CreditCardVO creditcard;
	@XStreamAlias("amount")
	private String amount;
	@XStreamAlias("currency")
	private String currency;
	@XStreamAlias("usrtransacction")
	private String usrtransacction;
	@XStreamAlias("emv")
	private String emv;
	@XStreamAlias("version")
	private String version;
	@XStreamAlias("no_operacion")
	private String no_operacion;
	@XStreamAlias("auth")
	private String auth;
	@XStreamAlias("crypto")
	private String crypto;
	
	public String getCrypto() {
		return crypto;
	}
	public void setCrypto(String crypto) {
		this.crypto = crypto;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	
	public String getNo_operacion() {
		return no_operacion;
	}
	public void setNo_operacion(String no_operacion) {
		this.no_operacion = no_operacion;
	}
	public String getMerchant() {
		return merchant;
	}
	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getTp_operation() {
		return tp_operation;
	}
	public void setTp_operation(String tp_operation) {
		this.tp_operation = tp_operation;
	}
	public CreditCardVO getCreditcard() {
		return creditcard;
	}
	public void setCreditcard(CreditCardVO creditcard) {
		this.creditcard = creditcard;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getUsrtransacction() {
		return usrtransacction;
	}
	public void setUsrtransacction(String usrtransacction) {
		this.usrtransacction = usrtransacction;
	}
	public String getEmv() {
		return emv;
	}
	public void setEmv(String emv) {
		this.emv = emv;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	

}
