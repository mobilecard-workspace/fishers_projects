package mx.mobilecard.mitec.vo;

public class RespuestaVO {
	 private String reference;
	 private String response;
	 private String foliocpagos;      
	 private String auth;
	 private String time;
	 private String date;
	 private String cd_error;
	 private String nb_error;
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getFoliocpagos() {
		return foliocpagos;
	}
	public void setFoliocpagos(String foliocpagos) {
		this.foliocpagos = foliocpagos;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCd_error() {
		return cd_error;
	}
	public void setCd_error(String cd_error) {
		this.cd_error = cd_error;
	}
	public String getNb_error() {
		return nb_error;
	}
	public void setNb_error(String nb_error) {
		this.nb_error = nb_error;
	}  
	@Override
	public String toString(){
		String res = "reference: " + reference +
		", response: " + response +
		", foliocpagos: " + foliocpagos +      
		", auth: " + auth + 
		", time: " + time +
		", date: " + date +
		", cd_error: " + cd_error +
		", nb_error: " + nb_error;
		return res;
	}
}
