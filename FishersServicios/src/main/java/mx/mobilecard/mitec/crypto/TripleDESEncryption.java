/**
 * 
 */
package mx.mobilecard.mitec.crypto;

import javax.crypto.*;
import javax.crypto.spec.*;

import java.security.*;
import java.security.spec.InvalidKeySpecException;

import sun.misc.*;

import java.io.*;


public class TripleDESEncryption {


	public TripleDESEncryption(){

	}

	/**
     * Encripta un String utilizando el algoritmo Triple DES
     * @param clearText Texto en ckaro a encriptar
     * @return texto encriptado en base 64
     */
    public static String encryptPgs (String clearText, String llave) {

		Key key = null;
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
			//key = skf.generateSecret(new DESedeKeySpec(secret));
			key = skf.generateSecret(new DESedeKeySpec(llave.getBytes()));
		}
		catch(NoSuchAlgorithmException nsae) {nsae.printStackTrace(); }
		catch(InvalidKeyException ike) {ike.printStackTrace(); }
        catch(InvalidKeySpecException ikse) {ikse.printStackTrace(); }

        String cipherTextB64 = "";

        try {

        // Necesitamos un cifrador
        Cipher cipher = Cipher.getInstance("DESede");

        // Ciframos el texto en claro
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte cipherText[] = cipher.doFinal(clearText.getBytes());

        // Codificamos el texto cifrado en base 64
        BASE64Encoder base64encoder = new BASE64Encoder();
        cipherTextB64 = base64encoder.encode(cipherText);

        }
        catch(NoSuchAlgorithmException nsae) {nsae.printStackTrace(); }
        catch(InvalidKeyException ike) {ike.printStackTrace(); }
        catch(NoSuchPaddingException nspe) {nspe.printStackTrace(); }
        catch(IllegalBlockSizeException ibse) {ibse.printStackTrace(); }
        catch(BadPaddingException bpe) {bpe.printStackTrace(); }

        // Retornamos el texto cifrado en BASE64


        return StringToHexString(cipherTextB64);
    }


    /**
     * Desencripta un testo cifrado en Triple DES i codificado en base 64
     * @param String cipherTextB64 Testo cifrado en Triple DES y codificado en B64
     * @return String Texto en claro
     */
    public static String decryptPgs (String cipherTextB64, String llave) {

		cipherTextB64 = hexStringToString(cipherTextB64);
    	Key key = null;
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("DESede");
            //key = skf.generateSecret(new DESedeKeySpec(secret));
            key = skf.generateSecret(new DESedeKeySpec(llave.getBytes()));
        }
        catch(NoSuchAlgorithmException nsae) {nsae.printStackTrace(); }
        catch(InvalidKeyException ike) {ike.printStackTrace(); }
        catch(InvalidKeySpecException ikse) {ikse.printStackTrace(); }

        String clearText = "";

        try {

        // Necesitamos un cifrador
        Cipher cipher = Cipher.getInstance("DESede");

        // La clave esta codificada en base 64
        BASE64Decoder base64decoder = new BASE64Decoder();
        byte cipherText[] = base64decoder.decodeBuffer(cipherTextB64);

        // Ciframos el texto en claro
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte bclearText[] = cipher.doFinal(cipherText);
        clearText = new String(bclearText);

        }
        catch(NoSuchAlgorithmException nsae) {nsae.printStackTrace(); }
        catch(NoSuchPaddingException nspe) {nspe.printStackTrace(); }
        catch(InvalidKeyException ike) {ike.printStackTrace(); }
        catch(IllegalBlockSizeException ibse) {ibse.printStackTrace(); }
        catch(BadPaddingException bpe) {bpe.printStackTrace(); }
        catch(IOException ioe) {ioe.printStackTrace(); }


        return clearText;
    }
       
	/**
	 * Encripta un String utilizando el algoritmo Triple DES.
	 * (Esta funci�n es compatible con php)
	 * @param clearText
	 * @param key
	 * @return
	 */
	public static String encrypt(String clearText, String key) {		
		String codeText = null;
		String strEncryptB64 = "";
		
		try {
			clearText = padString(clearText,8);
			SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "DESede");		

			Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, keyspec);
			byte[] encrypted = cipher.doFinal(clearText.getBytes());

			// Codificamos el texto cifrado en base 64
			BASE64Encoder base64encoder = new BASE64Encoder();
			strEncryptB64 = base64encoder.encode(encrypted);
			//pasamos el texto a hexadecimal
			codeText = StringToHexString(strEncryptB64);			
			
		} catch (InvalidKeyException e) {			
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		
		//System.out.println("***encrypt3DES:" + codeText);
		return codeText;
	}

	
	/**
	 * Desencripta un texto cifrado en Triple DES codificado en base 64
	 * (Esta funci�n es compatible con php)
	 * @param codeText
	 * @param key
	 * @return
	 */
	public static String decrypt( String codeText,String key) {
				
		String clearText = null;
		try {
			Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding");
			SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "DESede");	

			// La clave esta codificada en base 64
			BASE64Decoder base64decoder = new BASE64Decoder();
			byte cipherText[] = base64decoder.decodeBuffer(hexStringToString(codeText));
			
			cipher.init(Cipher.DECRYPT_MODE, keySpec);
			byte[] outText = cipher.doFinal(cipherText);
			clearText = new String(outText).trim();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//System.out.println("***decrypt3DES:" + clearText);
		return clearText;

	}

	/**
	 * Permite rellenar una cadena con espacios en blanco a la derecha
	 * @param source
	 * @param size
	 * @return
	 */
	private static String padString(String source, int size) {   
		  char paddingChar = ' ';   
		   
		  int padLength = size - source.length() % size;   
		  
		  for (int i = 0; i < padLength; i++) {   
		    source += paddingChar;   
		  }   
		  
		  return source;   
		}  
	
    /**
     * Convierte un hexadecimal a cadena
     * @param s
     * @return
     */
    public static String hexStringToString(String s)
    {
        String s1 = "";
        byte abyte0[] = new byte[s.length() / 2];
        for(int i = 0; i < abyte0.length; i++)
        {
            int j = i * 2;
            int k = Integer.parseInt(s.substring(j, j + 2), 16);
            s1 = s1 + (char)k;
        }

        return s1;
    }

    /**
     * Convierte una cadena a hexadecimal
     * @param s
     * @return
     */
    public static String StringToHexString(String s)
    {
        StringBuffer stringbuffer = new StringBuffer(s.length() * 2);
        for(int i = 0; i < s.length(); i++)
        {
            int j = s.charAt(i) & 0xff;
            if(j < 16)
                stringbuffer.append('0');
            stringbuffer.append(Integer.toHexString(j));
        }

        return stringbuffer.toString().toUpperCase();
    }

    /**
     * 
     * @param args
     */
	public static void main(String[] args) {
		String semilla = "516883805150505753525754";
		String encriptada = "386A597159354F50773031494B6F4C484F5666553267724A684B7131796578553142694D6B682F506B6846724C76746C3037496A7768444A637032523850384F5378364D4E6C3835654969580A626972316677766456374572436A4B557071717A744B566F496B754B6C4E357A2F535A6948414D3437794A56382B4E4C54556B6C715A6C375A4E766342784E6B472F544C617066396B3561770A566E6871445A506D656C4D523347573377763668772B6D45504337506D63796D6E5446514D393441314D684E7A466C6F5933503939684E57452B4274414A454D672F6A6351695A47714939660A64696C706D34484F74647375323863502B4D387458547776384C57354B4C6352623932314A7364416B6148766F69704739376376467576726E55414439306F6D797A567461383039535A69490A50576C577933424163776C4A6C4D666479455547514B675A423752576B54372F515379674D6F3346674F426278432F78684C4561734D30542B4E6335616E496D33684A4F4768646D3164422F0A36476156386C4F57764B616E587941524F573438563968444350424C507074574D63627966675A65574F6E474D7575475267714F544A764854484572427436394C77712F";
		String enClaro = "";
		enClaro = TripleDESEncryption.decrypt( encriptada,semilla);
		
		System.out.println("encriptada: " + encriptada);
		System.out.println("enClaro: " + enClaro);
		
			
		encriptada = TripleDESEncryption.encrypt("HOLA A TODOS", semilla);
		enClaro = TripleDESEncryption.decrypt( encriptada,semilla);
		System.out.println("encriptada2: " + encriptada);
		System.out.println("enClaro2: " + enClaro);
	}
//5482440197906692
}



