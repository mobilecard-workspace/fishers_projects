package mx.mobilecard.mitec.utils;

public enum Tarjeta {
	VISA_MASTERCARD("V/MC"),
	AMEX("AMEX");
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	private String tipo;
	Tarjeta(String tipo){
		this.tipo=tipo;
	}
}
