package mx.mobilecard.mitec.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


import org.apache.log4j.Logger;

public class Utils {
	private static Logger log = Logger.getLogger(Utils.class);
	private static Properties prop;
	public static String getPropiedad(String p){
		String propiedad=null;
		try{
			if(prop==null){
				obtenPropiedades();
			}
			propiedad=prop.getProperty(p);
		}catch(Exception e){
			log.error("Error al obtener propiedad : ",e);
		}
		return propiedad;
	}
	private static void obtenPropiedades(){
		InputStream input = null;
		String filename = "Application.properties";
		try {
			prop=new Properties();
    		input = Utils.class.getClassLoader().getResourceAsStream(filename);
    		if(input==null){
    	            System.out.println("Sorry, unable to find " + filename);
    		    return;
    		}
			prop.load(input);
		} catch (Exception io) {
			io.printStackTrace();
			log.error("Error al leer InputStream : ",io);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					log.error("Error al cerrar InputStream : ",e);
				}
			}
	 
		}
	}
}
