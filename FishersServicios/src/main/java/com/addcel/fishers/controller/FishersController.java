package com.addcel.fishers.controller;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.fishers.model.vo.UsuarioVO;
import com.addcel.fishers.services.FishersPagosService;
import com.addcel.fishers.services.FishersPagoAMEX;
import com.addcel.fishers.services.FishersService;
import com.addcel.fishers.model.vo.pagos.MitComVO;
import com.addcel.fishers.model.vo.pagos.TransactionMitecVO;
import com.addcel.fishers.model.vo.response.ResponseProcesaMitec;
import com.addcel.utils.AddcelCrypto;

/**
 * Handles requests for the application home page.
 */
@Controller
public class FishersController {
	
	private static final Logger logger = LoggerFactory.getLogger(FishersController.class);
	@Autowired
	private FishersService fService;
	@Autowired
	private FishersPagosService fpService;
	@Autowired
	private FishersPagoAMEX amexService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);				
		return "home";
	}
	@RequestMapping(value = "/getMarcas",method=RequestMethod.POST)
	public @ResponseBody String listaMarcas() {	
		logger.info("Dentro del servicio: /getMarcas");
		return fService.listaMarcas();
	}
	@RequestMapping(value = "/getSucursales",method=RequestMethod.POST)
	public @ResponseBody String listaSucursales(@RequestParam("json") String data) {	
		logger.info("Dentro del servicio: /getCajas");
		return fService.listaSucursales(data);
	}
	@RequestMapping(value = "/getToken")
	public @ResponseBody String getToken() {		
		logger.info("Dentro del servicio: /getToken");
		return fService.generaToken();
	
	}
	@RequestMapping(value = "/consultaCuenta", method=RequestMethod.POST)
	public @ResponseBody String consultaCuenta(@RequestParam("json") String data){
		logger.info("Dentro del servicio: /consultaCuenta");
		return fService.getCuenta(data);
	}
	
	@RequestMapping(value = "/pago-3dSecureAmex")
	public ModelAndView datosPago(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /pago-3dSecureAmex");
		ModelAndView mav=new ModelAndView("error");
		MitComVO procom = fpService.procesaPago(jsonEnc,null,2);
		if(procom!=null){			
			if(procom.getIdError()==0)
				mav=new ModelAndView("comerciofin");
			mav.addObject("mitec", procom);
			mav.addObject("processId", "1");
		}
		return mav;	
	}
	@RequestMapping(value = "/pago-3dSecure")
	public ModelAndView datosPagoAmex(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /pago-3dSecure");
		ModelAndView mav=new ModelAndView("error");
		MitComVO procom = null;
		try{
			UsuarioVO usuario = fService.getUsuario(jsonEnc);
			if(usuario!=null){
				switch(usuario.getIdTipoTarjeta()){
					case 1:
					case 2:
						procom = fpService.procesaPago(jsonEnc, usuario,1);
						break;
					case 3:	
						procom = fpService.procesaPago(jsonEnc, usuario,2);
						break;
				}
			} else
				procom = fpService.procesaPago(jsonEnc, null,1);
		}catch(Exception e){
			procom = new MitComVO(2,"Error en el pago: " + e.getMessage());
		}
		if(procom!=null){			
			if(procom.getIdError()==0)
				mav=new ModelAndView("comerciofin");
			mav.addObject("mitec", procom);
			mav.addObject("processId", "1");
		}
		return mav;	
	}
	
	@RequestMapping(value = "/pago-visa")
	public @ResponseBody String datosPagoVisa(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /pago-visa");
		SimpleDateFormat formato = new SimpleDateFormat("ddhhmmss");
		String response=null;
		try{
			UsuarioVO usuario = fService.getUsuario(jsonEnc);
			if(usuario!=null){
				switch(usuario.getIdTipoTarjeta()){
					case 1:
					case 2:
						response = fpService.procesaPagoVisa(jsonEnc,usuario);
						break;
					case 3:	
						response = amexService.procesaPago(jsonEnc, usuario);
						break;
				}
			}
			else
				response = fpService.procesaPagoVisa(jsonEnc, null);
		} catch(Exception e){
			response = AddcelCrypto.encryptSensitive(formato.format(new Date()),
					"{\"idError\":1,\"mensajeError\":\"La Transacción no su pudo completar, favor de reintentar más tarde\"}");
			logger.error("Error al realizar el pago: " + e.getMessage());
		}
			
		return response;
	}
	
	@RequestMapping(value = "/pago-amex") 
	public @ResponseBody String pagoAmex(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: //pago-amex");
		return amexService.procesaPago(data, null);
	}

	@RequestMapping(value = "/download")
	public String download(Model model) {
		logger.info("Dentro del servicio: /download");
		return "download";	
	}	
	@RequestMapping(value = "/busquedaPagos",method=RequestMethod.POST) 
	public @ResponseBody String busquedaPagos(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /busquedaPagos");
		return fService.busquedaPagos(data);
	}
	
	@RequestMapping(value = "/reenvioRec",method=RequestMethod.POST) 
	public @ResponseBody String reenvioRec(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /reenvioRec");
		return fService.reenvioRecibo(data);
	}
	
	@RequestMapping(value = "/comercio-con",method=RequestMethod.POST)
	public ModelAndView respuestaMitec(HttpServletRequest request) {
	
		
		String strResponse = null;
		String strIdCompany = null;
		String strIdMerchant = null;
		String reference = null;
		String flujoid = null;

		logger.info("Dentro del servicio: /comercio-con");
		ModelAndView mav=new ModelAndView("error");
		ResponseProcesaMitec respuesta = null;
						
		@SuppressWarnings("unchecked")
		Map<String, String[]> parameters = request.getParameterMap();

	    for(String key : parameters.keySet()) {
	        logger.info(key);
	        String[] vals = parameters.get(key);
	        if(key.equals("strResponse"))
	        	strResponse = vals[0];
	        else if(key.equals("strIdCompany"))
	        	strIdCompany = vals[0];
	        else if(key.equals("strIdMerchant"))
	        	strIdMerchant = vals[0];
	        else if(key.equals("referencia"))
	        	reference = vals[0];
	        else if(key.equals("flujoid"))
	        	flujoid = vals[0];
	        
	        for(String val : vals)
	        	logger.info(" -> " + val);
	    }
		
		TransactionMitecVO tp = new TransactionMitecVO();
		tp.setReference(reference);
		tp.setStrIdCompany(strIdCompany);
		tp.setStrIdMerchant(strIdMerchant);
		tp.setStrResponse(strResponse);
		if(flujoid==null)
			tp.setFlujoid("VISA");
		else
			tp.setFlujoid(flujoid);
		try{		
			respuesta = fpService.procesaRespuestaMitec(tp);
		} catch(Exception e){
			logger.error("Error al ejecutar pago: " + e.getMessage());
			respuesta = new ResponseProcesaMitec();
			respuesta.setRespuesta("error");
			respuesta.setProcessId("2");
		}
		if(respuesta != null ){			
			mav = new ModelAndView(respuesta.getRespuesta());
			mav.addObject("mitec", respuesta);
			mav.addObject("processId", respuesta.getProcessId());
		}
		return mav;	
	}	
}