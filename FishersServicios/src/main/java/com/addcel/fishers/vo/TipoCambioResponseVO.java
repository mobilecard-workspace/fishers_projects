package com.addcel.fishers.vo;

public class TipoCambioResponseVO {
	
	private String codeError;
	private String msgError;
	public String getCodeError() {
		return codeError;
	}
	public void setCodeError(String codeError) {
		this.codeError = codeError;
	}
	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

}
