package com.addcel.fishers.utils;

import java.io.BufferedReader;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.fishers.model.vo.CorreoVO;
import com.addcel.fishers.model.vo.FishersCuentaDetalleVO;
import com.addcel.fishers.model.vo.response.DetalleVO;
import com.addcel.fishers.utils.UtilsService;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	static Locale locale = new Locale("es","MX");
	static NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
	private static final String urlString = "http://localhost:8080/MailSenderAddcel/enviaCorreoAddcel";
	
	
	public static boolean generatedMail(DetalleVO detalleVO, UtilsService utilsService, String sucursal){
		logger.info("Genera objeto para envio de mail: " + detalleVO.getEmail());
		boolean res = false;
		try{
			CorreoVO correo = new CorreoVO();
			String mailBody = "<!DOCTYPE html><html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">" +
					"<style type=\"text/css\">  body { background: none repeat scroll 0 0 #FFFFFF; font-family: 'Open Sans', sans-serif;" +
					" font-size: 14px;  } </style></head><body data-twttr-rendered=\"true\"><div style=\"width: 680px; margin: 0 auto\">" +
					"<table width=\"60%\" align=\"center\"><tbody><tr><td><img src=\"cid:identifierCID00\" align=\"middle\"></td><td></td></tr></tbody></table>" +
					"<p style=\"font-weight: bold; font-size: 16px\">Estimado ";
			
			if(detalleVO.getNombre()!=null && !detalleVO.getNombre().equals(""))
				mailBody += detalleVO.getNombre();
			else
				mailBody += "Cliente";
			
			mailBody += "</p><br><p> Usted ha realizado un Pago usando el servicio <span style=\"font-size: 16px; font-weight: bold;\">" +
					"LA CUENTA POR FAVOR: </span>.</p><p>Con los siguientes datos:</p><p></p><table width=\"60%\">";
			
			mailBody += tableSum("Sucursal", sucursal,"","40","20","40");
			
			mailBody += tableSum("Número de cuenta",detalleVO.getNumero_cuenta(), "","40","20","40");
			
			mailBody += "</table><br>";
			/*		
			if(detalleVO.getComida()!=null && detalleVO.getComida().size()>0){
				if(detalleVO.getBebida()!=null && detalleVO.getBebida().size()>0)
					mailBody += detalle(detalleVO.getComida(),"COMIDA","above");
				else if (detalleVO.getOtros()!=null && detalleVO.getOtros().size()>0)
					mailBody += detalle(detalleVO.getComida(),"COMIDA","above");
				else
					mailBody += detalle(detalleVO.getComida(),"COMIDA","hsides");
			}
			if(detalleVO.getBebida()!=null && detalleVO.getBebida().size()>0){
				if (detalleVO.getOtros()!=null && detalleVO.getOtros().size()>0)
					mailBody += detalle(detalleVO.getBebida(),"BEBIDA","above");
				else
					mailBody += detalle(detalleVO.getBebida(),"BEBIDA","hsides");
			}
			if(detalleVO.getOtros()!=null && detalleVO.getOtros().size()>0){
				mailBody += detalle(detalleVO.getOtros(),"OTROS","hsides");
			}
			*/
			mailBody += "<table width=\"60%\">";
			
			mailBody += tableSum("Subtotal", nf.format(detalleVO.getSubtotal())," align=\"right\" ","30","50","20");
			
			mailBody += tableSum("Descuento", nf.format(detalleVO.getDescuento())," align=\"right\" ","30","50","20");
			
			mailBody += tableSum("Propina", nf.format(detalleVO.getPropina())," align=\"right\" ","30","50","20");
			
			mailBody += tableSum("Comision", nf.format(detalleVO.getComision())," align=\"right\" ","30","50","20");
			
			mailBody += tableSum("Total Pagado", nf.format(detalleVO.getTotal_pago())," align=\"right\" ","30","50","20");
			
			mailBody += "</table>";
			
			mailBody += "<br><br><p style=\"font-size: 14px\">Nota 1. El total pagado puede no cubrir " +
					"el total de la cuenta, ya que se aceptan pagos parciales.</p><p style=\"font-size: " +
					"14px\">Nota 2. Este correo es de carácter informativo, no es necesario que responda al " +
					"mismo.</p><p style=\"font-size: 14px\"><b>Gracias por usar MobileCard.</b></p></div>" +
					"</body></html>";
			
			String from = "no-reply@addcel.com";
			String subject = "Acuse Pago Fishers - Referencia: " + detalleVO.getReferencia();
			String[] to = {detalleVO.getEmail()};
			String[] cid = {"/usr/java/resources/images/Fishers/fishers-logo.png"};
			
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(mailBody);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			sendMail(utilsService.objectToJson(correo));
			logger.info("Fin proceso de envio email");
			res = true;
		}catch(Exception e){
			logger.error("Error al enviar el correo: " + e.getMessage());
		}
		return res;
	}	

	private static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			logger.info("data = " + data);

			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
	private static String detalle(List<FishersCuentaDetalleVO> detalles, String categoria, String frame){
		String detalleBody="";
		try{
			detalleBody = "<table width=\"60%\"><tr><td><span style=\"font-size: 14px; font-weight: bold;\">" + categoria +":" +
					"</span></td><td></td></tr></table><table width=\"60%\" frame=\"" + frame +"\"><tr><td width=\"30%\">" +
					"<span style=\"font-size: 16px; font-weight: bold;\">Cantidad</span></td><td width=\"50%\">" +
					"<span style=\"font-size: 16px; font-weight: bold;\">Producto</span></td><td width=\"20%\">" +
					"<span style=\"font-size: 16px; font-weight: bold;\">Importe</span></td></tr>";
			
			for(FishersCuentaDetalleVO detalle : detalles){
				detalleBody += "<tr><td>" + detalle.getCantidad() +"</td><td>" + detalle.getDescripcion_producto() + 
					"</td><td align=\"right\">" + nf.format(detalle.getImporte()) + 
					"</td></tr>";
			}			
			detalleBody +="</table><br>";
		}catch(Exception e){
			logger.error("Error en: detalle;" + e.getMessage() );
		}
		return detalleBody;
	}
	private static String tableSum(String concepto, String valor, String align, String width1, String width2, String width3){
		String tr="";
		try{
			tr = "<tr><td width=\""+width1+"%\"><span style=\"font-size: 14px; font-weight: bold;\">" + concepto
					+ ":</span></td><td width=\""+width2+"%\"></td><td width=\""+width3+"%\" " + align + " >" + valor +
					"</span></td></tr>";
		}catch(Exception e){
			logger.error("Error en: tableSum;" + e.getMessage() );
		}
		return tr;
	}
}
