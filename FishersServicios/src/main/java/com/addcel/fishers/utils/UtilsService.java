package com.addcel.fishers.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.utils.AddcelCrypto;

@Component
public class UtilsService {
	private static final String patron = "ddhhmmss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	@Autowired
	private ObjectMapper mapperJk;	
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json=mapperJk.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			logger.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			logger.error("2.- Error al mapperar objeccto: {}",e);
		} catch (IOException e) {
			logger.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (JsonParseException e) {
			logger.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			logger.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			logger.error("Error al parsear cadena json 3: {}", e);
		}
		return obj;
	}
	
	public String peticionUrlPostParams(String urlRemota,String parametros){
		String respuesta = null;
		//http://50.57.192.210:8080/Conversor/currency?fromCurrency=usd&toCurrency=MXN
		URL url;
		try {
			url = new URL(urlRemota);		
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		// Se indica que se escribira en la conexi�n
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		// Se escribe los parametros enviados a la url
		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());		
		wr.write(parametros);
		wr.close();			
		if(con.getResponseCode() == HttpURLConnection.HTTP_OK ||
				con.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED){			
			BufferedReader br = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String jsonString;
			while ((jsonString = br.readLine()) != null) {
				sb.append(jsonString);
			}
			respuesta=sb.toString();			
		}else{
			respuesta = "ERROR";
		}
		con.disconnect();
		} catch (MalformedURLException e) {
			logger.error("ERROR URL malformada: {}",e);
		} catch (IOException e) {
			logger.error("ERROR IO : {}",e);
		}
		return respuesta;
	}
	public String sendOperWS(String urlString, String datos, String endpoint, int Oper){

		String json = AddcelCrypto.encryptSensitive(formato.format(new Date()),"{\"idError\":1, \"mensajeError\":\"Servicio de consulta no disponible\"}");

		String data = "endpoint="+endpoint+"&oper="+Oper+"&json="+datos;
		
		logger.info("Enviando a servicio: " + data);

		try{		
			URL url = new URL(urlString);
		
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
		
			writter.write(data);
			writter.flush();

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line;
			StringBuilder sb = new StringBuilder();

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			
			json = sb.toString();
		} catch(Exception e){
			logger.error("Hubo un error al invocar al wrapper de ws: " + e.getMessage());
			
		}
		return json;
	}
}
