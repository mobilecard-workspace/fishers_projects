package com.addcel.fishers.services;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.addcel.fishers.model.mapper.FishersMapper;
import com.addcel.fishers.model.vo.FishersCuentaVO;
import com.addcel.fishers.model.vo.FishersCuentaDetalleVO;
import com.addcel.fishers.model.vo.FishersDetallePagoVO;
import com.addcel.fishers.model.vo.MarcaVO;
import com.addcel.fishers.model.vo.SucursalVO;
import com.addcel.fishers.model.vo.UsuarioVO;
import com.addcel.fishers.model.vo.request.ConsultaRequestVO;
import com.addcel.fishers.model.vo.request.DatosPago;
import com.addcel.fishers.model.vo.request.RequestCuenta;
import com.addcel.fishers.model.vo.request.RequestBusquedaPagos;
import com.addcel.fishers.model.vo.response.DetalleVO;
import com.addcel.fishers.utils.AddCelGenericMail;
import com.addcel.fishers.utils.UtilsService;

import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.json.me.JSONObject;

import crypto.Crypto;

@Service
public class FishersService {
	private static final Logger logger = LoggerFactory
			.getLogger(FishersService.class);
	private static final String KEY_PASSWORD="1234567890ABCDEF0123456789ABCDEF";
	private static final String patron = "ddhhmmss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);

	@Autowired
	private FishersMapper mapper;
	@Autowired
	private UtilsService utilService;	
	
	public String listaSucursales(String data) {
		String dataBack = "";
		String json = AddcelCrypto.decryptHard(data);
		try{
			JSONObject jsObject = new JSONObject(json);
			Long id_marca = Long.parseLong(jsObject.getString("id_marca"));
			if(id_marca==null){
					dataBack = "{\"idError\":1,\"mensajeError\":\"Falta el parametro id_sucursal.\"}";
			} else{
				List<SucursalVO> sucursales = mapper.listaSucursales(id_marca);
				if(sucursales.size()>0){
					dataBack = utilService.objectToJson(sucursales);
					dataBack = "{\"sucursales\":" + dataBack + "}";
				}else
					dataBack = "{\"idError\":2,\"mensajeError\":\"No existen sucursales para esa marca.\"}";
			}
		}catch(Exception e){
			dataBack = "{\"idError\":3,\"mensajeError\":\"Ocurrio un error al consultar las sucursales de la marca.\"}";
			logger.error("Ocurrio un error al consultar las sucursales de la marca: " + e.getMessage());
		}
		dataBack = AddcelCrypto.encryptHard(dataBack);
		return dataBack;
	}
	
	public String generaToken(){				
		String token=AddcelCrypto.encryptHard(mapper.getFechaActual());
		String json="{\"token\":\""+token+"\"}";
		json=AddcelCrypto.encryptHard(json);		
		return json;
	}

	public String generaPassword() {
		String kk = parsePass(KEY_PASSWORD);				
		String sPwd = (random(5869452) + random(5869452) + random(5869452)+ random(5869452))
				.substring(0, 10);
		logger.info("Password: {}",sPwd);
		sPwd =Crypto.aesEncrypt(kk, sPwd);				
		logger.info("Password enc: {}",sPwd);		
		return sPwd;
	}
	
	public String encriptaPassword(String password){
		String kk = parsePass(KEY_PASSWORD);
		logger.debug("password: {}",password);
		password =Crypto.aesEncrypt(kk, password);		
		logger.info("Password enc: {}",password);
		return password;
	}

	public String random(int n) {
		java.util.Random rand = new java.util.Random();
		int x = rand.nextInt(n);
		return "" + x;
	}		

	private String parsePass(String pass) {
		int len = pass.length();
		String key = "";

		for (int i = 0; i < 32 / len; i++) {
			key += pass;
		}

		int carry = 0;
		while (key.length() < 32) {
			key += pass.charAt(carry);
			carry++;
		}
		return key;
	}
	
	
	public String busquedaPagos(String data) {
		
		List<DetalleVO> res = null;
		RequestBusquedaPagos request = 
				(RequestBusquedaPagos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestBusquedaPagos.class);
		
		if(Integer.parseInt(request.getIdUser())==0)
			request.setIdUser(null);
		
		res = getDetalles(request.getIdUser(), request.getFechaIni(), request.getFechaFin(), null);
				
		return AddcelCrypto.encryptHard(utilService.objectToJson(res));		
	}
	
	public String reenvioRecibo(String data) {
		List<DetalleVO> detalleVO = null;
		RequestBusquedaPagos request = null;
		String json = null;
		String emailAlterno=null;
		try{
			request = (RequestBusquedaPagos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestBusquedaPagos.class);
			if(request.getIdUser() == null){
				json = "{\"idError\":2,\"mensajeError\":\"Falta el parametro idUsuario.\"}";
			}else if(request.getIdBitacora() == null){
				json = "{\"idError\":3,\"mensajeError\":\"Falta el parametro idBitacora.\"}";
			}else{
				emailAlterno = request.getEmail();
				logger.info("emailAlterno: " + emailAlterno);
				detalleVO = getDetalles(request.getIdUser(), request.getFechaIni(), request.getFechaFin(), null);
				
				if(detalleVO != null && detalleVO.size() >0){
					logger.info("Se obtuvieron datos, inicia proceso de reenvio de correo.");
					if(emailAlterno!=null)
						detalleVO.get(0).setEmail(emailAlterno);
					logger.info("Enviando correo a " + detalleVO.get(0).getEmail());
					if(detalleVO.get(0).getEmail()!=null){						
						boolean envio = AddCelGenericMail.generatedMail(detalleVO.get(0),
								utilService, 
								mapper.descripcionSucursal(detalleVO.get(0).getId_sucursal()));
						if(envio)
							json = "{\"idError\":0,\"mensajeError\":\"Recibo reenviado correctamente.\"}";
						else
							json = "{\"idError\":1,\"mensajeError\":\"El recibo no se pudo enviar, reintente de nuevo más tarde.\"}";
					}
					else
						json = "{\"idError\":1,\"mensajeError\":\"El recibo no se pudo enviar, no existe cuenta de correo asociada a este recibo.\"}";						
				}else{
					logger.error("ID_BITACORA: " + request.getIdBitacora() + ", No se obtuvieron datos para inicia proceso de reenvio del recibo.");
					json = "{\"idError\":1,\"mensajeError\":\"No se obtuvieron datos para inicia el proceso de reenvio del recibo.\"}";
				}
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error al reenviar el recibo: {}", e.getMessage());
			json = "{\"idError\":2,\"mensajeError\":\"Ocurrio un error al reenviar el recibo.\"}";
		}finally{
			json = AddcelCrypto.encryptHard(json);
		}
		return json;	
	}
	private List<DetalleVO>getDetalles(String user, String fechaInicial, String fechaFinal, String idBitacora){
		List<DetalleVO> results = null;
		try{
			results = mapper.getHeader(user, fechaInicial, fechaFinal, idBitacora);
			if(results!=null){
				for(DetalleVO result : results){
					List<FishersCuentaDetalleVO> detalles = mapper.getDetalle(result.getId_pago());
					if(detalles != null){
						List<FishersCuentaDetalleVO> comidaDetalle = new ArrayList<FishersCuentaDetalleVO>();
						List<FishersCuentaDetalleVO> bebidaDetalle = new ArrayList<FishersCuentaDetalleVO>();
						List<FishersCuentaDetalleVO> otrosDetalle = new ArrayList<FishersCuentaDetalleVO>();
						for(FishersCuentaDetalleVO detalle : detalles){
							switch(detalle.getCategoria()){
								case 1:
										comidaDetalle.add(detalle);
										break;
								case 2:
										bebidaDetalle.add(detalle);
										break;
								case 3:
										otrosDetalle.add(detalle);
										break;
							}
						}
						result.setComida(comidaDetalle);
						result.setBebida(bebidaDetalle);
						result.setOtros(otrosDetalle);
					}
				}
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al obtener los pagos: {}", e.getMessage());
			results = null;
		}
		return results;
	}
	public String getCuenta(String data){
		FishersCuentaVO response = new FishersCuentaVO();
		List<FishersDetallePagoVO> detallesPago = null;
		RequestCuenta request = null;
		String json=null;
		try{
			request = (RequestCuenta) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestCuenta.class);
			if(request.getNumero_cuenta() == null && request.getId_caja() == 0 && request.getId_sucursal() == 0){
				json = "{\"idError\":2,\"mensajeError\":\"Se deben proporcionar todos los parametros de busqueda\"}";
			} else{
				ConsultaRequestVO conReq = new ConsultaRequestVO();
				
				conReq.setCaja((int)request.getId_caja());
				conReq.setCuenta(request.getNumero_cuenta());
				
				String jsonReq = utilService.objectToJson(conReq);
				jsonReq = AddcelCrypto.encryptSensitive(formato.format(new Date()), jsonReq);
				jsonReq = URLEncoder.encode(jsonReq, "UTF-8");
				
				String endpoint = mapper.urlSucursal(request.getId_sucursal());
				endpoint = URLEncoder.encode(endpoint, "UTF-8");
				
				String wrapperUrl = mapper.getParametro("@FishersWSWraper");
				
				String jsonResp = utilService.sendOperWS(wrapperUrl, jsonReq, endpoint, 1);
				
				jsonResp = AddcelCrypto.decryptSensitive(jsonResp);
								
				JSONObject jsObject = new JSONObject(jsonResp);
				
				if(!jsObject.has("idError")){
					response = (FishersCuentaVO)utilService.jsonToObject(jsonResp, FishersCuentaVO.class);
					response.setId_caja(request.getId_caja());
					response.setId_sucursal(request.getId_sucursal());
					response.setNumero_cuenta(request.getNumero_cuenta());
					// Codigo de pagos
					double comision = (new Double(mapper.getParametro("@FishersComision"))).doubleValue();
					try{
						double importePagado = 0;
						double importeXPagar = 0;
						detallesPago = mapper.getDetallePago(request.getId_sucursal(), request.getId_caja(), request.getNumero_cuenta());
						if(detallesPago!=null){
							for(FishersDetallePagoVO d:detallesPago){
								importePagado += d.getTotal().doubleValue();							
							}
							response.setDetalle_pagos(detallesPago);
						} else
							response.setDetalle_pagos(new ArrayList<FishersDetallePagoVO>());						
						response.setImporte_pagado(new BigDecimal(importePagado));
						importeXPagar = response.getTotal().doubleValue()-importePagado;
						response.setImporte_x_pagar(new BigDecimal(importeXPagar));
						response.setComision(new BigDecimal(comision));
					}catch(Exception e){
						response.setImporte_pagado(new BigDecimal(0));
						response.setImporte_x_pagar(response.getTotal());
						response.setDetalle_pagos(new ArrayList<FishersDetallePagoVO>());
						response.setComision(new BigDecimal(comision));
					}
					json=utilService.objectToJson(response);
				} else {
					json = jsonResp;
					// Comentar en prod
					/*
					response.setId_caja(request.getId_caja());
					response.setId_sucursal(request.getId_sucursal());
					response.setNumero_cuenta(request.getNumero_cuenta());
					//response.setTotal(new BigDecimal(Math.floor((Math.random() * ((10.1) - 1)) * 100) / 100));
					response.setTotal(new BigDecimal(700));
					response.setSubtotal(new BigDecimal(response.getTotal().doubleValue()*1.10));
					response.setDescuento(new BigDecimal(response.getTotal().doubleValue()*0.10));
					response.setIva(new BigDecimal(0.00));
					FishersCuentaDetalleVO comidas1 = new FishersCuentaDetalleVO();
					comidas1.setCantidad(new BigDecimal(1.00));
					comidas1.setDescripcion_producto("Abulon");
					comidas1.setId_producto(new Long(2456));
					comidas1.setImporte(new BigDecimal(response.getSubtotal().doubleValue()/7.0));
					FishersCuentaDetalleVO comidas2 = new FishersCuentaDetalleVO();
					comidas2.setCantidad(new BigDecimal(1.00));
					comidas2.setDescripcion_producto("Camaron");
					comidas2.setId_producto(new Long(2457));
					comidas2.setImporte(new BigDecimal(response.getSubtotal().doubleValue()/7.0));
					FishersCuentaDetalleVO comidas3 = new FishersCuentaDetalleVO();
					comidas3.setCantidad(new BigDecimal(1.00));
					comidas3.setDescripcion_producto("Pulpo");
					comidas3.setId_producto(new Long(2451));
					comidas3.setImporte(new BigDecimal(response.getSubtotal().doubleValue()/7.0));
					List<FishersCuentaDetalleVO> comidas = new ArrayList<FishersCuentaDetalleVO>();
					comidas.add(comidas1);
					comidas.add(comidas2);
					FishersCuentaDetalleVO bebidas1 = new FishersCuentaDetalleVO();
					bebidas1.setCantidad(new BigDecimal(1.00));
					bebidas1.setDescripcion_producto("Ron Panfilas");
					bebidas1.setId_producto(new Long(2456));
					bebidas1.setImporte(new BigDecimal(response.getSubtotal().doubleValue()/7.0));
					FishersCuentaDetalleVO bebidas2 = new FishersCuentaDetalleVO();
					bebidas2.setCantidad(new BigDecimal(1.00));
					bebidas2.setDescripcion_producto("Te Helado");
					bebidas2.setId_producto(new Long(2457));
					bebidas2.setImporte(new BigDecimal(response.getSubtotal().doubleValue()/7.0));
					List<FishersCuentaDetalleVO> bebidas = new ArrayList<FishersCuentaDetalleVO>();
					FishersCuentaDetalleVO bebidas3 = new FishersCuentaDetalleVO();
					bebidas3.setCantidad(new BigDecimal(1.00));
					bebidas3.setDescripcion_producto("Coca");
					bebidas3.setId_producto(new Long(2455));
					bebidas3.setImporte(new BigDecimal(response.getSubtotal().doubleValue()/7.0));
					bebidas.add(bebidas1);
					bebidas.add(bebidas2);
					bebidas.add(bebidas3);
					FishersCuentaDetalleVO otros1 = new FishersCuentaDetalleVO();
					otros1.setCantidad(new BigDecimal(1.00));
					otros1.setDescripcion_producto("Puros");
					otros1.setId_producto(new Long(2456));
					otros1.setImporte(new BigDecimal(response.getSubtotal().doubleValue()/7.0));
					List<FishersCuentaDetalleVO> otros = new ArrayList<FishersCuentaDetalleVO>();
					otros.add(otros1);
					response.setComida(comidas);
					response.setBebida(bebidas);
					response.setOtros(otros);
					// Codigo de pagos
					double comision = (new Double(mapper.getParametro("@FishersComision"))).doubleValue();
					try{
						double importePagado = 0;
						double importeXPagar = 0;
						detallesPago = mapper.getDetallePago(request.getId_sucursal(), request.getId_caja(), request.getNumero_cuenta());
						if(detallesPago!=null){
							for(FishersDetallePagoVO d:detallesPago){
								importePagado += d.getTotal().doubleValue();							
							}
							response.setDetalle_pagos(detallesPago);
						} else
							response.setDetalle_pagos(new ArrayList<FishersDetallePagoVO>());
						response.setImporte_pagado(new BigDecimal(importePagado));	
						importeXPagar = response.getTotal().doubleValue()-importePagado;
						response.setImporte_x_pagar(new BigDecimal(importeXPagar));
						response.setComision(new BigDecimal(comision));
					}catch(Exception e){
						response.setImporte_pagado(new BigDecimal(0));
						response.setImporte_x_pagar(response.getTotal());
						response.setDetalle_pagos(new ArrayList<FishersDetallePagoVO>());
						response.setComision(new BigDecimal(comision));
					}
					json=utilService.objectToJson(response);
					*/
				}
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al consultar adeudos: " + e.getMessage());
			json = "{\"idError\":2,\"mensajeError\":\"Ocurrio un error al consultar adeudos.\"}";
		}finally{
			json = AddcelCrypto.encryptHard(json);
		}
		return json;
	}
	public UsuarioVO getUsuario(String jsonEnc){
		UsuarioVO usuario = null;
		try{
			DatosPago datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), DatosPago.class);
			if(datosPago.getId_usuario()!=null){
				usuario = mapper.getAddcelUsuario(Long.parseLong(datosPago.getId_usuario()));
			}
		} catch(Exception e){
			logger.error("Ocurrio un error al consultar usuario: " + e.getMessage());
			usuario = null;
		}
		return usuario;
	}
	public String listaMarcas(){
		String dataBack = "";
		List<MarcaVO> responseMarcas = mapper.listaMarcas(0);
		dataBack = utilService.objectToJson(responseMarcas);
		dataBack = "{\"Marcas\":" + dataBack + "}";
		dataBack = AddcelCrypto.encryptHard(dataBack);		
		return dataBack;
	}
}
