package com.addcel.fishers.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.mobilecard.mitec.crypto.TripleDESEncryption;
import mx.mobilecard.mitec.dao.Ambiente;
import mx.mobilecard.mitec.operaciones.RESPONSETX3DS;
import mx.mobilecard.mitec.service.Operaciones;
import mx.mobilecard.mitec.utils.Tarjeta;
import mx.mobilecard.mitec.vo.AuthCompraVO;
import mx.mobilecard.mitec.vo.CompraVO;
import mx.mobilecard.mitec.vo.RespuestaVO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.fishers.model.mapper.BitacorasMapper;
import com.addcel.fishers.model.mapper.FishersMapper;
import com.addcel.fishers.model.vo.FishersCuentaVO;
import com.addcel.fishers.model.vo.FishersCuentaDetalleVO;
import com.addcel.fishers.model.vo.SucursalDatosMitecVO;
import com.addcel.fishers.model.vo.pagos.ProsaPagoVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraVO;
import com.addcel.fishers.model.vo.pagos.TransactionMitecVO;
import com.addcel.fishers.model.vo.request.DatosPago;
import com.addcel.fishers.model.vo.request.RegistrarPagoRequestVO;
import com.addcel.fishers.model.vo.response.DetalleVO;
import com.addcel.fishers.model.vo.response.ResponseProcesaMitec;
import com.addcel.fishers.model.vo.UsuarioVO;
import com.addcel.fishers.utils.AddCelGenericMail;
import com.addcel.fishers.utils.UtilsService;
import com.addcel.fishers.model.vo.pagos.MitComVO;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.json.me.JSONObject;

@Service
public class FishersPagosService {
	private static final Logger logger = LoggerFactory.getLogger(FishersPagosService.class);
	
	private static final String URL_AUT_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth?";
	private static final boolean hardcodeProsa = true;
//	private static final String URL_AUT_PROSA = "http://50.57.192.213:8080/ProsaWeb/ProsaAuth";
//	private static final String URL_TIPO_CAMBIO="http://localhost:8080/Conversor/currency?fromCurrency=usd&toCurrency=MXN";
	@Autowired 
	private FishersMapper mapper;
	@Autowired
	private UtilsService utilService;	
	@Autowired
	private BitacorasMapper bm;
	@Autowired
	private FishersMapper mapperF;
	
	private static final String patron = "ddhhmmss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	private static final SimpleDateFormat formatoEnvio = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private static final DecimalFormat formatter = new DecimalFormat("#0.00");
	private static final String llave = "516883805150505753525754";
	
	private String referenceAuth = "";

	public String procesaPagoVisa(String jsonEnc, UsuarioVO usuarioAddcel){		
		// prueba
		DatosPago datosPago = null;
		FishersCuentaVO cuentaXPagar = null;
		DetalleVO detalleVO = null;
		
		String json = null;
		
		datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc).replace("\"null\"", "null"), DatosPago.class);
		cuentaXPagar = (FishersCuentaVO) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc).replace("\"null\"", "null"), FishersCuentaVO.class);
		detalleVO = (DetalleVO) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), DetalleVO.class);
		
		if(datosPago.getToken() == null){
			json = "{\"idError\":1,\"mensajeError\":\"El parametro TOKEN no puede ser NULL\"}";
			logger.error("El parametro TOKEN no puede ser NULL");
		}else{
			datosPago.setToken(AddcelCrypto.decryptHard(datosPago.getToken()));
			logger.info("token ==> {}",datosPago.getToken());
			
			if((mapperF.difFechaMin(datosPago.getToken())) > 30000){
//			if(false){
				json = "{\"idError\":2,\"mensajeError\":\"La Transacción ya no es válida\"}";
				logger.info("La Transacción ya no es válida");
			}else{
				
				if(usuarioAddcel!=null){					
					datosPago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioAddcel.getUsrTdcNumero()));
					datosPago.setNombre_completo(usuarioAddcel.getUsrNombre() + " " + usuarioAddcel.getUsrApellido() + " " + usuarioAddcel.getUsrMaterno());
					datosPago.setDireccion(usuarioAddcel.getUsrDireccion() + " " + usuarioAddcel.getUsrNumExt());
					datosPago.setCp(usuarioAddcel.getUsrCp());	
					datosPago.setVigencia(usuarioAddcel.getUsrTdcVigencia());
					if(datosPago.getEmail()!=null&&!datosPago.getEmail().equals(""))
						datosPago.setEmail(datosPago.getEmail());
					else
						datosPago.setEmail(usuarioAddcel.getEmail());
					cuentaXPagar.setEmail(datosPago.getEmail());
				} else
					datosPago.setId_usuario("0");
				long idBitacora=insertaBitacoras(datosPago, cuentaXPagar, "VISA");
				
				datosPago.setId_bitacora(idBitacora);
				
				String afiliacion = mapperF.getAfiliacion(datosPago.getId_sucursal(), datosPago.getId_caja());
				
				json = swichAbierto(datosPago,afiliacion,detalleVO);
					
				json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
			}
		}
		return json;
	}
	
	private String swichAbierto(DatosPago datosPago, String afiliacion, DetalleVO detalleVO){
		ProsaPagoVO prosaPagoVO = null;
		String json = null;
		try{
			logger.info("datosPago.getTarjeta(): {}", datosPago.getTarjeta());
			logger.info("datosPago.getVigencia(): {}", datosPago.getVigencia());
			logger.info("datosPago.getNombre_completo(): {}", datosPago.getNombre_completo());
			logger.info("datosPago.getCvv2(): {}", datosPago.getCvv2());
			logger.info("datosPago.getTotal(): {}", datosPago.getTotal_pago());
			logger.info("afiliacion(): {}", afiliacion);
			
			StringBuffer data = new StringBuffer() 
					.append( "card="      ).append( URLEncoder.encode(datosPago.getTarjeta(), "UTF-8") )
					.append( "&vigencia=" ).append( URLEncoder.encode(datosPago.getVigencia(), "UTF-8") )
					.append( "&nombre="   ).append( URLEncoder.encode(datosPago.getNombre_completo(), "UTF-8") )
					.append( "&cvv2="     ).append( URLEncoder.encode(datosPago.getCvv2(), "UTF-8") )
					.append( "&monto="    ).append( URLEncoder.encode(datosPago.getTotal_pago() + "", "UTF-8") )
					.append( "&afiliacion=" ).append( URLEncoder.encode(afiliacion, "UTF-8") );
			
			logger.info("Envío de datos VISA: {}", data);
			
			if(!hardcodeProsa){
				URL url = new URL(URL_AUT_PROSA);
				URLConnection urlConnection = url.openConnection();
			
				urlConnection.setDoOutput(true);
			
				OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
				wr.write(data.toString());
				wr.flush();
				logger.info("Datos enviados, esperando respuesta de PROSA");
			
				BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
				String line = null;
				StringBuilder sb = new StringBuilder();
			
				while ((line = rd.readLine()) != null) {
					sb.append(line);
				}
			
				wr.close();
				rd.close();
			
				prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(sb.toString(), ProsaPagoVO.class);
			} else{
				prosaPagoVO = new ProsaPagoVO();
				prosaPagoVO.setIsAuthorized(true);
				prosaPagoVO.setAutorizacion("6515");
				prosaPagoVO.setTransactionId("13165");
				
			}
			
			if(prosaPagoVO != null){
				// llenar detalleVO
				detalleVO.setReferencia(datosPago.getId_bitacora().toString());
				detalleVO.setLast4d("XXXX-XXXX-XXXX-" + datosPago.getTarjeta().substring(datosPago.getTarjeta().length()-4));
				detalleVO.setNombre(datosPago.getNombre_completo());
				datosPago.setDescRechazo(prosaPagoVO.getDescripcionRechazo());
				datosPago.setNoAutorizacion(prosaPagoVO.getAutorizacion());
				
				
				if(prosaPagoVO.isAuthorized()){
					datosPago.setStatus(1);
					String cajaDesc = "NO IDENTIFICADA";
					try{
						cajaDesc = mapper.descripcionCaja(datosPago.getId_sucursal(), datosPago.getId_caja());
					}catch(Exception e){
						logger.error("Error al recuperar descripción de la caja");
					}
					datosPago.setMsg("EXITO PAGO FISHERS VISA AUTORIZADA");
					json = "{\"idError\":0,\"mensajeError\":\"El pago fue exitoso.\",\"transaccion\":\"" + prosaPagoVO.getTransactionId() + 
							"\",\"autorizacion\":\"" + prosaPagoVO.getAutorizacion() +
							"\",\"tarjeta\":\"" + detalleVO.getLast4d() +
							"\",\"caja\":\"" + cajaDesc +
							"\",\"cuenta\":\"" + datosPago.getNumero_cuenta() +
							"\",\"monto\":\"" + datosPago.getTotal_pago() +
							"\",\"referencia\":\"" + datosPago.getId_bitacora() + "\"}";
					
					boolean statusEnvio = registraPago(datosPago.getId_sucursal(),(int)datosPago.getId_caja(), datosPago.getNumero_cuenta(),detalleVO.getLast4d(),prosaPagoVO.getAutorizacion(),new BigDecimal(datosPago.getTotal_pago().doubleValue()-datosPago.getComision().doubleValue()),datosPago.getPropina(),"Visa/Mastercard",datosPago.getId_bitacora().toString());
					if(statusEnvio)
						datosPago.setStatusEnvio(1);
					else
						datosPago.setStatusEnvio(0);
					AddCelGenericMail.generatedMail(detalleVO,utilService, mapper.descripcionSucursal(detalleVO.getId_sucursal()));
					
				}else if(prosaPagoVO.isRejected()){
					datosPago.setStatus(0);
					datosPago.setMsg("PAGO DUTY FREE VISA RECHAZADA");
					json = "{\"idError\":4,\"mensajeError\":\"El pago fue rechazado.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
					
//					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailError(datosPago,"RECHAZADA")));
					
				}else if(prosaPagoVO.isProsaError()){
					datosPago.setStatus(0);
					datosPago.setMsg("PAGO DUTY FREE VISA ERROR");
					json = "{\"idError\":5,\"mensajeError\":\"Ocurrio un error durante el pago Banco.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
					
//					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailError(datosPago,"ERROR")));
				}
				
				updateBitacoras(datosPago);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Ocurrio un error durante el pago al banco: {}", e);
			datosPago.setStatus(0);
			datosPago.setMsg("Ocurrio un error durante el pago al banco");
			json = "{\"idError\":6,\"mensajeError\":\"Ocurrio un error durante el pago.\"}";
			updateBitacoras(datosPago);
		}
		return json;
	}
	
	
	private long insertaBitacoras(DatosPago dp, FishersCuentaVO cuentaXPagar, String flujo){		
		TBitacoraVO tb = new TBitacoraVO(
				dp.getId_usuario(), "PAGO FISHERS " + flujo, String.valueOf(dp.getTotal_pago()), dp.getImei(), "PAGO FISHERS VISA",
				AddcelCrypto.encryptTarjeta(dp.getTarjeta()),dp.getTipo(), dp.getSoftware(), dp.getModelo(),dp.getWkey());
		bm.addBitacora(tb);
				
		TBitacoraProsaVO tbp = new TBitacoraProsaVO();
		tbp.setIdBitacora(tb.getIdBitacora());
		tbp.setIdUsuario(Long.parseLong(dp.getId_usuario()));
		tbp.setConcepto("PAGO FISHERS " + flujo);
		tbp.setCargo(dp.getTotal_pago().doubleValue());
		tbp.setComision(0.00);
		tbp.setCx(dp.getCx());
		tbp.setCy(dp.getCy());
		
		bm.addBitacoraProsa(tbp);
		
		cuentaXPagar.setId_bitacora(tb.getIdBitacora());
		cuentaXPagar.setId_usuario(dp.getId_usuario());
		cuentaXPagar.setExp(dp.getExp());
		
		bm.addFHeader(cuentaXPagar);
		
		insertaDetalleCuenta(cuentaXPagar.getComida(), 1, cuentaXPagar.getId_pago().intValue());
		insertaDetalleCuenta(cuentaXPagar.getBebida(), 2, cuentaXPagar.getId_pago().intValue());
		insertaDetalleCuenta(cuentaXPagar.getOtros(), 3, cuentaXPagar.getId_pago().intValue());
		return tb.getIdBitacora();
	}
	private void insertaDetalleCuenta(List<FishersCuentaDetalleVO> detalles, int categoria, int idPago){
		for(FishersCuentaDetalleVO detalle : detalles){
			detalle.setId_pago(new Long(idPago));
			detalle.setCategoria(categoria);
			bm.addFDetalle(detalle);
		}
	}
	
	private void updateBitacoras(DatosPago dp){
		TBitacoraVO tb = new TBitacoraVO();
		TBitacoraProsaVO tbp = new TBitacoraProsaVO();
		tb.setIdBitacora(dp.getId_bitacora());
		tb.setBitStatus(dp.getStatus());
		tb.setBitConcepto(dp.getMsg());
		tb.setBitNoAutorizacion(dp.getNoAutorizacion());
		bm.updateBitacora(tb);
		tbp.setIdBitacora(dp.getId_bitacora());
		tbp.setAutorizacion(dp.getNoAutorizacion());
		logger.info(tbp.toString());
		if(tbp.getAutorizacion()!=null)
			bm.updateBitacoraProsa(tbp);
		bm.updateFHeader(dp.getId_bitacora(), dp.getStatus(), dp.getStatusEnvio());
	}
	private boolean registraPago(long id_sucursal, int caja, java.lang.String cuenta_num, java.lang.String tarjeta, java.lang.String autorizacion, BigDecimal monto_pagado, BigDecimal propina, java.lang.String tipo_tarjeta, java.lang.String transaccion_addcel){
		boolean status = false;
		try{
			Calendar cal =  GregorianCalendar.getInstance();
			Date fecha = cal.getTime();
			String fecha_operacion = formatoEnvio.format(fecha);
			RegistrarPagoRequestVO request = new RegistrarPagoRequestVO();
			request.setCaja(caja);
			request.setCuenta_num(cuenta_num);
			request.setFecha_operacion(fecha_operacion);
			request.setTarjeta(tarjeta);
			request.setAutorizacion(autorizacion);
			request.setMonto_pagado(monto_pagado);
			request.setPropina(propina);
			request.setTipo_tarjeta(tipo_tarjeta);
			request.setTransaccion_addcel(transaccion_addcel);
			
			String jsonReq = utilService.objectToJson(request);
			jsonReq = AddcelCrypto.encryptSensitive(formato.format(new Date()), jsonReq);
			jsonReq = URLEncoder.encode(jsonReq, "UTF-8");
			
			String endpoint = mapper.urlSucursal(id_sucursal);
			endpoint = URLEncoder.encode(endpoint, "UTF-8");
			
			String wrapperUrl = mapper.getParametro("@FishersWSWraper");
			
			String jsonResp = utilService.sendOperWS(wrapperUrl, jsonReq, endpoint, 2);
			
			jsonResp = AddcelCrypto.decryptSensitive(jsonResp);
			
			JSONObject jsObject = new JSONObject(jsonResp);
			
			if(jsObject.has("idError")){
				String resultado = (String)jsObject.get("idError");
				logger.info("resultado del registro: " + resultado);
				if(resultado.equals("0"))
					status = true;
			}			
		}catch(Exception e){
			logger.error("Ocurrio un error durante registro de pago a fishers: {}", e);
			status = false;
		}
		return status;
	}

	public MitComVO procesaPago(String jsonEnc, UsuarioVO usuarioAddcel, int tipoFlujo){		
		// prueba
		DatosPago datosPago = null;
		MitComVO procom = null;
		FishersCuentaVO cuentaXPagar = null;
				
		datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc).replace("\"null\"", "null"), DatosPago.class);
				
		
		if(datosPago.getToken() == null){
			procom = new MitComVO(1,"El parametro TOKEN no puede ser NULL");
			logger.error("El parametro TOKEN no puede ser NULL");
		}else{
			datosPago.setToken(AddcelCrypto.decryptHard(datosPago.getToken()));
			logger.info("token ==> {}",datosPago.getToken());
			
			if((mapperF.difFechaMin(datosPago.getToken())) > 3000){
//			if(false){
				procom = new MitComVO(2,"La Transacción ya no es válida");
				logger.info("La Transacción ya no es válida");
			}else{
				cuentaXPagar = (FishersCuentaVO) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc).replace("\"null\"", "null"), FishersCuentaVO.class);
				if(usuarioAddcel!=null){					
					datosPago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioAddcel.getUsrTdcNumero()));
					datosPago.setNombre_completo(usuarioAddcel.getUsrNombre() + " " + usuarioAddcel.getUsrApellido() + " " + usuarioAddcel.getUsrMaterno());
					datosPago.setDireccion(usuarioAddcel.getUsrDireccion() + " " + usuarioAddcel.getUsrNumExt());
					datosPago.setCp(usuarioAddcel.getUsrCp());	
					datosPago.setVigencia(usuarioAddcel.getUsrTdcVigencia());
					if(datosPago.getEmail()!=null&&!datosPago.getEmail().equals(""))
						datosPago.setEmail(datosPago.getEmail());
					else
						datosPago.setEmail(usuarioAddcel.getEmail());
					cuentaXPagar.setEmail(datosPago.getEmail());
				} else
					datosPago.setId_usuario("0");
				datosPago.setExp(getExp(datosPago.getCvv2(),datosPago.getTarjeta(),datosPago.getVigencia(),datosPago.getNombre_completo()));
				long idBitacora=insertaBitacoras(datosPago, cuentaXPagar, tipoFlujo==1?"VISA":"AMEX");
				datosPago.setId_bitacora(idBitacora);
				if(tipoFlujo == 1 ){
					procom = comercioFin(datosPago);
					procom.setFlujoid("VISA");
				} else {
					procom = new MitComVO();					
					procom.setUrlMitAction(obtenParametro("@FishersVarUrlAuthBack"));
					procom.setReferencia(datosPago.getId_bitacora().toString());
					procom.setFlujoid("AMEX");
				}
					
			}
		}
		
		return procom;
	}	
	private MitComVO comercioFin(DatosPago dp) {
		MitComVO result = new MitComVO();
		Operaciones p = null;
		//String afiliacion = mapperF.getAfiliacion(dp.getId_sucursal(), dp.getId_caja());
		SucursalDatosMitecVO datosCon = mapperF.getSucursalDatosMitec(dp.getId_sucursal());
		result.setUrlMitAction(obtenParametro("@FishersVarUrlMitAuth"));
		result.setStrIdCompany(datosCon.getId_company3D());
		result.setStrIdMerchant(datosCon.getAfiliacion3d());
		try{
			AuthCompraVO compra = new AuthCompraVO();
			compra.setDatosCon(datosCon);
			compra.setCreditCardCvv(dp.getCvv2());
			Map<String,String> axomes = getVigencia(dp.getVigencia());
			compra.setCreditCardMonth((String)axomes.get("mes"));
			compra.setCreditCardYear((String)axomes.get("axo"));
			compra.setCreditCardName(dp.getNombre_completo());
			compra.setCreditCardNumber(dp.getTarjeta());
			compra.setCurrency("MXN");
			compra.setMerchant(datosCon.getAfiliacion3d());
			compra.setMonto(formatter.format(dp.getTotal_pago().doubleValue()));
			compra.setReference(dp.getId_bitacora().toString());
			compra.setUrlResponse(obtenParametro("@FishersVarUrlAuthBack"));
			p=Operaciones.getInstance(Ambiente.PROD);			
			String strXmlParameters = p.obtenXMLAutenticacion(compra,bm);
			result.setStrXmlParameters(strXmlParameters);			
		} catch(Exception e){
			result = new MitComVO(2,"Error al invocar autenticación: " + e.getMessage());
		}		
		return result;
    }
	public ResponseProcesaMitec procesaRespuestaMitec(TransactionMitecVO tp) {
		// insertar bitacora ecommerce
		// Valida digest,
		logger.info("Dentro de procesaRespuestaMitec");
		List<DetalleVO> detalleVO = null;
		ResponseProcesaMitec respuesta = new ResponseProcesaMitec();
		respuesta.setRespuesta("error");
		boolean vA = false;
		RespuestaVO respuestaVO=null;
		Operaciones p=null;

		if(tp.getFlujoid().equals("VISA")){
			vA = validaAutenticacion(tp);			
		} else {
			vA= true;
			referenceAuth = tp.getReference();
		}
		if(vA){
			TBitacoraVO b = new TBitacoraVO();
			TBitacoraProsaVO tbp = new TBitacoraProsaVO();
			detalleVO = mapperF.getHeader(null, null, null, referenceAuth);				
			if(detalleVO != null && detalleVO.size() >0){
				CompraVO compra=new CompraVO();
				SucursalDatosMitecVO datosCon = mapperF.getSucursalDatosMitec(detalleVO.get(0).getId_sucursal());
				compra.setDatosCon(datosCon);
				Map<String,String> expdata = getExpData(detalleVO.get(0).getExp());
				logger.info("ExpData: " + expdata);
				logger.info("ExpData: " + expdata.toString());
				p=Operaciones.getInstance(Ambiente.PROD);
				if(tp.getFlujoid().equals("VISA"))
					compra.setCreditCardType(Tarjeta.VISA_MASTERCARD);
				else
					compra.setCreditCardType(Tarjeta.AMEX);
				compra.setCreditCardCvv(expdata.get("cvv"));
				compra.setCreditCardNumber(expdata.get("number"));
				compra.setCreditCardMonth(expdata.get("mes"));
				compra.setCreditCardYear(expdata.get("axo"));
				compra.setCreditCardName(expdata.get("name"));
				compra.setMonto(formatter.format(detalleVO.get(0).getTotal_pago().doubleValue()));
				//if(Integer.parseInt(referenceAuth)%2==0||expdata.get("name").equals("MAURICIO LOPEZ SANCHEZ"))
					respuestaVO=p.ejecutaVMCAMEXM(compra, referenceAuth, bm);
				/*else{
					logger.info("Entre a dummy");
					respuestaVO = new RespuestaVO();
					respuestaVO.setAuth((new Integer(Integer.parseInt(referenceAuth)+1000)).toString());
					respuestaVO.setResponse("approved");
					respuestaVO.setReference(referenceAuth);
				//}*/				
				if(respuestaVO.getResponse().equals("approved")){
					String cardEnvio;
					if(tp.getFlujoid().equals("VISA"))					
						cardEnvio=("XXXX-XXXX-XXXX-"+expdata.get("number").substring(expdata.get("number").length()-4));
					else
						cardEnvio=("XXXX-XXXXXX-X-"+expdata.get("number").substring(expdata.get("number").length()-4));

					b.setIdBitacora(Long.parseLong(respuestaVO.getReference()));
					b.setBitStatus(1);
					b.setBitConcepto("PAGO FISHERS 3DSECURE EXITOSO");
					b.setBitNoAutorizacion(respuestaVO.getAuth());
					b.setTarjetaCompra(AddcelCrypto.encryptTarjeta(expdata.get("number")));
					bm.updateBitacora(b);
					tbp.setIdBitacora(Long.parseLong(respuestaVO.getReference()));
					tbp.setAutorizacion(respuestaVO.getAuth());
					bm.updateBitacoraProsa(tbp);
					boolean statusEnvio = true;
					if(tp.getFlujoid().equals("VISA"))
						statusEnvio = registraPago(detalleVO.get(0).getId_sucursal(),(int)detalleVO.get(0).getId_caja(), detalleVO.get(0).getNumero_cuenta(),cardEnvio,respuestaVO.getAuth(),new BigDecimal(detalleVO.get(0).getTotal_pago().doubleValue()-detalleVO.get(0).getComision().doubleValue()),detalleVO.get(0).getPropina(),"Visa/Mastercard",detalleVO.get(0).getId_bitacora().toString());
					else
						statusEnvio = registraPago(detalleVO.get(0).getId_sucursal(),(int)detalleVO.get(0).getId_caja(), detalleVO.get(0).getNumero_cuenta(),cardEnvio,respuestaVO.getAuth(),new BigDecimal(detalleVO.get(0).getTotal_pago().doubleValue()-detalleVO.get(0).getComision().doubleValue()),detalleVO.get(0).getPropina(),"Amex",detalleVO.get(0).getId_bitacora().toString());
					if(statusEnvio)
						bm.updateFHeader(Long.parseLong(respuestaVO.getReference()), 1, 1);
					else
						bm.updateFHeader(Long.parseLong(respuestaVO.getReference()), 1, 0);

					// envio correo
					
					respuesta.setRespuesta("comerciocon");
					respuesta.setAuth(respuestaVO.getAuth());
					respuesta.setRefNum(respuestaVO.getReference());
					respuesta.setName(compra.getCreditCardName());
					respuesta.setCard(cardEnvio);
					respuesta.setAmount("$"+compra.getMonto()+" MXN");
					logger.info("Pago correcto");
					logger.info("Enviando correo a " + detalleVO.get(0).getEmail());
					if(detalleVO.get(0).getEmail()!=null){
						detalleVO.get(0).setReferencia(respuestaVO.getAuth());
						AddCelGenericMail.generatedMail(detalleVO.get(0),utilService, mapper.descripcionSucursal(detalleVO.get(0).getId_sucursal()));
					}
				} else{
					b.setIdBitacora(Long.parseLong(referenceAuth));
					b.setBitStatus(0);
					b.setBitConcepto("PAGO FISHERS 3DSECURE FALLIDO");
					bm.updateBitacora(b);
				
					respuesta.setRespuesta("error");
					if(respuestaVO.getNb_error()!=null&&!respuestaVO.getNb_error().trim().equals(""))
						respuesta.setMensajeError(respuestaVO.getNb_error());
					else
						if(respuestaVO.getResponse().equals("denied"))
							respuesta.setMensajeError("La tarjeta de compra fue declinada por su banco emisor y por lo tanto no se realizó ningún cargo. Por favor intente con una tarjeta diferente");
						else
							respuesta.setMensajeError("Error desconocido, su tarjeta no fue afectada.");
					respuesta.setProcessId("1");
					logger.info("Pago incorrecto: " + respuesta.getMensajeError());
				}
			} else{
				respuesta.setRespuesta("error");
				respuesta.setMensajeError("No se encontraron los datos de la venta en la BD, favor de reintentar más tarde");
				respuesta.setProcessId("1");
			}			
		} else{
			respuesta.setRespuesta("error");
			respuesta.setMensajeError("La transacción no fue autorizada por el banco, favor de verificar con su banco.");
			respuesta.setProcessId("1");
		}
		return respuesta;
	}	

	private String obtenParametro(String reference){
		String param = null;
		param = mapperF.getParametro(reference);
		return param;
	}
	private Map<String,String> getVigencia(String vigencia){
		Map<String, String> data = new HashMap<String, String>();
		String [] datos = vigencia.split("\\/");
		data.put("axo", datos[1]);
		data.put("mes", datos[0]);		
		return data;
	}
	private boolean validaAutenticacion(TransactionMitecVO tp){
		boolean res = false;
		logger.info("Dentro de validaAutenticacion");
		try{
			Operaciones p=Operaciones.getInstance(Ambiente.DEV);
			logger.info("strResponse: " + tp.getStrResponse());
			RESPONSETX3DS r3ds = p.respuestaAuth(TripleDESEncryption.decrypt(tp.getStrResponse(),llave));			
			if(r3ds!=null){		
				logger.info(r3ds.toString());
				referenceAuth = r3ds.getR3ds_reference(); 
				String val = p.validateAuth(r3ds, bm);
				logger.info("val: " +val);
				if(val.equals("true"))
					res=true;
			} else
				res = false;
		} catch( Exception e){
			logger.error("Error al validar autenticacion: {}", e.getMessage());
			res = false;			
		}
		return res;
		
	}
	private String getExp(String ccv, String number, String vigencia, String name){
		String axo;
		String mes;
		Map<String,String> axomes = getVigencia(vigencia);
		axo = (String)axomes.get("axo");
		mes = (String)axomes.get("mes");		
		String res = ccv + '@' +
					 number + '@' +
					 mes + '@' +
					 axo + '@' +
					 name + '@';
		return AddcelCrypto.encryptTarjeta(res);					 
	}
	private Map<String,String> getExpData(String exp){
		Map<String, String> data = new HashMap<String, String>();
		String[] datos = AddcelCrypto.decryptTarjeta(exp).split("@");
		data.put("cvv", datos[0]);
		data.put("number", datos[1]);
		data.put("mes", datos[2]);
		data.put("axo", datos[3]);
		data.put("name", datos[4]);		
		return data;
	}
}
