package com.addcel.fishers.services;

import java.io.BufferedReader
;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.fishers.model.mapper.BitacorasMapper;
import com.addcel.fishers.model.mapper.FishersMapper;
import com.addcel.fishers.model.vo.FishersCuentaDetalleVO;
import com.addcel.fishers.model.vo.FishersCuentaVO;
import com.addcel.fishers.model.vo.RespuestaAmexVO;
import com.addcel.fishers.model.vo.UsuarioVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraVO;
import com.addcel.fishers.model.vo.pagos.TransactionProcomVO;
import com.addcel.fishers.model.vo.request.DatosPago;
import com.addcel.fishers.model.vo.request.RegistrarPagoRequestVO;
import com.addcel.fishers.model.vo.response.DetalleVO;
import com.addcel.fishers.utils.AddCelGenericMail;
import com.addcel.fishers.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.Utilerias;
import com.addcel.utils.json.me.JSONObject;

@Service
public class FishersPagoAMEX {
	
	private static final Logger logger = LoggerFactory.getLogger(FishersPagosService.class);
	private static final String urlStringAMEX = "http://localhost:8080/AmexWeb/AmexAuthorization";
	private static final String patron = "ddhhmmss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);

	private static final SimpleDateFormat formatoEnvio = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	@Autowired
	private BitacorasMapper mapper;
	@Autowired
	private UtilsService utils;
	@Autowired
	private FishersMapper mapperF;
	
	public String procesaPago(String dataEncryp, UsuarioVO usuarioAddcel) {
		RespuestaAmexVO respuestaAmex = new RespuestaAmexVO();
		DetalleVO detalleVO = null;
		FishersCuentaVO cuentaXPagar = null;
		String json = null;
		
		DatosPago pagoAmex = (DatosPago) utils.jsonToObject(AddcelCrypto.decryptSensitive(dataEncryp).replace("\"null\"", "null"), DatosPago.class);
		cuentaXPagar = (FishersCuentaVO) utils.jsonToObject(AddcelCrypto.decryptSensitive(dataEncryp).replace("\"null\"", "null"), FishersCuentaVO.class);
		detalleVO = (DetalleVO) utils.jsonToObject(AddcelCrypto.decryptSensitive(dataEncryp).replace("\"null\"", "null"), DetalleVO.class);
		
		if(pagoAmex.getToken() == null){
			respuestaAmex.setCode("1");
			respuestaAmex.setDsc("El parametro TOKEN no puede ser NULL");
			respuestaAmex.setError("1");
			respuestaAmex.setErrorDsc("El parametro TOKEN no puede ser NULL");
			logger.error("El parametro TOKEN no puede ser NULL");
		}else{
			pagoAmex.setToken(AddcelCrypto.decryptHard(pagoAmex.getToken()));
			logger.info("token ==> {}",pagoAmex.getToken());
			
			if((mapperF.difFechaMin(pagoAmex.getToken())) > 300000){
	//		if(false){
				respuestaAmex.setCode("2");
				respuestaAmex.setDsc("La Transacción ya no es válida");
				respuestaAmex.setError("2");
				respuestaAmex.setErrorDsc("La Transacción ya no es válida");
				logger.info("La Transacción ya no es válida");
			}else{
				logger.info("usuarioAddcel:" + usuarioAddcel);
				if(usuarioAddcel!=null){					
					pagoAmex.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioAddcel.getUsrTdcNumero()));
					pagoAmex.setNombre_completo(usuarioAddcel.getUsrNombre() + " " + usuarioAddcel.getUsrApellido() + " " + usuarioAddcel.getUsrMaterno());
					pagoAmex.setDireccion(usuarioAddcel.getUsrDireccion() + " " + usuarioAddcel.getUsrNumExt());
					pagoAmex.setCp(usuarioAddcel.getUsrCp());	
					pagoAmex.setVigencia(usuarioAddcel.getUsrTdcVigencia());
					if(pagoAmex.getEmail()!=null&&!pagoAmex.getEmail().equals(""))
						pagoAmex.setEmail(pagoAmex.getEmail());
					else
						pagoAmex.setEmail(usuarioAddcel.getEmail());
				} else
					pagoAmex.setId_usuario("0");
				
				long idBitacora = insertaBitacoras(pagoAmex,cuentaXPagar);
				
				pagoAmex.setId_bitacora(idBitacora);
				
				try {
					String afiliacion = mapperF.getAfiliacionAmex(pagoAmex.getId_sucursal(), pagoAmex.getId_caja());
					String data = "tarjeta=" + URLEncoder.encode(pagoAmex.getTarjeta(), "UTF-8") +
							"&vigencia=" + URLEncoder.encode(pagoAmex.getVigencia().replaceAll("\\/", ""), "UTF-8") +
							"&monto=" + URLEncoder.encode(String.valueOf(pagoAmex.getTotal()), "UTF-8") +
							"&cid=" + URLEncoder.encode(pagoAmex.getCvv2(), "UTF-8") + 
							"&direccion=" + URLEncoder.encode(pagoAmex.getDireccion(), "UTF-8") +
							"&cp=" + URLEncoder.encode(pagoAmex.getCp(), "UTF-8") +
							"&afiliacion=" + URLEncoder.encode(afiliacion, "UTF-8");
					
					logger.info("Envio de datos AMEX: {}", data);
					
					URL url = new URL(urlStringAMEX);
					URLConnection urlConnection = url.openConnection();
					
					urlConnection.setDoOutput(true);
					
					OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
					wr.write(data);
					wr.flush();
					logger.info("Datos enviados, esperando respuesta de AMEX");
					
					BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					String line;
					StringBuilder sb = new StringBuilder();
					
					while ((line = rd.readLine()) != null) {
						sb.append(line);
					}
					
					wr.close();
					rd.close();
					
					respuestaAmex = (RespuestaAmexVO) utils.jsonToObject(sb.toString(), RespuestaAmexVO.class);
					
					//*******************************************
					String patron = "000000";
			    	String patron2 = "000000000000";
			    	DecimalFormat formato = new DecimalFormat(patron);
			    	DecimalFormat formato2 = new DecimalFormat(patron2);
					Random  rnd = new Random();
			    	
			    	int aut = (int)(rnd.nextDouble() * 900000);
			    	long ref = (long)(rnd.nextDouble() * 900000000);
			    	
			    	respuestaAmex.setTransaction(formato2.format(ref));
			    	//*******************************************
					
					logger.info("Respuesta: {}", sb.toString());
					logger.info("*****************CODIGO AMEX {}", respuestaAmex.getCode());
					if (respuestaAmex.getCode().equals("000")) { //Pago realizado con �xito
						logger.info("Pago realizado con Exito.");
						pagoAmex.setStatus(1);
						pagoAmex.setMsg("EXITO PAGO AMEX FISHERS");
						pagoAmex.setNoAutorizacion(respuestaAmex.getTransaction());
						updateBitacoras( pagoAmex);
						
						detalleVO.setReferencia(pagoAmex.getId_bitacora().toString());
						detalleVO.setLast4d("XXXX-XXXXXX-X" + pagoAmex.getTarjeta().substring(pagoAmex.getTarjeta().length()-4));
						detalleVO.setNombre(pagoAmex.getNombre_completo());

						String cajaDesc = "NO IDENTIFICADA";
					
						json = "{\"idError\":0,\"mensajeError\":\"El pago fue exitoso.\",\"transaccion\":\"" + respuestaAmex.getTransaction() +
								"\",\"autorizacion\":\"" + respuestaAmex.getTransaction() +
								"\",\"tarjeta\":\"" + detalleVO.getLast4d() +
								"\",\"caja\":\"" + cajaDesc +
								"\",\"cuenta\":\"" + pagoAmex.getNumero_cuenta() +
								"\",\"monto\":\"" + pagoAmex.getTotal_pago() +
								"\",\"referencia\":\"" + pagoAmex.getId_bitacora() + "\"}";

						boolean statusEnvio = registraPago(pagoAmex.getId_sucursal(),(int)pagoAmex.getId_caja(), pagoAmex.getNumero_cuenta(),detalleVO.getLast4d(),pagoAmex.getNoAutorizacion(),new BigDecimal(pagoAmex.getTotal_pago().doubleValue()-pagoAmex.getComision().doubleValue()),pagoAmex.getPropina(),"AMEX",pagoAmex.getId_bitacora().toString());
						if(statusEnvio)
							pagoAmex.setStatusEnvio(1);
						else
							pagoAmex.setStatusEnvio(0);

						AddCelGenericMail.generatedMail(detalleVO,utils,mapperF.descripcionSucursal(detalleVO.getId_sucursal()));						
					} else {
						logger.info("El pago no se pudo realizar error {}", respuestaAmex.getDsc()!=null?respuestaAmex.getDsc():respuestaAmex.getErrorDsc());
						pagoAmex.setStatus(0);
						json = "{\"idError\":4,\"mensajeError\":\"El pago fue rechazado.\"}";
						pagoAmex.setMsg("DECLINADA PAGO AMEX FISHERS ");
						updateBitacoras( pagoAmex);
						
					}
					
				} catch (Exception ex) {
					logger.error("Error al procesar pago AMEX: {}", ex);
					logger.info("El pago no se pudo realizar error {}", ex);
					pagoAmex.setStatus(2);
					pagoAmex.setMsg("ERROR PAGO AMEX FISHERS");
					respuestaAmex.setCode("002");
					respuestaAmex.setDsc("Ocurrio un error durante la transacción: " + ex.getMessage());
					respuestaAmex.setError("2");
					respuestaAmex.setErrorDsc("Ocurrio un error durante la transacción: " + ex.getMessage());
					json = "{\"idError\":6,\"mensajeError\":\"El pago fue rechazado.\"}";
					updateBitacoras( pagoAmex);
				}
			}
		}
		return AddcelCrypto.encryptSensitive("12345678", json);
	}
	
	
	private long insertaBitacoras(DatosPago dp, FishersCuentaVO cuentaXPagar){		
		TBitacoraVO tb = new TBitacoraVO(
				dp.getId_usuario(), "PAGO FISHERS AMEX", String.valueOf(dp.getTotal()), dp.getImei(), "PAGO FISHERS AMEX",
				AddcelCrypto.encryptTarjeta(dp.getTarjeta()),dp.getTipo(), dp.getSoftware(), dp.getModelo(),dp.getWkey());
		mapper.addBitacora(tb);
		
		logger.info("id_bitacora", tb.getIdBitacora());
		
		TBitacoraProsaVO tbp = new TBitacoraProsaVO();

		tbp.setIdBitacora(tb.getIdBitacora());
		tbp.setIdUsuario(Long.parseLong(dp.getId_usuario()));
		tbp.setConcepto("PAGO FISHERS AMEX");
		tbp.setCargo(dp.getTotal_pago().doubleValue());
		tbp.setComision(0.00);
		tbp.setCx(dp.getCx());
		tbp.setCy(dp.getCy());

		mapper.addBitacoraProsa(tbp);
				
		cuentaXPagar.setId_bitacora(tb.getIdBitacora());
		cuentaXPagar.setId_usuario(dp.getId_usuario());
		
		mapper.addFHeader(cuentaXPagar);
		insertaDetalleCuenta(cuentaXPagar.getComida(), 1, cuentaXPagar.getId_pago().intValue());
		insertaDetalleCuenta(cuentaXPagar.getBebida(), 2, cuentaXPagar.getId_pago().intValue());
		insertaDetalleCuenta(cuentaXPagar.getOtros(), 3, cuentaXPagar.getId_pago().intValue());

		return tb.getIdBitacora();
	}

	private void insertaDetalleCuenta(List<FishersCuentaDetalleVO> detalles, int categoria, int idPago){
		for(FishersCuentaDetalleVO detalle : detalles){
			detalle.setId_pago(new Long(idPago));
			detalle.setCategoria(categoria);
			mapper.addFDetalle(detalle);
		}
	}

	
	private void updateBitacoras(DatosPago dp){
		TBitacoraVO tb = new TBitacoraVO();
		TBitacoraProsaVO tbp = new TBitacoraProsaVO();
		
		tb.setIdBitacora(dp.getId_bitacora());
		tb.setBitStatus(dp.getStatus());
		tb.setBitConcepto(dp.getMsg());
		tb.setBitNoAutorizacion(dp.getNoAutorizacion());
		mapper.updateBitacora(tb);
		tbp.setIdBitacora(dp.getId_bitacora());
		tbp.setAutorizacion(dp.getNoAutorizacion());
		if(tbp.getAutorizacion()!=null)
			mapper.updateBitacoraProsa(tbp);
		mapper.updateFHeader(dp.getId_bitacora(), dp.getStatus(), dp.getStatusEnvio());
	}	
	private boolean registraPago(long id_sucursal, int caja, java.lang.String cuenta_num, java.lang.String tarjeta, java.lang.String autorizacion, BigDecimal monto_pagado, BigDecimal propina, java.lang.String tipo_tarjeta, java.lang.String transaccion_addcel){
		boolean status = false;
		try{
			Calendar cal =  GregorianCalendar.getInstance();
			Date fecha = cal.getTime();
			String fecha_operacion = formatoEnvio.format(fecha);
			RegistrarPagoRequestVO request = new RegistrarPagoRequestVO();
			request.setCaja(caja);
			request.setCuenta_num(cuenta_num);
			request.setFecha_operacion(fecha_operacion);
			request.setTarjeta(tarjeta);
			request.setAutorizacion(autorizacion);
			request.setMonto_pagado(monto_pagado);
			request.setPropina(propina);
			request.setTipo_tarjeta(tipo_tarjeta);
			request.setTransaccion_addcel(transaccion_addcel);
			
			String jsonReq = utils.objectToJson(request);
			jsonReq = AddcelCrypto.encryptSensitive(formato.format(new Date()), jsonReq);
			jsonReq = URLEncoder.encode(jsonReq, "UTF-8");
			
			String endpoint = mapperF.urlSucursal(id_sucursal);
			endpoint = URLEncoder.encode(endpoint, "UTF-8");
			
			String wrapperUrl = mapperF.getParametro("@FishersWSWraper");
			
			String jsonResp = utils.sendOperWS(wrapperUrl, jsonReq, endpoint, 2);
			
			jsonResp = AddcelCrypto.decryptSensitive(jsonResp);
							
			JSONObject jsObject = new JSONObject(jsonResp);
			
			if(jsObject.has("idError")){
				String resultado = (String)jsObject.get("idError");
				if(resultado.equals("0"))
					status = true;
			}			
		}catch(Exception e){
			logger.error("Ocurrio un error durante registro de pago a fishers: {}", e);
			status = false;
		}
		return status;
	}
}