package com.addcel.fishers.model.vo;

public class SucursalVO {
	private long id_sucursal;
	private long id_marca;
	private long id_caja;
	private String descripcion;
	public long getId_sucursal() {
		return id_sucursal;
	}
	public void setId_sucursal(long id_sucursal) {
		this.id_sucursal = id_sucursal;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public long getId_marca() {
		return id_marca;
	}
	public void setId_marca(long id_marca) {
		this.id_marca = id_marca;
	}
	public long getId_caja() {
		return id_caja;
	}
	public void setId_caja(long id_caja) {
		this.id_caja = id_caja;
	}
}
