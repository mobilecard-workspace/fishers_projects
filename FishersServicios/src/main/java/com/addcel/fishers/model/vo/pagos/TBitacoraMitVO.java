package com.addcel.fishers.model.vo.pagos;

import com.thoughtworks.xstream.annotations.XStreamAlias;


public class TBitacoraMitVO {
	// request
	private Long idBitacoraMIT;	
	private String id_company;
	private String id_branch;
	private String country;
	private String user;
	private String pwd;
	private String merchant;
	private String reference;
	private String tp_operation;
	private String amount;
	private String currency;
	private String usrtransacction;
	private String version;
	private String no_operacion;
	private String operacion;	
	private String crypto;
	private String type;
	private String name;
	private String number;
	private String expmonth;
	private String expyear;
	//response
	private String response;
	private String foliocpagos;
	private String auth;
	private String cd_response;
	private String cd_error;
	private String time;
	private String date;
	private String nb_error;
	
	public String getId_company() {
		return id_company;
	}
	public void setId_company(String id_company) {
		this.id_company = id_company;
	}
	public String getId_branch() {
		return id_branch;
	}
	public void setId_branch(String id_branch) {
		this.id_branch = id_branch;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getMerchant() {
		return merchant;
	}
	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getTp_operation() {
		return tp_operation;
	}
	public void setTp_operation(String tp_operation) {
		this.tp_operation = tp_operation;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getUsrtransacction() {
		return usrtransacction;
	}
	public void setUsrtransacction(String usrtransacction) {
		this.usrtransacction = usrtransacction;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getNo_operacion() {
		return no_operacion;
	}
	public void setNo_operacion(String no_operacion) {
		this.no_operacion = no_operacion;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public String getCrypto() {
		return crypto;
	}
	public void setCrypto(String crypto) {
		this.crypto = crypto;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getExpmonth() {
		return expmonth;
	}
	public void setExpmonth(String expmonth) {
		this.expmonth = expmonth;
	}
	public String getExpyear() {
		return expyear;
	}
	public void setExpyear(String expyear) {
		this.expyear = expyear;
	}
	public Long getIdBitacoraMIT() {
		return idBitacoraMIT;
	}
	public void setIdBitacoraMIT(Long idBitacoraMIT) {
		this.idBitacoraMIT = idBitacoraMIT;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getFoliocpagos() {
		return foliocpagos;
	}
	public void setFoliocpagos(String foliocpagos) {
		this.foliocpagos = foliocpagos;
	}
	public String getCd_response() {
		return cd_response;
	}
	public void setCd_response(String cd_response) {
		this.cd_response = cd_response;
	}
	public String getCd_error() {
		return cd_error;
	}
	public void setCd_error(String cd_error) {
		this.cd_error = cd_error;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNb_error() {
		return nb_error;
	}
	public void setNb_error(String nb_error) {
		this.nb_error = nb_error;
	}
}
