package com.addcel.fishers.model.mapper;

import mx.mobilecard.mitec.operaciones.RESPONSETX3DS;
import mx.mobilecard.mitec.operaciones.VMCAMEXAUTH3DS;

import org.apache.ibatis.annotations.Param;

import com.addcel.fishers.model.vo.FishersCuentaVO;
import com.addcel.fishers.model.vo.FishersCuentaDetalleVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraMitAuthVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraMitVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.fishers.model.vo.pagos.TBitacoraVO;

public interface BitacorasMapper {
	int addBitacoraProsa(TBitacoraProsaVO b);
	int addBitacora(TBitacoraVO b);
	int updateBitacoraProsa(TBitacoraProsaVO b);
	int updateBitacora(TBitacoraVO b);
	int addFHeader(FishersCuentaVO td);
	int addFDetalle(FishersCuentaDetalleVO td);
	int updateFHeader(@Param(value="idBitacora")long idBitacora,@Param(value="status")int status, @Param(value="statusEnvio")int statusEnvio);
	int addMitBitacoraAuth(TBitacoraMitAuthVO ta);
	int updateMitBitacoraAuth(RESPONSETX3DS r3ds);
	int addMitBitacora(TBitacoraMitVO tbm);
	int updateMitBitacora(TBitacoraMitVO tbm);
	String getParametro(@Param(value = "clave")String clave);
}
