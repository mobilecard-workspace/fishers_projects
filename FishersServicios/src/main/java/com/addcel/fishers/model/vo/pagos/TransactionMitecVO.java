package com.addcel.fishers.model.vo.pagos;

public class TransactionMitecVO {
	private String strResponse;
	private String strIdCompany;
	private String strIdMerchant;
	private String reference;
	private String flujoid;
	public String getStrResponse() {
		return strResponse;
	}
	public void setStrResponse(String strResponse) {
		this.strResponse = strResponse;
	}
	public String getStrIdCompany() {
		return strIdCompany;
	}
	public void setStrIdCompany(String strIdCompany) {
		this.strIdCompany = strIdCompany;
	}
	public String getStrIdMerchant() {
		return strIdMerchant;
	}
	public void setStrIdMerchant(String strIdMerchant) {
		this.strIdMerchant = strIdMerchant;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getFlujoid() {
		return flujoid;
	}
	public void setFlujoid(String flujoid) {
		this.flujoid = flujoid;
	}

}
