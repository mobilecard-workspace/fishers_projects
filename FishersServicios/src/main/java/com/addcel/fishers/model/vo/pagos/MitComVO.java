package com.addcel.fishers.model.vo.pagos;

public class MitComVO {
	private String strIdCompany;
	private String strIdMerchant;
	private String strXmlParameters;
	private int idError;
	private String mensajeError;
	private String urlMitAction;
	private String referencia;
	private String flujoid;

	// Atributos Opcionales	
	
	public MitComVO(int idError, String mensajeError) {
		this.idError = idError;
		this.mensajeError = mensajeError;
	}

	public MitComVO() {
	}

	public String getStrIdCompany() {
		return strIdCompany;
	}
	public void setStrIdCompany(String strIdCompany) {
		this.strIdCompany = strIdCompany;
	}
	public String getStrIdMerchant() {
		return strIdMerchant;
	}
	public void setStrIdMerchant(String strIdMerchant) {
		this.strIdMerchant = strIdMerchant;
	}
	public String getStrXmlParameters() {
		return strXmlParameters;
	}
	public void setStrXmlParameters(String strXmlParameters) {
		this.strXmlParameters = strXmlParameters;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getUrlMitAction() {
		return urlMitAction;
	}

	public void setUrlMitAction(String urlMitAction) {
		this.urlMitAction = urlMitAction;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getFlujoid() {
		return flujoid;
	}

	public void setFlujoid(String flujoid) {
		this.flujoid = flujoid;
	}

}
