package com.addcel.fishers.model.vo.pagos;

import org.apache.ibatis.annotations.Param;

public class TBitacoraMitAuthVO {
	public TBitacoraMitAuthVO(){}
	public TBitacoraMitAuthVO(String bs_idCompany, String bs_idBranch,
			String bs_country, String bs_user, String bs_pwd,
			String tx_merchant, String tx_reference, String tx_amount,
			String tx_currency, String cc_name, String cc_expMonth,
			String cc_expYear) {
		super();
		this.bs_idCompany = bs_idCompany;
		this.bs_idBranch = bs_idBranch;
		this.bs_country = bs_country;
		this.bs_user = bs_user;
		this.bs_pwd = bs_pwd;
		this.tx_merchant = tx_merchant;
		this.tx_reference = tx_reference;
		this.tx_amount = tx_amount;
		this.tx_currency = tx_currency;
		this.cc_name = cc_name;
		this.cc_expMonth = cc_expMonth;
		this.cc_expYear = cc_expYear;
	}
	// Request
	private String bs_idCompany;
    private String bs_idBranch;
    private String bs_country;
    private String bs_user;
    private String bs_pwd;
    private String tx_merchant;
    private String tx_reference;
    private String tx_amount;
    private String tx_currency; 
    private String cc_name;
    private String cc_expMonth;
    private String cc_expYear;
    private String cc_number;
    private String cc_cvv;
	public String getBs_idCompany() {
		return bs_idCompany;
	}
	public void setBs_idCompany(String bs_idCompany) {
		this.bs_idCompany = bs_idCompany;
	}
	public String getBs_idBranch() {
		return bs_idBranch;
	}
	public void setBs_idBranch(String bs_idBranch) {
		this.bs_idBranch = bs_idBranch;
	}
	public String getBs_country() {
		return bs_country;
	}
	public void setBs_country(String bs_country) {
		this.bs_country = bs_country;
	}
	public String getBs_user() {
		return bs_user;
	}
	public void setBs_user(String bs_user) {
		this.bs_user = bs_user;
	}
	public String getBs_pwd() {
		return bs_pwd;
	}
	public void setBs_pwd(String bs_pwd) {
		this.bs_pwd = bs_pwd;
	}
	public String getTx_merchant() {
		return tx_merchant;
	}
	public void setTx_merchant(String tx_merchant) {
		this.tx_merchant = tx_merchant;
	}
	public String getTx_reference() {
		return tx_reference;
	}
	public void setTx_reference(String tx_reference) {
		this.tx_reference = tx_reference;
	}
	public String getTx_amount() {
		return tx_amount;
	}
	public void setTx_amount(String tx_amount) {
		this.tx_amount = tx_amount;
	}
	public String getTx_currency() {
		return tx_currency;
	}
	public void setTx_currency(String tx_currency) {
		this.tx_currency = tx_currency;
	}
	public String getCc_name() {
		return cc_name;
	}
	public void setCc_name(String cc_name) {
		this.cc_name = cc_name;
	}
	public String getCc_expMonth() {
		return cc_expMonth;
	}
	public void setCc_expMonth(String cc_expMonth) {
		this.cc_expMonth = cc_expMonth;
	}
	public String getCc_expYear() {
		return cc_expYear;
	}
	public void setCc_expYear(String cc_expYear) {
		this.cc_expYear = cc_expYear;
	}
	public String getCc_number() {
		return cc_number;
	}
	public void setCc_number(String cc_number) {
		this.cc_number = cc_number;
	}
	public String getCc_cvv() {
		return cc_cvv;
	}
	public void setCc_cvv(String cc_cvv) {
		this.cc_cvv = cc_cvv;
	}
    
}
