package com.addcel.fishers.model.vo;

public class SucursalDatosMitecVO {
	private String afiliacion; 	
	private String afiliacionAmex; 	
	private String country3D; 	
	private String id_company3D; 	
	private String id_branch3D; 	
	private String pwd3D; 	
	private String user3D; 	
	private String afiliacion3d; 	
	private String semilla; 	
	private String country; 	
	private String id_branch; 	
	private String id_company; 	
	private String pwd; 	
	private String user;
	private String crypto;
	private String tp_operation;
	private String currency;
	private String version;
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public String getAfiliacionAmex() {
		return afiliacionAmex;
	}
	public void setAfiliacionAmex(String afiliacionAmex) {
		this.afiliacionAmex = afiliacionAmex;
	}
	public String getCountry3D() {
		return country3D;
	}
	public void setCountry3D(String country3d) {
		country3D = country3d;
	}
	public String getId_company3D() {
		return id_company3D;
	}
	public void setId_company3D(String id_company3D) {
		this.id_company3D = id_company3D;
	}
	public String getId_branch3D() {
		return id_branch3D;
	}
	public void setId_branch3D(String id_branch3D) {
		this.id_branch3D = id_branch3D;
	}
	public String getPwd3D() {
		return pwd3D;
	}
	public void setPwd3D(String pwd3d) {
		pwd3D = pwd3d;
	}
	public String getUser3D() {
		return user3D;
	}
	public void setUser3D(String user3d) {
		user3D = user3d;
	}
	public String getAfiliacion3d() {
		return afiliacion3d;
	}
	public void setAfiliacion3d(String afiliacion3d) {
		this.afiliacion3d = afiliacion3d;
	}
	public String getSemilla() {
		return semilla;
	}
	public void setSemilla(String semilla) {
		this.semilla = semilla;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getId_branch() {
		return id_branch;
	}
	public void setId_branch(String id_branch) {
		this.id_branch = id_branch;
	}
	public String getId_company() {
		return id_company;
	}
	public void setId_company(String id_company) {
		this.id_company = id_company;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getCrypto() {
		return crypto;
	}
	public void setCrypto(String crypto) {
		this.crypto = crypto;
	}
	public String getTp_operation() {
		return tp_operation;
	}
	public void setTp_operation(String tp_operation) {
		this.tp_operation = tp_operation;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}

}
