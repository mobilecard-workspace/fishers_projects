package com.addcel.fishers.model.vo.response;

import com.addcel.fishers.model.vo.FishersCuentaVO;

public class DetalleVO extends FishersCuentaVO {

	private String referencia;
	private String nombre;
	private String last4d;

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLast4d() {
		return last4d;
	}

	public void setLast4d(String last4d) {
		this.last4d = last4d;
	}
}