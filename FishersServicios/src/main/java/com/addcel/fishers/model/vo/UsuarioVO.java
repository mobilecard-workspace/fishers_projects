package com.addcel.fishers.model.vo;

import java.util.Date;

public class UsuarioVO {
	private long idUsuario;
	private String usrLogin;
	private String usrPwd;
	private Date usrFechaNac;
	private String usrTelefono;
	private Date usrFechaRegistro;
	private String usrNombre;
	private String usrApellido;
	private String usrDireccion;
	private String usrTdcNumero;
	private String usrTdcVigencia;
	private int idBanco;
	private int idTipoTarjeta;
	private int idProveedor;
	private int idUsrStatus;
	private String email;
	private String imei;
	private String tipo;
	private String software;
	private String modelo;
	private String wkey;
	private String telefonoOriginal;
	private String usrMaterno;
	private String usrSexo;
	private String usrTelCasa;
	private String usrTelOficina;
	private int usrIdEstado;
	private String usrCiudad;
	private String usrCalle;
	private int usrNumExt;
	private String usrNumInterior;
	private String usrColonia;
	private String usrCp;
	private String usrDomAmex;
	private String usrTerminos;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getUsrLogin() {
		return usrLogin;
	}
	public void setUsrLogin(String usrLogin) {
		this.usrLogin = usrLogin;
	}
	public String getUsrPwd() {
		return usrPwd;
	}
	public void setUsrPwd(String usrPwd) {
		this.usrPwd = usrPwd;
	}
	public Date getUsrFechaNac() {
		return usrFechaNac;
	}
	public void setUsrFechaNac(Date usrFechaNac) {
		this.usrFechaNac = usrFechaNac;
	}
	public String getUsrTelefono() {
		return usrTelefono;
	}
	public void setUsrTelefono(String usrTelefono) {
		this.usrTelefono = usrTelefono;
	}
	public Date getUsrFechaRegistro() {
		return usrFechaRegistro;
	}
	public void setUsrFechaRegistro(Date usrFechaRegistro) {
		this.usrFechaRegistro = usrFechaRegistro;
	}
	public String getUsrNombre() {
		return usrNombre;
	}
	public void setUsrNombre(String usrNombre) {
		this.usrNombre = usrNombre;
	}
	public String getUsrApellido() {
		return usrApellido;
	}
	public void setUsrApellido(String usrApellido) {
		this.usrApellido = usrApellido;
	}
	public String getUsrDireccion() {
		return usrDireccion;
	}
	public void setUsrDireccion(String usrDireccion) {
		this.usrDireccion = usrDireccion;
	}
	public String getUsrTdcNumero() {
		return usrTdcNumero;
	}
	public void setUsrTdcNumero(String usrTdcNumero) {
		this.usrTdcNumero = usrTdcNumero;
	}
	public String getUsrTdcVigencia() {
		return usrTdcVigencia;
	}
	public void setUsrTdcVigencia(String usrTdcVigencia) {
		this.usrTdcVigencia = usrTdcVigencia;
	}
	public int getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(int idBanco) {
		this.idBanco = idBanco;
	}
	public int getIdTipoTarjeta() {
		return idTipoTarjeta;
	}
	public void setIdTipoTarjeta(int idTipoTarjeta) {
		this.idTipoTarjeta = idTipoTarjeta;
	}
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	public int getIdUsrStatus() {
		return idUsrStatus;
	}
	public void setIdUsrStatus(int idUsrStatus) {
		this.idUsrStatus = idUsrStatus;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getWkey() {
		return wkey;
	}
	public void setWkey(String wkey) {
		this.wkey = wkey;
	}
	public String getTelefonoOriginal() {
		return telefonoOriginal;
	}
	public void setTelefonoOriginal(String telefonoOriginal) {
		this.telefonoOriginal = telefonoOriginal;
	}
	public String getUsrMaterno() {
		return usrMaterno;
	}
	public void setUsrMaterno(String usrMaterno) {
		this.usrMaterno = usrMaterno;
	}
	public String getUsrSexo() {
		return usrSexo;
	}
	public void setUsrSexo(String usrSexo) {
		this.usrSexo = usrSexo;
	}
	public String getUsrTelCasa() {
		return usrTelCasa;
	}
	public void setUsrTelCasa(String usrTelCasa) {
		this.usrTelCasa = usrTelCasa;
	}
	public String getUsrTelOficina() {
		return usrTelOficina;
	}
	public void setUsrTelOficina(String usrTelOficina) {
		this.usrTelOficina = usrTelOficina;
	}
	public int getUsrIdEstado() {
		return usrIdEstado;
	}
	public void setUsrIdEstado(int usrIdEstado) {
		this.usrIdEstado = usrIdEstado;
	}
	public String getUsrCiudad() {
		return usrCiudad;
	}
	public void setUsrCiudad(String usrCiudad) {
		this.usrCiudad = usrCiudad;
	}
	public String getUsrCalle() {
		return usrCalle;
	}
	public void setUsrCalle(String usrCalle) {
		this.usrCalle = usrCalle;
	}
	public int getUsrNumExt() {
		return usrNumExt;
	}
	public void setUsrNumExt(int usrNumExt) {
		this.usrNumExt = usrNumExt;
	}
	public String getUsrNumInterior() {
		return usrNumInterior;
	}
	public void setUsrNumInterior(String usrNumInterior) {
		this.usrNumInterior = usrNumInterior;
	}
	public String getUsrColonia() {
		return usrColonia;
	}
	public void setUsrColonia(String usrColonia) {
		this.usrColonia = usrColonia;
	}
	public String getUsrCp() {
		return usrCp;
	}
	public void setUsrCp(String usrCp) {
		this.usrCp = usrCp;
	}
	public String getUsrDomAmex() {
		return usrDomAmex;
	}
	public void setUsrDomAmex(String usrDomAmex) {
		this.usrDomAmex = usrDomAmex;
	}
	public String getUsrTerminos() {
		return usrTerminos;
	}
	public void setUsrTerminos(String usrTerminos) {
		this.usrTerminos = usrTerminos;
	}
}
