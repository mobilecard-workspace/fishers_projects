package com.addcel.fishers.model.vo.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestCuenta {
	private long id_sucursal;
	private long id_caja;
	private String numero_cuenta;
	public long getId_sucursal() {
		return id_sucursal;
	}
	public void setId_sucursal(long id_sucursal) {
		this.id_sucursal = id_sucursal;
	}
	public long getId_caja() {
		return id_caja;
	}
	public void setId_caja(long id_caja) {
		this.id_caja = id_caja;
	}
	public String getNumero_cuenta() {
		return numero_cuenta;
	}
	public void setNumero_cuenta(String numero_cuenta) {
		this.numero_cuenta = numero_cuenta;
	}
}
