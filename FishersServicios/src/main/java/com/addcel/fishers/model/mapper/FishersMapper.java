package com.addcel.fishers.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.addcel.fishers.model.vo.FishersCuentaDetalleVO;
import com.addcel.fishers.model.vo.FishersDetallePagoVO;
import com.addcel.fishers.model.vo.MarcaVO;
import com.addcel.fishers.model.vo.SucursalDatosMitecVO;
import com.addcel.fishers.model.vo.SucursalVO;
import com.addcel.fishers.model.vo.UsuarioVO;
import com.addcel.fishers.model.vo.response.DetalleVO;


public interface FishersMapper {

	List<SucursalVO> listaSucursales(@Param(value = "id_marca") Long id_marca);
	
	String descripcionCaja(@Param(value = "id_sucursal") Long id_sucursal, @Param(value = "id_caja") Long id_caja);
	
	String descripcionSucursal(@Param(value = "id_sucursal") Long id_sucursal);
	
	String urlSucursal(@Param(value = "id_sucursal") Long id_sucursal);

	String getFechaActual();

	int difFechaMin(String fechaToken);

	List<DetalleVO> getHeader(@Param(value = "user") String user,
			@Param(value = "fi") String fechaInicial,
			@Param(value = "ff") String fechaFinal,
			@Param(value = "idBitacora") String idBitacora);
	
	List<FishersCuentaDetalleVO> getDetalle(@Param(value = "id_pago") Long id_pago);
	
	String getAfiliacion(@Param(value = "id_sucursal") Long id_sucursal, @Param(value = "id_caja") Long id_caja);
	
	String getAfiliacionAmex(@Param(value = "id_sucursal") Long id_sucursal, @Param(value = "id_caja") Long id_caja);
	
	String getParametro(@Param(value = "clave")String clave);
	
	UsuarioVO getAddcelUsuario(@Param(value = "idUsuario") long idUsuario);
	
	List<MarcaVO> listaMarcas(@Param(value = "id_marca") long id_marca);
	
	List<FishersDetallePagoVO> getDetallePago(@Param(value="id_sucursal")Long id_sucursal, @Param(value="id_caja")Long id_caja, @Param(value="numero_cuenta")String numero_cuenta);
	
	SucursalDatosMitecVO getSucursalDatosMitec(@Param(value = "id_sucursal") Long id_sucursal);
}
