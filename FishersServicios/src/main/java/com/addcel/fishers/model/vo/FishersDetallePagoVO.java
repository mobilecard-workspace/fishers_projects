package com.addcel.fishers.model.vo;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.fishers.services.FishersPagosService;
import com.addcel.utils.AddcelCrypto;

public class FishersDetallePagoVO {
	private static final Logger logger = LoggerFactory.getLogger(FishersPagosService.class);
    private Long id_pago;
    private BigDecimal total;
    private String autorizacion;
    private String id_bitacora;
    private String tarjeta;
	public Long getId_pago() {
		return id_pago;
	}
	public void setId_pago(Long id_pago) {
		this.id_pago = id_pago;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		String tarjetaDecrypt = "NO HAY INFORMACION";
		try{
			tarjetaDecrypt = AddcelCrypto.decryptTarjeta(tarjeta);
			switch(tarjetaDecrypt.length()){
				case 15:
					tarjetaDecrypt = "XXXX-XXXXXX-X" + tarjetaDecrypt.substring(tarjetaDecrypt.length()-4);
					break;
				case 16:
					tarjetaDecrypt = "XXXX-XXXX-XXXX-" + tarjetaDecrypt.substring(tarjetaDecrypt.length()-4);
					break;					
			}
		}catch(Exception e){
			logger.error("Error al desencriptar tarjeta: " + e.getMessage());
		}
		this.tarjeta = tarjetaDecrypt;
	}


}
