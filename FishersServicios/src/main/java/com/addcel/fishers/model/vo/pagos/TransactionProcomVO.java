/**
 * 
 */
package com.addcel.fishers.model.vo.pagos;

import java.io.Serializable;

/**
 * @author ELopez
 *
 */
public class TransactionProcomVO implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2055280406078330116L;
	private long transactionProcomId;
	private long transactionProcomMnt;
	private long transactionProcomMrchId;
	private long transactionProsaId;
	private String emResponse;
	private String emTotal;
	private String emOrderID;
	private String emMerchant;
	private String emStore;
	private String emTerm;
	private String emRefNum;
	private String emAuth;
	private String emDigest;
	
	private String moneda;
	private String producto;
	private String fecha;
		
	public long getTransactionProcomId() {
		return transactionProcomId;
	}
	public void setTransactionProcomId(long transactionProcomId) {
		this.transactionProcomId = transactionProcomId;
	}
	public long getTransactionProcomMnt() {
		return transactionProcomMnt;
	}
	public void setTransactionProcomMnt(long transactionProcomMnt) {
		this.transactionProcomMnt = transactionProcomMnt;
	}
	public long getTransactionProcomMrchId() {
		return transactionProcomMrchId;
	}
	public void setTransactionProcomMrchId(long transactionProcomMrchId) {
		this.transactionProcomMrchId = transactionProcomMrchId;
	}
	public long getTransactionProsaId() {
		return transactionProsaId;
	}
	public void setTransactionProsaId(long transactionProsaId) {
		this.transactionProsaId = transactionProsaId;
	}
	public String getEmResponse() {
		return emResponse;
	}
	public void setEmResponse(String emResponse) {
		this.emResponse = emResponse;
	}
	public String getEmTotal() {
		return emTotal;
	}
	public void setEmTotal(String emTotal) {
		this.emTotal = emTotal;
	}
	public String getEmOrderID() {
		return emOrderID;
	}
	public void setEmOrderID(String emOrderID) {
		this.emOrderID = emOrderID;
	}
	public String getEmMerchant() {
		return emMerchant;
	}
	public void setEmMerchant(String emMerchant) {
		this.emMerchant = emMerchant;
	}
	public String getEmStore() {
		return emStore;
	}
	public void setEmStore(String emStore) {
		this.emStore = emStore;
	}
	public String getEmTerm() {
		return emTerm;
	}
	public void setEmTerm(String emTerm) {
		this.emTerm = emTerm;
	}
	public String getEmRefNum() {
		return emRefNum;
	}
	public void setEmRefNum(String emRefNum) {
		this.emRefNum = emRefNum;
	}
	public String getEmAuth() {
		return emAuth;
	}
	public void setEmAuth(String emAuth) {
		this.emAuth = emAuth;
	}
	public String getEmDigest() {
		return emDigest;
	}
	public void setEmDigest(String emDigest) {
		this.emDigest = emDigest;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
}
