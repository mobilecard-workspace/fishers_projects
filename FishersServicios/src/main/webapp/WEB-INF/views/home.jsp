<%-- @ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"--%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
</head>
<body>
	<h1>Hello world!</h1>

	<P>The time on the server is ${serverTime}.</P>


	<form action="/FishersServicios/getToken" method="post">
		<input type="submit" value="GetToken">
	</form>

	<form action="/FishersServicios/getSucursales" method="post">
		<input name="json" type="text" value="">
		<br>
		<input type="submit" value="GetSucursales">
	</form>
	
	<form action="/FishersServicios/getMarcas" method="post">
		<input type="submit" value="GetMarcas">
	</form>
	
	<form action="/FishersServicios/pago-visa" method="post">
		<input name="json" type="text" value="">
		<br>
		<input type="submit" value="Pago Visa">
	</form>
	
	<form action="/FishersServicios/pago-amex" method="post">
		<input name="json" type="text" value="">
		<br>
		<input type="submit" value="Pago Amex">
	</form>

	<form action="/FishersServicios/pago-3dSecure" method="post">
		<input name="json" type="text" value="">
		<br>
		<input type="submit" value="Pago 3dSecure">
	</form>

	<form action="/FishersServicios/pago-3dSecureAmex" method="post">
		<input name="json" type="text" value="">
		<br>
		<input type="submit" value="Pago 3dSecure Amex">
	</form>

	<form action="/FishersServicios/consultaCuenta" method="post">
		<input name="json" type="text"  size="100"
		value="">
		<br>
		<input type="submit" value="Consulta Cuenta">
	</form> 
	 
</body>
</html>
