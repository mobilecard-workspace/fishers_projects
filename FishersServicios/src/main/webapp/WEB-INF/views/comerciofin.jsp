<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Redirección Mitec</title>
<script type="text/javascript">
	function sendform() {
		document.form1.submit();
	}
</script>
</head>
<body onload="sendform();">
	<form method="post" name="form1" action="${mitec.urlMitAction }">
		<input type="hidden" name="strIdCompany" value="${mitec.strIdCompany}" /> 
		<input type="hidden" name="strIdMerchant" value="${mitec.strIdMerchant}" /> 
		<input type="hidden" name="strXmlParameters" value="${mitec.strXmlParameters}" /> 
		<input type="hidden" name="referencia" value="${mitec.referencia}" />
		<input type="hidden" name="flujoid" value="${mitec.flujoid}"/>
	</form>
</body>
</html>