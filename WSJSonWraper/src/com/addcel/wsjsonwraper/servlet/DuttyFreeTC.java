package com.addcel.wsjsonwraper.servlet;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.addcel.wsjsonwraper.services.DuttyFreeServices;


public class DuttyFreeTC extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6133106784801543061L;
	private static Logger log = Logger.getLogger(Fishers.class);

	public DuttyFreeTC(){
		super();
	}
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String json = "{\"error\":\"1\",\"numError\":\"A0001\"}";
		try{
			DuttyFreeServices dts = new DuttyFreeServices();
			json = dts.consultaTC();
			
		} catch(Exception e){
			log.error("Error al consultar tc: "+ e.getMessage());
		}
		response.setContentType("application/Json");
		response.setContentLength(json.length());
		OutputStream out = response.getOutputStream();
		out.write(json.getBytes());
		out.flush();
		out.close();
	}
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}
