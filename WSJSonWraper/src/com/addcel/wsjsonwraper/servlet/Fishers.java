package com.addcel.wsjsonwraper.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.addcel.utils.AddcelCrypto;
import com.addcel.wsjsonwraper.services.FishersServices;

public class Fishers extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5868789150540832132L;
	private static Logger log = Logger.getLogger(Fishers.class);
	private static final String patron = "ddhhmmss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);

	public Fishers(){
		super();
	}
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		boolean isError = false;
		FishersServices services = new FishersServices();
		
		String json = (String) request.getAttribute("json");
		
		log.error("json: " + json);

		if (json == null) {
			json = (String) request.getParameter("json");
		}

		if (json == null) {
			try {
				json = getFromStream(request);
			} catch (UnsupportedEncodingException ueE) {
				isError = true;
				json = "{\"idError\":\"1\",\"mensajeError\":\"UE1001\"}";
			} catch (IOException ioE) {
				isError = true;
				json = "{\"idError\":\"1\",\"mensajeError\":\"A1001\"}";
			}
		}

		
		if (json != null) {
			if (!isError) {
				int oper = Integer.parseInt(request.getParameter("oper"));
				log.info("oper = "+oper);
				String endpoint = request.getParameter("endpoint");
				log.info("endpoint = "+endpoint);
				switch(oper){
					case 1:
						json = services.consultaCuenta(json, endpoint);
						break;
					case 2:
						json = services.registraPago(json, endpoint);
						break;
					default:
						json = "{\"idError\":\"1\",\"mensajeError\":\"Operacion no soportada\"}";
						break;
				}
				
			}				
		} else {

			json = "{\"idError\":\"1\",\"mensajeError\":\"A0001\"}";
		}
		
		if(json != null){
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
		}


		response.setContentType("application/Json");
		response.setContentLength(json.length());
		OutputStream out = response.getOutputStream();
		out.write(json.getBytes());
		out.flush();
		out.close();
	}

	private String getFromStream(HttpServletRequest request)
			throws UnsupportedEncodingException, IOException {

		String json = null;
		InputStream is = null;

		is = (InputStream) request.getInputStream();

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {

				if (is != null) {
					is.close();
				}
			}
			json = writer.toString();
		}

		return json;
	}
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
