package com.addcel.wsjsonwraper.clients.handler;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.GenericHandler;
import javax.xml.rpc.handler.MessageContext;
import javax.xml.rpc.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;

public class SimpleHandler extends GenericHandler {
	private static Logger log = Logger.getLogger(SimpleHandler.class);
	@Override
	public QName[] getHeaders() {
		return null;
	}
	@Override
	public boolean handleResponse(MessageContext context){
		log.info("Response: " + getStringMessage(context));	
		return super.handleResponse(context);
	}
	
	@Override
	public boolean handleRequest(MessageContext context){
		log.info("Request: " + getStringMessage(context));
		return super.handleRequest(context);
	}
	@Override
	public boolean handleFault(MessageContext context){
		log.info("Fault: " + getStringMessage(context));
		return super.handleFault(context);
	}
	private String getStringMessage(MessageContext context){
		String res = null;
		try {
			SOAPMessageContext ctx = (SOAPMessageContext) context;
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			ctx.getMessage().writeTo(stream);
			byte[] items = stream.toByteArray();
			res = new String(items);
		} catch(Exception e){
		}
		return res;
	}

}
