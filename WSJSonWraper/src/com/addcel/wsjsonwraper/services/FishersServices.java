package com.addcel.wsjsonwraper.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.tempuri.IntercambioSoapProxy;
import org.tempuri.Cuenta;
import org.tempuri.DetalleCuenta;
import com.addcel.fishers.model.vo.FishersCuentaDetalleVO;
import com.addcel.fishers.model.vo.FishersCuentaVO;
import com.addcel.fishers.model.vo.ConsultaRequestVO;
import com.addcel.fishers.model.vo.RegistrarPagoRequestVO;
import org.apache.log4j.Logger;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

public class FishersServices {
	private static Logger log = Logger.getLogger(FishersServices.class);
	public String consultaCuenta(String json, String endpoint){
		
		ConsultaRequestVO request;
		Gson gson = new Gson();
		IntercambioSoapProxy proxy = new IntercambioSoapProxy(endpoint);		
		String jsonRes = "{\"idError\":\"1\",\"mensajeError\":\"A1001\"}";
		
		try{
			log.info("Dentro de FishersServices.consultaCuenta");
			
			json = AddcelCrypto.decryptSensitive(json);
			
			request = gson.fromJson(json, ConsultaRequestVO.class);
			
			Cuenta cuenta = proxy.obtenerCuenta(request.getCaja(), request.getCuenta());
			
			if(cuenta!=null){
				log.info("Datos Obtenidos, parseando");
				FishersCuentaVO fishersCuenta = new FishersCuentaVO();
				fishersCuenta.setId_caja(request.getCaja());
				fishersCuenta.setNumero_cuenta(request.getCuenta());
				fishersCuenta.setSubtotal(new BigDecimal(cuenta.getSubtotal()));
				fishersCuenta.setDescuento(new BigDecimal(cuenta.getDescuento()));
				fishersCuenta.setIva(new BigDecimal(cuenta.getIva()));			
				fishersCuenta.setTotal(new BigDecimal(cuenta.getTotal()));
			
				
				List<FishersCuentaDetalleVO> comidas = new ArrayList<FishersCuentaDetalleVO>();
				List<FishersCuentaDetalleVO> bebidas = new ArrayList<FishersCuentaDetalleVO>();
				List<FishersCuentaDetalleVO> otros = new ArrayList<FishersCuentaDetalleVO>();
				
				if(cuenta.getDetalle()!=null&&cuenta.getDetalle().length!=0)
				{
					for(DetalleCuenta detalle:cuenta.getDetalle()){
						FishersCuentaDetalleVO tempDet = new FishersCuentaDetalleVO();
						tempDet.setId_producto(detalle.getId_producto());
						tempDet.setDescripcion_producto(detalle.getDescripcion_producto());
						tempDet.setCantidad(new BigDecimal(detalle.getCantidad()));
						tempDet.setImporte(new BigDecimal(detalle.getImporte()));
						tempDet.setId_pago(new Long(0));
						tempDet.setPrecio_unitario(new BigDecimal(0));
					
						switch(detalle.getCategoria()){
							case 1:
								comidas.add(tempDet);
								break;
							case 2:
								bebidas.add(tempDet);
								break;
							case 3:
							default:
								otros.add(tempDet);
								break;						
						}
					}
				
					fishersCuenta.setComida(comidas);
					fishersCuenta.setBebida(bebidas);
					fishersCuenta.setOtros(otros);
				
					jsonRes = gson.toJson(fishersCuenta, FishersCuentaVO.class);
					log.info("Regresando datos de consulta");
				}
				else
					jsonRes = "{\"idError\":\"2\",\"mensajeError\":\"La cuenta solicitada no tiene detalle de consumo, favor de reintentar\"}";
			} else
				jsonRes = "{\"idError\":\"2\",\"mensajeError\":\"No existen datos para la cuenta solicitada, favor de verificarla\"}";
			
			
		}catch(Exception e){
			jsonRes = "{\"error\":\"1\",\"numError\":\"A1001\"}";
		}		
		return jsonRes;
		
	}
	public String registraPago(String json, String endpoint){
		RegistrarPagoRequestVO request;
		Gson gson = new Gson();
		IntercambioSoapProxy proxy = new IntercambioSoapProxy(endpoint);
		String jsonRes = "{\"idError\":\"1\",\"numError\":\"A1001\"}";
		try{
			log.info("Dentro de FishersServices.registraPago");
			
			json = AddcelCrypto.decryptSensitive(json);
			
			request = gson.fromJson(json, RegistrarPagoRequestVO.class);
			
			boolean respuesta = proxy.registrarPago(request.getCaja(), request.getCuenta_num(), 
					request.getFecha_operacion(), request.getTarjeta(), request.getAutorizacion(), 
					request.getMonto_pagado().doubleValue(), request.getPropina().doubleValue(), 
					request.getTipo_tarjeta(), request.getTransaccion_addcel());
			log.info("respuesta: " + respuesta);
			if(respuesta)
				jsonRes= "{\"idError\":\"0\",\"mensajeError\":\"Registrado exitosamente\"}";
			else
				jsonRes= "{\"idError\":\"0\",\"mensajeError\":\"Error al registrar\"}";
		} catch(Exception e){
			log.error("Error al registrar pago" + e.getMessage());
			jsonRes = "{\"idError\":\"1\",\"mensajeError\":\"A1001\"}";
		}
		return jsonRes;
	}
}
