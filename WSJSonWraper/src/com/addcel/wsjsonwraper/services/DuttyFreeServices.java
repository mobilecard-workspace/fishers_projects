package com.addcel.wsjsonwraper.services;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import mx.org.banxico.dgie.ws.DgieWSPortProxy;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.addcel.fishers.model.vo.TipoCambioVO;
import com.google.gson.Gson;

public class DuttyFreeServices {
	private static Logger log = Logger.getLogger(FishersServices.class);
	
	public String consultaTC(){
		String json = null;
		TipoCambioVO tcVO = new TipoCambioVO();
		Gson gson = new Gson();
		try{
			DgieWSPortProxy proxy = new DgieWSPortProxy();
			String xml = proxy.tiposDeCambioBanxico();
			String tc = getTC(xml);
			if(!tc.equals("0.0000")){
				tcVO.setCode(0);
				tcVO.setConversionRate(new Double(tc));
				tcVO.setMessage("Exito al encontrar el tipo de cambio");
			}else{
				tcVO.setCode(1);
				tcVO.setConversionRate(new Double(0));
				tcVO.setMessage("Error al encontrar el tipo de cambio");
			}
		} catch(Exception e){
			log.error("Error al consultar TC");
			tcVO.setCode(1);
			tcVO.setConversionRate(new Double(0));
			tcVO.setMessage("Error al encontrar el tipo de cambio"+ e.getMessage());
			json = null;
		}
		json = gson.toJson(tcVO, TipoCambioVO.class);
		return json;
	}
	public static String getTC(String xml){
		String tc = "0.0000";
		try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(xml.getBytes());
			Document doc = dBuilder.parse(is);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("bm:DataSet");
			
			Element e1 = getElement((Element)nList.item(0),"bm:Series","TITULO", "Tipo de cambio pesos por dÃ³lar E.U.A. Tipo de cambio para solventar obligaciones denominadas en moneda extranjera Fecha de liquidaciÃ³n");
			if(e1!=null)
				e1 = getElement(e1,"bm:Obs","OBS_VALUE",null);
			if(e1!=null)
				tc=e1.getAttribute("OBS_VALUE");			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return tc;
	}
	public static Element getElement(Element base, String nodeName, String attribute, String value){
		Element element = null;
		try{
			NodeList nList = base.getElementsByTagName(nodeName);
			if(nList.getLength()>0){
				for(int i=0; i<nList.getLength();i++){
					Node nNode = nList.item(i);
					if(nNode.getNodeType() == Node.ELEMENT_NODE){
						Element eElement = (Element)nNode;
						if(eElement.hasAttribute(attribute)){
							if(value!=null){
								if(eElement.getAttribute(attribute).equals(value)){
									return eElement;
								}
							} else {
								return eElement;
							}
						}
					}
				}
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return element;
	}
}
