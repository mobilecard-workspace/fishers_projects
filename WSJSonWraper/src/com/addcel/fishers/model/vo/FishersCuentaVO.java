package com.addcel.fishers.model.vo;

import java.math.BigDecimal;
import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FishersCuentaVO {
	private Long id_pago;
	private Long id_bitacora;
	private String id_usuario;
	private String fecha;
	private int status;
	private String email;
	private long id_sucursal;
	private long id_caja;
	private String numero_cuenta;
	private BigDecimal subtotal;
	private BigDecimal descuento;
	private BigDecimal iva;
	private BigDecimal total;
	private BigDecimal propina;
	private BigDecimal total_pago;
	private List<FishersCuentaDetalleVO> comida;
	private List<FishersCuentaDetalleVO> bebida;
	private List<FishersCuentaDetalleVO> otros;
	public Long getId_pago() {
		return id_pago;
	}
	public void setId_pago(Long id_pago) {
		this.id_pago = id_pago;
	}
	public Long getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(Long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getId_sucursal() {
		return id_sucursal;
	}
	public void setId_sucursal(long id_sucursal) {
		this.id_sucursal = id_sucursal;
	}
	public long getId_caja() {
		return id_caja;
	}
	public void setId_caja(long id_caja) {
		this.id_caja = id_caja;
	}
	public String getNumero_cuenta() {
		return numero_cuenta;
	}
	public void setNumero_cuenta(String numero_cuenta) {
		this.numero_cuenta = numero_cuenta;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getDescuento() {
		return descuento;
	}
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getTotal_pago() {
		return total_pago;
	}
	public void setTotal_pago(BigDecimal total_pago) {
		this.total_pago = total_pago;
	}
	public List<FishersCuentaDetalleVO> getComida() {
		return comida;
	}
	public void setComida(List<FishersCuentaDetalleVO> comida) {
		this.comida = comida;
	}
	public List<FishersCuentaDetalleVO> getBebida() {
		return bebida;
	}
	public void setBebida(List<FishersCuentaDetalleVO> bebida) {
		this.bebida = bebida;
	}
	public List<FishersCuentaDetalleVO> getOtros() {
		return otros;
	}
	public void setOtros(List<FishersCuentaDetalleVO> otros) {
		this.otros = otros;
	}
	public BigDecimal getPropina() {
		return propina;
	}
	public void setPropina(BigDecimal propina) {
		this.propina = propina;
	}
	
	

}
