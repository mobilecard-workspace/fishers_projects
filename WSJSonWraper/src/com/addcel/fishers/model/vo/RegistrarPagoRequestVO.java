package com.addcel.fishers.model.vo;

import java.math.BigDecimal;

public class RegistrarPagoRequestVO {
	private int caja;
	private String cuenta_num;
	private String fecha_operacion;
	private String tarjeta;
	private String autorizacion;
	private BigDecimal monto_pagado;
	private BigDecimal propina;
	private String tipo_tarjeta;
	private String transaccion_addcel;
	public int getCaja() {
		return caja;
	}
	public void setCaja(int caja) {
		this.caja = caja;
	}
	public String getCuenta_num() {
		return cuenta_num;
	}
	public void setCuenta_num(String cuenta_num) {
		this.cuenta_num = cuenta_num;
	}
	public String getFecha_operacion() {
		return fecha_operacion;
	}
	public void setFecha_operacion(String fecha_operacion) {
		this.fecha_operacion = fecha_operacion;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public BigDecimal getMonto_pagado() {
		return monto_pagado;
	}
	public void setMonto_pagado(BigDecimal monto_pagado) {
		this.monto_pagado = monto_pagado;
	}
	public BigDecimal getPropina() {
		return propina;
	}
	public void setPropina(BigDecimal propina) {
		this.propina = propina;
	}
	public String getTipo_tarjeta() {
		return tipo_tarjeta;
	}
	public void setTipo_tarjeta(String tipo_tarjeta) {
		this.tipo_tarjeta = tipo_tarjeta;
	}
	public String getTransaccion_addcel() {
		return transaccion_addcel;
	}
	public void setTransaccion_addcel(String transaccion_addcel) {
		this.transaccion_addcel = transaccion_addcel;
	}
}
