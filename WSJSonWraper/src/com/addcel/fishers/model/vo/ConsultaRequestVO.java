package com.addcel.fishers.model.vo;

public class ConsultaRequestVO {
	private int caja;
	private String cuenta;
	public int getCaja() {
		return caja;
	}
	public void setCaja(int caja) {
		this.caja = caja;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
}
