/**
 * Cuenta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class Cuenta  implements java.io.Serializable {
    private long id_sucursal;

    private long id_caja;

    private java.lang.String numero_cuenta;

    private float subtotal;

    private float descuento;

    private float iva;

    private float total;

    private org.tempuri.DetalleCuenta[] detalle;

    public Cuenta() {
    }

    public Cuenta(
           long id_sucursal,
           long id_caja,
           java.lang.String numero_cuenta,
           float subtotal,
           float descuento,
           float iva,
           float total,
           org.tempuri.DetalleCuenta[] detalle) {
           this.id_sucursal = id_sucursal;
           this.id_caja = id_caja;
           this.numero_cuenta = numero_cuenta;
           this.subtotal = subtotal;
           this.descuento = descuento;
           this.iva = iva;
           this.total = total;
           this.detalle = detalle;
    }


    /**
     * Gets the id_sucursal value for this Cuenta.
     * 
     * @return id_sucursal
     */
    public long getId_sucursal() {
        return id_sucursal;
    }


    /**
     * Sets the id_sucursal value for this Cuenta.
     * 
     * @param id_sucursal
     */
    public void setId_sucursal(long id_sucursal) {
        this.id_sucursal = id_sucursal;
    }


    /**
     * Gets the id_caja value for this Cuenta.
     * 
     * @return id_caja
     */
    public long getId_caja() {
        return id_caja;
    }


    /**
     * Sets the id_caja value for this Cuenta.
     * 
     * @param id_caja
     */
    public void setId_caja(long id_caja) {
        this.id_caja = id_caja;
    }


    /**
     * Gets the numero_cuenta value for this Cuenta.
     * 
     * @return numero_cuenta
     */
    public java.lang.String getNumero_cuenta() {
        return numero_cuenta;
    }


    /**
     * Sets the numero_cuenta value for this Cuenta.
     * 
     * @param numero_cuenta
     */
    public void setNumero_cuenta(java.lang.String numero_cuenta) {
        this.numero_cuenta = numero_cuenta;
    }


    /**
     * Gets the subtotal value for this Cuenta.
     * 
     * @return subtotal
     */
    public float getSubtotal() {
        return subtotal;
    }


    /**
     * Sets the subtotal value for this Cuenta.
     * 
     * @param subtotal
     */
    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }


    /**
     * Gets the descuento value for this Cuenta.
     * 
     * @return descuento
     */
    public float getDescuento() {
        return descuento;
    }


    /**
     * Sets the descuento value for this Cuenta.
     * 
     * @param descuento
     */
    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }


    /**
     * Gets the iva value for this Cuenta.
     * 
     * @return iva
     */
    public float getIva() {
        return iva;
    }


    /**
     * Sets the iva value for this Cuenta.
     * 
     * @param iva
     */
    public void setIva(float iva) {
        this.iva = iva;
    }


    /**
     * Gets the total value for this Cuenta.
     * 
     * @return total
     */
    public float getTotal() {
        return total;
    }


    /**
     * Sets the total value for this Cuenta.
     * 
     * @param total
     */
    public void setTotal(float total) {
        this.total = total;
    }


    /**
     * Gets the detalle value for this Cuenta.
     * 
     * @return detalle
     */
    public org.tempuri.DetalleCuenta[] getDetalle() {
        return detalle;
    }


    /**
     * Sets the detalle value for this Cuenta.
     * 
     * @param detalle
     */
    public void setDetalle(org.tempuri.DetalleCuenta[] detalle) {
        this.detalle = detalle;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Cuenta)) return false;
        Cuenta other = (Cuenta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.id_sucursal == other.getId_sucursal() &&
            this.id_caja == other.getId_caja() &&
            ((this.numero_cuenta==null && other.getNumero_cuenta()==null) || 
             (this.numero_cuenta!=null &&
              this.numero_cuenta.equals(other.getNumero_cuenta()))) &&
            this.subtotal == other.getSubtotal() &&
            this.descuento == other.getDescuento() &&
            this.iva == other.getIva() &&
            this.total == other.getTotal() &&
            ((this.detalle==null && other.getDetalle()==null) || 
             (this.detalle!=null &&
              java.util.Arrays.equals(this.detalle, other.getDetalle())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getId_sucursal()).hashCode();
        _hashCode += new Long(getId_caja()).hashCode();
        if (getNumero_cuenta() != null) {
            _hashCode += getNumero_cuenta().hashCode();
        }
        _hashCode += new Float(getSubtotal()).hashCode();
        _hashCode += new Float(getDescuento()).hashCode();
        _hashCode += new Float(getIva()).hashCode();
        _hashCode += new Float(getTotal()).hashCode();
        if (getDetalle() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetalle());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetalle(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Cuenta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "Cuenta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_sucursal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Id_sucursal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_caja");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Id_caja"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero_cuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Numero_cuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subtotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Subtotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descuento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Descuento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iva");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Iva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Detalle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "DetalleCuenta"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "DetalleCuenta"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
