/**
 * IntercambioSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface IntercambioSoap extends java.rmi.Remote {
    public org.tempuri.Cuenta obtenerCuenta(int caja, java.lang.String cuenta_num) throws java.rmi.RemoteException;
    public boolean registrarPago(int caja, java.lang.String cuenta_num, java.lang.String fecha_operacion, java.lang.String tarjeta, java.lang.String autorizacion, double monto_pagado, double propina, java.lang.String tipo_tarjeta, java.lang.String transaccion_addcel) throws java.rmi.RemoteException;
}
