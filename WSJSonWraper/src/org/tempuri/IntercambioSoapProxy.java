package org.tempuri;

public class IntercambioSoapProxy implements org.tempuri.IntercambioSoap {
  private String _endpoint = null;
  private org.tempuri.IntercambioSoap intercambioSoap = null;
  
  public IntercambioSoapProxy() {
    _initIntercambioSoapProxy();
  }
  
  public IntercambioSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initIntercambioSoapProxy();
  }
  
  private void _initIntercambioSoapProxy() {
    try {
      intercambioSoap = (new org.tempuri.IntercambioLocator()).getIntercambioSoap();
      if (intercambioSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)intercambioSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)intercambioSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (intercambioSoap != null)
      ((javax.xml.rpc.Stub)intercambioSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.IntercambioSoap getIntercambioSoap() {
    if (intercambioSoap == null)
      _initIntercambioSoapProxy();
    return intercambioSoap;
  }
  
  public org.tempuri.Cuenta obtenerCuenta(int caja, java.lang.String cuenta_num) throws java.rmi.RemoteException{
    if (intercambioSoap == null)
      _initIntercambioSoapProxy();
    return intercambioSoap.obtenerCuenta(caja, cuenta_num);
  }
  
  public boolean registrarPago(int caja, java.lang.String cuenta_num, java.lang.String fecha_operacion, java.lang.String tarjeta, java.lang.String autorizacion, double monto_pagado, double propina, java.lang.String tipo_tarjeta, java.lang.String transaccion_addcel) throws java.rmi.RemoteException{
    if (intercambioSoap == null)
      _initIntercambioSoapProxy();
    return intercambioSoap.registrarPago(caja, cuenta_num, fecha_operacion, tarjeta, autorizacion, monto_pagado, propina, tipo_tarjeta, transaccion_addcel);
  }
  
  
}