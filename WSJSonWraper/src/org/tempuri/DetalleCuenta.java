/**
 * DetalleCuenta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class DetalleCuenta  implements java.io.Serializable {
    private long cantidad;

    private long id_producto;

    private java.lang.String descripcion_producto;

    private float precio_unitario;

    private float importe;

    private int categoria;

    public DetalleCuenta() {
    }

    public DetalleCuenta(
           long cantidad,
           long id_producto,
           java.lang.String descripcion_producto,
           float precio_unitario,
           float importe,
           int categoria) {
           this.cantidad = cantidad;
           this.id_producto = id_producto;
           this.descripcion_producto = descripcion_producto;
           this.precio_unitario = precio_unitario;
           this.importe = importe;
           this.categoria = categoria;
    }


    /**
     * Gets the cantidad value for this DetalleCuenta.
     * 
     * @return cantidad
     */
    public long getCantidad() {
        return cantidad;
    }


    /**
     * Sets the cantidad value for this DetalleCuenta.
     * 
     * @param cantidad
     */
    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }


    /**
     * Gets the id_producto value for this DetalleCuenta.
     * 
     * @return id_producto
     */
    public long getId_producto() {
        return id_producto;
    }


    /**
     * Sets the id_producto value for this DetalleCuenta.
     * 
     * @param id_producto
     */
    public void setId_producto(long id_producto) {
        this.id_producto = id_producto;
    }


    /**
     * Gets the descripcion_producto value for this DetalleCuenta.
     * 
     * @return descripcion_producto
     */
    public java.lang.String getDescripcion_producto() {
        return descripcion_producto;
    }


    /**
     * Sets the descripcion_producto value for this DetalleCuenta.
     * 
     * @param descripcion_producto
     */
    public void setDescripcion_producto(java.lang.String descripcion_producto) {
        this.descripcion_producto = descripcion_producto;
    }


    /**
     * Gets the precio_unitario value for this DetalleCuenta.
     * 
     * @return precio_unitario
     */
    public float getPrecio_unitario() {
        return precio_unitario;
    }


    /**
     * Sets the precio_unitario value for this DetalleCuenta.
     * 
     * @param precio_unitario
     */
    public void setPrecio_unitario(float precio_unitario) {
        this.precio_unitario = precio_unitario;
    }


    /**
     * Gets the importe value for this DetalleCuenta.
     * 
     * @return importe
     */
    public float getImporte() {
        return importe;
    }


    /**
     * Sets the importe value for this DetalleCuenta.
     * 
     * @param importe
     */
    public void setImporte(float importe) {
        this.importe = importe;
    }


    /**
     * Gets the categoria value for this DetalleCuenta.
     * 
     * @return categoria
     */
    public int getCategoria() {
        return categoria;
    }


    /**
     * Sets the categoria value for this DetalleCuenta.
     * 
     * @param categoria
     */
    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DetalleCuenta)) return false;
        DetalleCuenta other = (DetalleCuenta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.cantidad == other.getCantidad() &&
            this.id_producto == other.getId_producto() &&
            ((this.descripcion_producto==null && other.getDescripcion_producto()==null) || 
             (this.descripcion_producto!=null &&
              this.descripcion_producto.equals(other.getDescripcion_producto()))) &&
            this.precio_unitario == other.getPrecio_unitario() &&
            this.importe == other.getImporte() &&
            this.categoria == other.getCategoria();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getCantidad()).hashCode();
        _hashCode += new Long(getId_producto()).hashCode();
        if (getDescripcion_producto() != null) {
            _hashCode += getDescripcion_producto().hashCode();
        }
        _hashCode += new Float(getPrecio_unitario()).hashCode();
        _hashCode += new Float(getImporte()).hashCode();
        _hashCode += getCategoria();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DetalleCuenta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "DetalleCuenta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cantidad");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Cantidad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_producto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Id_producto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcion_producto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Descripcion_producto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("precio_unitario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Precio_unitario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importe");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Importe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Categoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
